

###Rules for prodetermining the dependency graph
.PHONY: graph
graph: dependency-graph.png

GRAPH_PROGRAM = dot
PATH_DOT := $(type -p $(GRAPH_PROGRAM))
##FIXME the above construc is wrong, to the bellow is use right now
PATH_DOT := "/usr/bin/dot"

###Inference Rules for making .png from .txt files
%.png : %.txt
#command -v $(GRAPH_PROGRAM) && $(GRAPH_PROGRAM) -Tpng $< -o $@ 
	-@command -v $(GRAPH_PROGRAM) >/dev/null 2>&1 && $(GRAPH_PROGRAM) -Tpng $< -o $@ || { echo >&2 "The program '$(GRAPH_PROGRAM)' is not installed, so no graphics will be produced. Please install it using:   sudo apt-get install graphviz"; }
	-@ ###Just so the earlier can be seen
	-@ echo $(GRAPH_PROGRAM) -Tpng $< -o $@	


JLFILES:= $(wildcard *.jl) $(wildcard */*.jl) $(wildcard */*/*.jl)

dependency-graph.txt:mk.dep.sh $(JLFILES)
	echo $(JLFILES)
	./mk.dep.sh "$(JLFILES)" . > dependency-graph.txt 


.PHONY: detect_doubles
detect_doubles:$(JLFILES)
	@grep "function  *.*(" $(JLFILES) | sed 's|\(.*.jl\):function *\([^ (]*\)(.*|\2|g' | sort > function_list.txt
	@grep "function  *.*(" $(JLFILES) | sed 's|\(.*.jl\):function *\([^ (]*\)(.*|\2 @ \1|g' | sort > function_list_long.txt
	@for DFUNC in $$(sort function_list.txt | uniq -d) ; do echo ; grep "$$DFUNC @" function_list_long.txt ; done
	@rm function_list.txt
	@rm function_list_long.txt

.PHONY: test
test: #### A shorthand to run all the tests...
	julia source/tests/Test_All.jl 

.PHONY: Readme
Readme:
	##Grip is installed through 'pip install grip'
	grip -b README.md

