if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open Greens.jl")
TabLevel=TabLevel*"    "

using LinearAlgebra
using ProgressTimer
using Dates
using Random
using Test

include("SurfaceGreens.jl")
include("SliceLattice.jl")
include("../lattices/SquareLattice.jl")


function test_nonmatching_chains(LengthSide,Width,SideStr)
    if LengthSide != Width
        throw(DomainError(LengthSide,
                          "The number of border elements on side \"$SideStr\" is $LengthSide "*
                          "whereas the width of the connecting wire is $Width !"))
    end
end

function do_MultichanelChainTransport_Slize(SideIndexList,EnergyVals,Params;
                                            eta=1e-10,Verbose=false,
                                            SlizeData=nothing,
                                            ChainHamiltonian=nothing,
                                            LeadVoltage=EnergyVals,eta_leads=eta)
    ###If chain hamiltonian is 'nothing' then it's assumed to be a XX-chain (vanilla tight binding chain)
    ###Wich couples with the same coordination as lattice attachement sites
    ###If the chain hamiltonian is specified, One the specifies three elemets of that hamiltonian
    ### * The hamiltonian that repeats for every slize of the chain
    ### * The hopping hamiltonian connecting the chain to the scattering area
    ### * The hopping hamiltonian connecting the leads together

    validate_IVM(Params["IVM"])
    HamGen=Params["GenHamElems"]
    NumLeads=length(SideIndexList)
    if SlizeData==nothing
        ###Generate the Lize data if it is not present
        Verbose && println("Generating Slize data")
        SlizeData=SlizeTheLattice(Params,SideIndexList,Plot=false)
    end
    Verbose && println("SlizeData")
    Verbose && display(SlizeData)
    (SlizeElems,ColToSlize)=SlizeData


    ####Now we can assume that we know where each lead attaches, and we can continue from there

    ####First we compute how the leads attach to the slizes
    if ChainHamiltonian==nothing
        LeadHamiltonianDictionaries=
            ConstructChainHamiltonian(SideIndexList,SlizeData,Params)
    else
        LeadHamiltonianDictionaries=
            MatchChainHamToDict(SideIndexList,ChainHamiltonian)
    end
    
    
    Transmission=fill(0.0,length(EnergyVals),NumLeads,NumLeads)
    NumEnvals=length(EnergyVals)
    TIMESTRUCT=TimingInit()
    for EnIndx in 1:NumEnvals
        ChemicalPotentialSA = EnergyVals[EnIndx] + im*eta
        ChemicalPotentialLeads = LeadVoltage[EnIndx]+ im*eta_leads
        
        
        DictGsi,DictSelfEn,DictGamma=
            CreateTheAxillaryGreensFunctions(ChemicalPotentialLeads,NumLeads,
                                             LeadHamiltonianDictionaries,Verbose=Verbose)
        
        Verbose && println("ColToSlize: $ColToSlize")        
        
        GMatrix = GreensFunRecursiveSlize(HamGen,SlizeElems,ChemicalPotentialSA,Params,
                                          DictGsi,LeadHamiltonianDictionaries[3],ColToSlize)
        
        Verbose && println("GMatrix:")
        Verbose && display(GMatrix)
        
        #println("GMatrix for slize alogrithm:")
        #display(GMatrix)

        
        LocTrans=fill(0.0,NumLeads,NumLeads)                                    
        for lead1 in 1:NumLeads
            for lead2 in 1:NumLeads
                #println("----($lead1,$lead2)----------")
                GL1L2=GMatrix[lead1,lead2]
                GL2L1=copy(GL1L2)'
                #println("(G_Slize[$lead1][$lead2]):")
                #display(GL1L2)
                #println("(GL2L1):",GL2L1)
                #println("(DictGamma[lead1]):",(DictGamma[lead1]))
                #println("(DictGamma[lead2]):",(DictGamma[lead2]))
                #println("size(GL1L2):",size(GL1L2))
                #println("size(GL2L1):",size(GL2L1))
                #println("size(DictGamma[lead1]):",size(DictGamma[lead1]))
                #println("size(DictGamma[lead2]):",size(DictGamma[lead2]))
                LocTrans[lead1,lead2] = real(tr(GL1L2*DictGamma[lead2]*GL2L1*DictGamma[lead1]))
            end
        end
        Transmission[EnIndx,:,:]=LocTrans
        TIMESTRUCT=TimingProgress(TIMESTRUCT,EnIndx,NumEnvals;Message="Computing transmission coefficent")
    end
return Transmission
end


function CreateTheAxillaryGreensFunctions(ChemicalPotential,NumLeads,LeadHamiltonianDictionaries;Verbose=false)
    DictChainH,DictVLeadLead,DictVLeadCentral=LeadHamiltonianDictionaries
    ###then we compute the semi infinate leads greens function
    DictGsi=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
    DictSelfEn=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
    DictGamma=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
    TIMESTRUCT2=TimingInit()
    for sideno in 1:NumLeads
        #@eval using PyPlot
        #PyPlot.figure()
        #PyPlot.imshow(ConMatrix)
        #PyPlot.title("ConMatrix lead=$sideno")
        DictGsi[sideno]=SolveSurfaceGreens(DictChainH[sideno],DictVLeadLead[sideno];mu=ChemicalPotential)
        TimingProgress(TIMESTRUCT2,sideno,NumLeads;Message="DictGsi")
        DictSelfEn[sideno]=DictVLeadLead[sideno] * DictGsi[sideno] * DictVLeadLead[sideno]'
        TimingProgress(TIMESTRUCT2,sideno,NumLeads;Message="DictSelfEn")
        ###FIXME, this is presumably always real....
        DictGamma[sideno]=im*(DictSelfEn[sideno] - DictSelfEn[sideno]')
        TimingProgress(TIMESTRUCT2,sideno,NumLeads;Message="DictGamma")
        
        Verbose && println("DictVLeadCentral[$sideno]:")
        Verbose && display(DictVLeadCentral[sideno])
        Verbose && println("DictGsi[$sideno]:")
        Verbose && display(DictGsi[sideno])
        Verbose && println("DictSelfEn[$sideno]:")
        Verbose && display(DictSelfEn[sideno])
        Verbose && println("DictGamma[$sideno]:")
        Verbose && display(DictGamma[sideno])
        Verbose && println(".-.,-.,-.,-.,-.,-.,-.,-.,-.")
        #@eval using PyPlot
        #PyPlot.figure()
        #PyPlot.imshow(abs.(DictGsi[sideno]))
        #PyPlot.title("G-semi-infiite lead=$sideno")
        TIMESTRUCT2=TimingProgress(TIMESTRUCT2,sideno,NumLeads;Message="Computing Attachment leads")
    end
    return DictGsi,DictSelfEn,DictGamma
end


function ConstructChaintoFullHamiltonian(SideIndexList,Ham;Verbose=false,t=1.0)
    ###We can shoehorn the local hamiltonian into a form that the more generic script can also use
    SlizeData=(SideIndexList,[x for x in 1:length(SideIndexList)])
    ConstructChainHamiltonian(SideIndexList,SlizeData;Verbose=false,t=1.0,ExplicitHam=Ham)
end
    

function ConstructChainHamiltonian(SideIndexList,SlizeData,Params=nothing;Verbose=false,t=1.0,
                                   ExplicitHam=nothing,CorrectForPahse=true)
    DictVLeadCentral=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
    DictVLeadLead=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
    DictChainH=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
    (SlizeElems,ColToSlize)=SlizeData
    if Params!=nothing
        IVM=Params["IVM"]
        GenHamElem=Params["GenHamElems"]
        ParamsTuple=Params["ParamsTuple"](Params)
        if ExplicitHam!=nothing
            throw(DomainError("Both Parametes and explicit hamiltonian is given. Which one to use?"))
        end
    end
    
    NumLeads=length(SideIndexList)
    WidthLists=[length(SI) for SI in SideIndexList]
    Verbose && println("WidthLists:")
    Verbose && display(WidthLists)
    Verbose && println("SideIndexList:\n",SideIndexList)

    TIMESTRUCT2=TimingInit()
    for sideno in 1:NumLeads
        Verbose && println("----------------sideno: $sideno -----------------")
        Verbose && println(" attaches to slizeno: ",ColToSlize[sideno])
        DictVLeadLead[sideno]=diagm(0 => fill(-t,WidthLists[sideno]))
        Verbose && println("DictVLeadLead:")
        Verbose && display(DictVLeadLead)
        LocalLeadElems=SideIndexList[sideno]
        LocalSlizeElems=SlizeElems[ColToSlize[sideno]]
        Verbose && println("LocalLeadElems:\n",LocalLeadElems)
        Verbose && println("LocalSlizeElems:\n",LocalSlizeElems)
        #### For the Hopping elment between the leads and slize we have a special hamilonian
        VMat=fill(0.0+im*0.0,length(LocalSlizeElems),WidthLists[sideno])
        CumPhase=0.0 ###Set the cumulative Phase
        for LeadElemNo in 1:WidthLists[sideno]
            LeadElemID=LocalLeadElems[LeadElemNo]
            ###Identify wich point this attaches to
            SlizeElemNo=findfirst( x-> x == LeadElemID,LocalSlizeElems)
            Verbose && println("SlizeElemNo: ",SlizeElemNo)
            VMat[SlizeElemNo,LeadElemNo]=-t
            if CorrectForPahse && LeadElemNo>=2
                ####If the elements is not the first (and we want to do pahse correction)
                ####we looknup the gauge to the previous element
                ###We do this by comparing the hopping pahse between the two elements along the boundary
                if Params!=nothing
                    PosGlob=IVM[:,LocalLeadElems[LeadElemNo]]
                    PosGlobPrevious=IVM[:,LocalLeadElems[LeadElemNo-1]]
                    Verbose && println("PosGlob: ",PosGlob)
                    Verbose && println("PosGlobPrevious: ",PosGlobPrevious)
                    ####This we do direclty with a call to the genrating hamiltonian
                    HoppingElements=GenHamElem(ParamsTuple,PosGlob,PosGlobPrevious)
                elseif ExplicitHam!=nothing
                    ####This we do by looking up the element in the explicit hamiltonian
                    HoppingElements=ExplicitHam[LocalLeadElems[LeadElemNo],LocalLeadElems[LeadElemNo-1]]
                else
                    throw(DomainError("Neither Paramewter not and explicit hamiltonian is given"))
                end
                ###The then take the angle of the hopping elemt (after we have multiplied with the -t that we knows will be the hopping phase on the lead head.
                StepPhase=angle(-t*HoppingElements)
                CumPhase+=StepPhase
                Verbose && println("The pahse from elem $SlizeElemNo to $(SlizeElemNo-1) is $StepPhase")
                Verbose && println("The cumlative phase from elem 1 to $SlizeElemNo is $CumPhase")
                VMat[SlizeElemNo,LeadElemNo]=VMat[SlizeElemNo,LeadElemNo]*exp(im*CumPhase)
            end
        end
        Verbose && println("VMat:")
        Verbose && display(VMat)
        DictVLeadCentral[sideno]=VMat
        DictChainH[sideno]=-t.*GetConnectivity(SideIndexList[sideno],Params,ExplicitHam=ExplicitHam)
        TIMESTRUCT2=TimingProgress(TIMESTRUCT2,sideno,NumLeads;Message="Computing lead data")
    end
    return DictChainH,DictVLeadLead,DictVLeadCentral
end

function MatchHamiltonianToSlize!(SideIndexList,SlizeData,LeadHamiltonianDictionaries)
    ####FIXME: This assume that the leads is just as wide as the place where it attaches
    Leads = length(SideIndexList)
    for LeadNo in 1:Leads
        println("-----------------------")
        println("LeadNo:",LeadNo)
        ###Extract The Hopping Hamiltonian between the leads and the scattering area
        VLeadCenter=LeadHamiltonianDictionaries[3][LeadNo]
        println("VLeadCenter[$LeadNo]:")
        display(VLeadCenter)
        SlizteTarget=SlizeData[2][LeadNo]
        println("SlizteTarget[$LeadNo]=$SlizteTarget")
        LocalSlize=SlizeData[1][SlizteTarget]
        println("LocalSlize[$LeadNo]: ", LocalSlize)
        AttachementPoints=SideIndexList[LeadNo]
        println("AttachementPoints[$LeadNo]: ",AttachementPoints)
        if length(AttachementPoints) != size(VLeadCenter)[2]
            throw(DomainError("The numbe rof attachmet points do not match the dimention of the second index of the VLeadCenter matrxi"))
        end
        ## The New VLeadCeter is extended to take into acount all the sites that the slize contains.
        ### Not only the ones that he slize attaches to.
        NewVLeadCenter=fill(0.0im,length(LocalSlize),size(VLeadCenter)[1])
        ##Figure out the position of the attachments in the Slize
        LocalPositions=[ findall(x -> x==AttachementPoints[siteNo],LocalSlize)[1] for siteNo in 1:length(AttachementPoints)]
        println("LocalPositions[$LeadNo]: ",LocalPositions)
        ###FIXME: Unsure how to treat the non-hermoitian case. So put in a blocker for that one for now....
        if !ishermitian(VLeadCenter)
            throw(DomainError("FIXME: The matrix VLeadCenter[$LeadNo] is NOT hermitian. "*
                              "Please revisit this code and sort out if one teaks the complex congutate here or not"))
        end
        NewVLeadCenter[LocalPositions,:]=VLeadCenter[:,:]
        println("NewVLeadCenter[$LeadNo]:")
        display(NewVLeadCenter)
        LeadHamiltonianDictionaries[3][LeadNo]=NewVLeadCenter
    end
end


function unique_list_elems(SideIndexList)
    #println("SideIndexList:",SideIndexList)
    AllBorderIndexes=fill(0,sum([length(SI) for SI in SideIndexList]))
    #println("AllBorderIndexes:",AllBorderIndexes)
    ####Merge the cannels into one###
    index=1
    for sideno in 1:length(SideIndexList)
        SideIndxes=SideIndexList[sideno]
        for indxno in 1:length(SideIndxes)
            AllBorderIndexes[index]=SideIndxes[indxno]
            index+=1
        end
    end
    #println("AllBorderIndexes:",AllBorderIndexes)
    return unique(AllBorderIndexes) ###This is not sorted!
end



function GreensFunRecursiveSlize(HamGen,HamSlizeElems::Dict,Energy::Number,Params::Dict,
                                 DictGsi::Dict,DictVLeadCentral::Dict,LeadToSlize::Array;
                                 Verbose=false)
    ##
    
    ParamsTuple=Params["ParamsTuple"](Params)
    GetTheNeighbours=Params["GetPossibleNeighbours"]
    
    ###We begin by working put the system size
    Cols=length(keys(HamSlizeElems))
    IVM=Params["IVM"]
    
    ###The recursive equations that we need are
    ##G_NN	= (1-g_NN*V_Nn*g_nn*V_nN)^(-1) g_NN
    ##G_N1	= -(1-g_NN*V_Nn*g_nn*V_nN)^(-1) g_NN*VNn*gn1
    ##G_1N	=-g_1n*V_nN*G_NN
    TIMESTRUCT=TimingInit()


    NumLeads=length(LeadToSlize)
    #### We begin by assuming that the left lead really is the first lead
    if LeadToSlize[1]!=1
        throw(DomainError(LeadToSlize[1],"The first lead is not attached to only the fist column!"))
    end
    for LeadNo in 2:NumLeads
        ###Exit with an error if any of the other leads are attached to the first column
        ###FIXME This should be handled
        if LeadToSlize[LeadNo] ==1
            throw(DomainError(LeadToSlize,"Lead no $LeadNo is also attached to the first collum! FIXME: This will be implemented later"))
        end
    end

    ####The firt lead will serve as the initiation slize
    GNN=copy(DictGsi[1])
    
    ###Initiate the greens function array and add the first lead as starting point
    gLL = Array{Array{ComplexF64, 2}}(undef,NumLeads,NumLeads)
    GLL = Array{Array{ComplexF64, 2}}(undef,NumLeads,NumLeads)
    GLn  = Array{Array{ComplexF64, 2}}(undef,NumLeads)
    gLn  = Array{Array{ComplexF64, 2}}(undef,NumLeads)
    GnL  = Array{Array{ComplexF64, 2}}(undef,NumLeads)
    gnL  = Array{Array{ComplexF64, 2}}(undef,NumLeads)
    GLN  = Array{Array{ComplexF64, 2}}(undef,NumLeads)
    GNL  = Array{Array{ComplexF64, 2}}(undef,NumLeads)
    ###In the beginning the first least is the entire system,,,,
    GLL[1,1] = DictGsi[1]
    GNL[1,1] = DictGsi[1]
    GLN[1,1] = DictGsi[1]
    GNN = DictGsi[1]
    
    TIMESTRUCT=TimingProgress(TIMESTRUCT,1,Cols;Message="Done col=1 of $Cols")    
    ###We can now assume that all points exist
    ##Now we set up the recusive step

    #println("GreensArray init:")
    #display(GreensArray)

    for N in 1:Cols ###Recusively add rows
        Verbose && println("------ N  = $N ------------")
        ### Update the old indexes
        n=N-1

        #println("Old size(gLn[1]):",size(gLn[1]))
        #println("Old size(gnL[1]):",size(gnL[1]))
        #println("Old size(GLN[1]):",size(GLN[1]))
        #println("Old size(GNL[1]):",size(GNL[1]))
        for L1 in 1:NumLeads
            if LeadToSlize[L1]<N || L1 == 1
                gLn[L1] = GLN[L1]
                gnL[L1] = GNL[L1]
                for L2 in 1:NumLeads
                    if LeadToSlize[L2]<N || L2 == 1
                        gLL[L1,L2] = GLL[L1,L2]
                    end
                end
            end
        end
        gnn = GNN
        Verbose && println(" :: gnn:")
        Verbose && display(gnn)

        #println("New size(gLn[1]):",size(gLn[1]))
        #println("New size(gnL[1]):",size(gnL[1]))

        #### We begin by computing the new GNN
        ###GNN =[HNN -(VNn*gnn*VnN +sum_r VNlr*glrlr*VlrN)]^(-1)

        ###Here we add the connector between the two layers
        #println("Attach the new collumn")
        if N==1
            Verbose &&  println("Use the pre-defined fist attachement vector")
            Verbose && println("size(DictVLeadCentral[1]): ",size(DictVLeadCentral[1]))
            VnN = DictVLeadCentral[1]'
        else
            Verbose && println("CreateHoppig Ham")
            VnN = MakeHoppingHam(ParamsTuple,HamGen,IVM,
                                 HamSlizeElems[N-1],HamSlizeElems[N],
                                 GetTheNeighbours)
        end
        VNn = (copy(VnN))' ###Hermititna conjugati

        Verbose && println(" :: VnN:")
        Verbose && display(VnN)
        
        ###Here we check if any leads are attached at this point

        ###Here we compute the self energy correction VgV
        ###For this to owrk we need to know how many colums should actually be added
        
        ###If there are more than one collums, this becommes the top quandrant in the larger VgV chain

        Verbose &&  println("Compute VgV")
        Verbose && println("size(VNn): ",size(VNn))
        Verbose && println("size(gnn): ",size(gnn))
        Verbose && println("size(VnN): ",size(VnN))
        ##println("Multiply")
        VgV = VNn*gnn*VnN
        Verbose && println(" :: VgV:")
        Verbose && display(VgV)


        Verbose &&  println("Add possible external leads")
        for LeadNo in 2:NumLeads
            ##Check if a new lead is being attached
            if LeadToSlize[LeadNo]==N

                Verbose &&  println("Attach lead no: $LeadNo")
                VNlr =  DictVLeadCentral[LeadNo]
                VlrN =  (copy(VNlr))'
                Verbose && println("VLeadCentral[$LeadNo]:")
                Verbose && display(DictVLeadCentral[LeadNo])
                Verbose && println("Gsi[$LeadNo]:")
                Verbose && display(DictGsi[LeadNo])
                #println("size(VNlr)=",size(VNlr))
                #println("size(DictGsi[$LeadNo])=",size(DictGsi[LeadNo]))
                #println("size(VlrN)=",size(VlrN))
                #println("size(VgV)=",size(VgV))
                #### Check that all the relevand slizes have the correct dimentionality
                if size(VgV)[1] != size(VNlr)[1]
                    throw(DomainError("The size of the attachment hamiltonian VNlr="
                                      *string(size(VNlr)[1])*
                                      " does not match the recursive slize size="*
                                      string(size(VgV)[1])))
                elseif size(VgV)[2] != size(VlrN)[2]
                        throw(DomainError("The size of the attachment hamiltonian VNlr="
                                          *string(size(VlrN)[2])*
                                          " does not match the recursive slize size="*
                                          string(size(VgV)[2])))
                end
                VgV+= VNlr*DictGsi[LeadNo]*VlrN
            end
        end

        #println("Slize Ham")
        hNN = MakeSlizeHam(ParamsTuple,HamGen,IVM,
                           HamSlizeElems[N],Energy)

        Verbose && println(" :: hNN:\n",hNN)

        
        Verbose && println("Now we invert")
        #println("Invert")
        GNN = inv( hNN - VgV)

        Verbose && println(" :: GNN:\n",GNN)

        
        ###Now we compute the other updated hopping elements
        for LeadNo in 1:NumLeads
            if LeadToSlize[LeadNo]<N ###already attached
                Verbose && println("Lead $LeadNo Already attached")
                Verbose && println("Old size(GNL):",size(GNN))
                Verbose && println("Old size(VNn):",size(VNn))
                Verbose && println("Old size(gnL[$LeadNo]):",size(gnL[LeadNo]))
                GNL[LeadNo] = -GNN*VNn*gnL[LeadNo]
                Verbose && println("Old size(GNL[$LeadNo]):",size(GNL[LeadNo]))
                GLN[LeadNo] = -gLn[LeadNo]*VnN*GNN
                Verbose && println("Old size(GLN[$LeadNo]):",size(GLN[LeadNo]))
            elseif LeadToSlize[LeadNo]==N ###Attached this iteration
                Verbose && println("Lead $LeadNo Attached this iteration")                
                Verbose && println("Already attached")                
                Verbose && println("using size(GNN):",size(GNN))
                Verbose && println("using size(DictVLeadCentral[$LeadNo]):",size(DictVLeadCentral[LeadNo]))
                Verbose && println("using size(DictGsi[$LeadNo]):",size(DictGsi[LeadNo]))
                GNL[LeadNo] = -GNN*DictVLeadCentral[LeadNo]*DictGsi[LeadNo]
                Verbose && println("New size(GNL[$LeadNo]):",size(GNL[LeadNo]))
                GLN[LeadNo] = -DictGsi[LeadNo]*(copy(DictVLeadCentral[LeadNo])')*GNN
                Verbose && println("New size(GLN[$LeadNo]):",size(GLN[LeadNo]))
            elseif LeadNo==1 && N==1 ###Left Lead first attachement round
                Verbose && println("Lead $LeadNo first attachement round")                
                GNL[LeadNo] = -GNN*DictVLeadCentral[LeadNo]*DictGsi[LeadNo]
                Verbose && println("Left Lead size(GNL[$LeadNo]):",size(GNL[LeadNo]))
                GLN[LeadNo] = -DictGsi[LeadNo]*(copy(DictVLeadCentral[LeadNo])')*GNN
                Verbose && println("Left Lead size(GLN[$LeadNo]):",size(GLN[LeadNo]))
            end
        end

        ###Run a second time since we will use the results form the first run here
        for LeadNo in 1:NumLeads
            if LeadToSlize[LeadNo]<N || (LeadNo==1 && N==1) ###already attached
                #println("----found lead $LeadNo already attached---")
                for LeadNo2 in 1:NumLeads 
                    if LeadToSlize[LeadNo2]<N || (LeadNo2==1 && N==1) ###already attached
                        GLL[LeadNo,LeadNo2]=-gLn[LeadNo]*VnN*GNL[LeadNo2]+gLL[LeadNo,LeadNo2]
                        #println("oo size(GLL[$LeadNo,$LeadNo2]]):",size(GLL[LeadNo,LeadNo2]))
                    elseif LeadToSlize[LeadNo2]==N ###already attached
                        GLL[LeadNo,LeadNo2]=-gLn[LeadNo]*VnN*GNL[LeadNo2]
                        #println("on size(GLL[$LeadNo,$LeadNo2]]):",size(GLL[LeadNo,LeadNo2]))
                    end
                end
            elseif LeadToSlize[LeadNo]==N ###Attached this iteration
                for LeadNo2 in 1:NumLeads
                    gll=DictGsi[LeadNo]
                    VlN=copy(DictVLeadCentral[LeadNo])'
                    #println("using size(gll):",size(gll))
                    #println("using size(VlN):",size(VlN))
                    #println("using size(GNN):",size(GNN))
                    if LeadToSlize[LeadNo2]<N ###already attached
                        GLL[LeadNo,LeadNo2]=-gll*VlN*GNL[LeadNo2]
                        #println("no size(GLL[$LeadNo,$LeadNo2]]):",size(GLL[LeadNo,LeadNo2]))
                    elseif LeadToSlize[LeadNo2]==N ###already attached
                        VNl=DictVLeadCentral[LeadNo2]
                        #println("using size(DictGsi[$LeadNo2]):",size(DictGsi[LeadNo2]))
                        GLL[LeadNo,LeadNo2]=gll*VlN*GNN*VNl*DictGsi[LeadNo2]
                        if LeadNo==LeadNo2
                            GLL[LeadNo,LeadNo2]+=gll
                        end
                        #println("nn size(GLL[$LeadNo,$LeadNo2]]):",size(GLL[LeadNo,LeadNo2]))
                    end
                end
            end
        end
        TIMESTRUCT=TimingProgress(TIMESTRUCT,N,Cols;Message="Done col=$N of $Cols")
    end
    return  GLL
end





function GreensFuncInvert(Ham,Energy)
    return inv(EnergyShift(Ham,Energy))
end

function EnergyShift(Ham,Energy)
    dim = size(Ham)[1]
    return Ham + diagm(0 => (zeros(dim) .+ Energy))
end



function GreenFunctContrib(From,To,Level,EigSys,EnergyShift)
    return ((EigSys.vectors[From,Level]*EigSys.vectors[To,Level]')
            ./ (EigSys.values[Level] .+ EnergyShift))
end

function GreensFunctionFixedEnergy(EigSys,Energy;FromList=nothing,ToList=nothing)
    ### The eigensystem satisfies the equation A*V = l*V.
    ###A*E.vectors[:,k]-E.values[1]*E.vectors[:,k]
    if FromList==nothing
        FromList=1:length(EigSys.values)
    end
    if ToList==nothing
        ToList=1:length(EigSys.values)
    end
    GreensFun=ones(Complex,(length(FromList),length(ToList))) .* 0.0
    NumEigs=length(EigSys.values)
    TIMESTRUCT=TimingInit()
    for k in 1:NumEigs
        GreensFun+=((EigSys.vectors[FromList,k]*EigSys.vectors[ToList,k]')
                    ./ (EigSys.values[k] .+ Energy))
        TIMESTRUCT=TimingProgress(TIMESTRUCT,k,NumEigs)
    end
    #figure()
    #imshow(abs.(GreensFun))
    #colorbar()
    #title("GreensFuncton")
    return GreensFun
end

function GreensFunctionFixedElement(EigSys,EnergyRange,(Ifrom,Ito),delta=0.0)
    ### The eigensystem satisfies the equation A*V = l*V.
    ###A*E.vectors[:,k]-E.values[1]*E.vectors[:,k]
    GreensFun=zeros(size(EnergyRange))
    for k in 1:length(EigSys.values)
        GreensFun += GreenFunctContrib(Ifrom,Ito,k,EigSys,EnergyRange .- delta*im)
    end
    return GreensFun
    
end


function MatchChainHamToDict(SideIndexList,(HamLead,HamLeadLead,HamLeadDevice))
    if isa(HamLead, Dict) && isa(HamLeadLead, Dict) && isa(HamLeadDevice, Dict)
        #println("Return since the dicts are dicts.")
        return HamLead,HamLeadLead,HamLeadDevice
    end
    NumLeads=length(SideIndexList)
    DictVLeadCentral=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
    DictVLeadLead=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
    DictChainH=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
    #println("HamLead:")
    #display(HamLead)
    #rintln("HamLeadLead:")
    #display(HamLeadLead)
    #println("HamLeadDevice:")
    #display(HamLeadDevice)
    for Leadno in 1:NumLeads
        ###We befin by assuming that the hamiltonians is the same for all the leads
        DictChainH[Leadno]=HamLead
        if Leadno==1
            DictVLeadLead[Leadno]=HamLeadLead
            DictVLeadCentral[Leadno]=HamLeadDevice
        elseif Leadno==2
            DictVLeadLead[Leadno]=1.0.*HamLeadLead'
            DictVLeadCentral[Leadno]=1.0.*HamLeadDevice'
        else
            thow(DomainError("FIXME, not implemented for more than two leads"))
        end
    end
    return DictChainH,DictVLeadLead,DictVLeadCentral
end



TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close Greens.jl")
