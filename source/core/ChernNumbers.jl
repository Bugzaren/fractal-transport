if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open ChernNumbers.jl")
TabLevel=TabLevel*"    "




function MakeChernData(SizeGen::Int,FracGen::Int,PhiRange,EnergyRange;
                       LT="SCS",Base=nothing,W::Real=0)
    ChernMatrix=zeros(length(EnergyRange),length(PhiRange))
    DOSMatrix=zeros(length(EnergyRange),length(PhiRange))

    println("W=$W for Make Chern")
    TIMESTRUCT=TimingInit()
    i=1
    for Phase in PhiRange
        println("Phase=$Phase")
        (ChernList,DOS)=SierpinskiChernNumbers(SizeGen,FracGen,Phase,EnergyRange,LT=LT,
                                               Base=Base,W=W)
        ChernMatrix[:,i]=ChernList
        DOSMatrix[:,i]=DOS
        TIMESTRUCT=TimingProgress(TIMESTRUCT,i,length(PhiRange);Message="Computing chern numbers")
        i+=1
    end
    Extent=[1.5*PhiRange[1]-.5*PhiRange[2],1.5*PhiRange[end]-.5*PhiRange[end-1],
            1.5*EnergyRange[1]-.5*EnergyRange[2],1.5*EnergyRange[end]-0.5*EnergyRange[end-1]]
    IDSTRING="LT_$LT"*"_sg_$SizeGen"*"_fg_$FracGen"*"_Base_$Base"*"_W_$W"


    figure()
    maxval=maximum(abs.(ChernMatrix))
    imshow(ChernMatrix[end:-1:1,:],aspect="auto",extent=Extent,vmin=-maxval,vmax=maxval,cmap="bwr")
    title("Chern Number for $LT sg=$SizeGen, fg=$FracGen, Base=$Base, W=$W")
    colorbar()
    xlabel("\\Phi/\\Phi_0")
    ylabel("Chern number")
    SavePyFile("pic_chern/CherNum_"*IDSTRING)
    figure()
    imshow(log.(DOSMatrix[end:-1:1,:] .+ 1),aspect="auto",extent=Extent,cmap="gist_heat_r")
    title("log(DOS +1 ) for $LT sg=$SizeGen, fg=$FracGen, W=$W")
    xlabel("\\Phi/\\Phi_0")
    ylabel("log(Density of States)")
    colorbar()
    SavePyFile("pic_dos/DOS_"*IDSTRING)
    
end



function ChernNumbers(Params::Dict,EnergyList,IVMStartPoint;
                      Plot=false,Base=nothing,EigSys=nothing,
                      SaveDir=nothing,SaveASCII=false)
    
    ####Determine the regions A,B,C used in Nuepert paper http://arxiv.org/abs/1807.00367v1
    (RegionA,RegionB,RegionC)=get_region_indices(Params,IVMStartPoint,Base,Plot=Plot)
    (RegionAp,RegionBp,RegionCp,FullRegion)=condense_region(RegionA,RegionB,RegionC)

    Base=length(RegionA)
    
    if EigSys==nothing
        EigSys=SolveHamiltonian(Params,Plot=false,SaveHDF5=false)
    end
    
    ###Make One chern number calculation per energy slice
    ChernList=fill(0.0,size(EnergyList))
    TIMESTRUCT=TimingInit()
    ####Choose the energy indexes randomli to get better clock estimations...
    RandIndex=Random.randperm(length(EnergyList))
    Counter=1
    StartTime=now() ###Start the clock
    for EnIndx in RandIndex
        #println("EnIndx:",EnIndx)
        ### FIXME (7/12-18) currently the bottleneck is creating the projector matrix
        ### Especially for larger systems
        PMp=get_projector_matrix(EigSys,EnergyList[EnIndx],FullRegion)
        ChernList[EnIndx]=get_chern_number(PMp,RegionAp,RegionBp,RegionCp)
        TIMESTRUCT=TimingProgress(TIMESTRUCT,Counter,length(EnergyList),Message="ChernNumber")
        Counter+=1
    end
    EndTime=now()
    DiffTime=PrettyTime(EndTime-StartTime)
    println("It took $DiffTime to compute chen numbers")
    if Plot
        figure()
        plot(EnergyList,real(ChernList),label="real")
        plot(EnergyList,imag(ChernList),label="imag")
        xlabel("Energy")
        ylabel("Chern number")
        grid()
        legend(loc="best")
    end

    if SaveASCII!=false
        if SaveASCII==true ###Generate name
            SaveASCII=("ChernValue_Base_"*string(Base)*"_E1_"*string(EnergyList[1])*"_EN_"*string(length(EnergyList))*
                       "_E2_"*string(EnergyList[end])*"_"*Params["IDSTRING"])
        end
        if SaveDir!=nothing
            SaveASCII=joinpath(SaveDir,SaveASCII)
        end
        println("Write the Chern values to file: $(SaveASCII).dat")
        mkpath(dirname(SaveASCII))
        SaveAsciiFile(SaveASCII,ChernList)
    end
    
    return ChernList

end

function get_chern_number(PM,RegionA,RegionB,RegionC)
    ####Now we sum over the projector matrix, using the indexes of the varius regions
    #### We use the formula
    ###  12*pi*i* sum_j,k,l (P_jk*P_kl*P_lj - P_jl*P_lk*P_kj)
    ###Where i,k,l are in the regions A,B,C
    ##Create a transpose copy, for faster array acces of transposes
    ##Comes at the cost of more memory, but makes code at least 30% faster
    PMtrans=copy(transpose(PM))  
    ####
    ChernNumber=0.0im
    #TIMESTRUCT=TimingInit()
    TimeCount=0
    for ja in RegionA
        for kb in RegionB
            PMjakb=12*pi*im*PM[ja,kb]
            PMkbja=12*pi*im*PMtrans[ja,kb]
            for lc in RegionC
                ChernNumber-=PMjakb*PMtrans[lc,kb]*PM[lc,ja]
                ChernNumber+=PMkbja*PMtrans[lc,ja]*PM[lc,kb]
            end
        end
        TimeCount+=1
        #TIMESTRUCT=TimingProgress(TIMESTRUCT,TimeCount,length(RegionA))
    end
    if !isapprox(imag(ChernNumber),0,atol=1e-10)
        throw(DomainError(ChernNumber,"Chern number has imaginary components!"))
    end
    return real(ChernNumber)
end

function get_region_indices(Params::Dict,IVMStartPoint,Base=nothing;Plot=false)
    #####Create a list that gives all the indices of a specific region

    ###Create the Map of Vectors
    IndVecMap=Params["IVM"]
    NumElem=size(IndVecMap)[2]
    ###Loop through the vector map, and if the indexes are in the correct region add them
    IndRegMap=zeros(Integer,NumElem)

    #### From Kitaev (A. Kitaev, Annals of Physics 321, 2 (2006)) we learn 
    #Note that the value of ν (P, A, B) will not change if we add arbitrary finite matrices to A and B. Moreover, the commutator [PAP, PBP] is nonzero only in the region where the supports of A, B, and C = 1−A − B touch each other, hence we can make arbitrary changes away from the triple contact point. Thus the operators A, B, C do not have to be projectors or even commute with each other; it is only important that they are supported by regions with the particular topological configuration (possibly with some overlap along the boundaries and triple overlap at the center).


    
    if Base==nothing
        Base=10 ###Chose the base to consist of 10 points
    end

    println("IVMStartPoint = $IVMStartPoint")

    ##Next we find the three closes elements to that point
    ##FIXME: For now this assuemes that there is an interpolatalbe mattping between the IVM and the IPM
    ##This needs not always be the case
    IVMStart=fill(0.0,length(IVMStartPoint),1)
    IVMStart[:,1]=collect(IVMStartPoint)*1.0
    IVM=Params["IVM"]
    if haskey(Params,"IVMtoIPM")
        ParamsTuple=Params["ParamsTuple"](Params)###Extract the parameters tuple
        IPMStart = Params["IVMtoIPM"](IVMStart,ParamsTuple)
        IPM=Params["IVMtoIPM"](IVM,ParamsTuple)
    else
        IPMStart = 1.0*IVMStart
        IPM=copy(IVM)
    end
    println("IPMStart = $IPMStart")



    #println("IPM = $IPM")
    
    #println("size(IPM): ",size(IPM))
    IPMDiff=vec(sum((IPM .- IPMStart).^2,dims=1))
    #println("IPMDiff = $IPMDiff")
    MinOrder=sortperm(IPMDiff)
    #println("MinOrder = $MinOrder")

    ###Insert the closes lattice points as the starting point
    IndRegMap[MinOrder[1]]+=1
    IndRegMap[MinOrder[2]]+=2
    IndRegMap[MinOrder[3]]+=4

    IPMStart1=IPM[:,MinOrder[1]]
    IPMStart2=IPM[:,MinOrder[2]]
    IPMStart4=IPM[:,MinOrder[4]]


    NeigbourFunciton=Params["GetPossibleNeighbours"]
    ParamsTuple=Params["ParamsTuple"](Params)
    
    ###After this the number in the regions are
    NumA=sum(rem.(div.(IndRegMap,1),2) .== 1)
    NumB=sum(rem.(div.(IndRegMap,2),2) .== 1)
    NumC=sum(rem.(div.(IndRegMap,3),2) .== 1)
    TIMESTRUCT=TimingInit()
    while NumA<Base || NumB<Base || NumC<Base
        #println("........................................")
        ###Run untill they are all the size of base
        if NumA<=NumB && NumA<=NumC ###If reg A is smalest
            #println("Region A")
            ClosestNeighbour=AddNeigbour(IndRegMap,1,IVM,IPM,IPMStart1,
                                           NeigbourFunciton,ParamsTuple)
            IndRegMap[ClosestNeighbour]+=1
            NumA+=1
        elseif NumB<=NumA && NumB<=NumC ###If reg B is smalest
            #println("Region B")
            ClosestNeighbour=AddNeigbour(IndRegMap,2,IVM,IPM,IPMStart2,
                                           NeigbourFunciton,ParamsTuple)
            IndRegMap[ClosestNeighbour]+=2
            NumB+=1
        elseif NumC<=NumA && NumC<=NumB ###If reg C is smalest
            #println("Region C")
            ClosestNeighbour=AddNeigbour(IndRegMap,4,IVM,IPM,IPMStart4,
                                           NeigbourFunciton,ParamsTuple)
            IndRegMap[ClosestNeighbour]+=4
            NumC+=1
        end
        TIMESTRUCT=TimingProgress(TIMESTRUCT,NumA+NumB+NumC,Base*3;Message="Making neigbours A=$NumA,a, B=$NumB, C=$NumC of $Base")
    end

    RegionA=findall(rem.(div.(IndRegMap,1),2) .== 1)
    RegionB=findall(rem.(div.(IndRegMap,2),2) .== 1)
    RegionC=findall(rem.(div.(IndRegMap,4),2) .== 1)
    if Plot
        figure()
        if true
            Metric=IPM
            MetricStart=IPMStart
        else
            Metric=IVM
            MetricStart=IVMStart
        end
        if size(IVM)[2] ==2
            scatter(Metric[2,:],Metric[1,:],color="blue")
            scatter(Metric[2,RegionA].+.1,Metric[1,RegionA],color="red")
            scatter(Metric[2,RegionB],Metric[1,RegionB].+.1,color="green")
            scatter(Metric[2,RegionC].-.1,Metric[1,RegionC].-.1,color="yellow")
            scatter(MetricStart[2],MetricStart[1],color="black")
        else size(IVM)[2] ==3
            Dim=2
            TOF=(Metric[3,:] .== Dim)
            TOFA=(Metric[3,RegionA] .== Dim)
            TOFB=(Metric[3,RegionB] .== Dim)
            TOFC=(Metric[3,RegionC] .== Dim)
            scatter(Metric[2,TOF],Metric[1,TOF],color="blue")
            scatter(Metric[2,RegionA[TOFA]].+.1,Metric[1,RegionA[TOFA]],
                    color="red")
            scatter(Metric[2,RegionB[TOFB]],Metric[1,RegionB[TOFB]].+.1,
                    color="green")
            scatter(Metric[2,RegionC[TOFC]].-.1,Metric[1,RegionC[TOFC]].-.1,
                    color="yellow")
            scatter(MetricStart[2],MetricStart[1],color="black")
        end
        axis("equal")
    end
    
    
    return (RegionA,RegionB,RegionC)
end



function get_projector_matrix(EigSys,Energy,StateList=nothing;Slice="best")
    if StateList==nothing
        StateList=1:length(EigSys.values)
    end
    ####The projector matrix is defined as
    ### P = \sum_j |j><j| \theta(Energy-lambda_j)
    ###where lambda_j is the energy eigenvalue, and
    ## \theta is the heavyside function ###


    if Slice=="full"
        ###Begin by finding all inidces that give smaller energies than energy
        SmallEnIndx=findall(EigSys.values .< Energy)
        ChooseSmall=true
    elseif Slice=="empty"
        ###Begin by finding all inidces that give larger or equal energies than energy
        SmallEnIndx=findall(EigSys.values .>= Energy)
        ChooseSmall=false
    elseif Slice=="best"
        ###Begin by finding all inidces that give smaller energies than energy
        SmallEnIndx=findall(EigSys.values .< Energy)
        if length(SmallEnIndx)*2 > length(EigSys.values)
            ### This contains more than half of the ölements
            ### thus use all the larger elements instead
            SmallEnIndx=findall(EigSys.values .>= Energy)
            ChooseSmall=false
        else
            ChooseSmall=true
        end
    else
        throw(DomainError(Slice,"Valid slices are {full,empty,best} with defualt=\"best\"."))
    end
    if ChooseSmall
        P=zeros(ComplexF64,(length(StateList),length(StateList)))
    else
        P= LinearAlgebra.Diagonal(ones(ComplexF64,length(StateList)))
    end
    TIMESTRUCT=TimingInit()
    for Indx in SmallEnIndx
        P+=(-1)^(ChooseSmall-1)*EigSys.vectors[StateList,Indx]*EigSys.vectors[StateList,Indx]'
        TIMESTRUCT=TimingProgress(TIMESTRUCT,Indx,length(SmallEnIndx))
    end
    return P 
end

function AddNeigbour(IndRegMap,ID,IndVecMap,IndPosMap,PosStart,NeigbourFunction,ParamsTuple)
    ####Find empy neigbouring sites (square lattice assumed)
    #println("\n...............\n")
    GroupIndexes=findall(rem.(div.(IndRegMap,ID),2) .== 1)

    #println("GroupIndexes($ID): ",GroupIndexes)
    
    ###loop over group indexes
    NgbDict = Dict()
    for GI in GroupIndexes
        #println("GI:",GI)
        ### loop over all possible neigbours
        IVMpos=IndVecMap[:,GI]
        Neigbours=NeigbourFunction(IVMpos,ParamsTuple)
        for Nbr in Neigbours
            setDict!(NgbDict,Nbr,IndRegMap,IndVecMap)
        end
    end

    if length(keys(NgbDict))==0
        throw(DomainError("Base count that is supplied is to large. Only Base="
                          *string(length(GroupIndexes))*" can be used. "*
                          "Or change the origin of the choosen chern areas"))
    end
    
    #println("Neighbors:")
    #display(NgbDict)
    Dist=Inf
    NearestID=nothing
    for key in keys(NgbDict)
        Position=IndPosMap[:,get(NgbDict,key,nothing)]
        NewDist=sqrt(sum((Position .- PosStart ).^2))
        #println("key=$key")
        #println("PosStart=$PosStart")
        #println("Position=$Position")
        #println("NewDist=$NewDist")
        if NewDist < Dist
            Dist=NewDist
            NearestID=key
            #println("Dist=$Dist")
        end
        #println("NearestID=$NearestID")
        #println("Dist=$Dist")
    end
    #println("NearestID is $NearestID at ",get(NgbDict,NearestID,nothing))
    return NearestID
end


function setDict!(NgbDict,IVMpos,IndRegMap,IndVecMap)
    NeighbourIndx=IsOnLattice(IVMpos,IndVecMap)
    if NeighbourIndx!=nothing && IndRegMap[NeighbourIndx]==0
        #println("Neighbour at $NeighbourIndx !")
        NgbDict[NeighbourIndx]=NeighbourIndx
    end
end



function condense_region(RegionA,RegionB,RegionC)
    FullRegion=unique(vcat(RegionA,RegionB,RegionC))
    RegionAp=zeros(Integer,length(RegionA))
    RegionBp=zeros(Integer,length(RegionB))
    RegionCp=zeros(Integer,length(RegionC))
    for IndxA in 1:length(RegionA)
        RegionAp[IndxA]=findall(FullRegion.==RegionA[IndxA])[1]
    end
    for IndxB in 1:length(RegionB)
        RegionBp[IndxB]=findall(FullRegion.==RegionB[IndxB])[1]
    end
    for IndxC in 1:length(RegionC)
        RegionCp[IndxC]=findall(FullRegion.==RegionC[IndxC])[1]
    end
    return (RegionAp,RegionBp,RegionCp,FullRegion)
end
TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close ChernNumbers.jl")
