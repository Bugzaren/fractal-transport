if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open PlotMisc.jl")
TabLevel=TabLevel*"    "


#using PyPlot  ----  Dont load it here.... but a top level if needed

function StackSlize(SlizeVec,IndxToLattice)
    Elems=length(SlizeVec)
    Dim=size(IndxToLattice)[1]
    if Dim == 2 ###For Two D lattices continue
        (MinRow,MinCol,MaxRow,MaxCol)=LaticeBounds(IndxToLattice)
        TheSlize=fill(0.0im,MaxRow-MinRow+1,MaxCol-MinCol+1).*NaN
        for j in 1:Elems
            TheSlize[IndxToLattice[1,j]-MinRow+1,IndxToLattice[2,j]-MinCol+1] = SlizeVec[j]
        end
        return TheSlize,[MinRow-.5,MaxRow+.5,MinCol-.5,MaxCol+.5]
    elseif Dim == 3 ###For Two D lattices continue
        (MinRow,MinCol,MinHeight,MaxRow,MaxCol,MaxHeight)=LaticeBounds(IndxToLattice)
        ###Here we embed the third dimention into the first (for now we only doe this if the number of slizes is 2 or 1)
        #println("The Lattice BOunds:",
        #        (MinRow,MinCol,MinHeight,MaxRow,MaxCol,MaxHeight))
        NumRows=MaxRow-MinRow+1
        NumCols=MaxCol-MinCol+1
        NumHeightSlizes=MaxHeight-MinHeight+1
        TheSlize=fill(0.0im,NumRows+2,(NumCols+1)*NumHeightSlizes+1).*NaN
        for j in 1:Elems
            TheSlize[IndxToLattice[1,j]-MinRow+2,
                     IndxToLattice[2,j]-MinCol+1 +
                     (NumCols+1)*(IndxToLattice[3,j]-MinHeight)+1
                     ]= SlizeVec[j]
        end
        #println("TheSlize:")
        #display(TheSlize)
        return TheSlize,[MinCol-1.5,
                         MinCol+(NumCols+1)*NumHeightSlizes-0.5,
                         MinRow-1.5,MaxRow+1.5]
    end

end

function LaticeBounds(IndxToLattice)
    Dim=size(IndxToLattice)[1]
    if Dim==2
        return (minimum(IndxToLattice[1,:]),
                minimum(IndxToLattice[2,:]),
                maximum(IndxToLattice[1,:]),
                maximum(IndxToLattice[2,:]))
    elseif Dim==3
        return (minimum(IndxToLattice[1,:]),
                minimum(IndxToLattice[2,:]),
                minimum(IndxToLattice[3,:]),
                maximum(IndxToLattice[1,:]),
                maximum(IndxToLattice[2,:]),
                maximum(IndxToLattice[3,:]))
    end
end

using Printf
function MakeMovie(EigSys,IndxToLattice,Mode="abs")
    (_,NumSlizes)=size(EigSys.vectors) ###The second index enumerates the indices
    figure()
    i=1
    Step=1
    while true
        Energy=@sprintf("%.2f", EigSys.values[i])
        clf()
        println("$i of $NumSlizes")
        Slize,Extent=StackSlize(EigSys.vectors[:,i],IndxToLattice)
        if Mode=="abs"
            MinMax=maximum(filter(!isnan,abs.(Slize)))
            Mycmap=ColorMap("hot_r")
            Mycmap."set_bad"("grey",1.)            
            imshow(abs.(Slize[end:-1:1,:]),cmap=Mycmap,vmin=0,vmax=MinMax,
                   extent=Extent)
            title("|\$\\psi\$| for $i of $NumSlizes E=$Energy")
        elseif Mode=="real"
            MinMax=maximum(filter(!isnan,abs.(real.(Slize))))
            Mycmap=ColorMap("bwr")
            Mycmap."set_bad"("grey",1.)            
            imshow(real.(Slize[end:-1:1,:]),cmap=Mycmap,vmin=-MinMax,vmax=MinMax,
                   extent=Extent)
            title("Re(\$\\psi\$) for $i of $NumSlizes E=$Energy")
        elseif Mode=="imag"
            MinMax=maximum(filter(!isnan,abs.(imag.(Slize))))
            Mycmap=ColorMap("bwr")
            Mycmap."set_bad"("grey",1.)            
            imshow(imag.(Slize[end:-1:1,:]),cmap=Mycmap,vmin=-MinMax,vmax=MinMax,
                   extent=Extent)
            title("Im(\$\\psi\$) for $i of $NumSlizes E=$Energy")
        elseif Mode=="angle"
            Mycmap=ColorMap("bwr")
            Mycmap."set_bad"("grey",1.)            
            imshow(angle.(Slize[end:-1:1,:]),cmap=Mycmap,vmin=-pi,vmax=pi,
                   extent=Extent)
            title("angle(\$\\psi\$) for $i of $NumSlizes E=$Energy")
        else
            throw(DomainError(Mode, "Only Mode={abs,real,imag,angle} are supported"))
        end

        colorbar()
        println("Press enter to continute ( q + Enter to abort, r + Enter to reverse)!")
        inp=readline(stdin)
        if inp=="q"
            println("quiting!")
            return
        elseif inp=="r"
            Step=-Step
        elseif tryparse(Int,inp)!=nothing
            Parse=tryparse(Int,inp)
            if Parse<1
                println("WARNING: Number to small")
                i-=Step
            elseif Parse>NumSlizes
                println("WARNING: Number to large")
                i-=Step
            else
                i=Parse-Step
            end
        end
        i+=Step
        if i==0
            i=1
            println("NOTE: This is the first state")
        elseif i>NumSlizes
            println("NOTE: This is the last state")
            i=NumSlizes
        end
    end
end


function plot_attachments(AP,IVM)
    patch = pyimport("matplotlib.patches")
    
    MinY = minimum(IVM[1,:])
    MaxY = maximum(IVM[1,:])
    MinX = minimum(IVM[2,:])
    MaxX = maximum(IVM[2,:])
    Lattice=fill(0,MaxY-MinY+1,MaxX-MinX+1)
    Shift = [1-MinX,1-MinY]
    for Indx in 1:(size(IVM)[2])
        #println(Indx)
        LocPos=IVM[:,Indx] + Shift
        Lattice[LocPos[1],LocPos[2]] = 1
    end
    imshow(Lattice[end:-1:1,:],extent=[MinX-.5,MaxX+.5,MinY-.5,MaxY+.5])
    for LeadNo in 1:length(AP)
        LeadIndx=AP[LeadNo]
        LX=fill(0,size(LeadIndx))
        LY=fill(0,size(LeadIndx))
        Color=""
        for LI in 1:length(LeadIndx)
            LX[LI]=IVM[2,LeadIndx[LI]]
            LY[LI]=IVM[1,LeadIndx[LI]]
            if LI==1
                PLOTOBJ=plot(LX[LI],LY[LI],"p")
                Color=PLOTOBJ[1]."get_color"()
            end
            circle1=patch.Circle((LX[LI],LY[LI]),.5,color=Color)
            gcf()."gca"()."add_artist"(circle1)
            text(LX[LI],LY[LI],"$LI")
        end
    end
end

using PyPlot
using PyCall
PltColors = pyimport("matplotlib.colors")
function AngleCmap()


    MyList=[
            [-2, 0.5, 0.5, 0.50], ##### (grey)
            [-1.5, 1.0, 0.5,0.0 ], ## (ornage)
            [-1, 1.0, 0.2,0.1 ], ## (red)
            [0, 1.0, 1.0,1.0 ], ## (white)
            [1, 0.1, 0.2,1.0 ], ## (blue)
            [1.5, 0.0, 1.0, 0.0 ], ## (green)
            [2, 0.5, 0.5, 0.5], ## (grey)
            ]


    cdict = Dict("red" => [[x*.25+.5,r,r] for (x,r,g,b) in MyList],
                 "green" => [[x*.25+.5,g,g] for (x,r,g,b) in MyList],
                 "blue" =>  [[x*.25+.5,b,b] for (x,r,g,b) in MyList])
    Mycmap = PltColors.LinearSegmentedColormap("MyCmap", segmentdata=cdict, N=256)

    return Mycmap
end



function PlotHamiltonian(Hamiltonian,PosList;scale=1.0)
    HamSize=size(Hamiltonian)[2]
    HeadWidth=scale*.3
    HeadLength=scale*.4
    PyPlot.xlim([minimum(PosList[1,:])-scale,
                 maximum(PosList[1,:])+scale])
    PyPlot.ylim([minimum(PosList[2,:])-scale,
                 maximum(PosList[2,:])+scale])
    PyPlot.title("Hopping elements in the hamiltonian")
    
    #MyCmap=PyPlot.cm[:get_cmap]("bwr")
    MyCmap=AngleCmap()
    for i in 1:HamSize
        Pos1=PosList[i]
        (xval1,yval1)=PosList[:,i]
        for j in 1:HamSize
            if i!=j && !(abs(Hamiltonian[i,j]) ≈ 0)
                (xval2,yval2)=PosList[:,j]
                Angle=angle(-Hamiltonian[i,j])
                color=MyCmap(Int(round(256*(Angle/(2*pi))))+128)
                #println("$i,$j: ($xval,$yval1) ($xval2,$yval2) angle=$Angle color=$color")
                PyPlot.arrow(xval1 + (xval2-xval1)*(HeadLength/scale),
                             yval1 + (yval2-yval1)*(HeadLength/scale),
                             (xval2-xval1)*(1-HeadLength/scale),
                             (yval2-yval1)*(1-HeadLength/scale),
                             #head_width=0.3, head_length=0.4,
                             head_width=HeadWidth, head_length=HeadLength,
                             facecolor=color,
                             length_includes_head=true)
            end
        end
    end
    PyPlot.imshow(randn(2,2),extent=[0,0.001,0,0.001],vmin=-1,vmax=1,
                  cmap=MyCmap)
    cbar = PyPlot.colorbar()#,[-pi,-pi/2,0,pi/2,pi])
    #cbar."ax"."set_yticklabels"(["pi","pi/2",0,"pi/2","pi"])

    println("The hamiltonian")
end

function MaxminOfArrows(Base,Diff)
    return (minimum((minimum(Base),minimum(Base+Diff)))-.5,
            maximum((maximum(Base),maximum(Base+Diff)))+.5)
end

function PlotHamiltonian3D(Hamiltonian,IndToMat)
    @eval using PyPlot
    HamSize=size(IndToMat)[2]
    HeadWidth=.5
    fig = PyPlot.figure()
    ax = fig."gca"()#PyPlot.axes(projection = "3d")

    println("IndToMat:")
    display(IndToMat)
    
    PyPlot.title("Hopping elements in the hamiltonian")
    XList=[];
    YList=[];
    ZList=[];
    XDList=[];
    YDList=[];
    ZDList=[];
    CList=[];
    
    for i in 1:HamSize
        (X1,Y1,Z1)=IndToMat[:,i]
        #plot(row1,col1,"kp")
        for j in 1:HamSize
            if i!=j && !(abs(Hamiltonian[i,j]) ≈ 0)
                (X2,Y2,Z2)=IndToMat[:,j]
                push!(XList,X1)
                push!(YList,Y1)
                push!(ZList,Z1)
                push!(XDList,X2-X1)
                push!(YDList,Y2-Y1)
                push!(ZDList,Z2-Z1)
                Angle=angle(-Hamiltonian[i,j])
                color=PyPlot.cm[:get_cmap]("gnuplot")(Int(round(256*(Angle/(2*pi))))+128)
                push!(CList,color)
                #println("$i,$j: ($row1,$col1) ($row2,$col2) angle=$Angle color=$color")
            end
        end
    end
    #println("XList:",XList)
    #println("YList:",YList)
    #println("ZList:",ZList)
    #println("XDList:",XDList)
    #println("YDList:",YDList)
    #println("ZDList:",ZDList)
    
    ###Make3DList
    #XLList=fill(0.0,length(XList),2)
    #YLList=fill(0.0,length(YList),2)
    #ZLList=fill(0.0,length(ZList),2)
    #for indx in 1:length(XList)
    #    XLList[indx,:]=[XList[indx],XList[indx]+XDList[indx]]
    #    YLList[indx,:]=[YList[indx],YList[indx]+YDList[indx]]
    #    ZLList[indx,:]=[ZList[indx],ZList[indx]+ZDList[indx]]
    #end
    
    # Make the grid
    PyPlot.plot3D(IndToMat[1,:],IndToMat[2,:],IndToMat[3,:],"p")
    
    
    for indx in 1:length(XList)
        PyPlot.plot3D((XList[indx],XList[indx]+XDList[indx]*.5),
                      (YList[indx],YList[indx]+YDList[indx]*.5),
                      (ZList[indx],ZList[indx]+ZDList[indx]*.5),
                      color=CList[indx],linewidth=2)
    end
    xlabel("X-dir")
    ylabel("Y-dir")
    zlabel("Z-dir")

    #PyPlot.xlim(MaxminOfArrows(XList,XDList))
    #PyPlot.ylim(MaxminOfArrows(YList,YDList))
    #PyPlot.zlim(MaxminOfArrows(ZList,ZDList))
    
    println("The hamiltonian")
end    


function PlotHamiltonianMatrix(Hamiltonian;PlotType="Abs")
    PyPlot.figure()
    if PlotType=="Abs"
        PyPlot.imshow(abs.(Hamiltonian))
        PyPlot.title("The abs-value of the elements of the hamiltonian")
    elseif PlotType=="Angle"
        PyPlot.imshow(angle.(Hamiltonian) ./ pi, cmap="bwr",vmin=-1,vmax=1)
        PyPlot.title("The angle (in units of pi) of the hamiltonian")
    end
    PyPlot.colorbar()
end



function SavePyFile(FullFileName)
    SaveDir=dirname(FullFileName)
    if !ispath(SaveDir)
        println("Make the path: $SaveDir")
        mkpath(SaveDir)
    end
    println("Save file: ",FullFileName*".png")
    savefig(FullFileName*".png")
    println("Save file: ",FullFileName*".pdf")
    savefig(FullFileName*".pdf")
end


TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close PlotMisc.jl")
