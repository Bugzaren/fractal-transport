if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open SurfaceGreens.jl")
TabLevel=TabLevel*"    "

include("Misc.jl")


function Chain_Solution(Energies,t)
    #### This is the solutions for the greens-function of the semi_infinate chain
    w = Energies./ (2*t)
    return ( w .- im .* sqrt.( 1 .- w .^ 2 ) ) ./ t
end


function SolveSurfaceGreens(Ham11,Ham12;Verbose=false,mu=0.0)
    local Lambdafactor
    let Ham12Dagger,Ham12DaggerRed
        Ham12Dagger=copy(Ham12')
        MaxPos=findmax(abs.(Ham12))[2] ###Only pso intersting
        Lambdafactor=Ham12Dagger[MaxPos] / Ham12[MaxPos]
        #println("Pos of largest integer ",MaxPos)
        #println("Lambdafactor ",Lambdafactor)
        ###Here we check that the hopping Hamiltonian is hermitian up to an overall factor lambda
        if !isapprox(Ham12 .* Lambdafactor,Ham12Dagger,atol=sqrt(eps()))
            throw(DomainError("The hopping hamiltonian V does not fulfill V^\\dagger=V*\\lambda where \\lambda is a (complex) scalar"))
        end
    end
    Verbose && println("Lambdafactor ",Lambdafactor)
    
    ####The greens function g11 at the end of the sqmi-infite matrix solves the equations
    ##  g0-g11 = -g11*A*g11*B*g0
    ## there g0 = h^-1 is for an isolated slice of the chain
    ## here A is the hopping from one chain ot the other, and B=A^\dagger is the hopping back.

    ## If we assume that B=A*\lambda then we have
    ## 1-g11*h = -lambda*g11*A*g11*A
    ## We also assume that A^-1 is well defined. We may then diagonalize
    ## A^-1*h = U*D*U^dagger and define G = U^\dagger*g11*A*U
    ## to arrive at the equation
    ## 0 = 1- G*D + lambda*G^2
    ## This can then be assumed to be on a diaognals form where the dolsuions are (component by component)
    ## G = D/(2*lambda) \pm sqrt(1/lambda-(D/(2*\lambda))^2)
    ## Finally we transform back
    ## g11 = U*G*U^-1*A^⁻1 
    
    ##FIXME some care may be needed if lambda is not strictly positive. 

    ## Extract The hopping inverse
    HoppInverse=inv(Ham12)

    DimH11=size(Ham11)[1]
    
    
    ###Diagonalize V^-1*h
    StartTime=now()
    EigH=eigen(HoppInverse*(Ham11 + diagm(0=>fill(mu,DimH11))))
    EndTime=now()
    DiffTime=PrettyTime(EndTime-StartTime)
    Verbose && println("It took $DiffTime to diagnoalize to find surface greens function")


    Energies = EigH.values
    #figure()
    #plot(sort(abs.(Energies)))
    #title("Eigenenergies of the surface")
    #println("Press enter:")
    #readline(stdin)
    
    Verbose &&  println("The eigenvalues are $Energies")


    if Verbose
        figure()
        plot(real(Energies),imag(Energies),"p",label="Energies")
        title("Eigenvalues of Diaognal Ham")
        legend(loc="best")
    end


    
    ScaleEn=Energies ./ (2*Lambdafactor)
    ###Here we solve using  G = D/(2*lambda) \pm sqrt(1/lambda-(D/(2*\lambda))^2)
    GDiagFixed = ScaleEn
    ###Solve for the square root part. Make sure the argument under the root is a complex float
    RootArgument=(1.0+0.0im).*(1/Lambdafactor .- ScaleEn .^ 2)
    GDiagRaw  = im .* sqrt.(RootArgument)

    if Verbose
        figure()
        plot(real(GDiagFixed),imag(GDiagFixed),"p",label="Fixed")
        plot(real(GDiagRaw),imag(GDiagRaw),"p",label="Raw sqrt")
        title("Elements of the Diagonal equation")
        legend(loc="best")
    end
    
    
    ####Now we need to choose the signs of the GDiag part.
    ###WE can fix this by demanding that the trace is maximally positive
    

    ###We use that the density of states is the negative imaginary part of the trace of g11.
    ### We can use that g11 = U*G*U^-1*A^⁻1 and G is diagonal to write that trace as
    ## rho = Im(Tr(U*G*U^-1*A^⁻1))=Im(Tr(G*M)), where M=U^-1*A^⁻1*U
    ## Now since G_ij=delta_ij*G_j we have
    ## rho = Im(sum_ij(delta_ij*G_j*M_ji))=Im(sum_j(G_j*M_jj))
    ## From this we see that the element M_jj determines the sign we should choose for the square root in G_j.

    ###Also since we only need M_jj=U_jk^-1*A_kl^⁻1*U_lj we need not construct the full M
    ###But can construct first only  N_jl = U_jk^-1*A_kl^⁻1 and the take the appropriate inner product with U_lj.

    StartTime=now()
    InvU=inv(EigH.vectors)
    U = EigH.vectors ##This just maps the pointers
    EndTime=now()
    DiffTime=PrettyTime(EndTime-StartTime)
    Verbose && println("It took $DiffTime to invert the eigenvectors")

    
    N_matrix= InvU * HoppInverse

    MDiag=fill(0.0im,DimH11)
    for j in 1:DimH11
        #println("Size N_matrix[$j,:] ",size(N_matrix[j,:]))
        #println("Size: U[:,$j]",size(U[:,j]))
        MDiag[j]=sum(N_matrix[j,:].*U[:,j])
    end
    DiagContribs=(imag.(MDiag.*(GDiagRaw)) .<= 0).*2 .- 1
    GDiagSign=DiagContribs.*GDiagRaw

    if Verbose
        figure()
        plot(real(GDiagFixed),imag(GDiagFixed),"p",label="Fixed")
        plot(real(GDiagSign),imag(GDiagSign),"p",label="Signed sqrt")
        plot(real(GDiagFixed+GDiagSign),imag(GDiagFixed+GDiagSign),"p",label="Total")
        title("Elements of the Diagonal equation")
        legend(loc="best")
        
        figure()
        plot(real(MDiag.*(GDiagFixed)),imag(MDiag.*(GDiagFixed)),"p",label="DosContrib fiexed")
        plot(real(MDiag.*(GDiagSign)),imag(MDiag.*(GDiagSign)),"p",label="DosContrib signed")
        plot(real(MDiag.*(GDiagFixed+GDiagSign)),imag(MDiag.*(GDiagFixed+GDiagSign)),"p",label="DosContrib together")
        title("Contributoins to DOS")
        legend(loc="best")
    end
    
    ###Here we transform back using g11 = U*G*U^\dagger*A^⁻1 
    SurfaceGreens =  U*diagm(0=>(GDiagFixed+GDiagSign))*InvU*HoppInverse

    ValidateSurfaceGreens(SurfaceGreens,Ham11,Ham12,mu=mu)
    
    return SurfaceGreens
    
end



function ValidateSurfaceGreens(SurfaceGreens,Ham11,Ham12;Verbose=false,mu=0.0)
    ####The greens function g11 at the end of the sqmi-infite matrix solves the equations
    ## If we assume that B=A*\lambda then we have
    ## 1-g11*h = -g11*A*g11*B
    ## here A is the hopping from one chain ot the other, and B=A^\dagger is the hopping back.
    
    
    DimH11=size(Ham11)[1]

    
    ###Thus we compute the left and right hand sides
    LefthandSide=(diagm(0=>fill(1.0,DimH11))
                  -SurfaceGreens*(Ham11 + diagm(0=>fill(mu,DimH11)))
                  +SurfaceGreens*Ham12*SurfaceGreens*(Ham12'))
    RightHandSide=fill(0.0,DimH11,DimH11)
    

    if !isapprox(LefthandSide,RightHandSide,atol=sqrt(eps()))
        #throw(ErrorException("The Left hand side of 1-g11*h+g11*A*g11*B=0 is not zero!."))
    else
        Verbose &&  println("The Left and Right hand sides are the same")
    end


    DOS=-imag(tr(SurfaceGreens))
    if DOS  < 0
        if !isapprox(abs(DOS),0.0,atol=sqrt(eps()))
            throw(ErrorException("Density of states = $DOS is less than zero:\nSurfacegreens=\n$SurfaceGreens"))
        ##else
            ###Density of states if close to zero (rounding errors could happen
        end
    end
    
    true
end



function ExactSemiInfiniteChain(Dim,mu;t=1.0,ConMatrix=nothing,Verbose=false)
    if ConMatrix==nothing
        ConMatrix = diagm(1=>trues(Dim-1),-1=>trues(Dim-1))
    end
    if Dim != size(ConMatrix)[1] || Dim != size(ConMatrix)[2]
        throw(DomainError((Dim,ConMatrix),"The Connectivity matrix does not fit the number of elements in the chain"))
    end
    
    ####The greens function g11 at the end of the sqmi-infite matrix solves the equations
    ##  g0-g11 = -g11*A*g11*B*g0
    ## there g0 = d^-1 is for an isolated slice of the chain
    ## here A is the hopping from one chain ot the other, and B=A^\dagger is the hopping back.

    ### Recuslively we can perform
    ### g11 = g0*(1 - A*gp11*B*g0)^-1
    ##where now gp11 is the previous iteration

    StartTime=now()
    
    ###Lets now greate the matrices
    A = ChainHopping(Dim,t=t) ## The hoppi terms
    B = A' ###Hermitian conjugate
    h = -t.*ConMatrix  ## The sliece hamiltonian
    one = diagm(0=>ones(Dim))
    ###Since A=b=t we can diagonalize and the use those energies as
    ### g0-g11 = -t^2*g11^2*g0

    EndTime=now()
    DiffTime=PrettyTime(EndTime-StartTime)
    Verbose && println("It took $DiffTime to create inital matrices")

    
    ###Diagonalize h
    StartTime=now()
    EigH=eigen(h)
    EndTime=now()
    DiffTime=PrettyTime(EndTime-StartTime)
    Verbose &&  println("It took $DiffTime to diagnoalize the hamiltonian")
    Energies = EigH.values .+ mu

    ###Note that
    # H = E.vec * E.val * E.vec'

    ### Thus this also holds for the full G

    StartTime=now()
    DiagG = Chain_Solution(Energies,t)
    #println("Found_semi_infinite_chain_solution for E=$mu")
    SurfaceGreens =  EigH.vectors*diagm(0=>DiagG)*EigH.vectors'
    EndTime=now()
    DiffTime=PrettyTime(EndTime-StartTime)
    Verbose && println("It took $DiffTime to finalize the surface greens")

    
    ###Finally we test that the surface greesn functions solves the original equation
    if false
        StartTime=now()
        H = EigH.vectors * diagm(0=>Energies) * EigH.vectors'
        LeftHand = SurfaceGreens * H
        RightHand = one + t^2 .* SurfaceGreens * SurfaceGreens
        @test isapprox(LeftHand,RightHand,atol=sqrt(eps()))
        EndTime=now()
        DiffTime=PrettyTime(EndTime-StartTime)
        Verbose && println("It took $DiffTime to sanity check")
    end

    ValidateSurfaceGreens(SurfaceGreens,h,A,mu=mu)
    return SurfaceGreens
end


TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close SurfaceGreens.jl")
