if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open ChainAttachments.jl")
TabLevel=TabLevel*"    "

function get_chain_attachment_points(IndVecMap,Terminals,LeadWidth=nothing)
    if size(IndVecMap)[1]==2
        get_chain_attachment_points2D(IndVecMap,Terminals,LeadWidth)
    elseif size(IndVecMap)[1]==3
        get_chain_attachment_points3D(IndVecMap,Terminals,LeadWidth)
    else
        throw(DomainError(size(IndVecMap),"Lattice Dimention Not implemented"))
    end
end
        
function get_chain_attachment_points3D(IndVecMap,Terminals,LeadWidth)
    FrontSideIndx=get_border_border_indexes3D(IndVecMap,"F")
    BackSideIndx=get_border_border_indexes3D(IndVecMap,"B")
    LeftSideIndx=get_border_border_indexes3D(IndVecMap,"L",LeadWidth)
    RightSideIndx=get_border_border_indexes3D(IndVecMap,"R",LeadWidth)
    UppSideIndx=get_border_border_indexes3D(IndVecMap,"U",LeadWidth)
    DownSideIndx=get_border_border_indexes3D(IndVecMap,"D",LeadWidth)
    if Terminals==2
        SideIndexes=(FrontSideIndx,BackSideIndx)
    elseif Terminals==4
        SideIndexes=(FrontSideIndx,BackSideIndx,LeftSideIndx,RightSideIndx)
    else
        throw(DomainError(Terminals,"Number of terminals not implemented"))
    end
end
    

    
function get_chain_attachment_points2D(IndVecMap,Terminals,LeadWidth)
    LeftSideIndx=get_border_border_indexes(IndVecMap,"L")
    RightSideIndx=get_border_border_indexes(IndVecMap,"R")
    UppSideIndx=get_border_border_indexes(IndVecMap,"U")
    DownSideIndx=get_border_border_indexes(IndVecMap,"D")
    #println("LeftSideIndx:",LeftSideIndx)
    #println("RightSideIndx:",RightSideIndx)
    #println("UppSideIndx:",UppSideIndx)
    #println("DownSideIndx:",DownSideIndx)
    if LeadWidth==nothing && Terminals!=2
        throw("'LeadWidth' cannot be 'noting' if there are more than two leads.")
    end
    if Terminals==2
        SideIndexes=(LeftSideIndx,RightSideIndx)
    elseif Terminals==3
        Middle=Int(round((length(UppSideIndx)+1)/2))
        MidL=Middle-Int(floor(LeadWidth/2))
        MidR=Middle+Int(floor((LeadWidth-1)/2))
        #println("Middle:",Middle)
        #println("MidL:",MidL)
        #println("MidR:",MidR)
        
        SideIndexes=(LeftSideIndx,RightSideIndx,
                     UppSideIndx[MidL:MidR])
    elseif Terminals==4
        TwoMiddle=length(UppSideIndx)+1
        println("length(UppSideIndx): ",length(UppSideIndx))
        println("LeadWidth: ",LeadWidth)
        if length(UppSideIndx)%2==0 ##Even length
            if LeadWidth%2==0 ##Even lead length
                MidL=Int((TwoMiddle-LeadWidth+1)/2)
                MidR=Int((TwoMiddle+LeadWidth-1)/2)
            else ##Odd length lead
                MidL=Int((TwoMiddle-LeadWidth)/2)
                MidR=Int((TwoMiddle+LeadWidth-2)/2)
            end
        else 
            if LeadWidth%2==0 ##Even lead length
                MidL=Int((TwoMiddle-LeadWidth)/2)
                MidR=Int((TwoMiddle+LeadWidth-2)/2)
            else ##Odd length lead
                MidL=Int((TwoMiddle-LeadWidth+1)/2)
                MidR=Int((TwoMiddle+LeadWidth-1)/2)
            end
        end
        #println("TwoMiddle: ",TwoMiddle)
        #println("MidL: ",MidL)
        #println("MidR: ",MidR)
        
        SideIndexes=(LeftSideIndx,RightSideIndx,
                     UppSideIndx[MidL:MidR],
                     DownSideIndx[MidL:MidR])
    elseif Terminals==6
        Left=Int(round((length(UppSideIndx)+1)*(1/3)))
        Right=Int(round((length(UppSideIndx)+1)*(2/3)))
        #println("Left:",Left)
        #println("Right:",Right)
        LeftL=Left-Int(floor(LeadWidth/2))
        LeftR=Left+Int(floor((LeadWidth-1)/2))
        RightL=Right-Int(floor((LeadWidth-1)/2))
        RightR=Right+Int(floor((LeadWidth)/2))
        #println("LeftL:",LeftL)
        #println("LeftR:",LeftR)
        SideIndexes=(LeftSideIndx,RightSideIndx,
                     UppSideIndx[LeftL:LeftR],UppSideIndx[RightL:RightR],
                     DownSideIndx[LeftL:LeftR],DownSideIndx[RightL:RightR])
    else
        throw(DomainError("Number of terminal leads not implemented"))
    end
    return SideIndexes
end    




function get_border_border_indexes(IndVecMap,Side;depth=0)
    if size(IndVecMap)[1]==2
        get_border_border_indexes2D(IndVecMap,Side;depth=depth)
    elseif size(IndVecMap)[1]==3
        get_border_border_indexes3D(IndVecMap,Side;depth=depth)
    else
        raise(DomainError(size(IndVecMap),"Lattice Dimention Not implemented"))
    end
end


function get_border_border_indexes3D(IndVecMap,Side,LeadWidth=-1)
    ###LeadWidth==-1  means we use the full width
    ###Index order is (X,Y,Z)  #X=left/right, Y=front/back, Z=Up/Down
    if Side == "F" ##Front
        Min=true;Dim=1
        WDim=2 ##Yvariable
    elseif Side == "B" ##Back
        Min=false;Dim=1
        WDim=2 ##Yvariable
    elseif Side == "L" ##Left
        Min=false;Dim=2
        WDim=1 ##Xvariable
    elseif Side == "R" ###Right
        Min=true;Dim=2
        WDim=1 ##Xvariable
    elseif Side == "D"  ##Down
        Min=true;Dim=3
        WDim=1 ##Xvariable
    elseif Side == "U" ###Up
        Min=false;Dim=3
        WDim=1 ##Xvariable
    else
        throw(DomainError(Side,"Only F,B,L,R,U,D are valid sides"))
    end
    if Min
        ExtremeIndex=minimum(IndVecMap[Dim,:])
    else
        ExtremeIndex=maximum(IndVecMap[Dim,:])
    end
    FullEdgeIndexSet=findall(IndVecMap[Dim,:].==ExtremeIndex)
    if LeadWidth==-1
        return FullEdgeIndexSet
    else
        ReducedIVM=IndVecMap[:,FullEdgeIndexSet]
        #println("ReducedIVM:")
        #display(ReducedIVM)
        ###Find the position of the boundaries of the lead
        MinWL=minimum(ReducedIVM[WDim,:])
        MaxWL=maximum(ReducedIVM[WDim,:])
        Middle=div(MinWL+MaxWL,2)
        #println("MinWL:",MinWL)
        #println("MaxWL:",MaxWL)
        #println("Middle:",Middle)
        ElmsToLeft=div(LeadWidth,2) ###Half ot the left
        #println("ElmsToLEft:",ElmsToLeft)
        ElmsToCenterRight=LeadWidth-ElmsToLeft
        #println("ElmsToCenterRight:",ElmsToCenterRight)
        DimRange=(Middle-ElmsToLeft):(Middle+ElmsToCenterRight-1)
        #println("DimRange:",DimRange)
        PartialEdgeIndexSet=[]
        for DimElem in DimRange
            #println("PartialEdgeIndexSet:",PartialEdgeIndexSet)
            #println("DimElem:",DimElem)
            LocalElems=findall(ReducedIVM[WDim,:].==DimElem)
            #println("LocalElems:",LocalElems)
            append!(PartialEdgeIndexSet,LocalElems)
        end
        #println("PartialEdgeIndexSet:",PartialEdgeIndexSet)
        ActuallIndexes=FullEdgeIndexSet[PartialEdgeIndexSet]
        #println("ActuallIndexes:",ActuallIndexes)
        return ActuallIndexes
    end
end


function get_border_border_indexes2D(IndVecMap,Side;depth=0)
    if Side == "L"
        IndexValues = -IndVecMap[2,:]
    elseif Side == "R"
        IndexValues = IndVecMap[2,:]
    elseif Side == "D"
        IndexValues = -IndVecMap[1,:]
    elseif Side == "U"
        IndexValues = IndVecMap[1,:]
    elseif Side == "UR"
        IndexValues = IndVecMap[1,:] + IndVecMap[2,:]
    elseif Side == "UL"
        IndexValues = IndVecMap[1,:] - IndVecMap[2,:]
    elseif Side == "DR"
        IndexValues = -IndVecMap[1,:] + IndVecMap[2,:]
    elseif Side == "DL"
        IndexValues = -IndVecMap[1,:] - IndVecMap[2,:]
    else
        throw(DomainError(Side,"Only L,R,U,D,UL,UR,DR,DL are valid sides.\nUL=Upper-Left diagonal,UR=Upper-Right diagonal."))
    end
    ExtremeIndex=maximum(IndexValues)-depth
    return findall(IndexValues.==ExtremeIndex)

    
end


TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close ChainAttachments.jl")
