if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open LevelStatistics.jl")
TabLevel=TabLevel*"    "

###This file defines the functions nececarry to diagponalise the serpinsly carpet, gaskes and triangle....

###using PyPlot ###use the pyplot back-end - but load it at top level
using Statistics
using FFTW

include("Sierpinski.jl")



function SLSScan(Params::Dict,PName::String,
                 ParList;Iters::Integer=10,Plot=true,Save=true)

    RmeanList=ones(length(ParList)) .* 0.0
    RstdList=ones(length(ParList)) .* 0.0
    RerrList=ones(length(ParList)) .* 0.0
    Params["GaugeChoise"]=1
    Dim=0

    TimeDict=Dict("TS"=>TimingInit(),"Count"=>0,
              "MaxCount"=>length(ParList)*Iters)
    for runno in 1:length(ParList)
        ##update the relevant parameter
        Params[PName]=ParList[runno]
        (rm,rsd,rerr,Dim)=SLSAverage(Params;Iters=Iters,TD=TimeDict)
        RmeanList[runno]=rm
        RstdList[runno]=rsd
        RerrList[runno]=rerr
    end


    if Plot
        LT=Params["LT"]
        SizeGen=Params["sg"]
        FracGen=Params["fg"]
        Phase=Params["ph"]
        W=Params["W"]
        tW=Params["tW"]

        figure()
        errorbar(ParList,RmeanList,yerr=RstdList)
        errorbar(ParList,RmeanList,yerr=RerrList)
        XLIM=xlim()
        plot(XLIM, [1,1] .*P0avg() ,":g")
        plot(XLIM, [1,1] .*P1avg() ,":r")
        plot(XLIM, [1,1] .*P2avg() ,":c")
        plot(XLIM, [1,1] .*P4avg() ,":m")
        legend(["possion","GOE","GUE","GSE","data"],loc="upper right")

        xlabel("Scan Variable: "*PName)
        ylabel("r-statistics <r>")
        title("R-statistics Dim=$Dim sg=$SizeGen fg=$FracGen\n Ph=$Phase W=$W, tW=$tW, LT=$LT")
        if Save
            IDSTRING="r_scan_"*PName*"_LT_$LT"*"_sg_$SizeGen"*"_fg_$FracGen"*"_Ph_$Phase"*"_W_$W"*"_tW_$tW"
            SavePyFile("pic_level_scan_new/"*IDSTRING)
        end
    end
    return RmeanList,RstdList,RerrList
end


function SLSAverage(Params::Dict;Iters::Integer=10,TD=nothing)
    Rvals=ones(Iters) .* 0.0
    Dim=0
    if TD!=nothing
        j=TD["Count"]
        TIMESTRUCT=TD["TS"]
        Ncount=TD["MaxCount"]
    end
    for runno in 1:Iters
        Rdistr=SierpinskiLevelStatistics(Params)
        Rvals[runno]=mean(Rdistr)
        Dim=length(Rdistr)
        if TD!=nothing
            j=j+1
            TIMESTRUCT=TimingProgress(TIMESTRUCT,j,Ncount)
        end
    end
    if TD!=nothing
        TD["Count"]=j
        TD["TS"]=TIMESTRUCT
    end
    return (mean(Rvals),std(Rvals),std(Rvals)/sqrt(10),Dim)  
end

function SierpinskiLevelStatistics(Params::Dict,Verbose=false)
    Hamiltonian,_=CreateSierpinskiHamiltonianRotSym(Params,0,Verbose=Verbose)
    
    HamSize=size(Hamiltonian)[2]
    if Verbose 
        println("Solve the N=$HamSize dim eigensystem")
        StartTime=now()
    end
    Eigvals = eigvals(Hamiltonian)
    if Verbose
        EndTime=now()
        DiffTime=Dates.canonicalize(Dates.CompoundPeriod(Dates.Millisecond(EndTime-StartTime)))
        println("It took $DiffTime to fully diagonalize")
    end
    Rdistr=get_r_stat(Eigvals)
end



function SLSPlot(Params::Dict;Bins::Integer=0)

    LT=Params["LT"]
    SizeGen=Params["sg"]
    FracGen=Params["fg"]
    Phase=Params["ph"]
    W=Params["W"]
    tW=Params["tW"]
    t=Params["t"]

    Rdistr=SierpinskiLevelStatistics(Params::Dict)
    NumVals=length(Rdistr)
    
    ##Now we make a histogram over the r (and s) -statistcs

    if Bins<=0
        Bins=minimum([50,Int(round(NumVals/10))])
    end
    BinVals=range(0,stop=1.0,length=(Bins+1))
    BinMids=(BinVals[1:Bins]+BinVals[2:(Bins+1)])./2

    figure()
    PyPlot.plt["hist"](Rdistr,BinVals)
    plot(BinMids,pre(BinMids).*NumVals./Bins,"g")
    plot(BinMids,pr1(BinMids).*NumVals./Bins,"r")
    plot(BinMids,pr2(BinMids).*NumVals./Bins,"c")
    plot(BinMids,pr4(BinMids).*NumVals./Bins,"m")
    legend(["possion","GOE","GUE","GSE","data"],loc="upper right")
    YLIM=ylim()
    plot([1,1] .*P0avg() ,YLIM,":g")
    plot([1,1] .*P1avg() ,YLIM,":r")
    plot([1,1] .*P2avg() ,YLIM,":c")
    plot([1,1] .*P4avg() ,YLIM,":m")
    plot([1,1] .*mean(Rdistr) ,YLIM,"-k")
    title("R-statistics Bins=$Bins Dim=$NumVals sg=$SizeGen fg=$FracGen\n Ph=$Phase W=$W, tW=$tW, LT=$LT")

end

function P0avg() ; 0.38629 ; end
function P1avg() ; 4.0-2*sqrt(3) ; end
function P2avg() ; -(1/2) + (2*sqrt(3))/pi ; end
function P4avg() ; -(1/2) + 32/(5*sqrt(3)*pi) ; end

function pe(s) ; @. exp(-s) ;end
function p1(s) ; @. (s*pi/2.0)*exp(-pi*s^2/4.0) ; end
function p2(s) ; @. (s^2*32/pi^2)*exp(-4.0*s^2/pi) ; end

function pre(r) ; @. 2/(1.0+r)^2 ;end
function pr1(r) ; PW(r,1,8.0/27) ;end
function pr2(r) ; PW(r,2,(4.0/81)*(pi/sqrt(3))) ;end
function pr4(r) ; PW(r,4,(4.0/729)*(pi/sqrt(3)));end
function PW(r,beta,Z)
    @. 2*(r+r^2)^beta/(Z*(1.0+r+r^2)^(1.0+3*beta/2.0))
end



function get_r_stat(Energies)
    ###Vectorize and sort the energies
    SortEnergies=sort(vec(Energies))
    ###Can now assume list is flat

    Length=length(SortEnergies)
    Diff1=SortEnergies[2:(Length-1)]-SortEnergies[1:(Length-2)]
    Diff2=SortEnergies[3:(Length-0)]-SortEnergies[2:(Length-1)]

    Rvec=ones(Length-2) .* 0.0 ##vec of zeros
    for indx in 1:Length-2
        D1=Diff1[indx]
        D2=Diff2[indx]
        #println("D1=$D1, D2=$D2")
        if D1 >= D2
            Rvec[indx] = D2 / D1
        else
            Rvec[indx] = D1 / D2
        end
        #println("Rvec[indx]=",Rvec[indx])
    end
    return Rvec
end

function get_s_stat(Energies)
    ###Vectorize and sort the energies
    SortEnergies=sort(vec(Energies))
    ###Can now assume list is flat

    Length=length(SortEnergies)
    Diff1=SortEnergies[2:(Length-1)]-SortEnergies[1:(Length-2)]
    return Diff1/maximum(Diff1)
end

TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close LevelStatistics.jl")
