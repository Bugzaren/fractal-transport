if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open SliceLattice.jl")
TabLevel=TabLevel*"    "

using ProgressTimer

function check_nonoverlaping_leads(SideIndexes)
    for LeadNo1 in 1:(length(SideIndexes)-1)
        for LeadNo2 in (LeadNo1+1):length(SideIndexes)
            for Elem1 in SideIndexes[LeadNo1]
                for Elem2 in SideIndexes[LeadNo2]
                    if Elem1==Elem2
                        throw(DomainError(SideIndexes,
                                          "Some of the lead $LeadNo1 and lead $LeadNo2 "*
                                          "attach to the same site = $Elem1\n"*
                                          "FIXME: This has not been implemented yet!"))
                    end
                end
            end
        end
    end
    nothing
end

function SlizeTheLattice(Params,LeadAttachments=nothing;Plot=false,Verbose=false)
    
    IVM=Params["IVM"]
    validate_IVM(IVM) ###Check that the IVM is valid

    ###Check that all the leads are attached
    checkLeadsAttachmentsExists(LeadAttachments,IVM)

    Verbose && println("Create the Lattice Slizes")

    
    Verbose && println("IVM:")
    Verbose && display(IVM)

    TotNumElems=size(IVM)[2]
    
    SlizeID=fill(0,TotNumElems)
    Counter=1:TotNumElems
    #Verbose && println("SlizeID")
    #Verbose && display(SlizeID)
    #### Compute Possbile neighbours
    
    SlizeDict=Dict{Integer,Array{Int64,1}}() ###The slize dict contains the global id numbers

    ###Extract the indexes to be in the first slize.
    FirstElem,LocalIndexes,LeadID,LeadSlizeAttach=
        ExtractStartingElement(LeadAttachments,Verbose)
    Verbose && println("LocalIndexes")
    Verbose && display(LocalIndexes)
    
    SlizeDict[1]=FirstElem ###First slice has only one point
    SlizeID[FirstElem].=1
    
    ###We should assume the scattering area is connected (it makes life easier)
    SlizeCount=1
    LocalLeads=length(LocalIndexes)
    TIMESTRUCT=TimingInit()
    UsedPoints=0
    GetNeigbours=Params["GetPossibleNeighbours"]
    ParamsTuple=Params["ParamsTuple"](Params)
    while true
        NumNeigbours=0
        Verbose && println("SlizeCount:",SlizeCount)
        ###We first look up all the neigbours of the first elementns
        Verbose && println("Slice dictionary step $SlizeCount:\n",SlizeDict[SlizeCount])
        for element in SlizeDict[SlizeCount]
            Verbose && println("element:",element)
            Verbose && println("pos:",IVM[:,element])
            FromPos=Tuple(IVM[:,element])
            TheNeighbours=GetNeigbours(FromPos,ParamsTuple)
            Verbose && println("TheNeighbours:\n  ",TheNeighbours)
            ###Look up these neigbours in the IVM
            for Neigbour in TheNeighbours
                #println("  Neigbour: ",Neigbour)
                Jval=IsOnLattice(Neigbour,IVM)
                if Jval != nothing
                    #println("  Jval: ",Jval)
                    ###Look up lattice designation for this point
                    LatticeID=SlizeID[Jval]
                    #println("  LatticeID: ",LatticeID)
                    if LatticeID==0 ###Latticepoint ahs not been given slize yet
                        ##Add to the next slize
                        SlizeID[Jval]=SlizeCount+1
                        #println("    Set to next lattice")
                        NumNeigbours+=1
                        ### Chec if this point is also in the lead lists
                        if LocalLeads>0
                            #println("There could be matches with the leads")
                            for RemLeadNo in LocalLeads:-1:1
                                LeadIndexes=LocalIndexes[RemLeadNo]
                                #println("LeadIndexes:\n",LeadIndexes)
                                for LeadIndx in LeadIndexes
                                    if Jval==LeadIndx
                                        #println("Found a match in remaining lead no $RemLeadNo")
                                        SlizeID[LeadIndexes] .= (SlizeCount + 1)
                                        LeadSlizeAttach[LeadID[RemLeadNo]]=SlizeCount + 1
                                        LocalIndexes=LocalIndexes[[1:(RemLeadNo-1);(RemLeadNo+1):LocalLeads]]
                                        LeadID=LeadID[[1:(RemLeadNo-1);(RemLeadNo+1):LocalLeads]]
                                        #println("LocalIndexes:\n",LocalIndexes)
                                        LocalLeads-=1                          
                                        break
                                    end
                                end
                            end
                        end
                    else
                        #println("    Already on a slize")
                    end
                else
                    #println("  Not on lattice")
                end
            end
        end
        UsedPoints+=length(SlizeDict[SlizeCount])
        EsstimateNumberLines=SlizeCount+Int(ceil(sqrt(TotNumElems-UsedPoints)))
        TIMESTRUCT=TimingProgress(TIMESTRUCT,SlizeCount,EsstimateNumberLines;
                                  Message="Assigned $UsedPoints of $TotNumElems")
        
        SlizeCount+=1
        if NumNeigbours==0
            #println("No updates this round")
            break
        else
            #println("Update the Slice dictionary at step $SlizeCount")
            ### Makes sure that slize is sorted!
            SlizeDict[SlizeCount]=sort(Counter[SlizeID .== SlizeCount])
        end
    end
    Verbose && println("SlizeDict:")
    Verbose && display(SlizeDict)
    if Plot
        plotSlizeDict(SlizeDict,IVM)
    end
    return (SlizeDict,LeadSlizeAttach)
end


function ExtractStartingElement(LeadAttachments,Verbose=false)
    if LeadAttachments!=nothing
        ###Make sure the leads are not attaching to the same sites (that is not implemented yet)
        check_nonoverlaping_leads(LeadAttachments)
        Verbose && println("LeadAttachments")
        Verbose && display(LeadAttachments)
        FirstElem=LeadAttachments[1] ###If this exits we use the first element
        LocalIndexes  = Array{Array{Int64, 1}}(undef,length(LeadAttachments)-1)
        for LeadNo in 2:length(LeadAttachments)
            LocalIndexes[LeadNo-1]=LeadAttachments[LeadNo]
        end
        LeadSlizeAttach=fill(0,length(LeadAttachments))
        LeadSlizeAttach[1]=1
        LeadID=2:length(LeadAttachments)
    else
        LocalIndexes=[]
        if FirstElem!=nothing
            FirstElem=Int(ceil(rand()*TotNumElems))
        end
    end
    return FirstElem,LocalIndexes,LeadID,LeadSlizeAttach
end

#using PyPlot
function plotSlizeDict(SlizeDict,IVM)
    (Dim,_)=size(IVM)
    Keys=keys(SlizeDict)
    figure()
    if Dim==2
        plot([],[])
    elseif Dim==3
        plot3D([],[],[])
    end
    for Key in Keys
        println("Key: ",Key)
        Elements=SlizeDict[Key]
        ElemPos=IVM[:,Elements]
        println("ElemPos:")
        display(ElemPos)
        Color=NumToCol(Key)
        if Dim==2
            plot(ElemPos[2,:],ElemPos[1,:],Color*"p")
            text(ElemPos[2,1],ElemPos[1,1],"$Key")
        elseif Dim==3
            plot3D(ElemPos[1,:],ElemPos[2,:],ElemPos[3,:],Color*"p")
            text3D(ElemPos[1,1],ElemPos[2,1],ElemPos[3,1],"$Key")
        else
            raise(DomainError(Dim,"Unimplemented Lattice dimention"))
        end
        #for ElemNo in 1:length(Elements)
        #    circle1=patch.Circle((ElemPos[2,ElemNo],ElemPos[1,ElemNo]),.5,color=Color)
        #    gcf()["gca"]()["add_artist"](circle1)
        #    text(ElemPos[2,ElemNo],ElemPos[1,ElemNo],"$Key")
        #end
    end
    if Dim==2
        xlabel("X")
        ylabel("Y")
    elseif Dim==3
        xlabel("X")
        ylabel("Y")
        zlabel("Z")
    end       
end

function NumToCol(NumIn::Int)
    Num = mod(NumIn-1,7)+1
    if Num==1
        return "b"
    elseif  Num==2
        return "g"
    elseif  Num==3
        return "r"
    elseif  Num==4
        return "c"
    elseif  Num==5
        return "m"
    elseif  Num==6
        return "y"
    elseif  Num==7
        return "k"
    end
end



function checkLeadsAttachmentsExists(LeadAttachments,IVM)
    ####Check the the Lead attachments exists
    NumElemets=size(IVM)[2]
    for Lead in LeadAttachments
        for AttachmentPoint  in Lead
            if AttachmentPoint > NumElemets
                throw(DomainError(AttachmentPoint,"The AttachmentPoint=$AttachmentPoint does not exist. System size is only $NumElemets."))
            end
        end
    end
end


function SlizeStraight(IVM)
    SlizeDict=Dict{Integer,Array{Int64,1}}() ###The slize dict contains the global id numbers
    MinRows=minimum(IVM[2,:])
    MaxRows=maximum(IVM[2,:])
    RowId=1
    for Row in MinRows:MaxRows
        SlizeDict[RowId]=findall(IVM[2,:] .== Row)
        RowId+=1
    end
    return (SlizeDict,[1,RowId-1])
end


TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close SliceLattice.jl")
