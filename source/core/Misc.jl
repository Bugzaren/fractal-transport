if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open Misc.jl")
TabLevel=TabLevel*"    "

using ProgressTimer #### user defined timing library
using LinearAlgebra
using DelimitedFiles
using Dates

function PrettyTime(DiffTime)
    return Dates.canonicalize(Dates.CompoundPeriod(Dates.Millisecond(DiffTime)))
end

function PrettyDiff(Time2,Time1)
    return Dates.canonicalize(Dates.CompoundPeriod(Dates.Millisecond(Time2-Time1)))
end


function VIM_from_IVM(IVM)
    if size(IVM)[1]==2
        VIM_from_IVM_2D(IVM)
    elseif size(IVM)[1]==3
        VIM_from_IVM_3D(IVM)
    else
        throw(DomainError("Dimention of IVM not implemented"))
    end
end
     
function VIM_from_IVM_2D(IVM)
    ###Find all unique numbers,sorted
    MaxRow=maximum(IVM[1,:])
    MaxCol=maximum(IVM[2,:])
    MinRow=minimum(IVM[1,:])
    MinCol=minimum(IVM[2,:])

    VIMap = fill(0,MaxRow-MinRow+1,MaxCol-MinCol+1)
    for Elem in 1:(size(IVM)[2])
        Row,Col=IVM[:,Elem]
        #println("Row,Col:$Row,$Col")
        VIMap[Row-MinRow+1,Col-MinCol+1]=Elem
    end
    #println("MinRow,MinCol:$MinRow,$MinCol")
    return (VIMap,(MinRow,MinCol))
end

function VIM_from_IVM_3D(IVM)
    println("Find Maximum and Minimum")
    MaxX=maximum(IVM[1,:])
    MaxY=maximum(IVM[2,:])
    MaxZ=maximum(IVM[3,:])
    MinX=minimum(IVM[1,:])
    MinY=minimum(IVM[2,:])
    MinZ=minimum(IVM[3,:])
    (OffX,OffY,OffZ)=-[MinX,MinY,MinZ].+ 1
    println("Making VI-Map")
    VIMap = fill(0,MaxX-MinX+1,MaxY-MinY+1,MaxZ-MinZ+1)
    for Elem in 1:(size(IVM)[2])
        (X,Y,Z)=IVM[:,Elem]
        #println("X,Y,Z: $X, $Y, $Z")
        VIMap[X+OffX,Y+OffY,Z+OffZ]=Elem
    end
    #println("MinRow,MinCol:$MinRow,$MinCol")
    return (VIMap,(MinX,MinY,MinZ))
end


function IsAnElement(Target,VIM)
    if length(Target) == 2
        IsAnElement2D(Target,VIM)
    elseif length(Target) == 3
        IsAnElement3D(Target,VIM)
    else
        raise(DomainError(Target,"target Dimention Not implemented"))
    end
end

function IsAnElement2D(Target,VIM)
    (TRow,TCol)=Target
    #println("Target=$Target")
    (VIMap,(MinRow,MinCol))=VIM
    (MaxRow,MaxCol)=size(VIMap).+(MinRow-1,MinCol-1)
    if MinRow>TRow || MinCol>TCol
        return nothing
    elseif MaxRow<TRow || MaxCol<TCol
        return nothing
    else
        Value = VIMap[TRow-MinRow+1,TCol-MinCol+1]
        if Value == 0
            return nothing
        else
            return Value
        end
    end
end


function IsAnElement3D(Target,VIM)
    (X,Y,Z)=Target
    #println("Target=$Target")
    (VIMap,(MinX,MinY,MinZ))=VIM
    #println("VIMap")
    #display(VIMap)
    #println("MinX,MinY,MinZ: $MinX, $MinY, $MinZ")
    (MaxX,MaxY,MaxZ)=size(VIMap).+(MinX-1,MinY-1,MinZ-1)
    if MinX>X || MinY>Y || MinZ>Z
        return nothing
    elseif MaxX<X || MaxY<Y || MaxZ<Z
        return nothing
    else
        Value = VIMap[X-MinX+1,Y-MinY+1,Z-MinZ+1]
        if Value == 0
            return nothing
        else
            return Value
        end
    end
end


function PhaseFactor(thopp::Real,(yfrom,xfrom),(yto,xto),flux::Real,GaugeChoise::Integer)
    ####This functions gives the phase factor one expects from a homogenenous magnetic field of a given gauge choise.
    ###The funcitons assume that the lattice is a square lattice (or a least that the metric is flat)
    ####Such that each closed square of area 'A' picks up a phase of A*\phi.
    ####Here we assume the the minmal cell to have area A=1. (but that could in principle be changed)
    
    ### If A=(a*y,b*x) then
    ##Phase=a Dx*y0+b*Dy*x0+(1/2)(a+b)Dx*Dy
    if GaugeChoise==0 ###Landau
        ##a=-2*pi*flux, b=0
        ##Phase=a*Dx( y0+(1/2)*Dy ) = .5*a*Dx( y0 + y1 )
        ##  =  -pi*flux*Dx( y0 + y1 )
        #println("(xto+xfrom):",(xto+xfrom))
        #println("(yto-yfrom):",(yto-yfrom))
        Phase=-pi*flux*(yto-yfrom)*(xto+xfrom)
    elseif GaugeChoise==-1 ###Landau
        ##a=0 b=2*pi*flux
        ##Phase=b*Dy*(x0+x1)
        ##  =  pi*flux*Dy( x0 + x1 )
        #println("(xto-xfrom):",(xto-xfrom))
        #println("(yto+yfrom):",(yto+yfrom))
        Phase=pi*flux*(yto+yfrom)*(xto-xfrom)
    elseif GaugeChoise==1 ###Symmetric
        ##a=-pi*phi, b=pi*flux
        ##Phase=b*(-Dx*y0+Dy*x0)=pi*flux*(-Dx*y0+Dy*x0)
        Phase=pi*flux*((yfrom-yto)*xfrom+(xto-xfrom)*yfrom)
    else
        throw(DomainError(GaugeChoise,"Unknown Gauge choise"))
    end
    return -thopp*exp(im*Phase)
    
end


function GetNonZeroElements(Lattice,Offset=(0,0))
    ####Create a lsit with the non-zero elemets of the lattice
    (Ox,Oy)=Offset
    (Nx,Ny)=size(Lattice)
    NCount=sum(Lattice)
    #println("(Nx,Ny)=($Nx,$Ny)")
    #println("NCount=$NCount")
    IndVecMap=zeros(Int32,2,NCount) ###Numbers here need to be larger than the lattice size
    indx=1
    for ix in 1:Nx
        for iy in 1:Ny
            #println("($ix,$iy)="*string(Lattice[ix,iy]))
            if Lattice[ix,iy]
                IndVecMap[1,indx]=ix-1-Ox
                IndVecMap[2,indx]=iy-1-Oy
                indx+=1
            end
        end
    end
    return IndVecMap,NCount
end

function GetNonZeroElements3D(Lattice,Offset=(0,0,0))
    ####Create a lsit with the non-zero elemets of the lattice
    (Ox,Oy,Oz)=Offset
    (Nx,Ny,Nz)=size(Lattice)
    NCount=sum(Lattice)
    #println("(Nx,Ny)=($Nx,$Ny)")
    #println("NCount=$NCount")
    IndVecMap=zeros(Int32,3,NCount) ###Numbers here need to be larger than the lattice size
    indx=1
    for ix in 1:Nx
        for iy in 1:Ny
            for iz in 1:Nz
                #println("($ix,$iy)="*string(Lattice[ix,iy]))
                if Lattice[ix,iy,iz]
                    IndVecMap[1,indx]=ix-1-Ox
                    IndVecMap[2,indx]=iy-1-Oy
                    IndVecMap[3,indx]=iz-1-Oz
                    indx+=1
                end
            end
        end
    end
    return IndVecMap,NCount
end


function GetNonZeroLineElements(Line,col)
    ####Create a lsit with the non-zero elemets of the lattice
    Nx=length(Line)
    NCount=sum(Line)
    #println("(Nx,Ny)=($Nx,$Ny)")
    #println("NCount=$NCount")
    IndVecMap=zeros(Int32,2,NCount) ###Numbers here need to be larger than the lattice size
    indx=1
    for ix in 1:Nx
        if Line[ix]
            IndVecMap[1,indx]=ix-1
            IndVecMap[2,indx]=col
            indx+=1
        end
    end
    return IndVecMap,NCount
end


function do_IsOnLattice(Target,IVM,NMin::Integer,NMax::Integer,
                        ElMin,ElMax,IVMis1D)
    #println(".-.-.-.-.-.-.-.-.-.-.-.")
    #println("Min Element at $NMin is $ElMin")
    #println("Max Element at $NMax is $ElMax")
    if Equal(ElMin,Target)
        #println("Elements is the smallest in list")
        return NMin
    elseif Equal(ElMax,Target)
        #println("Elements is largest in list")
        return NMax
    elseif LargerThan(ElMin,Target)
        #println("Elements is smaller than smallest in list, so cannot exist")
        return nothing
    elseif LargerThan(Target,ElMax)
        #println("Elements is larger than largest in list, so cannot exist")
        return nothing
    else ### The element if somewhere in between
        ####Choose a new element in the middle
        if NMax==(NMin+1) ###There is no lement in between the two checked
            #println("No elements in between min and max, no element does not exist")
            return nothing
        end
        #println("Element is somewhere in middle of list (of it exists)")
        return Choose_Next_IsOnLattice_Step(NMax,NMin,Target,
                                            ElMin,ElMax,IVM,IVMis1D)
    end
end

function Choose_Next_IsOnLattice_Step(NMax,NMin,Target,ElMin,ElMax,IVM,IVMis1D)
    NMid=div(NMax+NMin,2) ###Integer divison to get the middle
    if IVMis1D
        ElMid=IVM[NMid]
    else
        ElMid=Tuple(IVM[:,NMid])
    end
    #println("Mid Element at $NMid is $ElMid")
    if Equal(ElMid,Target)
            #println("Elements is the choosen one")
        return NMid
    elseif LargerThan(ElMid,Target)
        #println("Elements is smaller than the chosen one")
        return do_IsOnLattice(Target,IVM,NMin,NMid,ElMin,ElMid,IVMis1D)
    else 
        #println("Elements is larger than the chosen one")
        return do_IsOnLattice(Target,IVM,NMid,NMax,ElMid,ElMax,IVMis1D)
    end
end

function IsOnLattice(Target,IVM;Strict=false)
    ###This functions returns the index of an element (row,col) in IVM.
    #### If the element IVM does not exist, then it returns zero.
    ### it is assumed that IVM is sorted on first row and then column
    IVMis1D=(length(size(IVM))==1)
    if IVMis1D
        LenIVM=length(IVM)
    else
        (_,LenIVM)=size(IVM)
    end
    #println("..................................")
    #println("Searching for ($row,$col) among $LenIVM elements")
    if IVMis1D
        ElMin=IVM[1]
        ElMax=IVM[LenIVM]
    else
        ElMin=Tuple(IVM[:,1])
        ElMax=Tuple(IVM[:,LenIVM])
    end
    Indx=do_IsOnLattice(Target,IVM,1,LenIVM,ElMin,ElMax,IVMis1D)
    #println("the result found was Indx")
    #display(Indx)
    if Strict && Indx==nothing
        throw(DomainError("The sought element (row,col)=($row,$col) does not exist"))
    end
    return Indx
end

function LargerThan(T1,T2)
    #println("CheckLargerThan")
    if length(T1)!=length(T2)
        throw(DomainError((T1,T2),"Lists of Unequal Length"))
    end
    for index = 1:length(T1)
        if T1[index]>T2[index]
            return true
        elseif T1[index]<T2[index]
            return false
        end
        ###If the elements are the same check the next number
    end
    ###If all the elements are the same they are eual - return false
    return false
end
        
function LargerEqual(T1,T2)
    #println("CheckLargerEqualThan")
    if length(T1)!=length(T2)
        throw(DomainError((T1,T2),"Lists of Unequal Length"))
    end
    for index = 1:length(T1)
        if T1[index]>T2[index]
            return true
        elseif T1[index]<T2[index]
            return false
        end
        ###If the elements are the same check the next number
    end
    ###If all the elements are the same they are eual - return true
    return true
end

function Equal(T1,T2)
    #println("CheckEqual")
    if length(T1)!=length(T2)
        throw(DomainError((T1,T2),"Lists of Unequal Length"))
    end
    ###Check Equal
    for index = 1:length(T1)
        if T1[index]!=T2[index]
            ##Of the index differs, return false
            return false
        end
    end
    return true
end


function SaveAsciiFile(FullFileName,ArrayToSave)
    SaveDir=dirname(FullFileName)
    if !ispath(SaveDir)
        println("Make the path: $SaveDir")
        mkpath(SaveDir)
    end
    println("Save file: ",FullFileName*".dat")
    if length(ArrayToSave)>2 ###Bigger than 2D
        ###Flatten then seconday indexes
        ArrayToSave2=reshape(ArrayToSave,size(ArrayToSave)[1],:)
        writedlm(FullFileName*".dat",ArrayToSave2)
    else
        writedlm(FullFileName*".dat",ArrayToSave)
    end
end


function get_box_size(IndVecMap)
    ExtremeLeft=minimum(IndVecMap[2,:])
    ExtremeRight=maximum(IndVecMap[2,:])

    ExtremeDown=minimum(IndVecMap[1,:])
    ExtremeUp=maximum(IndVecMap[1,:])

    return (ExtremeRight-ExtremeLeft+1,ExtremeUp-ExtremeDown+1)
    
end

function GetConnectivity(SideIndexes,Params;ExplicitHam=nothing)
    ##Switch between two alogrithms depending of whether a parameter
    ## or a hamiltonian is given
    if Params!=nothing && ExplicitHam!=nothing
        throw(DomainError("You cannot have both Paramets and Hamiltonian. Which one do i use?"))
    elseif Params!=nothing
        Connectivity=GetConnectivitySlize(SideIndexes,Params)
    elseif ExplicitHam!=nothing
        Connectivity=GetConnectivityHamiltonian(SideIndexes,ExplicitHam)
    end
    return Connectivity
end



function GetConnectivitySlize(SideIndexes,Params)
    ###Loop over the elements in the site index lists and make a makrk if tow elemts are next to each other
    NoElems=length(SideIndexes)
    Connectivity=fill(false,NoElems,NoElems)
    IVM=Params["IVM"]
    ParamsTuple=Params["ParamsTuple"](Params)
    PossibleNiegbors=Params["GetPossibleNeighbours"]
    for J1 in 1:NoElems
        FromPoint=Tuple(IVM[:,SideIndexes[J1]])
        for J2 in 1:NoElems
            ToPoint=Tuple(IVM[:,SideIndexes[J2]])
            if FromPoint == ToPoint
                Connectivity[J1,J1]=false
            else
                NeigbourList=PossibleNiegbors(FromPoint,ParamsTuple)
                for Neigbours in NeigbourList
                    if ToPoint == Neigbours
                        Connectivity[J1,J2] = true
                            break ###Break out of the for loop
                    end
                end
            end
        end
    end
    return Connectivity
end


function GetConnectivityHamiltonian(SideIndexes,ExplicitHam)
    ###Loop over the elements in the site index lists and make a makrk if tow elemts are next to each other
    NoElems=length(SideIndexes)
    Connectivity=fill(false,NoElems,NoElems)
    for J1 in 1:NoElems
        FromPoint=SideIndexes[J1]
        for J2 in 1:NoElems
            ToPoint=SideIndexes[J2]
            if FromPoint == ToPoint
                Connectivity[J1,J1]=false
            else
                ####Look at the explicit hopping element,
                ###If it is present we consider there to be a conneciton
                HamElem=ExplicitHam[FromPoint,ToPoint]
                if abs(HamElem) != 0.0 ###
                    Connectivity[J1,J2] = true
                end
            end
        end
    end
    return Connectivity
end


function MakeSlizeHam(ParamsTuple::Tuple,HamElemGen,IVM,HamSlizeElems,Energy)
    NoElems=length(HamSlizeElems)
    SlizeHam=fill(0.0+im*0.0,NoElems,NoElems)
    for J1 in 1:NoElems
        FromPoint=Tuple(IVM[:,HamSlizeElems[J1]])
        for J2 in J1:NoElems
            ToPoint=Tuple(IVM[:,HamSlizeElems[J2]])
            #println("Extract Hamiltonian element $J1,$J2")
            #println("From $FromPoint to ,$ToPoint")
            HamElem = HamElemGen(ParamsTuple,FromPoint,ToPoint)
            #println("HamElem: $HamElem")
            if J1 == J2
                HamElem += Energy
                SlizeHam[J1,J1]=HamElem
            else
                SlizeHam[J1,J2]=HamElem
                SlizeHam[J2,J1]=HamElem'
            end
        end
    end
    return SlizeHam
end

function MakeHoppingHam(ParamsTuple::Tuple,HamElemGen,IVM,HamSlize1,HamSlize2,GetNeigbours)
    NoElems1=length(HamSlize1)
    NoElems2=length(HamSlize2)
    SlizeHam=fill(0.0+im*0.0,NoElems1,NoElems2)
    IVM1=IVM[:,HamSlize1]
    IVM2=IVM[:,HamSlize2]
    #println("IVM1:",IVM1)
    #println("IVM2:",IVM2)
    for J1 in 1:NoElems1
        FromPoint=Tuple(IVM1[:,J1])
        ###Look up the neigbours
        TheNeighbours=GetNeigbours(FromPoint,ParamsTuple)
        #println("TheNeighbours:",TheNeighbours)
        for ToPoint in TheNeighbours
            J2=IsOnLattice(ToPoint,IVM2)
            #println("ToPoint:",ToPoint)
            if J2!=nothing
                #println("J2:",J2)
                SlizeHam[J1,J2]=HamElemGen(ParamsTuple,FromPoint,ToPoint)
                #println("SlizeHam[$J1,$J2]:",SlizeHam[J1,J2])
            end
        end
    end
    #println("SlizeHam:",SlizeHam)
    return SlizeHam
end


function get_col_indx(IndVecMap,Rows,col::Integer)
    colindx=fill(0,Rows)
    for row in 0:(Rows-1)
        colindx[row+1]=IsOnLattice((row,col),IndVecMap)
    end
    return colindx
end

function get_col_indx(IndVecMap,col::Integer)
    LocalElems= sort(IndVecMap[1,IndVecMap[2,:] .== col])
    colindx=fill(0,length(LocalElems))
    for row in 1:length(LocalElems)
        colindx[row]=IsOnLattice((LocalElems[row],col),IndVecMap)
    end
    return colindx
end


function sort_IVM(IVM)
    IVM2=copy(IVM)
    sort_IVM!(IVM2)
    return IVM2    
end

function sort_IVM!(IVM)
    ###This funciton sorts the IVM in place using inverted bubble sort
    (Dim,NumElems)=size(IVM)
    for elem1 in 1:(NumElems-1)
        StartElem=Tuple(IVM[:,elem1])
        MinElem=Tuple(IVM[:,elem1])
        MinPos=elem1
        #print("....")
        #println("Start element[$elem1] is:",StartElem)
        #println("Min element[$MinPos] is:",MinElem)
        for elem2 in (elem1+1):NumElems
            Elem2=Tuple(IVM[:,elem2])
            if LargerThan(MinElem,Elem2)
                MinElem=Tuple(IVM[:,elem2])
                MinPos=elem2
                #println("New Min element[$MinPos] is:",MinElem)

            end
        end
        ##We now know which position has the smalest element
        ##We now swap the two elements
        for D in 1:Dim
            IVM[D,elem1]=MinElem[D]
            IVM[D,MinPos]=StartElem[D]
        end
    end
end

function validate_IVM(IVM)
    ###This funciton validates that the IVM is cordered in the correct way!
    if length(size(IVM)) > 2
        throw("IVM with size $(size(IVM)) is not a 1D or 2D array")
    end
    is1D = (length(size(IVM))==1)
    if is1D ###1D is special
        NumElems=size(IVM)[1]
    else
        (_,NumElems)=size(IVM)
    end
    sorted=true
    for elem in 2:NumElems
        if is1D
            Elem1=(IVM[elem-1],)
            Elem2=(IVM[elem],)
        else
            Elem1=Tuple(IVM[:,elem-1])
            Elem2=Tuple(IVM[:,elem])
        end
        if Equal(Elem1,Elem2)
            throw("The IVM (Index Vector Map) contains duplicates!\n"*
                  "The elements "*string(elem-1)*" and "*string(elem)*" are the same: "*
                  string(Elem1)*"!")
        elseif LargerThan(Elem1,Elem2)
            throw("The IVM (Index Vector Map) is not well ordered. "*
                  "The elements need to be sorted on the first dimention followed by the second. "*
                  "The elements "*string(elem-1)*"=>"*string(Elem1)*" and "*
                  string(elem)*"=>"*string(Elem2)*" are backwards!\n"*
                  "You may use 'sort_IVM!(IVM)' or 'sort_IVM(IVM)' to rectify this.")
        end
    end
    return true
end

function BuildHamiltonian(Params::Dict;Plot=false,Verbose=false,PlotHamSize=1000,PrintHamSize=10)
    IVM=Params["IVM"]
    validate_IVM(IVM) ###Check the IVM is valid
    IVMis1D=(length(size(IVM))==1) ###Check if the IVM is 1D
    if IVMis1D
        HamDim=length(IVM)
    else
        HamDim=size(IVM)[2]
    end
    ###Extract the parametrs tuple
    ParamsTuple=Params["ParamsTuple"](Params)
    ####Exctract the function that creates the hamiltonian elements
    HamElemGen=Params["GenHamElems"]
    ###Extract the funcitons to find neigbours
    GetNeigbours=Params["GetPossibleNeighbours"]
    if Verbose
        println("Creating empty Hamiltoinian of dimension D=$HamDim.")
    end
    StartTime=now()
    Hamiltonian = fill(0.0+im*0.0,HamDim,HamDim)
    if Verbose 
        EndTime=now()
        DiffTime=PrettyTime(EndTime-StartTime)
        println("It took $DiffTime to create the empty hamiltonian the Hamiltonian")
    end

    ###Empty hamiltonian
    ###Populate with hopping elements

    if Verbose
        println("Building hamiltonian:")
    end
    StartTime=now()
    
    TIMESTRUCT=TimingInit()
    for J1 in 1:HamDim
        if IVMis1D
            FromPoint=IVM[J1]
        else
            FromPoint=Tuple(IVM[:,J1])
        end
        if Verbose
            println("Element $J1 at point $FromPoint")
        end

        HamElem = HamElemGen(ParamsTuple,FromPoint,FromPoint)
        ###Add the diagonal hamiltonian element
        Hamiltonian[J1,J1]=HamElem
        ###Look up the neigbours
        TheNeighbours=GetNeigbours(FromPoint,ParamsTuple)
        if Verbose
            println("TheNeighbours:",TheNeighbours)
        end
        for ToPoint in TheNeighbours
            J2=IsOnLattice(ToPoint,IVM)
            if Verbose
                print("Possible target $ToPoint:")
                if J2 == nothing
                    println("is not present.")
                else
                    println("is Element no $J2.")
                end
            end
            #println("ToPoint:",ToPoint)
            if J2!=nothing && J2 > J1 ###only occupy elemts in the "Upper triangle"
                HamElement=HamElemGen(ParamsTuple,FromPoint,ToPoint)
                if Verbose
                    println("HamElement is $HamElement")
                end
                #println("J2:",J2)
                Hamiltonian[J1,J2]=HamElement
                ###Add the complex conjugate directly in case there is stochastic elements present like disorder
                Hamiltonian[J2,J1]=conj(Hamiltonian[J1,J2])
                #println("Hamiltonian[$J1,$J2]:",Hamiltonian[J1,J2])
            end
        end
        TIMESTRUCT=TimingProgress(TIMESTRUCT,J1,HamDim;Message="j=$J1 => posisiton $FromPoint")
    end

    if Verbose 
        EndTime=now()
        DiffTime=PrettyTime(EndTime-StartTime)
        println("It took $DiffTime to complete the Hamiltonian")
    end
    
    #if false #### disable thats below
    if HamDim < PrintHamSize && Plot
        figure()
        println("The elemets of the hamiltonian:")
        display(Hamiltonian)
        println()
    end
    
    if HamDim < PlotHamSize  && Plot
        figure()
        imshow(abs.(Hamiltonian))
        title("The elements of the hamiltonian")
        colorbar()
    end
    
    if Plot && HamDim < PlotHamSize
        figure()
        if haskey(Params, "IVMtoIPM")
            PlotHamiltonian(Hamiltonian,Params["IVMtoIPM"](IVM,ParamsTuple))
        else
            PlotHamiltonian(Hamiltonian,IVM)
        end
    end
        
    return Hamiltonian
end
    

function SolveHamiltonian(Params::Dict;Plot=false,SaveHDF5=false,Verbose=false,
                          SaveDir=nothing,SaveASCII=false,PlotHamSize=500,Ham=nothing,PlotSolutionSize=1500)

    
    println("Starting run at: ",now())
    #println("W=$W for Solve")
    if Ham==nothing
        Ham=BuildHamiltonian(Params,Verbose=Verbose,Plot=Plot,PlotHamSize=PlotHamSize)
    end
    IndToMat=Params["IVM"]
    
    HamSize=size(IndToMat)[2]
    if !ishermitian(Ham)
        throw(DomainError("Hamiltonian is not hermittian!"))
    end
    
    println("Solve the N=$HamSize dim eigensystem")
    StartTime=now()
    EigenSystem = eigen(Ham)
    EndTime=now()
    DiffTime=PrettyTime(EndTime-StartTime)
    println("It took $DiffTime to fully diagonalize")
        
    ### The eigensystem satisfies the equation A*V = l*V.
    ###A*E.vectors[:,k]-E.values[1]*E.vectors[:,k]

    if SaveHDF5!=false || SaveASCII!=false  ##There might be a memory leak here.... so use carefully...
        SaveHamiltonian(Params,EigenSystem,IndToMat,
                        SaveHDF5=SaveHDF5,SaveDir=SaveDir,
                        SaveASCII=SaveASCII)
    end

    if Plot
        HamSize < PlotSolutionSize && Plot
        figure()
        imshow(abs.(EigenSystem.vectors))
        colorbar()

        figure()
        plot(EigenSystem.values,"p")
        title("Eigenvalues")

        figure()
        PyPlot.plt["hist"](EigenSystem.values,51)
        title("Density of States")

        MakeMovie(EigenSystem,IndToMat)
        
        println("Press enter to close all windows!"); readline(stdin)
        close("all")

    end
    return EigenSystem
end

using HDF5
function SaveHamiltonian(Params,EigenSystem,IndToMat;
                         SaveASCII=true,SaveHDF5=false,SaveDir=nothing)
    ParamsTuple=Params["ParamsTuple"](Params)
    ParamsNames=Params["ParamsNames"](Params)

    if SaveHDF5==true ###Generate name
        SaveHDF5="Data_"*Params["IDSTRING"]*".hdf5"
    end
    if SaveASCII==true ###Generate name
        SaveASCII="Energy_"*Params["IDSTRING"]
    end
    if SaveDir!=nothing
        if SaveHDF5!=false
            SaveHDF5=joinpath(SaveDir,SaveHDF5)
        end
        if SaveASCII!=false
            SaveASCII=joinpath(SaveDir,SaveASCII)
        end
    end
    if SaveHDF5!=false
        println("Write the Eigenvectors and eigenvalues to file: $SaveHDF5")
        println("Create dirname: ",dirname(SaveHDF5))
        mkpath(dirname(SaveHDF5))
        fid=h5open(SaveHDF5,"w")
        println("Write eigenvalues")
        fid["eigvals"]=EigenSystem.values
        println("Write real part of eigenvectors")
        fid["Re_eigvec"]=real(EigenSystem.vectors)
        println("Write imag part of eigenvectors")
        fid["Im_eigvec"]=imag(EigenSystem.vectors)
        println("Write the index-to-lattice map")
        fid["IndToMat"]=IndToMat
        for indx in 1:length(ParamsTuple)
            attrs(fid["/"])[ParamsNames[indx]]=ParamsTuple[indx]
        end
        close(fid)
    end
    if SaveASCII!=false
        println("Write the Eigenvalues to file: $(SaveASCII).dat")
        println("Create dirname: ",dirname(SaveASCII))
        mkpath(dirname(SaveASCII))
        SaveAsciiFile(SaveASCII,EigenSystem.values)
    end
end



function ensure_the_same(A,B;atol=sqrt(eps()),Alab="A",Blab="B")
    if !isapprox(A,B,atol=atol)
        println("$Alab:")
        display(A)
        println("$Blab:")
        display(B)
        println("$Alab-$Blab:")
        display(A-B)
        throw(DomainError("\n$Alab:\n$A\n\n$Blab:\n$B\n$Alab and $Blab are not the same!"))
    end
    true
end


function DisplayChainHamiltinainInfo(ChainHamiltonian,Terminals)
    for Lead in 1:Terminals
        println(".........")
        println("Lead=$Lead Slize Ham")
        display(ChainHamiltonian[1][Lead])
        println("Lead=$Lead Slize Hopping")
        display(ChainHamiltonian[2][Lead])
        println("Lead=$Lead Slize to Scattering Area")
        display(ChainHamiltonian[3][Lead])
    end
end


function GetHamiltonianSlizes(colFrom,colTo,Params)
    IndVecMap=Params["IVM"]
    ParamsTuple=Params["ParamsTuple"](Params)
    ParamsNames=Params["ParamsNames"](Params)
    #println("ParamsNames: ",ParamsNames)
    #println("ParamsTuple: ",ParamsTuple)
    colftoindx=get_col_indx(IndVecMap,colTo)
    colfromindx=get_col_indx(IndVecMap,colFrom)
    HamGen=Params["GenHamElems"]
    HamSlize=fill(0.0+im*0.0,length(colfromindx),length(colftoindx))
    #println("Size of HamGen: ",size(HamSlize))
    for indxFrom in 1:length(colfromindx)
        for indxTo in 1:length(colftoindx)
            nFrom=colfromindx[indxFrom]
            nTo=colftoindx[indxTo]
            #println("indxFrom:$indxFrom -> indxTo:$indxTo")
            #println("nFrom:$nFrom -> nTo:$nTo")
            #println("nFrom:",IndVecMap[:,nFrom]," -> nTo:",IndVecMap[:,nTo])
            HamSlize[indxFrom,indxTo]=HamGen(ParamsTuple,IndVecMap[:,nFrom],IndVecMap[:,nTo])
        end
    end
    return HamSlize
end

TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close Misc.jl")
