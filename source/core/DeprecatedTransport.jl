if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open DeprecatedTransport.jl")
TabLevel=TabLevel*"    "

function do_trivial_chain_transport(Width::Integer,EnergyVals;eta=1e-10)
    Transmission=zeros(Real,length(EnergyVals))
    NumEnvals=length(EnergyVals)
    TIMESTRUCT=TimingInit()
    for EnIndx in 1:NumEnvals
        E = EnergyVals[EnIndx] + im*eta
        E0 = EnergyVals[EnIndx]+ im*0.0
        ###We compute the semi infinate leads greens function
        GsiLeft=ExactSemiInfiniteChain(Width,E0)
        GsiRight=GsiLeft .* 1 ###These are mirror images (FIMXE is there a transpose here?)
        ###We now also compute the chain hopping matrices
        V_LeftCentral = ChainHopping(Width)
        V_RightCentral = ChainHopping(Width)
        ###From this we compute also the self energy and broadening funciton
        SelfEnLeft = V_LeftCentral * GsiLeft * V_LeftCentral'
        SelfEnRight = V_RightCentral * GsiRight * V_RightCentral'
        ###The broadening function is
        GammaLeft=im*(SelfEnLeft - SelfEnLeft')
        GammaRight=im*(SelfEnRight - SelfEnRight')
        
        ### Now we glue the semi infinite leads to each other using dysons equations
        ###   G = G0 + GVG0
        ### The green functions device is now
        ###   ( GLl     0  )
        ###   (  0     GRl )
        ####  The intercation matrix is then
        ###   ( 0     A  )
        ###   ( B     0  )
        ##where  A,B are the hopping matrices
        ####We thus have

        FULLWIDTH=2*Width

        G0 = ones(Complex,FULLWIDTH,FULLWIDTH) .* 0.0
        G0[1:Width,1:Width]=GsiLeft
        G0[Width .+ (1:Width),Width .+ (1:Width)]=GsiRight
        
        V0 = ones(Complex,FULLWIDTH,FULLWIDTH)  .* 0.0
        V0[0*Width .+ (1:Width),1*Width .+ (1:Width) ]=V_LeftCentral
        V0[1*Width .+ (1:Width),0*Width .+ (1:Width) ]=V_LeftCentral'

        if false
            figure()
            imshow( ((abs.(G0) .> 0 ) .*1) .+ ((abs.(V0) .> 0 ) .*2),
                    extent=[-.5,FULLWIDTH-.5,-.5,FULLWIDTH-.5])
            AX=gca()
            # Major ticks
            AX["set_xticks"](0:1:(FULLWIDTH-1));
            AX["set_yticks"](0:1:(FULLWIDTH-1));
            
            # Minor ticks
            AX["set_xticks"](-.5:1:FULLWIDTH, minor=true);
            AX["set_yticks"](-.5:1:FULLWIDTH, minor=true);
            
            # Gridlines based on minor ticks
            AX["grid"](which="minor", color="w", linestyle="-", linewidth=2)

            title("V_0+G_0")
        end
        
        one = diagm(0=>ones(Complex,FULLWIDTH))
       
        #### Now we use matrix inversion to obtain
        ##G = G0*(1-V*G0)^-1
        DressedGreensFun=G0 * inv(one - V0 * G0)
        #println("DressedGreensFun")
        #display(abs.(DressedGreensFun))
        
        ####The tranport coeffs is now
        ###T = Tr ( G *GammaL * G^\dagger * GammaR)
        GLeftToRight=DressedGreensFun[(1:Width),Width .+ (1:Width)]
        GRightToLeft=GLeftToRight'
        LocTrans = tr( GLeftToRight*GammaLeft*GRightToLeft*GammaRight)
        Transmission[EnIndx]=real(LocTrans)
        TIMESTRUCT=TimingProgress(TIMESTRUCT,EnIndx,NumEnvals;Message="Computing transmission coefficent")
    end
    
    return Transmission
end

function do_MultichanelChainTransport_Ham(SideIndexList,EnergyVals,Ham;eta=sqrt(eps()),eta_leads=eta,
                                          Verbose=false,Plot=false,ChainHamiltonian=nothing)
    NumLeads=length(SideIndexList)
    HamSize=size(Ham)[1]
    WidthLists=[length(SI) for SI in SideIndexList]
    #println("WidthLists:")
    #display(WidthLists)
    #println("SideIndexList:\n",SideIndexList)

    ###Loop over the sites and create a map conncting the particular leads to the site indexes
    SideIndexDict=Dict{Integer,Array{Int64,1}}()##We use integers to map to arrays
    for sideno in 1:NumLeads
        SideIndexDict[sideno]=SideIndexList[sideno]
    end
    Verbose && println("SideIndexDict:\n",SideIndexDict)    
    

    ####The total size of the system will be
    FULLWIDTH=sum(WidthLists)+HamSize


    ####First we compute how the leads attach to the slizes
    if ChainHamiltonian==nothing
        ChainHamfromFullHam=
            ConstructChaintoFullHamiltonian(SideIndexList,Ham)
    else
        ChainHamfromFullHam=
            MatchChainHamToDict(SideIndexList,ChainHamiltonian)
    end
    
    Transmission=fill(0.0,length(EnergyVals),NumLeads,NumLeads)
    NumEnvals=length(EnergyVals)
    TIMESTRUCT=TimingInit()
    for EnIndx in 1:NumEnvals
        
        E = EnergyVals[EnIndx] + im*eta
        E0 = EnergyVals[EnIndx]+ im*eta_leads

        DictGsi,DictSelfEn,DictGamma=
            CreateTheAxillaryGreensFunctions(E0,NumLeads,ChainHamfromFullHam)
        
        Verbose && println("DictGsi:\n",DictGsi)
        Verbose && println("DictVLeadCentral:\n",DictVLeadCentral)
        Verbose && println("DictSelfEn:\n",DictSelfEn)
        Verbose && println("DictGamma:\n",DictGamma)


        #### Extract The green funcitons between the leads by construcing a big hamiltonian and inverting it
        GMatrixHam = GreensFunFullHamiltonian(Ham,SideIndexList,E,DictGsi,
                                              ChainHamfromFullHam[3],Plot=false)
        
        TransmissionMatrix=fill(0.0,NumLeads,NumLeads)
        ####The tranport coeffs is now
        ###T = Tr ( G *GammaL * G^\dagger * GammaR)
        for lead1 in 1:NumLeads
            for lead2 in 1:NumLeads
                GL1L2=GMatrixHam[lead1,lead2]
                GL2L1=copy(GL1L2')
                LocTrans = tr(GL1L2*DictGamma[lead2]*GL2L1*DictGamma[lead1])
                TransmissionMatrix[lead1,lead2]=real(LocTrans)
            end
        end
        Transmission[EnIndx,:,:]=TransmissionMatrix
        TIMESTRUCT=TimingProgress(TIMESTRUCT,EnIndx,NumEnvals;Message="Computing transmission coefficent from Hamiltonian")
    end
    
    return Transmission
end



function GreensFunFullHamiltonian(Ham,LeadAttachments,ChemicalPotential,
                                  DictGsiHam,DictVLeadCentral;Plot=false)
    ##WE will here construct the hamiltonian for the full system (including the leads)
    ##The price we pay is that the hamiltonian is larger than otherwise expected,
    ##But the good thing is that this system will not have any poles on real energies
    ## since the leads all have non-hermititan greens functions 
        
    ### For a list of leads labeld 1,...,n we have the lead green function
    ### G_11,...,G_nn, and G_dd the device greens function
    ### We then also have the hopping amplitudes V_1d,...,V_nd.
    ### We invert the greens funcitons of the lads to obtain the hamiltonian forms
    ### H_11,...,H_nn. We alsready have the device hamiltonian H_dd 
    
    ###The Hamiltonians is then 
    ###   ( G_dd    0      0     0)
    ###   (  0     G_11    0     0)
    ###   (  0      0     ....   0 )
    ###   (  0      0      0    G_nn)
    ### With the hopping matrix 
    ###   ( H_dd  V_d1   ....  V_dn)
    ###   ( V_1d  H_11    0     0 )
    ###   ( ...    0     ....    0 )
    ###   ( V_nd   0      0    H_nn)
    
    ### It is understood that the V_dj elements has a lot of zeros
    ### to make the dimensions come out correctly

    ####NB:Using this formalism we (should be able to) treat leads that attach to the same sites,
    ###without any problems.


    HamSize=size(Ham)[1] ##The size of the hamiltonian
    LeadSizes=[length(SI) for SI in LeadAttachments] ###The sizes of the leads
    FULLWIDTH=HamSize + sum(LeadSizes)
    
    NumLeads=length(LeadAttachments)
    ###Here we will save the various greeen funcitons between the various
    GLL = Array{Array{ComplexF64, 2}}(undef,NumLeads,NumLeads)
    
    ###Initiate the full hamiltonian also containing the leads
    H0 = fill(0.0+im*0.0,FULLWIDTH,FULLWIDTH)
    ####First we populate the hamiltoinian for the scattering area
    H0[1:HamSize,1:HamSize]=Ham
    ##Add the chemical potential offset
    for indx in 1:HamSize
        H0[indx,indx]+= ChemicalPotential
    end
    
    OffsetWidth=HamSize  ####WE knwo how big the hamiltonian is, so we can start counting from there...
    for sideno in 1:NumLeads
        #println("H0 ES")
        #display(H0)
        #println("-------------- sideno:",sideno)
        LocalWidth=LeadSizes[sideno]
        WidthRange=OffsetWidth .+ (1:LocalWidth)
        H0[WidthRange,WidthRange]=inv(DictGsiHam[sideno])###Here we add the inverted surface greens function
        H0[WidthRange,LeadAttachments[sideno]]=DictVLeadCentral[sideno]'
        H0[LeadAttachments[sideno],WidthRange]=DictVLeadCentral[sideno]
        
        
        OffsetWidth+=LocalWidth ##Update the offset width
    end
    
    if Plot
        figure()
        imshow( ((abs.(H0) .> 0 )),
                extent=[-.5,FULLWIDTH-.5,-.5,FULLWIDTH-.5])
        AX=gca()
        # Major ticks
        AX["set_xticks"](0:1:(FULLWIDTH-1));
        AX["set_yticks"](0:1:(FULLWIDTH-1));
        
        # Minor ticks
        AX["set_xticks"](-.5:1:FULLWIDTH, minor=true);
        AX["set_yticks"](-.5:1:FULLWIDTH, minor=true);
        
        # Gridlines based on minor ticks
        #AX["grid"](which="minor", color="w", linestyle="-", linewidth=2)
        
        title("V_0+H_0")
    end
    #### Now we use matrix inversion to obtain the full greens function
    DressedGreensFun=inv(H0)
    
    OffsetWidth1=HamSize
    for lead1 in 1:NumLeads
        OffsetWidth2=HamSize
        LocalWidth1=LeadSizes[lead1]
        WidthRange1=OffsetWidth1 .+ (1:LocalWidth1)
        for lead2 in 1:NumLeads
            LocalWidth2=LeadSizes[lead2]
            WidthRange2=OffsetWidth2 .+ (1:LocalWidth2)
            ###Extract the parts of the full green function corresponding to greensfunctions between various parts
            GLL[lead1,lead2]=DressedGreensFun[WidthRange1,WidthRange2]
            OffsetWidth2+=LocalWidth2 ##Update the offset width
        end
        OffsetWidth1+=LocalWidth1 ##Update the offset width
    end
    return GLL
end

    


function do_MultichanelChainTransport_ES(SideIndexList,EnergyVals,EigenSystem;eta=1e-10)

    NumLeads=length(SideIndexList)
    WidthLists=[length(SI) for SI in SideIndexList]
    #println("WidthLists:")
    #display(WidthLists)
    #println("SideIndexList:\n",SideIndexList)
    MergedSideIndexes = unique_list_elems(SideIndexList)
    #println("BorderIndexes:",MergedSideIndexes)    


    ###Loop over the sites and create a map conncting the particular leads to the site indexes
    BorderIndexDict=Dict{Integer,Array{Int64,1}}()##We use integers to map to arrays
    for sideno in 1:NumLeads
        BorderIndex=fill(0,WidthLists[sideno])
        SideIndex=SideIndexList[sideno]
        for Lindx in 1:WidthLists[sideno]
            #println("Lindx=$Lindx side indx =",SideIndex[Lindx])
            BorderIndex[Lindx]=findfirst(MergedSideIndexes.==SideIndex[Lindx])
            #println("Lindx=$Lindx indx pos=",BorderIndex[Lindx])
        end
        BorderIndexDict[sideno]=BorderIndex
    end
    #println("BorderIndexDict:\n",BorderIndexDict)    

    BordLen=length(MergedSideIndexes)
    
    Transmission=fill(0.0,length(EnergyVals),NumLeads,NumLeads)

    ####The total size of the system will be
    FULLWIDTH=sum(WidthLists)+BordLen


    
    NumEnvals=length(EnergyVals)
    TIMESTRUCT=TimingInit()
    for EnIndx in 1:NumEnvals
        
        E = EnergyVals[EnIndx] + im*eta
        E0 = EnergyVals[EnIndx]+ im*0.0
        ###then we compute the semi infinate leads greens function
        DictGsi=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
        DictVLeadCentral=Dict{Integer,Array{Float64,2}}()##We use integers to map to arrays
        DictSelfEn=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
        DictGamma=Dict{Integer,Array{Complex{Float64},2}}()##We use integers to map to arrays
        for sideno in 1:NumLeads
            DictGsi[sideno]=ExactSemiInfiniteChain(WidthLists[sideno],E0)
            DictVLeadCentral[sideno]=ChainHopping(WidthLists[sideno])
            DictSelfEn[sideno]=DictVLeadCentral[sideno] * DictGsi[sideno] * DictVLeadCentral[sideno]'
            ###FIXME, this is presumably always real....
            DictGamma[sideno]=im*(DictSelfEn[sideno] - DictSelfEn[sideno]') 
        end
        #println("DictGsi:\n",DictGsi)
        #println("DictVLeadCentral:\n",DictVLeadCentral)
        #println("DictSelfEn:\n",DictSelfEn)
        #println("DictGamma:\n",DictGamma)

        GreensFunDevice = GreensFunctionFixedEnergy(EigenSystem,E;
                                                    FromList=MergedSideIndexes,
                                                    ToList=MergedSideIndexes)

        #println("GreensFunDevice ES")
        #display(GreensFunDevice)
        #### For a list of leads labeld 1,...,n we have the lead green funciton
        ### G_11,...,G_nn, and G_dd the device greens function
        ### We then also have the hopping amplitudes V_1d,...,V_nd.

        ###The greens function structure is then
        ###   ( G_dd    0      0     0)
        ###   (  0     G_11    0     0)
        ###   (  0      0     G_22   0 )
        ###   (  0      0      0    G_33)
        ### With the hopping matrix 
        ###   (  0    V_d1   V_d2  V_d3)
        ###   ( V_d1   0      0     0 )
        ###   ( V_d2   0      0     0 )
        ###   ( V_d3   0      0     0  )

        ### It is understood that the V_dj elements has a lot of zeros
        ### to make the dimensions come out
        

        G0 = fill(0.0+im*0.0,FULLWIDTH,FULLWIDTH)
        V0 = fill(0.0,FULLWIDTH,FULLWIDTH)
        G0[1:BordLen,1:BordLen]=GreensFunDevice
        OffsetWidth=BordLen
        for sideno in 1:NumLeads
            #println("G0 ES")
            #display(G0)
            #println("V0 ES")
            #display(V0)
            #println("-------------- sideno:",sideno)
            LocalWidth=WidthLists[sideno]
            WidthRange=OffsetWidth .+ (1:LocalWidth)
            G0[WidthRange,WidthRange]=DictGsi[sideno]

            V0[WidthRange,BorderIndexDict[sideno]]=DictVLeadCentral[sideno]
            V0[BorderIndexDict[sideno],WidthRange']=DictVLeadCentral[sideno]
            
            OffsetWidth+=LocalWidth ##Update the offset width
        end

        
        #println("G0 ES")
        #display(G0)
        #println("V0 ES")
        #display(V0)

        if false
            figure()
            imshow( ((abs.(G0) .> 0 ) .*1) .+ ((abs.(V0) .> 0 ) .*2),
                    extent=[-.5,FULLWIDTH-.5,-.5,FULLWIDTH-.5])
            AX=gca()
            # Major ticks
            AX["set_xticks"](0:1:(FULLWIDTH-1));
            AX["set_yticks"](0:1:(FULLWIDTH-1));
            
            # Minor ticks
            AX["set_xticks"](-.5:1:FULLWIDTH, minor=true);
            AX["set_yticks"](-.5:1:FULLWIDTH, minor=true);
            
            # Gridlines based on minor ticks
            AX["grid"](which="minor", color="w", linestyle="-", linewidth=2)

            title("V_0+G_0")
        end

        ###Construct the identity matrix
        one = diagm(0=>ones(Complex,FULLWIDTH))
        
        #### Now we use matrix inversion to obtain
        ##G = G0*(1-V*G0)^-1
        DressedGreensFun=G0 * inv(one - V0 * G0)
        #println("DressedGreensFun ES") 
        #display(DressedGreensFun)
        TransmissionMatrix=fill(0.0,NumLeads,NumLeads)
              
        
        ####The tranport coeffs is now
        ###T = Tr ( G *GammaL * G^\dagger * GammaR)
        OffsetWidth1=BordLen
        for lead1 in 1:NumLeads
            OffsetWidth2=BordLen
            LocalWidth1=WidthLists[lead1]
            WidthRange1=OffsetWidth1 .+ (1:LocalWidth1)
            for lead2 in 1:NumLeads
                LocalWidth2=WidthLists[lead2]
                WidthRange2=OffsetWidth2 .+ (1:LocalWidth2)
                GL1L2=DressedGreensFun[WidthRange1,WidthRange2]
                GL2L1=GL1L2'
                #println("(G_ES[$lead1][$lead2]):")
                #display(GL1L2)

                LocTrans = tr(GL1L2*DictGamma[lead2]*GL2L1*DictGamma[lead1])
                OffsetWidth2+=LocalWidth2 ##Update the offset width
                TransmissionMatrix[lead1,lead2]=real(LocTrans)
            end
            OffsetWidth1+=LocalWidth1 ##Update the offset width
        end
        Transmission[EnIndx,:,:]=TransmissionMatrix
        TIMESTRUCT=TimingProgress(TIMESTRUCT,EnIndx,NumEnvals;Message="Computing transmission coefficent")
    end
    
    return Transmission
end



function GreensFunRecursiveES(Hamiltonian,Energy,IndVecMap)
    ###The recursive equations that we need are
    
    ##G_NN	= (1-g_NN*V_Nn*g_nn*V_nN)^(-1) g_NN
    ##G_N1	= -(1-g_NN*V_Nn*g_nn*V_nN)^(-1) g_NN*VNn*gn1
    ##G_1N	=-g_1n*V_nN*G_NN

    ###We begin by working put the system size
    (Cols,Rows)=get_box_size(IndVecMap)
    println("(Cols,Rows)=($Cols,$Rows)")

    if length(IndVecMap[1,:])!=Cols*Rows
        throw(ErrorException("Not all lattice points are present"))
    end


    colNindx=get_col_indx(IndVecMap,Rows,0)
    hNN=Hamiltonian[colNindx,colNindx]
    GNN=GreensFuncInvert(hNN,Energy)
    G1N=copy(GNN)
    GN1=copy(GNN)
    G11=copy(GNN)
    
    ###We can now assume that all points exist
    ###Now we extract the First and second collumn

    
    #### Now we set up the recusive step
    
    for N in 2:Cols ###Recusively add rows
        println("Recursion step N=$N ")
        n=N-1
        colnindx=get_col_indx(IndVecMap,Rows,N-2)
        colNindx=get_col_indx(IndVecMap,Rows,N-1)

        ### Update the old indexes
        g11 = G11
        gnn = GNN
        g1n = G1N
        gn1 = GN1
        hNN = Hamiltonian[colNindx,colNindx]
        VnN = Hamiltonian[colnindx,colNindx]
        VNn = Hamiltonian[colNindx,colnindx]
        gNN = GreensFuncInvert(hNN,Energy)

        #println("g_nn=g_$n$n")
        #display(gnn)

        #println("g_1n=g_1$n")
        #display(g1n)
        
        #println("g_n1=g_$n"*"1")
        #display(gn1)

        #println("V_nN=V_$n$N")
        #display(VnN)

        #println("g_NN=g_$N$N")
        #display(gNN)


        ##G_NN	= (1-g_NN*V_Nn*g_nn*V_nN)^(-1) g_NN
        ##G_N1	= -(1-g_NN*V_Nn*g_nn*V_nN)^(-1) g_NN*VNn*gn1
        ##G_1N	=-g_1n*V_nN*G_NN

        SelfEnergyMatrix=inv( diagm(0 => ones(Rows)) - gNN*VNn*gnn*VnN)
        #println("SelfEnergyMatrix")
        #display(SelfEnergyMatrix)
        
        GNN	= SelfEnergyMatrix * gNN
        GN1	= -SelfEnergyMatrix * gNN*VNn*gn1
        G1N	= -g1n*VnN*GNN
        G11	= g11 - g1n*VnN*GN1
    end
        
    
    return  G11,G1N,GN1,GNN
    
end

function HamGen(FromRow,ToRow,Params::Dict)
    ###ExtractRowMap
    Energy=0
    IVM=Params["IVM"]
    ParamsTuple=Params["ParamsTuple"](Params)
    GenHamElems=Params["GenHamElems"]
    GetNeigbours=Params["GetPossibleNeighbours"]
    R1Map=findall(x-> x == FromRow,IVM[2,:])
    R2Map=findall(x-> x == ToRow,IVM[2,:])
    if FromRow==ToRow
        SlizeHam=MakeSlizeHam(ParamsTuple,GenHamElems,IVM,R1Map,Energy)
    else
        SlizeHam=MakeHoppingHam(ParamsTuple,GenHamElems,IVM,R1Map,R2Map,GetNeigbours)
    end
    return SlizeHam
end
    
TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close DeprecatedTransport.jl")
