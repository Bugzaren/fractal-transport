if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open ScanFunctions.jl")
TabLevel=TabLevel*"    "

using ProgressTimer
using DelimitedFiles

include("Greens.jl")
include("ChainAttachments.jl")
include("PlotMisc.jl")
include("SliceLattice.jl")
include("DeprecatedTransport.jl")


function ScanMultiTransport(Params,ScanVals,ScanType,Eval;eta=sqrt(eps()),EvalType="Slize",
                            Terminals=2,LeadWidth=1,Plot=false,Voltages=false,Save=false,
                            SavePerLead=true,ChainHam=nothing)
    ####It is kind of understood here that changing these parameters do not changes the geometry
    ## of the scattering region
    ##Test if ScanType is in the dictionary
    Transmission=fill(0.0,length(ScanVals),length(Eval),Terminals,Terminals)
    NumScanVals=length(ScanVals)

    InitScanVal=0.0
    try ; InitScanVal=Params[ScanType]
    catch error
        if isa(error, KeyError)
            StringKeys=string(keys(Params))
            Text="Unknown key: \""*string(ScanType)*"\". Valid keys are "*StringKeys
            throw(DomainError(Text))
       end
    end

    IndVecMap = Params["IVM"]
    SideIndexes=get_chain_attachment_points(IndVecMap,Terminals,LeadWidth)
    if EvalType == "Slize"
        SlizeData=SlizeTheLattice(Params,SideIndexes)
    end
    TIMESTRUCT=TimingInit()
    for ScanNo in 1:NumScanVals
        Params[ScanType]=ScanVals[ScanNo]
        if EvalType == "ES" || EvalType == "Ham"
            ####Next create the hamiltonian of the middle chain segment and compute the edge greens funcitons
            ChainH=BuildHamiltonian(Params)
            
            if EvalType == "ES"
                ### Solve the chain hamiltonian
                println("Solve the N="*string(size(ChainH)[1])*" dim eigensystem")
                StartTime=now()
                EigenSystem = eigen(ChainH)
                EndTime=now()
                println("EigenSystem")
                display(EigenSystem)
                DiffTime=Dates.canonicalize(Dates.CompoundPeriod(Dates.Millisecond(EndTime-StartTime)))
                println("It took $DiffTime to fully diagonalize")
                ####Call the generic for transport
                LocalTransmission=do_MultichanelChainTransport_ES(SideIndexes,Eval,EigenSystem;eta=eta)
            elseif EvalType == "Ham"
                LocalTransmission=do_MultichanelChainTransport_Ham(SideIndexes,Eval,ChainH;eta=eta)
            end
        elseif EvalType == "Slize"
            LocalTransmission=do_MultichanelChainTransport_Slize(SideIndexes,Eval,Params;
                                                                 eta=eta,SlizeData=SlizeData,
                                                                 ChainHamiltonian=ChainHam)
        else
            throw(DomainError("Unknown Evaluation type:\"$EvalType\""))
        end
        #println("LocalTransmission")
        #display(LocalTransmission)
        Transmission[ScanNo,:,:,:]=LocalTransmission[:,:,:]
        TIMESTRUCT=TimingProgress(TIMESTRUCT,ScanNo,NumScanVals;Message="Scanning over transmission")
    end

    Params[ScanType]=InitScanVal

    if Save
        if !SavePerLead && (length(Eval)==1)
            EStr="_Eval_$(Eval)_$(ScanType)_"*string(ScanVals[1])*"_"*string(length(ScanVals))*"_"*string(ScanVals[end])*"_"
        else
            EStr="_Eval_#$(length(Eval))_$(ScanType)_"*string(ScanVals[1])*"_"*string(length(ScanVals))*"_"*string(ScanVals[end])*"_"
        end
        ID="_"*string(Int(ceil(rand()*10^6)))
        SaveAsciiFile("ScanData/Term_$(Terminals)_Width_$(LeadWidth)_"*Params["IDSHORT"]*
                      "/ScanValues_term_$(Terminals)_Width_$(LeadWidth)"*EStr*Params["IDSTRING"]*ID,ScanVals)
        if !SavePerLead && (length(Eval)==1)
            SaveAsciiFile("ScanData/Term_$(Terminals)_Width_$(LeadWidth)_"*Params["IDSHORT"]*
                          "/Transmission_term_$(Terminals)_Width_$(LeadWidth)"*EStr*Params["IDSTRING"]*ID,Transmission)
        else
            for T1 in 1:Terminals, T2 in 1:Terminals
                SaveAsciiFile("ScanData/Term_$(Terminals)_Width_$(LeadWidth)_"*Params["IDSHORT"]*
                              "/Transmission_term_$(Terminals)_Width_$(LeadWidth)"*
                              "_T1_$(T1)_T2_$(T2)"*EStr*Params["IDSTRING"]*ID,Transmission[:,:,T1,T2])
            end
        end 
    end

    
    if Plot && length(Eval)==1
        PlotTransmissions(Transmission[:,1,:,:],ScanVals,ScanType,Params,
                          Terminals,LeadWidth,Eval=Eval)
    end
    if Voltages
        VList=0
        try
            VList=Transmission_to_Voltages(Transmission,Terminals)
            if Plot && length(Eval)==1
                PlotVoltages(VList[:,1,:],ScanVals,ScanType,Params,Terminals,LeadWidth,Eval=Eval)
            end
            if Save
                SaveAsciiFile("TransmissionData/Term_$(Terminals)_Width_$(LeadWidth)_"*Params["IDSHORT"]*
                              "/Voltage_term_$(Terminals)_Width_$(LeadWidth)"*EStr*Params["IDSTRING"]*ID,VList)
            end
        catch
            VList = 0.0 .* Transmission
        end
        return Transmission,VList
    end
    
    return Transmission
end

function MultiTransport(Params::Dict,EnergyVals;eta=sqrt(eps()),EvalType="Slize",Plot=false,
                        Terminals=2,LeadWidth=1,Eshift=0.000,Voltages=false,Save=false,
                        ChainHam=nothing)
    IndVecMap=Params["IVM"]
    SideIndexes=get_chain_attachment_points(IndVecMap,Terminals,LeadWidth)
    MaxTrans=maximum([length(SI) for SI in SideIndexes])
    println("MaxTrans:",MaxTrans)
    if EvalType == "Slize"
        SlizeData=SlizeTheLattice(Params,SideIndexes)
    end
    println("EnergyVals:",EnergyVals)
    if EvalType == "ES" || EvalType == "Ham"
        ####Next create the hamiltonian of the middle chain segment and compute the edge greens funcitons
        IndVecMap = Params["IVM"]
        ChainH = BuildHamiltonian(Params)
        if EvalType == "ES"
            ### Solve the chain hamiltonian
            println("Solve the N="*string(size(ChainH)[1])*" dim eigensystem")
            StartTime=now()
            EigenSystem = eigen(ChainH)
            EndTime=now()
            println("EigenSystem")
            display(EigenSystem)
            DiffTime=Dates.canonicalize(Dates.CompoundPeriod(Dates.Millisecond(EndTime-StartTime)))
            println("It took $DiffTime to fully diagonalize")
            ####Call the generic for transport
            Transmission=do_MultichanelChainTransport_ES(SideIndexes,EnergyVals,EigenSystem;eta=eta)
        elseif EvalType == "Ham"
            Transmission=do_MultichanelChainTransport_Ham(SideIndexes,EnergyVals,ChainH,eta=eta)
        end
        #println("Transmission")
        #display(Transmission)
    elseif EvalType == "Slize"
        Transmission=do_MultichanelChainTransport_Slize(SideIndexes,EnergyVals,Params;
                                                        eta=eta,SlizeData=SlizeData,
                                                        ChainHamiltonian=ChainHam)
    else
        throw(DomainError("Unknown Evaluation type:\"$EvalType\""))
    end


    if Save
        EStr="_Eval_"*string(EnergyVals[1])*"_"*string(length(EnergyVals))*"_"*string(EnergyVals[end])*"_"
        ID="_"*string(Int(ceil(rand()*10^6)))
        SaveAsciiFile("TransmissionData/Term_$(Terminals)_Width_$(LeadWidth)_"*Params["IDSHORT"]*
                      "/Energy_term_$(Terminals)_Width_$(LeadWidth)"*EStr*Params["IDSTRING"]*ID,EnergyVals)
        SaveAsciiFile("TransmissionData/Term_$(Terminals)_Width_$(LeadWidth)_"*Params["IDSHORT"]*
                      "/Transmission_term_$(Terminals)_Width_$(LeadWidth)"*EStr*Params["IDSTRING"]*ID,Transmission)
    end
    
    
    if Plot
        PlotTransmissions(Transmission,EnergyVals,"Energy",Params,Terminals,LeadWidth,Eshift=Eshift,MaxTrans=MaxTrans)
    end
    
    if Voltages
        try VList=Transmission_to_Voltages(Transmission,Terminals)
            if Plot
                PlotVoltages(VList,EnergyVals,"Energy",Params,Terminals,LeadWidth,Eshift=Eshift)
            end
            if Save
                SaveAsciiFile("TransmissionData/Term_$(Terminals)_Width_$(LeadWidth)_"*Params["IDSHORT"]*
                              "/Voltage_term_$(Terminals)_Width_$(LeadWidth)"*EStr*Params["IDSTRING"]*ID,VList)
            end
        catch
            VList=0.0 .* Transmission
        end
        return Transmission,VList
    end
    return Transmission
end




function Transmission_to_Voltages(Transmission,Terminals)
    ###Assume the first index is for the scan variable
    NumVals=size(Transmission)[1]
    
    ###Here we compute the conductancs.  We begin by constructing the V \to I matrix.
    VtoI=-Transmission
    for indx1=1:Terminals
        for indx2=1:Terminals
            VtoI[:,indx1,indx1]+=Transmission[:,indx1,indx2]
        end
    end
    ##Next we remove the lead 2 element, and put it as gound
    VtoIRed=VtoI[:,[1;3:Terminals],[1;3:Terminals]]
    ###Next we invert the whole thing
    ItoVRed=fill(0.0,NumVals,Terminals-1,Terminals-1)
    for enno in 1:NumVals
        ItoVRed[enno,:,:]=inv(VtoIRed[enno,:,:])
    end
    ##Finally we set I_1=I and the rest to zero. This corresponds to letting only looking at the first slize of ItoVRed.
    VList=fill(0.0,NumVals,Terminals)
    VList[:,[1;3:Terminals]]=ItoVRed[:,:,1]
    return VList
end


function ChainTransport(Length=10,Width=10,EnergyVals=-4.1001:0.1:4.001;
                        eta=sqrt(eps()),W=0.0,Phase=0.0,LeftLeadSide="L",RightLeadSide="R",
                        EvalType="Ham",Plot=true)
    Transmission=0.0
    if EvalType == "ES" || EvalType == "Ham"
        ####Next create the hamiltonian of the middle chain segment and compute the edge greens funcitons
        ChainH,IndVecMap = CreateChainHamiltonian(Length,Width,Phase,W=W)
        LeftSideIndx=get_border_border_indexes(IndVecMap,LeftLeadSide)
        RightSideIndx=get_border_border_indexes(IndVecMap,RightLeadSide)

        println("LeftSideIndx:",LeftSideIndx)
        println("RightSideIndx:",RightSideIndx)
        SideIndexes=(LeftSideIndx,RightSideIndx)
        if EvalType == "ES"
            ### Solve the chain hamiltonian
            println("Solve the N="*string(size(ChainH)[1])*" dim eigensystem")
            StartTime=now()
            EigenSystem = eigen(ChainH)
            EndTime=now()
            DiffTime=Dates.canonicalize(Dates.CompoundPeriod(Dates.Millisecond(EndTime-StartTime)))
            println("It took $DiffTime to fully diagonalize")
            ####Call the generic for transport
            Transmission=do_MultichanelChainTransport_ES(SideIndexes,EnergyVals,EigenSystem;eta=eta)
        elseif EvalType == "Ham"
            Transmission=do_MultichanelChainTransport_Ham(SideIndexes,EnergyVals,ChainH,
                                                          Params;eta=eta)
        end
    elseif EvalType == "Dyn"
        Params = getChainParams(Length,Width,Phase,W=W)
        Transmission=do_ChainTransportDyn(Width,EnergyVals,getChainSlices,
                                          getChainSize,Params;eta=eta)
    end       
    if Plot
        figure()
        plot(EnergyVals,EnergyVals.*0,"-k")
        shift=0
        plot(EnergyVals,Transmission[:,1,1],"-",label="1,1")
        plot(EnergyVals,Transmission[:,1,2],"--",label="1,2")
        legend(loc="best")
        #plot(ELinspace./2 .+ 2,Width*acos.(ELinspace ./ 4) ./ (pi),"--k")
        #plot(-ELinspace./2 .- 2,Width*acos.(ELinspace ./ 4) ./ (pi),"--k")
        title("Transmission $Length x $Width scattering area phase=$Phase, noise W=$W")
        xlabel("Energy")
        ylabel("Transmission")
        IDSTRING="l_$Length"*"_w_$Width"*"_ph_$Phase"*"_W_$W"
        SavePyFile("Transmission/Transmission_2_term_"*IDSTRING)
    end
    return Transmission
end


function PlotTransmissions(TransmissionIn,ScanValsIn,ScanType,Params,Terminals,Width;
                           Eshift=0.0,Eval=nothing,MaxTrans=nothing)
    ScanVals=collect(ScanValsIn)
    @eval using PyPlot
    upshift=0
    Transmission=copy(TransmissionIn)
    for indx1=1:Terminals
        Transmission[:,indx1,indx1].=0
    end
    MaxRealTrans=maximum(maximum(Transmission))
    if MaxTrans == nothing
        MaxTrans = MaxRealTrans
    else
        MaxTrans = minimum((MaxRealTrans,MaxTrans))
    end
    MaxVal=Int(floor(MaxTrans))
    Scale=5
    HelpStep=Scale^(max(floor(log(MaxVal/2.0)/log(Scale)),0.0))
    figure()
    plot(ScanVals.*1.0,ScanVals.*0,"-k",label="__nolabel",lw=2)
    for Val in HelpStep:HelpStep:(MaxVal+1)
        plot(ScanVals,ScanVals.*0 .+ Val,"--k",label="__nolabel",lw=2)
        plot(ScanVals,ScanVals.*0 .- Val,"--k",label="__nolabel",lw=2)
    end

    for indx1=1:Terminals
        #plot(EnergyVals,upshift .+ Transmission[:,indx1,indx1],":",label="$indx1,$indx1")
        #upshift+=0.01
        for indx2=(indx1+1):Terminals
            PLOTOBJ=plot(ScanVals,upshift .+ Transmission[:,indx1,indx2],"--",label="$indx1,$indx2",lw=2)
            Color=PLOTOBJ[1]["get_color"]()
            plot(ScanVals,-upshift .- Transmission[:,indx2,indx1],":",label="$indx2,$indx1",color=Color,lw=2)
            upshift+=Eshift
        end
    end

    if Eval==nothing
        title("$(Terminals)-Terminal Transmission width=$Width\n"*Params["title"])
    else
        title("$(Terminals)-Terminal Transmission width=$Width E=$Eval\n"*Params["title"])
    end
    legend(loc="best")
    ylim([-MaxTrans*1.1,MaxTrans*1.1])
    xlabel(ScanType)
    ylabel("Transmission")
    if Eval==nothing
        SavePyFile("TransmissionScan/Transmission_$(ScanType)_$(Terminals)_term_"*Params["IDSTRING"])
    else
        SavePyFile("TransmissionScan/Transmission_$(ScanType)_$(Terminals)_term_Eval_$(Eval)_"*Params["IDSTRING"])
    end
end



function PlotVoltages(VList,ScanVals,ScanType,Params,Terminals,Width;
                      Eshift=0.0,Eval=nothing)
    if Terminals==2
        LabelName=["V_L","V_R"]
    elseif Terminals==4
        LabelName=["V_L","V_R","V_u","V_d"]
    elseif Terminals==6
        LabelName=["V_L","V_R","V_uL","V_uR","V_dL","V_dR"]
    end

    MaxVal=Int(floor(maximum(maximum(abs.(VList)))))
    figure()
    plot(ScanVals,ScanVals.*0,"-k",label="__nolabel",lw=2)
    for Val in -(MaxVal+1):(MaxVal+1)
        plot(ScanVals,ScanVals.*0 .+ Val,"--k",label="__nolabel",lw=2)
    end

    upshift=0.0
    for indx1=1:Terminals
        plot(ScanVals,upshift .+ VList[:,indx1],"--",label=LabelName[indx1],lw=2)
        upshift+=Eshift
    end
    if Terminals==4
        plot(ScanVals,upshift .+ VList[:,3]-VList[:,4],"-",label="V_Hall",lw=2)
        upshift+=Eshift
    elseif Terminals==6
        plot(ScanVals,upshift .+ VList[:,3]-VList[:,6],"-",label="V_Hall",lw=2)
        upshift+=Eshift
        plot(ScanVals,upshift .+ VList[:,3]-VList[:,4],"-",label="V_Trans",lw=2)
        upshift+=Eshift
    end

    
    if Eval==nothing
        title("$(Terminals)-Terminal Voltage width=$Width\n"*Params["title"])
    else
        title("$(Terminals)-Terminal Voltage width=$Width E=$Eval\n"*Params["title"])
    end
    legend(loc="best")
    xlabel(ScanType)
    ylabel("Voltage")
    if Eval==nothing
        SavePyFile("VoltagesScan/Voltages_$(ScanType)_$(Terminals)_term_"*Params["IDSTRING"])
    else
        SavePyFile("VoltagesScan/Voltages_$(ScanType)_$(Terminals)_term_Eval_$(Eval)_"*Params["IDSTRING"])
    end
end



TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close ScanFunctions.jl")
