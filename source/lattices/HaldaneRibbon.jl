if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open HaldaneRibbon.jl")
TabLevel=TabLevel*"    "

##### 
using LinearAlgebra
using PyPlot
function GenerateRibbonHamiltonian(W::Int,B0=0.,B1=1.,B2=0.,B3=0.,k=0. )
    Hamiltonian=fill(im*0.,2W,2W)
    ### hamiltonian index j=2(y-1)+o, o=orbital index
    for y=1:W
        ## on-site
        Hamiltonian[2y-1,2y-1]=B0+2*B1*cos(k)
        Hamiltonian[2y,2y]=-(B0+2*B1*cos(k))
        Hamiltonian[2y-1,2y]=2*B3*sin(k)
        Hamiltonian[2y,2y-1]=2*B3*sin(k)
        ## up (y'=y+1)
        if y!=W
            Hamiltonian[2y-1,2y+1]=B1+2*B2*cos(k)
            Hamiltonian[2y,2y+2]=-(B1+2*B2*cos(k))
            Hamiltonian[2y-1,2y+2]=2*im*B3*(-cos(k)-sin(k))
            Hamiltonian[2y,2y+1]=2*im*B3*(-cos(k)+sin(k))
        end
        # down (y'=y-1)
        if y!=1
           Hamiltonian[2y-1,2y-3]=B1+2*B2*cos(k)
           Hamiltonian[2y,2y-2]=-(B1+2*B2*cos(k))
           Hamiltonian[2y-1,2y-2]=2*im*B3*(cos(k)-sin(k))
           Hamiltonian[2y,2y-3]=2*im*B3*(cos(k)+sin(k))
        end
    end

    return Hamiltonian
end



### find energy eigenvalues
function SolveHamiltonian(Hamiltonian)
    #Eigenvalues=eigvals(Hamiltonian)
#    println("solving Eigensystem")
    Eigensystem=eigen(Hamiltonian)
return Eigensystem
end






### Parameters example for topological phase: (B0,B1,B2,B3)=(-1.5,0.5,1,0.25)

function computeEigenvectors(Width,B0,B1,B2,B3,k)
    
    Eigenvectors=fill(im*0.,2*Width,2*Width)
    
 
    Hamiltonian=GenerateRibbonHamiltonian(Width,B0,B1,B2,B3,k)
     
    Eigenvectors=SolveHamiltonian(Hamiltonian).vectors
    
    return Eigenvectors
    
end


function computeEigenvalues(Width,B0=-2,B1=0.5,B2=1,B3=0.25)
    Steps=100   
    Stepsize=2*pi/Steps
    Momenta=fill(0.,Steps+1)
    Eigenvals=fill(0.,Steps+1,2*Width)
    
   
    for k=0:Steps
        Momenta[k+1]=-pi+k*Stepsize
        Hamiltonian=GenerateRibbonHamiltonian(
                            Width,  #Width
                            B0,B1,B2,B3, # couplings
                            Momenta[k+1], 
                            )
        println("k=$(Momenta[k+1])")
        #println(Hamiltonian)
        for j=1:2*Width    
            Eigenvals[k+1,j]=SolveHamiltonian(Hamiltonian).values[j]
            
        end
    end
     
    return Eigenvals
end

function PlotEigenvals(Eigenvals)
    Steps=size(Eigenvals)[1]
    Width=size(Eigenvals)[2]
    Momenta=fill(0., Steps)
    for k=0:(Steps-1)
        Momenta[k+1]=pi*(-1+2*k/(Steps-1))
    end        
    figure()
    
    for j=1:Width 
        plot(Momenta[:],Eigenvals[:,j],label="Spectrum band $j")
    end
#    legend(loc="lower right")
    
    

 end

function PlotEigenvector(Width,k,j,B0=-2,B1=0.5,B2=1,B3=0.25)         ### k=momentum, j is the jth eigenvalue
    Sites=fill(0.,2*Width)
    for l=1:2*Width
        Sites[l]=l
    end
    Eigenvector=computeEigenvectors(Width,B0,B1,B2,B3,k)
    figure()
    plot(Sites[:],abs.(Eigenvector)[:,j],label="$j th Eigenstate for momentum k=$k")
    legend(loc="auto")    
end

function PlotListOfEigenvectors(Width,k,List=[30 31],B0=-2,B1=0.5,B2=1,B3=0.25)
    Sites=fill(0.,2*Width)
     for l=1:2*Width
         Sites[l]=l
     end    
    Eigenvector=fill(im*0., size(List)[2],2*Width)
    Eigenvector=computeEigenvectors(Width,B0,B1,B2,B3,k)
 
    figure()
    for l=1:size(List)[2]
        plot(Sites[:],abs.(Eigenvector)[:,List[l]], label="$(List[l]) th Eigenstate at momentum k=$k")
    end
    legend(loc="lower right")
end

function TranslateParams(M,B,B2)

    B3=0.25
    B1=0.5*B
    B0=0.25*M-B-B2
    

    return (B0,B1,B2,B3)
end



TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close HaldaneRibbon.jl")
