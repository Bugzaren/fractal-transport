if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open LatticeMisc.jl")
TabLevel=TabLevel*"    "

function is_bond_allowed(row1,col1,row2,col2,BondType::Char)
    DiffRow=abs(row1-row2)
    DiffCol=abs(col1-col2)
    #println("row1,col1,row2,col2: $row1, $col1, $row2, $col2")
    #println("BondType: '$BondType'")
    if !(BondType == 'S' || BondType == 'T' || BondType == 'R' || BondType == 'X')
        throw(DomainError("FIXME: Unknown Lattice type ='$BondType'?"))
    end
    
    ###Carpet and triangle are defined in terms of their lattice points alone
    if (DiffRow > 1) || (DiffCol > 1)
        return false ##bonds to faar away
    elseif ( DiffRow==0 && DiffCol==1 ) || ( DiffRow==1 && DiffCol==0 )
        ##Square lattice
        return true
    elseif ((row1-row2 == 1) && (col1-col2 == 1))  ||
        ((row1-row2 == -1) && (col1-col2 == -1))
        ###reverse triangular
        if BondType == 'R' || BondType == 'X'
            return true
        else
            return false
        end
    elseif ((row1-row2 == 1) && (col1-col2 == -1))  ||
        ((row1-row2 == -1) && (col1-col2 == 1))
        ###Triangular
        if BondType == 'T' || BondType == 'X'
            return true
        else
            return false
        end
    else
        throw(DomainError("FIXME: Unknown execution point"))
    end
end


function GaugeChoiseToNum(Gauge)
    if Gauge=="LandauX"
        GaugeChoise=0
    elseif Gauge=="LandauY"
        GaugeChoise=-1
    elseif Gauge=="Symmetric"
        GaugeChoise=1
    else
        throw(DomainError(Gauge,"Unknown gauge choise: Valid are LandauX, LandauY, Symmetric"))
    end
    return GaugeChoise
end


function get_triangular_IPM(IVM)
    IPM=fill(0.0,size(IVM))
    IPM[1,:]=IVM[1,:] .* sqrt(3.0/4.0)
    IPM[2,:]=IVM[2,:] .+ .5 .* IVM[1,:]
    return IPM
end

function get_anti_triangular_IPM(IVM)
    IPM=fill(0.0,size(IVM))
    IPM[1,:]=IVM[1,:] .* sqrt(3.0/4.0)
    IPM[2,:]=IVM[2,:] .- .5 .* IVM[1,:]
    return IPM
end


function GetLatticeNeighbours((row,col),LatticeType::Char)
    if LatticeType=='T' ### Triangular lattice
        return ((row,col-1), #backward
                (row,col+1),#forward
                (row-1,col),#down
                (row+1,col),#up
                (row+1,col-1),#(back/up)
                (row-1,col+1))#(forw/down)
    elseif LatticeType=='R' ### Anti-Triangular lattice
        return ((row,col-1), #backward
                (row,col+1),#forward
                (row-1,col),#down
                (row+1,col),#up
                (row+1,col+1),#(back/down)
                (row-1,col-1))#(forw/up)
    elseif LatticeType=='X'  ### X- lattice
        return ((row,col-1), #backward
                (row,col+1),#forward
                (row-1,col),#down
                (row+1,col),#up
                (row+1,col-1),#(back/up)
                (row-1,col+1),#(forw/down)
                (row+1,col+1),#(forw/up)
                (row-1,col-1))#(back/down)
    elseif LatticeType=='S'### Square lattice
        return ((row,col-1), #backward
                (row,col+1),#forward
                (row-1,col),#down
                (row+1,col))#up
    else
        throw(DomainError(LatticeType,"Unknown Lattice = '$LatticeType'?"))
    end
end




TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close LatticeMisc.jl")
