if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open WeylLattice.jl")
TabLevel=TabLevel*"    "

include("../core/Misc.jl")

using ProgressTimer
using Dates

function getWeylParams(Length::Integer,Width::Integer,Height::Integer;
                        t::Real=1.0,W::Real=0.0,Plot=false,PBC=false)
    println("Creating the lattice for $Length x $Width x $Height Weyl mettal")
    ####Begin by building the lattice####
    ###We make the lattice twice as big in the one of the directions to accomodate the two spin species. Here we choose the Width
    TheLattice=ones(Bool,Length,Width,Height*2)
    println("Constructing lattice-vector-map for the lattice")
    IndVecMap,Ncount=GetNonZeroElements3D(TheLattice) ###Ellemtns, ordered after X, Y and then Z
    println("Make VIM Map:")
    VecIndMap=VIM_from_IVM(IndVecMap)
    #println("List of Nonzero elements:")
    #display(IndVecMap)
    println("Ncount=$Ncount")

    return Dict("Length"=>Length,"Width"=>Width,"Height"=>Height,"t"=>t,"W"=>W,
                "PBC"=>PBC,
                "ParamsTuple"=>WeylParamsTuple,
                "GenHamElems"=>getWeylElems,
                "GetPossibleNeighbours"=>GetWeylNeighbours,
                "IVM"=>IndVecMap,"VIM"=>VecIndMap,
                "title"=>"Weyl lattice $(Length)x$(Width)x$(Height), noise W=$W",
    "IDSTRING"=>"Weyl_l_$(Length)_w_$(Width)_h_$(Height)_W_$W",
    "IDSHORT"=>"Weyl_l_$(Length)_w_$(Width)_h_$(Height)_W_$W")
end

function GetWeylNeighbours((X,Y,Z),(Length,Width,Height,_,_,PBC))
    'S'### Square lattice
    ####Here the (in the Z direction) the odd are the up and the even are the down sites
    SpinType=mod(Z,2)  ###Parity tells wich is wich
    OtherType=mod(SpinType+1,2)
    ZPos=div(Z,2)
    if PBC
        NList= ((mod(X-1,Length),Y,2*ZPos+OtherType),#left (spinflip)
                (mod(X+1,Length),Y,2*ZPos+OtherType),#right (spinflip)
                (X,mod(Y-1,Width),2*ZPos+OtherType),#backward (spinflip)
                (X,mod(Y+1,Width),2*ZPos+OtherType),#forward (spinflip)
                (X,Y,mod(2*(ZPos-1)+SpinType,Height*2)),#down (same spin)
                (X,Y,mod(2*(ZPos+1)+SpinType,Height*2)))#up (same spin)
    else
        NList= ((X-1,Y,2*ZPos+OtherType),#left (spinflip)
                (X+1,Y,2*ZPos+OtherType),#right (spinflip)
                (X,Y-1,2*ZPos+OtherType),#backward (spinflip)
                (X,Y+1,2*ZPos+OtherType),#forward (spinflip)
                (X,Y,2*(ZPos-1)+SpinType),#down (same spin)
                (X,Y,2*(ZPos+1)+SpinType))#up (same spin)
    end        
    return NList
end


function WeylIPM(IVM)
    IPM=IVM .* 1 ###Copy the IVM
    IPM[3,:]=div.(IPM[3,:],2) ##The third components has a baked in spin degree of freedom
    return IPM
end

function getWeylElems((Length,Width,Height,t,W,PBC),(X1,Y1,Z1),(X2,Y2,Z2);Strict=false)
    SpinType1=mod(Z1,2)  ###Parity tells wich is wich
    SpinType2=mod(Z2,2)  ###Parity tells wich is wich
    Z1Pos=div(Z1,2)
    Z2Pos=div(Z2,2)
    HamElem=0.0im
    if X2==X1 && Y1==Y2 && Z1Pos==Z2Pos && SpinType1==SpinType2
        HamElem=W*2*(rand()-.5)
    else
        if PBC
            ###X hopping
            if (mod(X2,Length)==mod(X1+1,Length) &&
                Y1==Y2 && Z1Pos==Z2Pos && SpinType1!=SpinType2)
                HamElem=im
            elseif (mod(X2,Length)==mod(X1-1,Length) &&
                    Y1==Y2 && Z1Pos==Z2Pos && SpinType1!=SpinType2)
                HamElem=-im
                ###Y hopping
            elseif (X2==X1 && mod(Y2,Width)==mod(Y1+1,Width) &&
                    Z1Pos==Z2Pos && SpinType1!=SpinType2)
                HamElem=1.0*(2*SpinType1-1)###Last term makes the sign spin type dependent
            elseif (X2==X1 && mod(Y2,Width)==mod(Y1-1,Width) &&
                    Z1Pos==Z2Pos && SpinType1!=SpinType2)
                HamElem=-1.0*(2*SpinType1-1)###Last term makes the sign spin type dependent
                ###Z hopping
            elseif (X2==X1 && Y2==Y1 &&
                    mod(Z2Pos,Height)==mod(Z1Pos+1,Height) &&
                    SpinType1==SpinType2)
                HamElem=1.0*(2*SpinType1-1)###Last term makes the sign spin type dependent
            elseif (X2==X1 && Y2==Y1 &&
                    mod(Z2Pos,Height)==mod(Z1Pos-1,Height) &&
                    SpinType1==SpinType2)
                HamElem=1.0*(2*SpinType1-1)###Last term makes the sign spin type dependent
            else
                if Strict
                    throw(DomainError(((X1,Y1,Z1),(X2,Y2,Z2)),"This connection not exist"))
                else
                    HamElem=0.0
                end
            end        
        else
            ###X hopping
            if X2==X1+1 && Y1==Y2 && Z1Pos==Z2Pos && SpinType1!=SpinType2
                HamElem=im
            elseif X2==X1-1 && Y1==Y2 && Z1Pos==Z2Pos && SpinType1!=SpinType2
                HamElem=-im
                ###Y hopping
            elseif X2==X1 && Y2==Y1+1 && Z1Pos==Z2Pos && SpinType1!=SpinType2
                HamElem=1.0*(2*SpinType1-1)###Last term makes the sign spin type dependent
            elseif X2==X1 && Y2==Y1-1 && Z1Pos==Z2Pos && SpinType1!=SpinType2
                HamElem=-1.0*(2*SpinType1-1)###Last term makes the sign spin type dependent
                ###Z hopping
            elseif X2==X1 && Y2==Y1 && Z2Pos==Z1Pos+1 && SpinType1==SpinType2
                HamElem=1.0*(2*SpinType1-1)###Last term makes the sign spin type dependent
            elseif X2==X1 && Y2==Y1 && Z2Pos==Z1Pos-1 && SpinType1==SpinType2
                HamElem=1.0*(2*SpinType1-1)###Last term makes the sign spin type dependent
            else
                if Strict
                    throw(DomainError(((X1,Y1,Z1),(X2,Y2,Z2)),"This connection not exist"))
                else
                    HamElem=0.0
                end
            end
        end
        HamElem=-t*HamElem
    end
    return HamElem
end

function WeylParamsTuple(Params::Dict)
    return  (Params["Length"],Params["Width"],Params["Height"],
             Params["t"],Params["W"],Params["PBC"])
end

TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close WeylLattice.jl")
