if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open HaldaneFractal.jl")
TabLevel=TabLevel*"    "

###This file defines the functions nececarry to diagponalise the serpinsly carpet, gaskes and triangle....

using LinearAlgebra
using Dates
using ProgressTimer
using StatsBase
using Random
using Test
using CRC32c

include("../core/Misc.jl")
#include("../core/RecursiveGreens.jl")
include("../core/PlotMisc.jl")



function getHaldaneSierpinskiParams(SizeGen::Integer,FracGen::Integer,B0=1., B1=1., B2=1.0,B3=1.,mu=0.1;Plot=false,Verbose=false)
#    validate_LT(LT)
    N=get_lattice_size(SizeGen)
    ####Begin by building the lattice####
    if Verbose
        println("Creating the lattice at sg=$SizeGen, fg=$FracGen")
    end
   ### first generate a 2DLattice
   Lattice2D=GetTheLattice(SizeGen,FracGen)
    println("lattice done")
   if Plot
        figure()
        imshow(Lattice2D[end:-1:1,:],extent=[-.5,N-.5,-.5,N-.5],cmap="gnuplot2_r")

        title("The available lattice points")
        #colorbar()
    end
    if Verbose
        println("Constructing lattice-vector-map at for sg=$SizeGen, fg=$FracGen")
    end
    println("computing IVM")
    IVM2D,Ncount=GetNonZeroElements(Lattice2D)
    IVM2D=IVM2D .+ 1 ###The GetNonZeroElemets starts counting at zero
    println("done")
    #println("IVM2D")
    #display(IVM2D)
    println("computing VIM")
    VIM2D=VIM_from_IVM(IVM2D)[1]
    println("doublingIVM")
    #faster than the sorting algorithm
    sizeIVM1=size(IVM2D)[1]
    sizeIVM2=size(IVM2D)[2]
    IndVecMap=fill(0,sizeIVM1+1, 2*sizeIVM2)
    for col=1:sizeIVM2
        IndVecMap[1,2*col]=IVM2D[1,col]
        IndVecMap[1,2*col-1]=IVM2D[1,col]
        IndVecMap[2,2*col]=IVM2D[2,col]
        IndVecMap[2,2*col-1]=IVM2D[2,col]
        IndVecMap[3,2*col-1]=1
        IndVecMap[3,2*col]=2
    end
    #IndVecMap=DoubleIVM(IVM2D)
    println("done")
    #println("IndVecMap")
    #display(IndVecMap)
     ### maybe later a 3D-Version of this is useful?
    println("constructing double VIM")
    VecIndMap=VIM_from_IVM(IndVecMap)
#    println("List of Nonzero elements:")
#    display(IndVecMap)
    

    ###Load the parameters
   Params=Dict("sg"=>SizeGen,"fg"=>FracGen,
				"Length"=>N,"Width"=>N,"B0"=>B0,"B1"=>B1,"B2"=>B2,"B3"=>B3, "mu"=> mu, "ParamsTuple"=>HaldaneParamsTuple)
				
    ###Load the functions related to the parameters

	Params["ParamsNames"]=SierpinskiParamsNames
	Params["IVM"]=IndVecMap
	Params["IVM2D"]=IVM2D
#   Params["IVMtoIPM"]=get_Sierpinski_IPM
	Params["VIM"]=VecIndMap
	Params["VIM2D"]=VIM2D
	Params["GenHamElems"]=getHamElems
	Params["GetPossibleNeighbours"]=GetHaldaneNeighbours				
    ####Load some of the names that can be extracted
	Params["title"]=("Sierpinski  carpet sg=$SizeGen fg=$FracGen")
#	Params["IDSTRING"]=SierpinskiID(SizeGen,FracGen,B0,B1,B2,B3)
#   Params["IDSHORT"]="Sierpinski_$(LT)_sg_$(SizeGen)_fg_$(FracGen)"
   return Params
end

						
    
    





function get_lattice_size(SizeGen)
        ### get lattice width from maximal possible generation 
        if SizeGen<0
            Text="SizeGen=$SizeGen has to be >= 0!"
            throw(DomainError(SizeGen,Text))
        else
           return 3^SizeGen
        end
end



function GetTheLattice(SizeGen,FracGen)
    N=get_lattice_size(SizeGen)
    LatticeMatrix=fill(false,N,N)
    #LatticeMatrix=fill(0,N,N)
    for col in 1:N
        for row in 1:N
            LatticeMatrix[row,col]=IsOnFractal(row,col,SizeGen,FracGen)
    #        println("considered site (row,col)=($row , $col )")
        end
    end
    #LatticeMatrix[15,54]=5
#    println("Lattice size gen $SizeGen ")
    return LatticeMatrix
end

function IsOnFractal(row,col,SizeGen,FracGen)
    if SizeGen<0
        Text="SizeGen=$SizeGen has to be >= 0!"
        throw(DomainError(SizeGen,Text))
    end
    if FracGen<0
        Text="FracGen=$FracGen has to be >= 0!"
        throw(DomainError(FracGen,Text))
    end
    if SizeGen<FracGen
        Text="SizeGen=$SizeGen has to be bigger or equal to FracGen=$FracGen"*"!"
        throw(DomainError(FracGen,Text))
    end
    FracGen==0 && return true
        
    ### if for any considered generation the site is in the second third for row and coloumn it has to get cut out, otherwise it stays
    #println("consider site ( $row , $col )")
    site_exists=true
    for currentgen=1:FracGen
        
        rowthird=which_third(row,SizeGen,currentgen)
        colthird=which_third(col,SizeGen,currentgen)
       # println("for gen=$currentgen rowthird=$rowthird colthird=$colthird")
        if (which_third(row,SizeGen,currentgen)==2)&&(which_third(col,SizeGen,currentgen)==2)
        #    println("site should be cut out")
            site_exists=false
        end
    end
    return site_exists
end


function which_third(idx,SizeGen,generation)
    UnitLength=3^(SizeGen-generation)
    #println("unit length for gen=$generation and Sizegen=$SizeGen is $UnitLength")
    third=mod(div(idx-1,UnitLength)+1,3)
    ### result should be 1,2,0 instead of 1,2,3  but we only care about the case 2 anyway
    
    
   # println(" therefore index $idx is in the $third third")
    return third

end

function SierpinskiParamsNames(Params)

    return  ("B0","B1","B2","B3")
end
    

function HaldaneParamsTuple(Params::Dict)
    return  (Params["Length"],Params["Width"],Params["B0"],Params["B1"],Params["B2"],Params["B3"],Params["VIM2D"], Params["mu"])
end

### This can be used to create for example the hamiltonian for the density of states
function do_CreateCentralHamiltonian(Params;Plot=false)
    FG=Params["fg"]
    SG=Params["sg"]
    B0=Params["B0"]
    B1=Params["B1"]
    B2=Params["B2"]
    B3=Params["B3"]
    mu=Params["mu"]
    VIM=Params["VIM2D"]
    IndVecMap=Params["IVM2D"]
    

    println("Creating empty hamiltonian for Sizegen=$SG, FracGen=$FG, B0=$B0 , B1=$B1 , B2=$B2 , B3=$B3")
    StartTime=now()

    ### N corresponds to length/width of lattice -> have 2*N^2 possible sites    
    N=get_lattice_size(SG)

    ###Factor 2 because we have 2 orbitals
    Hamiltonian = zeros(Complex,2*N^2,2*N^2)

    ###Empty hamiltonian
    ###Populate with hopping elements

    println("Building hamiltonian for $N x $N lattice with fracgen $FG, B0=$B0 , B1=$B1 , B2=$B2 , B3=$B3")
    TIMESTRUCT=TimingInit()
    for row=1:N, col=1:N
        if site_exists((row,col),VIM)
           
        ####Add diaognal element
        j=N*(col-1)+row
    #    println("j=$j")
        Hamiltonian[2j-1,2j-1]=B0+mu*(2*rand()-1)
        Hamiltonian[2j,2j]=-B0+mu*(2*rand()-1)
    #    println("diagonal element for j=$j done")
        k=j-N # (backward)
        if site_exists((row,col-1),VIM)
            
            Hamiltonian[2j-1,2k-1]=B1
            Hamiltonian[2j-1,2k] =im*B3/2
            Hamiltonian[2j,2k-1]=im*B3/2
            Hamiltonian[2j,2k]=-B1
        end
    #    println("backward element for j=$j done")

        k=j+N ## (forward)
        if site_exists((row,col+1),VIM)
            #println("forward k=$k")
            Hamiltonian[2j-1,2k-1]=B1
            Hamiltonian[2j-1,2k] =-im*B3/2
            Hamiltonian[2j,2k-1]=-im*B3/2
            Hamiltonian[2j,2k]=-B1

        end
        k=j-1# (down)
        if site_exists((row-1,col),VIM)
            #println("down k=$k")
            Hamiltonian[2j-1,2k-1]=B1
            Hamiltonian[2j-1,2k] =-B3/2
            Hamiltonian[2j,2k-1]=B3/2
            Hamiltonian[2j,2k]=-B1
        end
        k=j+1  # (up)       
        if site_exists((row+1,col),VIM)
        #println("up k=$k")
            Hamiltonian[2j-1,2k-1]=B1
            Hamiltonian[2j-1,2k] =B3/2
            Hamiltonian[2j,2k-1]=-B3/2
            Hamiltonian[2j,2k]=-B1
        end
        k=j+N+1 #(up right)
        if site_exists((row+1,col+1),VIM)
            #println("up right k=$k")
            Hamiltonian[2j-1,2k-1]=B2
            Hamiltonian[2j-1,2k] =-im*B3*(1-im)/4 
            Hamiltonian[2j,2k-1]=-im*B3*(1+im)/4  
            Hamiltonian[2j,2k]=-B2
        end
        k=j-N+1  # (up left)
        if site_exists((row+1,col-1),VIM)
            #println("up left k=$k")
            Hamiltonian[2j-1,2k-1]=B2
            Hamiltonian[2j-1,2k] =-im*B3*(1+im)/4 
            Hamiltonian[2j,2k-1]=-im*B3*(1-im)/4  
            Hamiltonian[2j,2k]=-B2
        end
        k=j-1+N  # (down right)
        if site_exists((row-1,col+1),VIM)
            #println("down right k=$k")
            Hamiltonian[2j-1,2k-1]=B2
            Hamiltonian[2j-1,2k] =-im*B3*(-1-im)/4 
            Hamiltonian[2j,2k-1]=-im*B3*(-1+im)/4  
            Hamiltonian[2j,2k]=-B2
        end
        k=j-1-N  # (down left)
        if site_exists((row-1,col-1),VIM)
            Hamiltonian[2j-1,2k-1]=B2
            Hamiltonian[2j-1,2k] =-im*B3*(-1+im)/4   
            Hamiltonian[2j,2k-1]=-im*B3*(-1-im)/4   
            Hamiltonian[2j,2k]=-B2
        end
        TIMESTRUCT=TimingProgress(TIMESTRUCT,j,N;Message="j=$j row=$row col=$col")
    end
    end
    EndTime=now()
    DiffTime=Dates.canonicalize(Dates.CompoundPeriod(Dates.Millisecond(EndTime-StartTime)))
    println("It took $DiffTime to complete the hamiltonian")
    ## size(IndVecMap)=Ncount*Ncount, 2 orbitals->2Ncount*2Ncount
    HamSize=4*size(IndVecMap)[2]
    #if false #### disable thats below
    if HamSize < 10 && Plot
        println("The hamiltonian")
        display(Hamiltonian)
        println()
    end

    if N^2 < 500 && Plot
        figure()
        imshow(abs.(Hamiltonian))
        title("The elements of the hamiltonian")
        colorbar()
    end

    if N^2 < 100 && Plot
        PlotHamiltonian(Hamiltonian,IndVecMap)
    end

    return Hamiltonian
end

function getHamElems(ParamsTuple,FromPoint,ToPoint)
    (Length,Width,B0,B1,B2,B3,VIM2D,mu)=ParamsTuple
	(FRow,FCol,FOrb)=FromPoint
    (TRow,TCol,TOrb)=ToPoint
    
    HamElem=im*0.
    ### if site where we want to hop onto/out of does not exist, the HamElem will not be 0
    if (!site_exists(ToPoint,VIM2D)) || (!site_exists(FromPoint,VIM2D)) 
        return HamElem
    else
        if FromPoint==ToPoint 
		    if FOrb==1
			    HamElem=B0+mu*(2*rand()-1)
		    else
			    HamElem=-B0+mu*(2*rand()-1)
		    end
	    elseif FRow==TRow && (FCol-1)==TCol # (backward)
		    if FOrb==1 && TOrb==1
			    HamElem=B1
    		elseif FOrb==1 && TOrb==2
	    		HamElem=im*B3/2
		    elseif FOrb==2 && TOrb==1
			    HamElem=im*B3/2
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B1
		    end
    	elseif FRow==TRow && (FCol+1)==TCol # (forward)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B1
		    elseif FOrb==1 && TOrb==2
			    HamElem=-im*B3/2
		    elseif FOrb==2 && TOrb==1
			    HamElem=-im*B3/2
		    elseif FOrb==2 && TOrb==2
			    HamElem=-B1
		    end
	    elseif (FRow-1)==TRow && FCol==TCol # (down)
		    if FOrb==1 && TOrb==1
			    HamElem=B1
    		elseif FOrb==1 && TOrb==2
	    		HamElem=-B3/2
		    elseif FOrb==2 && TOrb==1
			    HamElem=B3/2
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B1
		    end
    	elseif (FRow+1)==TRow && FCol==TCol # (up)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B1
    		elseif FOrb==1 && TOrb==2
	    		HamElem=B3/2
		    elseif FOrb==2 && TOrb==1
			    HamElem=-B3/2
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B1
		    end    
    	elseif(FRow+1)==TRow && (FCol+1)==TCol           ##(up+right)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B2
    		elseif FOrb==1 && TOrb==2
	    		HamElem=-im*B3*(1-im)/4
	    	elseif FOrb==2 && TOrb==1
		    	HamElem=-im*B3*(1+im)/4
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B2
		    end
    	elseif(FRow+1)==TRow && (FCol-1)==TCol      ##(up+left)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B2
    		elseif FOrb==1 && TOrb==2
	    		HamElem=-im*B3*(1+im)/4
		    elseif FOrb==2 && TOrb==1
			    HamElem=-im*B3*(1-im)/4
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B2
		    end
    	elseif(FRow-1)==TRow && (FCol+1)==TCol      ##(down+right)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B2
    		elseif FOrb==1 && TOrb==2
	    		HamElem=-im*B3*(-1-im)/4
		    elseif FOrb==2 && TOrb==1
			    HamElem=-im*B3*(-1+im)/4
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B2
		    end
    	elseif(FRow-1)==TRow && (FCol-1)==TCol      ##(down+left)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B2
    		elseif FOrb==1 && TOrb==2
	    		HamElem=-im*B3*(-1+im)/4
		    elseif FOrb==2 && TOrb==1
			    HamElem=-im*B3*(-1-im)/4
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B2
		    end
    	end
        return HamElem
    end
end


function DoubleIVM(IVM)
    NCount=size(IVM)[2]
    IndVecMap=zeros(Int32,3,2*NCount)
    IndVecMap[3,1:NCount].=1
    IndVecMap[3,(1+NCount):(2*NCount)].=2
    IndVecMap[1:2,1:NCount]=IVM ###Copy the IVM
    IndVecMap[1:2,(1+NCount):(2*NCount)]=IVM ###Copy the IVM
    #println("IndVecMap")
    #display(IndVecMap)
    sort_IVM!(IndVecMap) ###Sort the IVM in place
    return IndVecMap
end
	
	
### in case 2D version works:
function GetHaldaneNeighbours((row,col))
	## may need to be adjusted to include orbitals 
        return ((row,col-1), #backward
                (row,col+1),#forward
                (row-1,col),#down
                (row+1,col), #up
                (row+1,col+1),#up+forward
                (row+1,col-1),#up+backward
                (row-1,col+1),#down+forward
                (row-1,col-1))#down+backward               

end
### in case 3D version is needed

function GetHaldaneNeighbours((row,col,orb),_)
    return((row,col-1,1),(row,col-1,2), #backward
                (row,col+1,1), (row,col+1,2),#forward
                (row-1,col,1),(row-1,col,2),#down
                (row+1,col,1),(row+1,col,2),  #up
                (row+1,col+1,1),(row+1,col+1,2),#up+forward
                (row+1,col-1,1),(row+1,col-1,2),#up+backward
                (row-1,col+1,1),(row-1,col+1,2),#down+forward
                (row-1,col-1,1),(row-1,col+1,2))#down+backward               
            
end     


function site_exists(Point,VIM)
    ## VIM has a non-zero entry at location(row,col) if the site exists and zero otherwise
    #println("check if site $Point exists")
    ### check first if point is allowed on square lattice 
    if length(Point)==3
        (Row,Col,Orb)=Point
    elseif length(Point)==2
        (Row,Col)=Point
    else
        println(length(Point))
    end
    (W,L)=size(VIM) ###The L and W are just the dimentions of the lattice
    ### do we need an offset?
    if (Row < 1) || (Row > W)
        return false
    elseif (Col < 1) || (Col > L)
        return false
    elseif VIM[Row,Col]==0 
        ## in a square lattice this should never happen,  in a fractal it will be where the holes are
        return false
    else
        return true
    end
end

function FindPotentials(L1:: Int,L2:: Int, TransMission; I0=1)
    ## L1, L2 are leads through which the current is non-zero, I0 is the strength of that current
  
  
    Energies=size(TransMission)[1]
    ### can fix one of them, for now fix V[1], second index is the energy value
    V=fill(0.,Energies,4)
    
    V0=fill(0.,3)
    if L1==L2
        println("error: both leads are the same")
    end
    eps=0.0001 # margin of numerical error used for sanity checks
    ### set current values, they should never change
    Idummy=fill(0.,3)
    ### dummy current for the calculation
    I=fill(0.,4)
    if L1!=4
        Idummy[L1]=I0
    end
    if L2!=4
        Idummy[L2]=-I0
    end
    I[L1]=I0
    I[L2]=-I0
    #### if there is no transport return
    for w=1:Energies ### runs over energy values
        ### disregard last line such that M becomes invertible, this corresponds to setting V[4,w]=0
        M=fill(0.,3,3)



 
        for j=1:3, k=1:3,
            M2=0
            if j==k
                for l=1:4
                    M2+=TransMission[w,j,l]
                end
            end     
            M[j,k]=-TransMission[w,j,k]+M2
        end              
        if det(M)==0
            println("Matrix to be inverted at energystep w=$w has determinant 0")
            println(M) 
            return fill(0.,Energies,4)
        end
        ### next calculate the 3 potentials, not yet shifted
        V0=inv(M)*Idummy
        ### testvalue for V[4], first set up  matrix
        test=I[4]-(TransMission[w,4,1]*V0[1]+TransMission[w,4,2]*V0[2]+TransMission[w,4,3]*V0[3])
        if test>eps
            println("result not within numerical margin")
            println("error size = $test")
        end
        


        ## shift such that V[a1]=0
        if L1==4 ### no shift neccessary, V[4]=0
            for j=1:3
                V[w,j]=V0[j]
            end
            V[w,4]=0
        else ### need to shift
             
            for j=1:3
                V[w,j]=V0[j]-V0[L1]
            end
            ## set fourth term
            V[w,4]=-V0[L1]
            
        end
    end



    return V

end

function DoubleLeadMatrix(LeadHamiltonian)

Width=size(LeadHamiltonian)[1]## should be a square matrix
RealHamiltonian=fill(im*0., 2*Width,2*Width)
    for j=1:Width,k=1:Width
        RealHamiltonian[2*j,2*k]=LeadHamiltonian[j,k]
        RealHamiltonian[2*j-1,2*k-1]=LeadHamiltonian[j,k]
    end
    return RealHamiltonian
end





function BuildLeadHamiltonian(LeadAttachmentPoints,j,t)

    leadsize=size(LeadAttachmentPoints[j])[1]
    if t==0
       t=1
    end     
    Hamiltonian=DoubleLeadMatrix(BuildHamiltonian(getChainParams(div(leadsize,2),1,t)))

#   println("Hamiltonian in Lead $j with leadsize $leadsize")
#   display(Hamiltonian)
    return Hamiltonian
end




function BuildLeadToLeadHopping(LeadAttachmentPoints,j,t)
    leadsize=size(LeadAttachmentPoints[j])[1]
    if t==0
        t=1
    end

    Hopping=fill(0., leadsize, leadsize)
    for i=1:leadsize
        Hopping[i,i]=-t
    end
    return Hopping
end



TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close HaldaneFractal.jl")
