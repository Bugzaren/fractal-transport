if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open Sierpinski.jl")
TabLevel=TabLevel*"    "

###This file defines the functions nececarry to diagponalise the serpinsly carpet, gaskes and triangle....

using LinearAlgebra
using Dates
using ProgressTimer #### user defined timing library
using StatsBase
using Random
using Test
using CRC32c

include("../core/Misc.jl")
#include("../core/RecursiveGreens.jl")
include("../core/PlotMisc.jl")
include("../lattices/LatticeMisc.jl")

function get_errtext()
    return "Only LatticeType=S{C=carpet,N=Net,G=gasket,T=triangle}{S=Square,T=Tiangle,R=Reverse-Triangle,X=[TR]=X-hopping} are supported"
end


function validate_LT(LT)
    if LT[1]!='S' || length(LT)!=3
        throw(DomainError(LT,get_errtext()))
    end
    if LT[2]!='C' && LT[2]!='H' && LT[2]!='T' && LT[2]!='G' && LT[2]!='N'
        throw(DomainError(LT,get_errtext()))
    end
    if LT[3]!='S' && LT[3]!='T' && LT[3]!='R' && LT[3]!='X'
        throw(DomainError(LT,get_errtext()))
    end
end

function get_lattice_size(SizeGen,LT)
    validate_LT(LT)
    if SizeGen<0
        Text="SizeGen=$SizeGen has to be >= 0!"
        throw(DomainError(SizeGen,Text))
    end
    if LT[2]=='C' || LT[2]=='H'  ###Carpet or Honeycomb
        return 3^SizeGen
    elseif LT[2]=='T' ###Triangle
        return 2^SizeGen
    elseif LT[2]=='G' ###Gascet
        return 2^SizeGen+1
    elseif LT[2]=='N'  ###Net
        if SizeGen==1
            return 3
        elseif SizeGen==0
            return 1
        else
            return get_lattice_size(SizeGen-1,LT)*3-2
        end
    end
end



function Sierpinski_bond_is_present(LT,SizeGen,FracGen,(row1,col1),(row2,col2);verbose=false)
        if col2 < col1 || (col2==col1 && row2<row1)
        ###Order such that we onlyce consider hopping from left to right or down to up
        return Sierpinski_bond_is_present(LT,SizeGen,FracGen,(row2,col2),(row1,col1))
    elseif abs(row1-row2) > 1 || abs(col1-col2) > 1
        ###Allow only nearest neighbors or next nearest neighbors
        return false
    else
        if LT[2] == 'C' || LT[2]=='T' || LT[2]=='H'
            ###Carpet and triangle and honekomb are defined in terms of their lattice points alone
            return is_bond_allowed(row1,col1,row2,col2,LT[3])
        elseif LT[2] == 'G'
            if !is_bond_allowed(row1,col1,row2,col2,LT[3])
                return false ###The bond has to be at least allowed
            end
            ###For the gascet there is on level 1 a special case
            if FracGen==0
                return true
            else
                SideSize=2^(SizeGen-1)
                if verbose 
                    println("SideSize: $SideSize")
                    println("SizeGen,FracGen: $SizeGen, $FracGen")
                    println("(row1,col1)=($row1,$col1)")
                    println("(row2,col2)=($row2,$col2)")
                end
                if SizeGen==1
                    return true
                elseif  SizeGen>=2
                    if ((row1,col1) == (0, SideSize-1) &&
                        (row2,col2) == (0, SideSize)) ##Horizontal ok
                        return true
                    elseif ((row1,col1) == (SideSize-1,0) &&
                            (row2,col2) == (SideSize,0)) ##Vertical ok
                        return true
                    elseif ((row1,col1) == (1, SideSize-1) &&
                            (row2,col2) == (1, SideSize)) ##Horizontal+1 not ok
                        return false
                    elseif ((row1,col1) == (SideSize-1,1) &&
                            (row2,col2) == (SideSize,1)) ##Vertical+1 not ok
                        return false
                    elseif ((row1,col1) == (0, SideSize-1) &&
                            (row2,col2) == (1, SideSize)) ### "up/right"
                        return false ##not ok
                    elseif ((row1,col1) == (SideSize-1,0) &&
                            (row2,col2) == (SideSize,1)) ### "up/right"
                        return false ##not ok
                    elseif ((row1,col1) == (SideSize-1,1) &&
                            (row2,col2) == (SideSize,2)) ### "up/right"
                        return false ##not ok
                    elseif ((row1,col1) == (1, SideSize-1) &&
                            (row2,col2) == (2, SideSize)) ### "up/right"
                        return false ##not ok
                    elseif ((row1,col1) == (1, SideSize-1) &&
                            (row2,col2) == (0, SideSize)) ### "down/right"
                        return true ## ok
                    elseif ((row1,col1) == (SideSize-1,1) &&
                            (row2,col2) == (SideSize,2)) ### "down/right"
                        return true ## ok
                    elseif ((row1,col1) == (SideSize,SideSize-1) &&
                            (row2,col2) == (SideSize-1,SideSize)) ### "down/right
                        return false ## ok
                    elseif col1>= SideSize
                        return Sierpinski_bond_is_present(LT,SizeGen-1,FracGen-1,
                                               (row1,col1-SideSize),(row2,col2-SideSize))
                    elseif row1>=SideSize
                        return Sierpinski_bond_is_present(LT,SizeGen-1,FracGen-1,
                                               (row1-SideSize,col1),(row2-SideSize,col2))
                    elseif row1 < SideSize && col1 < SideSize && row2 < SideSize && col2 < SideSize
                        return Sierpinski_bond_is_present(LT,SizeGen-1,FracGen-1,(row1,col1),(row2,col2))
                    end
                end
            end
        end
    end
    return false
end


###These two come teogether
function SierpinskiParamsTuple(Params)
    return  (Params["LT"],Params["sg"],Params["fg"],Params["tW"],
             Params["W"],Params["t"],Params["ph"],Params["GaugeChoise"])
end


function SierpinskiParamsNames(_)
    return SierpinskiParamsNames()
end
function SierpinskiParamsNames()
    return  ("LT","sg","fg","tW","W","t","ph","GaugeChoise")
end

function SierpinskiTuplePos(S::String)
    Res=findfirst(x->x==S,SierpinskiParamsNames())
end

function getSierpinskiParams(SizeGen::Integer,FracGen::Integer,Phase::Real=0.0;
                             t::Real=1.0,W::Real=0.0,LT="SCS",tW::Real=0.0,Gauge="LandauY",Plot=false,Verbose=false)
    validate_LT(LT)
    N=get_lattice_size(SizeGen,LT)
    ####Begin by building the lattice####
    if Verbose 
        println("Creating the lattice for $LT at sg=$SizeGen, fg=$FracGen")
    end
    TheLattice=GetTheSierpinskyLattice(SizeGen,FracGen,LT) ###Plot if exists
    if Plot
        figure()
        imshow(TheLattice[end:-1:1,:],extent=[-.5,N-.5,-.5,N-.5],cmap="gnuplot2_r")
        title("The available lattice points")
        #colorbar()
    end
    if Verbose     
        println("Constructing lattice-vector-map $LT at for sg=$SizeGen, fg=$FracGen")
    end
    
    IndVecMap,Ncount=GetNonZeroElements(TheLattice) ###Ellemtns, ordered after row and then column
    VecIndMap=VIM_from_IVM(IndVecMap)
    GaugeChoise=GaugeChoiseToNum(Gauge)

    ###Load the parameters
    Params=Dict("sg"=>SizeGen,"fg"=>FracGen,"ph"=>Phase,"t"=>t,
                "W"=>W,"tW"=>tW,"LT"=>LT,"N"=>N,
                "GaugeChoise"=>GaugeChoise)
    ###Load the functions related to the parameters
    Params["ParamsTuple"]=SierpinskiParamsTuple
    Params["ParamsNames"]=SierpinskiParamsNames
    Params["IVM"]=IndVecMap
    Params["IVMtoIPM"]=get_Sierpinski_IPM
    Params["VIM"]=VecIndMap
    Params["GenHamElems"]=getSierpinskiElems
    Params["GetPossibleNeighbours"]=GetSierpinskiNeighbours
    ####Load some of the names that can be extracted
    Params["title"]=("Sierpinski  $LT sg=$SizeGen fg=$FracGen, phase=$Phase, noise W=$W")
    Params["IDSTRING"]=SierpinskiID(SizeGen,FracGen,Phase,t,LT,W)
    Params["IDSHORT"]="Sierpinski_$(LT)_sg_$(SizeGen)_fg_$(FracGen)"
    return Params
end

function GetSierpinskiNeighbours((row,col),ParamsTuple::Tuple)
    LT=ParamsTuple[1][3]
    return GetLatticeNeighbours((row,col),LT)
end


function getSierpinskiElems(ParamsTuple::Tuple,FromPoint,ToPoint)
    #println("FromPoint:",FromPoint)
    #println("ToPoint:",ToPoint)
    ##println("Params:",Params)
    (LT,SizeGen,FracGen,tW,W,t,Phase,GaugeChoise)=ParamsTuple
    HamElem=0.0
    if FromPoint == ToPoint
        if W!=0.0
            HamElem=W*2*(rand()-.5)
        end
    else
        if  Sierpinski_bond_is_present(LT,SizeGen,FracGen,FromPoint,ToPoint)
            if tW!=0.0
                trand=t*(1.0+tW*2*(rand()-.5))
            else
                trand=t
            end
            HamElem=PhaseFactor(trand,ToPoint,FromPoint,Phase,GaugeChoise)
        end
    end
    return HamElem
end

function get_Sierpinski_IPM(IVM,ParamsTuple::Tuple)
    #println("IVM:",IVM)
    #println("size(IVM):",size(IVM))
    LT=ParamsTuple[SierpinskiTuplePos("LT")]
    SizeGen=ParamsTuple[SierpinskiTuplePos("sg")]
    if LT[2]=='G' || LT[2]=='T' || LT[2]=='H'
        MaxRow=2^SizeGen
        IVMMOD=fill(0.,size(IVM)) ##Float to handle float values
        IVMMOD[1,:]=MaxRow .- IVM[2,:]
        IVMMOD[2,:]=IVM[1,:]
        IVMMOD[1,:]=IVMMOD[1,:]-IVMMOD[2,:]
        IPM = get_triangular_IPM(IVMMOD)
    elseif LT[3]=='T'
        IPM = get_triangular_IPM(IVM)
    elseif LT[3]=='R'
        IPM = get_anti_triangular_IPM(IVM)
    elseif LT[3]=='X' || LT[3]=='S'
        IPM = IVM .* 1.0
    end
    return IPM
end


function GetTheSierpinskyLattice(SizeGen,Fracgen,LT)
    N=get_lattice_size(SizeGen,LT)
    LatticeMatrix=fill(false,N,N)
    #LatticeMatrix=fill(0,N,N)
    for col in 0:(N-1)
        for row in 0:(N-1)
            LatticeMatrix[col+1,row+1]=IsOnFractal(row,col,SizeGen,Fracgen,LT)
        end
    end
    #LatticeMatrix[15,54]=5
    return LatticeMatrix
end

function IsOnFractal(row,col,SizeGen,FracGen,LT,verb=false)
    #println("-------------------------")
    if SizeGen<0
        Text="SizeGen=$SizeGen has to be >= 0!"
        throw(DomainError(SizeGen,Text))
    end
    if FracGen<0
        Text="FracGen=$FracGen has to be >= 0!"
        throw(DomainError(FracGen,Text))
    end
    if LT[2]=='C'
        IsOnSCFractal(row,col,SizeGen,FracGen,verb)
    elseif LT[2]=='H'
        doIsOnSHFractal(row,col,SizeGen,FracGen,verb=verb)
    elseif LT[2]=='N'
        IsOnSNFractal(row,col,SizeGen,FracGen,verb)
    elseif LT[2]=='T'
        IsOnSTFractal(row,col,SizeGen,FracGen,verb)
    elseif LT[2]=='G'
        IsOnSGFractal(row,col,SizeGen,FracGen,verb)
    else
        throw(DomainError(LT,get_errtext()))
    end
end

function IsOnSGFractal(row,col,SizeGen,FracGen,verb=false)
    if SizeGen<2 && FracGen>0
        Text="There is no nontrivial FracGen!=0 for SizeGen=$SizeGen"*"!"
        throw(DomainError(FracGen,Text))
    elseif SizeGen>=2 && FracGen==(SizeGen-1)
        IsOnSGFractal(row,col,SizeGen,FracGen-1,verb)
    elseif SizeGen>=2 && FracGen>(SizeGen-1)
        Text="There is no nontrivial FracGen>"*string(SizeGen-1)*" for SizeGen=$SizeGen"*"!"
        throw(DomainError(FracGen,Text))
    end
    ###Determine if a point if on the fractal
    EffSize=2^SizeGen
    ###We first want to define a triangle of exclusion (this is independent of the FracGen)
    verb && println("($row,$col) of SizeGen $SizeGen : #$EffSize")
    if row+col>EffSize
        return false
    elseif FracGen==0
        return true
    else
        ###First we determin which half this is in
        rowpart=div(row,2^(SizeGen-1))
        colpart=div(col,2^(SizeGen-1))
        rowdiff=rem(row,2^(SizeGen-1))
        coldiff=rem(col,2^(SizeGen-1))
        verb && println("($rowpart,$colpart)")
        if !(rowpart==1 && colpart==1)
            verb && println("On this generation of the lattice")
            if SizeGen>1
                verb && println("($rowdiff,$coldiff)")
                return IsOnSGFractal(rowdiff,coldiff,SizeGen-1,FracGen-1,verb)
            else
                return true
            end
        else
            if rowdiff==0 && coldiff==0
                return true
            else
                return false
            end
        end
    end
end


function IsOnSTFractal(row,col,SizeGen,FracGen,verb=false)
    if SizeGen<2 && FracGen>0
        Text="There is no nontrivial FracGen!=0 for SizeGen=$SizeGen"*"!"
        throw(DomainError(FracGen,Text))
    elseif SizeGen>=2 && FracGen>(SizeGen-1)
        Text="There is no nontrivial FracGen>"*string(SizeGen-1)*" for SizeGen=$SizeGen"*"!"
        throw(DomainError(FracGen,Text))
    end
    ###Determine if a point if on the fractal
    EffSize=2^SizeGen
    ###We first want to define a triangle of exclusion (this is independent of the FracGen)
    verb && println("..........................")
    verb && println("($row,$col) of SizeGen=$SizeGen, FracGen=$FracGen : #$EffSize")
    if row+col>(EffSize-1)
        return false
    elseif FracGen==0
        return true
    else
        ###First we determin which half this is in
        rowpart=div(row,2^(SizeGen-1))
        colpart=div(col,2^(SizeGen-1))
        rowdiff=rem(row,2^(SizeGen-1))
        coldiff=rem(col,2^(SizeGen-1))
        verb && println("quandrant ($rowpart,$colpart)")
        if !(rowpart==1 && colpart==1)
            verb && println("On this generation of the lattice")
            if SizeGen>1
                verb && println("($rowdiff,$coldiff)")
                verb && println(".,.,.,.,.,.,.,.,")
                return IsOnSTFractal(rowdiff,coldiff,SizeGen-1,FracGen-1,verb)
            else
                return true
            end
        else
            return false
        end
    end
end


function IsOnSCFractal(row,col,SizeGen,FracGen,verb=false)
    if SizeGen<FracGen
        Text="SizeGen=$SizeGen has to be bigger or equal to FracGen=$FracGen"*"!"
        throw(DomainError(FracGen,Text))
    end
    FracGen==0 && return true
    ###Determine if a point if on the fractal
    EffSize=3^SizeGen
    verb && println("($row,$col) of SizeGen $SizeGen : #$EffSize")
    ###First we determin which third this is in
    rowpart=div(row,3^(SizeGen-1))
    colpart=div(col,3^(SizeGen-1))
    verb && println("($rowpart,$colpart)")
    if !(rowpart==1 && colpart==1)
        verb && println("On this generation of the lattice")
        if SizeGen>1
            rowdiff=rem(row,3^(SizeGen-1))
            coldiff=rem(col,3^(SizeGen-1))
            verb && println("($rowdiff,$coldiff)")
            return IsOnSCFractal(rowdiff,coldiff,SizeGen-1,FracGen-1,verb)
        else
            return true
        end
    else
        return false
    end
end


function IsRegionFilled(colrowpart,dir)
    res=doIsRegionFilled(colrowpart,dir)
    #println("    $res=IsRegionFilled($colrowpart,$dir)")
    return res
end

function doIsRegionFilled(colrowpart,dir)
    TruthValue=nothing
    if colrowpart == (0,0)
        if dir==2 || dir==3 || dir==4 || dir==5 || dir==0
            TruthValue=false
        elseif dir==1 || dir==6 
            TruthValue=true
        else
            throw(DomainError(dir),"unknown  dir=$dir")
        end
    elseif colrowpart == (2,2)
        if dir==1 || dir==2 || dir==6 || dir==5 || dir==0
            TruthValue=false
        elseif dir==3 || dir==4 
            TruthValue=true
        else
            throw(DomainError(dir),"unknown  dir=$dir")
        end
    elseif colrowpart == (1,1)
        TruthValue=true
    elseif colrowpart == (1,2)
        if dir==1 || dir==2 || dir==3 || dir==5 || dir==6 || dir== 0
            TruthValue=false
        elseif  dir==4 
            TruthValue=true
        else
            throw(DomainError(dir),"unknown  dir=$dir")
        end
    elseif colrowpart == (0,2)
        if dir==1 || dir==2 || dir==3 || dir==4 || dir==6 || dir== 0
            TruthValue=false
        elseif dir==5
            TruthValue=true
        else
            throw(DomainError(dir),"unknown  dir=$dir")
        end
    elseif colrowpart == (0,1)
        if dir==1 || dir==2 || dir==3 || dir==4 || dir==5 || dir== 0
            TruthValue=false
        elseif   dir==6 
            TruthValue=true
        else
            throw(DomainError(dir),"unknown  dir=$dir")
        end
    elseif colrowpart == (1,0)
        if dir==2 || dir==4 || dir==5 || dir==3 ||  dir==6 || dir== 0
            TruthValue=false
        elseif dir==1  
            TruthValue=true
        else
            throw(DomainError(dir),"unknown  dir=$dir")
        end
    elseif colrowpart == (2,0)
        if dir==1 || dir==3 || dir==4 || dir==5 || dir==6 || dir== 0
            TruthValue=false
        elseif dir==2
            TruthValue=true
        else
            throw(DomainError(dir),"unknown  dir=$dir")
        end
    elseif colrowpart == (2,1)
        if dir==1 || dir==2 || dir==4  || dir==5 || dir==6 || dir== 0
            TruthValue=false
        elseif  dir==3 
            TruthValue=true
        else
            throw(DomainError(dir),"unknown  dir=$dir")
        end
    else
       throw(DomainError(colrowpart),"unknown  colrowpart=$colrowpart")
    end
    return TruthValue
end

function GetNewWall(colrowpart,dir,Wall,SizeGen,OrigParams)
    #println("GetWallparams:colrowpart,dir,Wall: $colrowpart, $dir, $Wall")
    #println("SizeGen=$SizeGen, OrigParams: (osize,orow,ocol)=$OrigParams")
    (osize,orow,ocol)=OrigParams

    ###First we determin which third this is in
    corserow=div(orow,3^(SizeGen-1))
    corsecol=div(ocol,3^(SizeGen-1))
    #println("corsecol,corserow=$corsecol,$corserow")

    col,row=colrowpart
    if osize > 3 && SizeGen == 2
        SmallerSize=IsOnSHFractal(corsecol,corserow-1,osize-SizeGen+1,0)
        Wall1=SmallerSize > 0
    else
        if row>0 ##Below wall is determined if there is a block below
            Wall1=IsRegionFilled((col,row-1),dir)
        else
            Wall1=Wall[1]
        end
    end
    if osize > 3 && SizeGen == 2
        SmallerSize=IsOnSHFractal(corsecol+1,corserow,osize-SizeGen+1,0)
        Wall2=SmallerSize > 0
    else
        if col<2 ##Right wall is determined if there is a block to right
            Wall2=IsRegionFilled((col+1,row),dir)
        else
            Wall2=Wall[2]
        end
    end
    if osize > 3 && SizeGen == 2
        SmallerSize=IsOnSHFractal(corsecol,corserow+1,osize-SizeGen+1,0)
        Wall3=SmallerSize > 0
    else
        if row<2 ##Above wall is determined if there is a block above
            Wall3=IsRegionFilled((col,row+1),dir)
        else
            Wall3=Wall[3]
        end
    end
    if osize > 3 && SizeGen == 2
        SmallerSize=IsOnSHFractal(corsecol-1,corserow,osize-SizeGen+1,0)
        Wall4=SmallerSize > 0
    else
        if col>0 ##Left wall is determined if there is a block to right
            Wall4=IsRegionFilled((col-1,row),dir)
        else
            Wall4=Wall[4]
        end
    end
    Wallout=(Wall1,Wall2,Wall3,Wall4)
    #println("Wallout: ",Wallout)
    return  (Wall1,Wall2,Wall3,Wall4)
end

function doIsOnSHFractal(row,col,SizeGen,FracGen;verb=false)
    IsOnSHFractal(row,col,SizeGen,FracGen)>=0
end

function IsOnSHFractal(row,col,SizeGen,FracGen;verb=false,dir=0,Wall=fill(false,4),
                       OrigParams=(SizeGen,row,col))
    FalseValue=-1
    TrueValue=SizeGen

    FullSize=3^SizeGen
    
    if row<0 || col<0 || row>=FullSize || col>=FullSize
        return FalseValue
    end
    
    if SizeGen==0
        return TrueValue
    elseif SizeGen<=FracGen
        Text="SizeGen=$SizeGen has to be bigger than to FracGen=$FracGen"*"!"
        throw(DomainError(FracGen,Text))
    end

    ###First we determin which third this is in
    colrowpart=(div(col,3^(SizeGen-1)),div(row,3^(SizeGen-1)))
    #println("colrowpart=$colrowpart")
    rowdiff=rem(row,3^(SizeGen-1))
    coldiff=rem(col,3^(SizeGen-1))

    ###Even if the fractal generation is zero we still want to exclude certain lattice sites
    if FracGen==0
        row3=rem(row,3)
        col3=rem(col,3)
        if row3==col3 ###Removes all the diagonals (gives a honecomb)
            ##If these are present its a triangular lattice
            return FalseValue
        end
        if SizeGen==0
            #println("Size zero good to go")
            return TrueValue
            #if false
            #a=1+1
        else ##Also we wish to remove certain cells to get a koch curve as boundary
            if colrowpart == (0,0)
                if !IsRegionFilled(colrowpart,dir)
                    WallNew=GetNewWall(colrowpart,dir,Wall,SizeGen,OrigParams)
                    if WallNew[1] || WallNew[4]
                        return TrueValue
                    else
                        return FalseValue
                    end
                else
                    return TrueValue
                end
            elseif colrowpart == (2,2)
                if !IsRegionFilled(colrowpart,dir)
                    WallNew=GetNewWall(colrowpart,dir,Wall,SizeGen,OrigParams)
                    if WallNew[2] || WallNew[3]
                        return TrueValue
                    else
                        return FalseValue
                    end
                else
                    return TrueValue
                end
            elseif colrowpart == (1,1) ;  return TrueValue
            elseif colrowpart == (1,2)
                if !IsRegionFilled(colrowpart,dir)
                    WallNew=GetNewWall(colrowpart,dir,Wall,SizeGen,OrigParams)
                    IsOnSHFractal(rowdiff,coldiff,SizeGen-1,FracGen,dir=1,Wall=WallNew,
                                  OrigParams=OrigParams)
                else
                    return TrueValue
                end
            elseif colrowpart == (0,2)
                if !IsRegionFilled(colrowpart,dir)
                    WallNew=GetNewWall(colrowpart,dir,Wall,SizeGen,OrigParams)
                    IsOnSHFractal(rowdiff,coldiff,SizeGen-1,FracGen,dir=2,Wall=WallNew,
                                  OrigParams=OrigParams)
                else
                    return TrueValue
                end
            elseif colrowpart == (0,1)
                if !IsRegionFilled(colrowpart,dir)
                    WallNew=GetNewWall(colrowpart,dir,Wall,SizeGen,OrigParams)
                    IsOnSHFractal(rowdiff,coldiff,SizeGen-1,FracGen,dir=3,Wall=WallNew,
                                  OrigParams=OrigParams)
                else
                    return TrueValue
                end
            elseif colrowpart == (1,0)
                if !IsRegionFilled(colrowpart,dir)
                    WallNew=GetNewWall(colrowpart,dir,Wall,SizeGen,OrigParams)
                    IsOnSHFractal(rowdiff,coldiff,SizeGen-1,FracGen,dir=4,Wall=WallNew,
                                  OrigParams=OrigParams)
                else
                    return TrueValue
                end
            elseif colrowpart == (2,0)
                if !IsRegionFilled(colrowpart,dir)
                    WallNew=GetNewWall(colrowpart,dir,Wall,SizeGen,OrigParams)
                    IsOnSHFractal(rowdiff,coldiff,SizeGen-1,FracGen,dir=5,Wall=WallNew,
                                  OrigParams=OrigParams)
                else
                    return TrueValue
                end
            elseif colrowpart == (2,1)
                if !IsRegionFilled(colrowpart,dir)
                    WallNew=GetNewWall(colrowpart,dir,Wall,SizeGen,OrigParams)
                    IsOnSHFractal(rowdiff,coldiff,SizeGen-1,FracGen,dir=6,Wall=WallNew,
                                  OrigParams=OrigParams)
                else
                    return TrueValue
                end
            else
                throw(DomainError(colrowpart,"unknown  colrowpart=$colrowpart"))
            end
        end
    else
        if !(colrowpart[1]==colrowpart[2])
            verb && println("On this generation of the lattice")
            if SizeGen>1
                verb && println("($rowdiff,$coldiff)")
                return IsOnSHFractal(rowdiff,coldiff,SizeGen-1,FracGen-1)
            else
                return TrueValue 
            end
        else
            return FalseValue
        end
    end
end


function IsOnSNFractal(row,col,SizeGen,FracGen,verb=false)
    if SizeGen<FracGen
        Text="SizeGen=$SizeGen has to be bigger or equal to FracGen=$FracGen"*"!"
        throw(DomainError(FracGen,Text))
    end
    ###Determine if a point if on the fractal
    FullSize=get_lattice_size(SizeGen,"SNS")
    
    verb && println("($row,$col) of SizeGen $SizeGen : #$FullSize")
    if row >= FullSize || col >= FullSize || row < 0 || col < 0
        verb && println("Outside")
        return false
    elseif FracGen==0
        verb && println("FracGen==0")
        return true
    elseif row == (FullSize-1) || col == (FullSize-1) || row == 0 || col == 0
        verb && println("on Boundary")
        return true
    else ###Now you are on the inner squares
        if SizeGen==0 ###Should be onlyone lelemnts
            verb && println("SizeGen==0")
            return true
        elseif SizeGen==1 ###Should be onlyone lelemnts
            if (row==1 && col==1)
                verb && println("SizeGen==1, middle")
                return false
            else
                verb && println("SizeGen==1, edge")
                return true
            end
        else ###aka SizeGen>1
            ###First we determin which third this is in (but make smaller by one)
            NextSize=get_lattice_size(SizeGen-1,"SNS")
            rowpart=div(row+1,NextSize)
            colpart=div(col+1,NextSize)
            verb && println("(rowpart,colpart)=($rowpart,$colpart)")
            if !(rowpart==1 && colpart==1)
                rowdiff=rem(row,NextSize-1)
                coldiff=rem(col,NextSize-1)
                verb && println("(rowdiff,coldiff)=($rowdiff,$coldiff)")
                return IsOnSNFractal(rowdiff,coldiff,SizeGen-1,FracGen-1,verb)
            else
                rowdiff=rem(row+1,NextSize)
                coldiff=rem(col+1,NextSize)
                if rowdiff == (NextSize-1) || coldiff == (NextSize-1) || rowdiff == 0 || coldiff == 0
                    verb && println("SizeGen>1, inner bundary")
                    return true
                else
                    verb && println("SizeGen>1, middle")
                    return false
                end
            end
        end
    end
end



function SierpinskiID(SizeGen::Integer,FracGen::Integer,Phase::Real,t::Real,LT::String,W::Real)
    return ("Sierpinski_$(LT)_sg_$(SizeGen)_fg_$(FracGen)_ph_$(Phase)_t_$(t)_W_$(W)")
end

function GetBoundarySites(SizeGen,Fracgen,LT,Side,IndVecMap=nothing)
    N=get_lattice_size(SizeGen,LT)
    if IndVecMap==nothing ###Crete the mapt if you don't have it
        println("Recreating IndVecMap")
        IndVecMap,_=GetNonZeroElements(GetTheSierpinskyLattice(SizeGen,Fracgen,LT))
    end
    EdgeSiteIndx=nothing
    #println("Select for ($LT,$Side):")
    if Side[1]=='L'
        #println("Choose side L:")
        if LT[2]=='C' || LT[2]=='G' || LT[2]=='T'
            ###Carpet,Gasket, Triangle all have a side to the left
            EdgeSiteIndx=zeros(Int,N)
            for row in 1:N
                EdgeSiteIndx[row]=IsOnLattice((row-1,0),IndVecMap)
            end
        end
    elseif Side[1]=='R'
        #println("Choose side R:")
        if LT[2]=='C' ###Carpet
            EdgeSiteIndx=zeros(Int,N)
            for row in 1:N
                EdgeSiteIndx[row]=IsOnLattice((row-1,N-1),IndVecMap)
            end
        elseif LT[2]=='G' || LT[2]=='T'
            ###There is just one point to the right for the Gasket and triangle
            EdgeSiteIndx=IsOnLattice((0,N-1),IndVecMap)
        end
    elseif Side[1]=='T'
        #println("Choose side T:")
        if LT[2]=='C' ###Carpet
            EdgeSiteIndx=zeros(Int,N)
            for col in 1:N
                EdgeSiteIndx[col]=IsOnLattice((N-1,col-1),IndVecMap)
            end
        elseif LT[2]=='G' || LT[2]=='T'
            ###There is just one point on the top of the Gasket and triangle
            EdgeSiteIndx=IsOnLattice((N-1,0),IndVecMap)
        end
    elseif Side[1]=='B'
        #println("Choose side B:")
        if LT[2]=='C' || LT[2]=='G' || LT[2]=='T'
            ###Carpet,Gasket, Triangle all have a side to the bottom
            EdgeSiteIndx=zeros(Int,N)
            for col in 1:N
                EdgeSiteIndx[col]=IsOnLattice((0,col-1),IndVecMap)
            end
        end
    end
    if EdgeSiteIndx==nothing ##Nothings been do with it
        throw(DomainError((LT,Side),"The option ($LT,$Side) not implemented"))
    end
    #println("EdgeSiteIndx for ($LT,$Side)\n",EdgeSiteIndx)
    return EdgeSiteIndx
end


####--------------------------------------
####      Methods related to chern numbers
####--------------------------------------


function SierpiskyChernStart(Params::Dict)
    ##### This funcitons can be used to figure out what site would be goo to use for a chern number calulation
    LT=Params["LT"]
    SizeGen=Params["sg"]
    if LT[2]=='C' ####Carpet
        colpoint=3^(SizeGen-1)-1//2
        rowpoint=3^(SizeGen-1)//2-1
    elseif LT[2]=='H' ####Carpet
        #### Compute by noting the recusive relation
        #### rowpoint[SizeGen]=colpoint[SizeGen-1]
        #### colpoint[SizeGen]=2*colpoint[SizeGen-1]+2*(SizeGen == 0 mod 3)
        ### And the startz
        #### rowpoint[1]=colpoint[1]=0
        ColSteps=(3. .^((SizeGen-1):-1:0)
                  .* (mod.(-(0:(SizeGen-1)),3).-1))
        RowSteps=(3. .^((SizeGen-1):-1:0)
                  .* (mod.(-((-1):(SizeGen-2)),3).-1))
        println("ColSteps: ",ColSteps)
        println("RowSteps" ,RowSteps)
        rowstart=(3^(SizeGen)-1)/2
        colstart=(3^(SizeGen)-1)/2
        println("rowstart: ",rowstart)
        println("colstart: " ,colstart)
        RowSteps[end]=RowSteps[end]*.5
        ColSteps[end]=ColSteps[end]*.5
        rowpoint=rowstart + sum(RowSteps)
        colpoint=colstart + sum(ColSteps)
    elseif LT[2]=='T' ####Triangle
        #### Compute by noting the recusive relation
        #### rowpoint[SizeGen]=colpoint[SizeGen-1]
        #### colpoint[SizeGen]=2*colpoint[SizeGen-1]+2*(SizeGen == 0 mod 3)
        ### And the start
        #### rowpoint[1]=colpoint[1]=0
        (colpoint,rowpoint)=(0,0)
        #println("n: (colpoint,rowpoint): ",1," ",(colpoint,rowpoint))
        SizeIter=2
        while SizeIter <=  SizeGen
            Addition=1*(mod(SizeIter,3)==0)
            (colpoint,rowpoint)=(2*(colpoint + Addition),colpoint)
            #println("n: (colpoint,rowpoint): ",SizeIter," ",(colpoint,rowpoint))
            SizeIter+=1
        end
        (colpoint,rowpoint)=(colpoint+1//2,rowpoint+1//2)
    elseif LT[2]=='G' ####Triangle
        #### Compute by noting the recusive relation
        #### rowpoint[SizeGen]=colpoint[SizeGen-1]
        #### colpoint[SizeGen]=2*colpoint[SizeGen-1]+4*(SizeGen == 0 mod 3)
        ### And the start
        #### rowpoint[1]=colpoint[1]=0
        (colpoint,rowpoint)=(0,0)
        #println("n: (colpoint,rowpoint): ",1," ",(colpoint,rowpoint))
        SizeIter=2
        while SizeIter <=  SizeGen
            Addition=2*(mod(SizeIter,3)==1)
            (colpoint,rowpoint)=(2*(colpoint + Addition),colpoint)
            #println("n: (colpoint,rowpoint): ",SizeIter," ",(colpoint,rowpoint))
            SizeIter+=1
        end
        rowpoint+=1
        colpoint+=0
        (colpoint,rowpoint)=(colpoint+1//2,rowpoint+1//2)
    end
    return (colpoint,rowpoint)
end 


TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close Sierpinski.jl")
