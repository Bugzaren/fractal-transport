if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open CubeLattice.jl")
TabLevel=TabLevel*"    "

function getCubeParams(Length::Integer,Width::Integer,Height::Integer;
                        t::Real=1.0,W::Real=0.0,Plot=false)
    println("Creating the lattice for $Length x $Width x $Height cube")
    ####Begin by building the lattice####
    TheLattice=ones(Bool,Length,Width,Height)
    println("Constructing lattice-vector-map $Length x $Width chain")
    IndVecMap,Ncount=GetNonZeroElements3D(TheLattice) ###Ellemtns, ordered after row and then column
    println("Make VIM Map:")
    VecIndMap=VIM_from_IVM(IndVecMap)
    #println("List of Nonzero elements:")
    #display(IndVecMap)
    println("Ncount=$Ncount")

    return Dict("Length"=>Length,"Width"=>Width,"Height"=>Height,"t"=>t,"W"=>W,
                "ParamsTuple"=>CubeParamsTuple,
                "Ham"=>do_CreateCubeHamiltonian,
                "GenHamElems"=>getCubeElems,
                "GetPossibleNeighbours"=>GetCubeNeighbours,
                "PhaseFactor"=>getCubePhaseFactor,
                "IVM"=>IndVecMap,"VIM"=>VecIndMap,
                "title"=>"Cube lattice $(Length)x$(Width)x$(Height), noise W=$W",
    "IDSTRING"=>"Cube_l_$(Length)_w_$(Width)_h_$(Height)_W_$W",
    "IDSHORT"=>"Cube_l_$(Length)_w_$(Width)_h_$(Height)_W_$W")
end

function GetCubeNeighbours((X,Y,Z),ParamsTuple::Tuple)
    'S'### Square lattice
    return ((X-1,Y,Z),#left
            (X+1,Y,Z),#right
            (X,Y-1,Z),#backward
            (X,Y+1,Z),#forward
            (X,Y,Z-1),#down
            (X,Y,Z+1))#up
end

function getCubePhaseFactor(FromPos,ToPos,Params::Dict)
    return 1.0 ###there is no phase to worry about
end


function getCubeElems(ParamsTuple::Tuple,FromPoint,ToPoint)
    (Length,Width,Height,t,W)=ParamsTuple
    HamElem=0.0
    ###Make sure these are tuples
    FromPoint=Tuple(FromPoint)
    ToPoint=Tuple(ToPoint)
    if FromPoint == ToPoint
        if W!=0.0
            HamElem=W*2*(rand()-.5)
        end
    else
        NeigbourList=GetCubeNeighbours(FromPoint,ParamsTuple)
        for Neigbours in NeigbourList
            if ToPoint == Neigbours
                HamElem = -t
                break ###Break out of the for loop
            end
        end
    end
    return HamElem
end


function CubeParamsTuple(Params::Dict)
    return  (Params["Length"],Params["Width"],Params["Height"],
             Params["t"],Params["W"])
end


function CreateCubeHamiltonian(Width::Integer,Length::Integer,Height::Integer;
                                t::Real=1.0,W::Real=0.0,Plot=false)
    Params=getCubeParams(Length,Width,Height,t=t,W=W)
    do_CreateCubeHamiltonian(Params;Plot=Plot)
end

function do_CreateCubeHamiltonian(Params;Plot=false)
    Length=Params["Length"]
    Width=Params["Width"]
    Height=Params["Height"]
    t=Params["t"]
    W=Params["W"]
    IndVecMap=Params["IVM"]
    Ncount=size(IndVecMap)[2]
    GetNiegbours=Params["GetPossibleNeighbours"]
    ParamsTuple=Params["ParamsTuple"](Params)
    
    println("Creating empty hamiltonian for $Length x $Width x $Height cube with noise strength W=$W")
    StartTime=now()

    Hamiltonian = zeros(Complex,Ncount,Ncount)
    
    ###Empty hamiltonian
    ###Populate with hopping elements

    println("Building hamiltonian for $Length x $Width x $Height chain")
    TIMESTRUCT=TimingInit()
    for j in 1:Ncount
        ####Add diaognal element
        Hamiltonian[j,j]=W*2*(rand()-.5)

        (X,Y,Z)=IndVecMap[:,j]
        NeigbourList=GetNiegbours((X,Y,Z),ParamsTuple)
        #println("NeigbourList:",NeigbourList)
        for Neigbours in NeigbourList
            j2=IsOnLattice(Neigbours,IndVecMap)
            if j2!=nothing
                Hamiltonian[j,j2] = -t
            end
        end
        TIMESTRUCT=TimingProgress(TIMESTRUCT,j,Ncount;Message="j=$j (X,Y,Z)=($X,$Y,$Z)")
    end
    
    EndTime=now()
    DiffTime=PrettyTime(EndTime-StartTime)
    println("It took $DiffTime to complete the hamiltonian")

    HamSize=size(IndVecMap)[2]
    #if false #### disable thats below
    if HamSize < 20 && Plot
        println("The hamiltonian")
        display(Hamiltonian)
        println()
    end
    
    if HamSize < 1000 && Plot
        @eval using PyPlot
        println("Plot the hamiltonian")
        PyPlot.figure()
        PyPlot.imshow(abs.(Hamiltonian))
        PyPlot.title("The elements of the hamiltonian")
        PyPlot.colorbar()
    end
    
    if Length*Width < 100 && Plot && false
        PlotHamiltonian3D(Hamiltonian,IndVecMap)
    end

    return (Hamiltonian,IndVecMap)
end


TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close CubeLattice.jl")
