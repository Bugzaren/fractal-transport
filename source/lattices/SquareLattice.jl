if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open SquareLattice.jl")
TabLevel=TabLevel*"    "


function ChainSlice(Width::Integer,Phase::Real=0;t::Real=1.0,W::Real=0.0)
    return diagm(0=> (W*2).*(rand(Width) .- .5),1=>ones(Width-1)*(-t),-1=>ones(Width-1)*(-t))
end

function ChainHopping(Width::Integer,Phase::Real=0;t::Real=1.0,W::Real=0.0)
    return diagm(0=>-t.*exp.((im*pi*2*Phase).*(0:(Width-1))))
end

function getChainParams(Length::Integer,Width::Integer,Phase::Real=0;
                        t::Real=1.0,W::Real=0.0,Plot=false,minimal=false)

    println("Creating the lattice for $Length x $Width chain")
    ####Begin by building the lattice####
    TheLattice=ones(Bool,Width,Length)
    #display(TheLattice)
    if Plot
        figure()
        imshow(TheLattice[end:-1:1,:])#,extent=[-.5,Length-.5,-.5,Width-.5])
        title("The available lattice points")
        #colorbar()
    end
    println("Constructing lattice-vector-map $Length x $Width chain")
    IndVecMap,Ncount=GetNonZeroElements(TheLattice) ###Ellemtns, ordered after row and then column
    VecIndMap=VIM_from_IVM(IndVecMap)
    #println("List of Nonzero elements:")
    display(IndVecMap)
    println("Ncount=$Ncount")

    if minimal
        Dictionary=Dict("Length"=>Length,"Width"=>Width,"Phase"=>Phase,"t"=>t,"W"=>W,
                        "ParamsTuple"=>ChainParamsTuple,
                        "GenHamElems"=>getChainElems,
                        "GetPossibleNeighbours"=>GetChainNeighbours,
                        "PhaseFactor"=>getChainPhaseFactor, ###FIXME
                        ###This one should not be nececarry  if the
                        ### ConstructChainHamiltonian would have worked properly
                        "IVM"=>IndVecMap,
                        "VIM"=>VecIndMap)
    else
        Dictionary=Dict("Length"=>Length,"Width"=>Width,"Phase"=>Phase,"t"=>t,"W"=>W,
                        "ParamsTuple"=>ChainParamsTuple,
                        "Ham"=>do_CreateChainHamiltonian,
                        "HamGen"=>getChainSlices,
                        "GenHamElems"=>getChainElems,
                        "PhaseFactor"=>getChainPhaseFactor,
                        "GetPossibleNeighbours"=>GetChainNeighbours,
        "IVM"=>IndVecMap,
        "VIM"=>VecIndMap,
        "SlizeSizes"=>fill(Width,Length),
        "title"=>"Sqaure lattice $(Length)x$(Width), phase=$Phase, noise W=$W",
        "IDSTRING"=>"Chain_l_$(Length)_w_$(Width)_ph_$(Phase)_W_$W",
        "IDSHORT"=>"Chain_l_$(Length)_w_$(Width)")
    end
    return Dictionary
end
    
    
function GetChainNeighbours((row,col),ParamsTuple::Tuple)
    'S'### Square lattice
    return ((row,col-1), #backward
            (row,col+1),#forward
            (row-1,col),#down
            (row+1,col))#up
end

function getChainElems(ParamsTuple::Tuple,FromPoint,ToPoint)
    (Length,Width,Phase,t,W)=ParamsTuple
    HamElem=0.0
    if FromPoint == ToPoint
        if W!=0.0
            HamElem=W*2*(rand()-.5)
        end
    else
        (FRow,FCol)=FromPoint
        (TRow,TCol)=ToPoint
        if FRow==TRow && (FCol-1)==TCol # (backward)
            HamElem = -t*exp(im*pi*FRow*2*Phase)
        elseif FRow==TRow && (FCol+1)==TCol # (forward)
            HamElem = -t*exp(-im*pi*FRow*2*Phase)
        elseif (FRow-1)==TRow && FCol==TCol # (down) 
            HamElem = -t
        elseif (FRow+1)==TRow && FCol==TCol # (up) 
            HamElem = -t
        end
    end
    return HamElem
end


function ChainParamsTuple(Params::Dict)
    return  (Params["Length"],Params["Width"],Params["Phase"],Params["t"],Params["W"])
end


function getChainPhaseFactor(FromPos,ToPos,Params)
    PhaseFactor(-1.0,FromPos,ToPos,Params["Phase"],-1)
end


function getChainSlices(FromCol::Integer,ToCol::Integer,Params)
    Length=Params["Length"]
    Width=Params["Width"]
    Phase=Params["Phase"]
    t=Params["t"]
    W=Params["W"]
    #println("FromCol,ToCol=$FromCol,$ToCol")
    if FromCol >= Length || FromCol < 0 || ToCol >= Length || ToCol < 0
        throw(ErrorException("Trying to get colums ($FromCol,$ToCol) outside of chain"))
    elseif FromCol==ToCol
        return ChainSlice(Width,Phase,t=t,W=W)
    elseif FromCol== (ToCol +1)
        return ChainHopping(Width,Phase,t=t,W=W)
    elseif FromCol== (ToCol - 1)
        return ChainHopping(Width,Phase,t=t,W=W)'
    else
        return diagm(0=>ones(Width)*0)
    end
end

function getChainSize(Params)
    Length=Params["Length"]
    Width=Params["Width"]
    return fill(Width,Length)
end



function CreateChainHamiltonian(Length::Integer,Width::Integer,Phase::Real=0;
                                t::Real=1.0,W::Real=0.0,Plot=false)
    Params=getChainParams(Length,Width,Phase,t=t,W=W)
    do_CreateChainHamiltonian(Params;Plot=Plot)
end

function do_CreateChainHamiltonian(Params;Plot=false)
    Length=Params["Length"]
    Width=Params["Width"]
    Phase=Params["Phase"]
    t=Params["t"]
    W=Params["W"]
    IndVecMap=Params["IVM"]
    Ncount=size(IndVecMap)[2]
    
    println("Creating empty hamiltonian for $Length x $Width chain, ph=$Phase with noise strength W=$W")
    StartTime=now()

    Hamiltonian = zeros(Complex,Ncount,Ncount)
    
    ###Empty hamiltonian
    ###Populate with hopping elements

    println("Building hamiltonian for $Length x $Width chain, ph=$Phase")
    TIMESTRUCT=TimingInit()
    for j in 1:Ncount
        ####Add diaognal element
        Hamiltonian[j,j]=W*2*(rand()-.5)
        #println("----------------")
        (row,col)=IndVecMap[:,j]
        j2=IsOnLattice((row,col-1),IndVecMap) # (backward)
        if j2!=nothing
            #println("backward j2=$j2")
            Hamiltonian[j,j2] = -t*exp(im*pi*row*2*Phase)
        end
        j2=IsOnLattice((row,col+1),IndVecMap) ## (forward)
        if j2!=nothing
            #println("forward j2=$j2")
            Hamiltonian[j,j2] = -t*exp(-im*pi*row*2*Phase)
        end
        j2=IsOnLattice((row-1,col),IndVecMap)# (down)
        if j2!=nothing
            #println("down j2=$j2")
            Hamiltonian[j,j2] = -t
        end
        j2=IsOnLattice((row+1,col),IndVecMap)  # (up)
        if j2!=nothing
            #println("up j2=$j2")
            Hamiltonian[j,j2] = -t
        end
        TIMESTRUCT=TimingProgress(TIMESTRUCT,j,Ncount;Message="j=$j row=$row col=$col")
    end

    EndTime=now()
    DiffTime=Dates.canonicalize(Dates.CompoundPeriod(Dates.Millisecond(EndTime-StartTime)))
    println("It took $DiffTime to complete the hamiltonian")

    HamSize=size(IndVecMap)[2]
    #if false #### disable thats below
    if HamSize < 10 && Plot
        println("The hamiltonian")
        display(Hamiltonian)
        println()
    end
    
    if HamSize < 500 && Plot
        figure()
        imshow(abs.(Hamiltonian))
        title("The elements of the hamiltonian")
        colorbar()
    end
    
    if Length*Width < 100 && Plot
        PlotHamiltonian(Hamiltonian,IndVecMap)
    end
    
    return (Hamiltonian,IndVecMap)
end


TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close SquareLattice.jl")
