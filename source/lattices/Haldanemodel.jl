if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open Haldanemodel.jl")
TabLevel=TabLevel*"    "

### TODO: Ask, which of these packages are necessary and what they do

using LinearAlgebra
using Dates
using StatsBase
using Random
using Test
using CRC32c

include("../core/Misc.jl")
#include("../core/RecursiveGreens.jl")
include("../core/PlotMisc.jl")


function TopologyParameters(DesiredChernNumber)
    if DesiredChernNumber==0
        (5,0.5,1.0,.25)
    elseif DesiredChernNumber==1
        (-2.5,0.5,1.0,.25)
    elseif DesiredChernNumber==2
        (-1.5,0.5,1.0,.25)
    else
        throw(DomainError("This chern number is never realized"))
    end
end



#TheLattice=GetTheLattice(SizeGen,FracGen,LT) ###Plot if exists
#if Plot
#    figure()
#    imshow(TheLattice[end:-1:1,:],extent=[-.5,N-.5,-.5,N-.5],cmap="gnuplot2_r")
#    title("The available lattice points")
#    #colorbar()
#end
#if Verbose     
#    println("Constructing lattice-vector-map $LT at for sg=$SizeGen, fg=$FracGen")
#end

    

function getHaldaneParams(Length::Integer,Width::Integer;
                          B0=-2.5,B1=0.5 , B2=1. , B3= 0.25,
                          Plot=false,Verbose=false)

    if Verbose

        println("Creating the lattice for $Length x $Width chain")
    end
    ###Begin by building the lattice####
    ### This needs to be adjusted for fractal, only square lattice supported 

    TheLattice=ones(Bool,Width,Length)
    println("lattice done")
    #display(TheLattice)
    if Plot
        figure()
        imshow(TheLattice[end:-1:1,:],extent=[-.5,N-.5,-.5,N-.5],cmap="gnuplot2_r")
        title("The available lattice points")
        #colorbar()
    end
    if Verbose
        println("Constructing lattice-vector-map $Length x $Width Square lattice")
    end

    ###Elements, ordered after row and then column then orbital, function is found in Misc.jl
    IVM2D,Ncount=GetNonZeroElements(TheLattice)
    IVM2D=IVM2D .+ 1 ###The GetNonZeroElemets starts counting at zero
    #println("IVM2D")
    #display(IVM2D)
    VIM2D=VIM_from_IVM(IVM2D)[1]
    IndVecMap=DoubleIVM(IVM2D)
    #println("IndVecMap")
    #display(IndVecMap)
     ### maybe later a 3D-Version of this is useful?
    VecIndMap=VIM_from_IVM(IndVecMap)
    #println("List of Nonzero elements:")
    #display(IndVecMap)
    #println("VIM")
    #display(VecIndMap) 
    ### GaugeChoise=GaugeChoiseToNum(Gauge) ### TODO: Do I  Need this? And where?
    
    ### t is the coupling strength to the Leads
    Params=Dict("Length"=>Length,"Width"=>Width,"B0"=>B0,"B1"=>B1,"B2"=>B2,"B3"=>B3,"ParamsTuple"=>HaldaneParamsTuple)
    ###Load the functions related to the parameters
    #    Params["ParamsTuple"]=HaldaneParamsTuple
    Params["ParamsNames"]=HaldaneParamsNames			
    Params["IVM2D"]=IVM2D
    Params["IVM"]=IndVecMap								
    #Params["IVMtoIPM"]=get_Sierpinski_IPM				## TODO I do not understand this function
    Params["VIM"]=VecIndMap								
    Params["VIM2D"]=VIM2D
    Params["GenHamElems"]=getHamElems					### this is way simpler in my version than in the sierpinsky-file, am I missing something?
    Params["GetPossibleNeighbours"]=GetHaldaneNeighbours		### return 3D points, maybe 2D would be enough?
    ####Load some of the names that can be extracted
    Params["title"]=("Haldane, Square Lattice")

    #Params["IDSTRING"]=SierpinskyID(SizeGen,FracGen,Phase,t,LT,W) ### TODO


    Params["IDSHORT"]="Haldane_Square"
    return Params
end


function HaldaneParamsNames(Params)

    return  ("B0","B1","B2","B3","t")
end
    

function HaldaneParamsTuple(Params::Dict)
    return  (Params["Length"],Params["Width"],Params["B0"],Params["B1"],Params["B2"],Params["B3"],Params["VIM2D"])
end

### right now this function never gets called, may be useful for plotting a hamiltonian though
function do_CreateCentralHamiltonian(Params;Plot=false)
    L=Params["Length"]
    W=Params["Width"]
    B0=Params["B0"]
    B1=Params["B1"]
    B2=Params["B2"]
    B3=Params["B3"]
    VIM=Params["VIM2D"]
    IndVecMap=Params["IVM2D"]
    

    println("Creating empty hamiltonian for $Length x $Width central region, B0=$B0 , B1=$B1 , B2=$B2 , B3=$B3")
    StartTime=now()
    ###Factor 2 because we have 2 orbitals
    Hamiltonian = zeros(Complex,2*Ncount,2*Ncount)

    ###Empty hamiltonian
    ###Populate with hopping elements

    println("Building hamiltonian for $Length x $Width central region, B0=$B0 , B1=$B1 , B2=$B2 , B3=$B3")
    TIMESTRUCT=TimingInit()
    for row=1:W, col=1:L
        ####Add diaognal element
        j=W*(col-1)+row
        Hamiltonian[2j-1,2j-1]=B0
        Hamiltonian[2j,2j]=-B0
        #println("----------------")
        k=j-W # (backward)
        if site_exists((row,col-1),VIM)
            #println("backward k=$k")
            Hamiltonian[2j-1,2k-1]=B1
            Hamiltonian[2j-1,2k] =im*B3
            Hamiltonian[2j,2k-1]=im*B3
            Hamiltonian[2j,2k]=-B1
        end
        k=j+W ## (forward)
        if site_exists((row,col+1),VIM)
            #println("forward k=$k")
            Hamiltonian[2j-1,2k-1]=B1
            Hamiltonian[2j-1,2k] =-im*B3
            Hamiltonian[2j,2k-1]=-im*B3
            Hamiltonian[2j,2k]=-B1

        end
        k=j-1# (down)
        if site_exists((row-1,col),VIM)
            #println("down k=$k")
            Hamiltonian[2j-1,2k-1]=B1
            Hamiltonian[2j-1,2k] =-B3
            Hamiltonian[2j,2k-1]=B3
            Hamiltonian[2j,2k]=-B1
        end
        k=j+1  # (up)       
        if site_exists((row+1,col),VIM)
        #println("up k=$k")
            Hamiltonian[2j-1,2k-1]=B1
            Hamiltonian[2j-1,2k] =B3
            Hamiltonian[2j,2k-1]=-B3
            Hamiltonian[2j,2k]=-B1
        end
        k=j+W+1 #(up right)
        if site_exists((row+1,col+1),VIM)
            #println("up right k=$k")
            Hamiltonian[2j-1,2k-1]=B2
            Hamiltonian[2j-1,2k] =B3*(-1-im)
            Hamiltonian[2j,2k-1]=B3*(1-im)
            Hamiltonian[2j,2k]=-B2
        end
        k=j-W+1  # (up left)
        if site_exists((row+1,col-1),IVM)
            #println("up left k=$k")
            Hamiltonian[2j-1,2k-1]=B2
            Hamiltonian[2j-1,2k] =B3*(1-im)
            Hamiltonian[2j,2k-1]=B3*(-1-im)
            Hamiltonian[2j,2k]=-B2
        end
        k=j-1+W  # (down right)
        if site_exists((row-1,col+1),IVM)
            #println("down right k=$k")
            Hamiltonian[2j-1,2k-1]=B2
            Hamiltonian[2j-1,2k] =B3*(-1+im)
            Hamiltonian[2j,2k-1]=B3*(1+im)
            Hamiltonian[2j,2k]=-B2
        end
        k=j-1-W  # (down left)
        if site_exists((row-1,col-1),VIM)
            Hamiltonian[2j-1,2k-1]=B2
            Hamiltonian[2j-1,2k] =B3*(1+im)
            Hamiltonian[2j,2k-1]=B3*(-1+im)
            Hamiltonian[2j,2k]=-B2
        end
        TIMESTRUCT=TimingProgress(TIMESTRUCT,j,Ncount;Message="j=$j row=$row col=$col")
    end

    EndTime=now()
    DiffTime=Dates.canonicalize(Dates.CompoundPeriod(Dates.Millisecond(EndTime-StartTime)))
    println("It took $DiffTime to complete the hamiltonian")
    ## size(IndVecMap)=Ncount*Ncount, 2 orbitals->2Ncount*2Ncount
    HamSize=4*size(IndVecMap)[2]
    #if false #### disable thats below
    if HamSize < 10 && Plot
        println("The hamiltonian")
        display(Hamiltonian)
        println()
    end

    if HamSize < 500 && Plot
        figure()
        imshow(abs.(Hamiltonian))
        title("The elements of the hamiltonian")
        colorbar()
    end

    if Length*Width < 100 && Plot
        PlotHamiltonian(Hamiltonian,IndVecMap)
    end

    return (Hamiltonian,IndVecMap)
end

function getHamElems(ParamsTuple,FromPoint,ToPoint)
    (Length,Width,B0,B1,B2,B3,VIM2D)=ParamsTuple
	(FRow,FCol,FOrb)=FromPoint
    (TRow,TCol,TOrb)=ToPoint
    
    HamElem=im*0.
    ### if site where we want to hop onto/out of does not exist, the HamElem will not be 0
    if (!site_exists(ToPoint,VIM2D)) || (!site_exists(FromPoint,VIM2D)) 
        return HamElem
    else
        if FromPoint==ToPoint 
		    if FOrb==1
			    HamElem=B0+0.1*(2*rand()-1)
		    else
			    HamElem=-B0+0.1*(2*rand()-1)
		    end
	    elseif FRow==TRow && (FCol-1)==TCol # (backward)
		    if FOrb==1 && TOrb==1
			    HamElem=B1
    		elseif FOrb==1 && TOrb==2
	    		HamElem=im*B3
		    elseif FOrb==2 && TOrb==1
			    HamElem=im*B3
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B1
		    end
    	elseif FRow==TRow && (FCol+1)==TCol # (forward)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B1
		    elseif FOrb==1 && TOrb==2
			    HamElem=-im*B3
		    elseif FOrb==2 && TOrb==1
			    HamElem=-im*B3
		    elseif FOrb==2 && TOrb==2
			    HamElem=-B1
		    end
	    elseif (FRow-1)==TRow && FCol==TCol # (down)
		    if FOrb==1 && TOrb==1
			    HamElem=B1
    		elseif FOrb==1 && TOrb==2
	    		HamElem=-B3
		    elseif FOrb==2 && TOrb==1
			    HamElem=B3
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B1
		    end
    	elseif (FRow+1)==TRow && FCol==TCol # (up)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B1
    		elseif FOrb==1 && TOrb==2
	    		HamElem=B3
		    elseif FOrb==2 && TOrb==1
			    HamElem=-B3
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B1
		    end    
    	elseif(FRow+1)==TRow && (FCol+1)==TCol           ##(up+right)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B2
    		elseif FOrb==1 && TOrb==2
	    		HamElem=B3*(-1-im)
	    	elseif FOrb==2 && TOrb==1
		    	HamElem=B3*(1-im)
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B2
		    end
    	elseif(FRow+1)==TRow && (FCol-1)==TCol      ##(up+left)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B2
    		elseif FOrb==1 && TOrb==2
	    		HamElem=B3*(1-im)
		    elseif FOrb==2 && TOrb==1
			    HamElem=B3*(-1-im)
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B2
		    end
    	elseif(FRow-1)==TRow && (FCol+1)==TCol      ##(down+right)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B2
    		elseif FOrb==1 && TOrb==2
	    		HamElem=B3*(-1+im)
		    elseif FOrb==2 && TOrb==1
			    HamElem=B3*(1+im)
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B2
		    end
    	elseif(FRow-1)==TRow && (FCol-1)==TCol      ##(down+left)
	    	if FOrb==1 && TOrb==1
		    	HamElem=B2
    		elseif FOrb==1 && TOrb==2
	    		HamElem=B3*(1+im)
		    elseif FOrb==2 && TOrb==1
			    HamElem=B3*(-1+im)
    		elseif FOrb==2 && TOrb==2
	    		HamElem=-B2
		    end
    	end
        return HamElem
    end
end


function DoubleIVM(IVM)
    NCount=size(IVM)[2]
    IndVecMap=zeros(Int32,3,2*NCount)
    IndVecMap[3,1:NCount].=1
    IndVecMap[3,(1+NCount):(2*NCount)].=2
    IndVecMap[1:2,1:NCount]=IVM ###Copy the IVM
    IndVecMap[1:2,(1+NCount):(2*NCount)]=IVM ###Copy the IVM
    #println("IndVecMap")
    #display(IndVecMap)
    sort_IVM!(IndVecMap) ###Sort the IVM in place
    return IndVecMap
end
	
	
### in case 2D version works:
function GetHaldaneNeighbours((row,col))
	## may need to be adjusted to include orbitals 
        return ((row,col-1), #backward
                (row,col+1),#forward
                (row-1,col),#down
                (row+1,col), #up
                (row+1,col+1),#up+forward
                (row+1,col-1),#up+backward
                (row-1,col+1),#down+forward
                (row-1,col-1))#down+backward               

end
### in case 3D version is needed

function GetHaldaneNeighbours((row,col,orb),_)
    return((row,col-1,1),(row,col-1,2), #backward
                (row,col+1,1), (row,col+1,2),#forward
                (row-1,col,1),(row-1,col,2),#down
                (row+1,col,1),(row+1,col,2),  #up
                (row+1,col+1,1),(row+1,col+1,2),#up+forward
                (row+1,col-1,1),(row+1,col-1,2),#up+backward
                (row-1,col+1,1),(row-1,col+1,2),#down+forward
                (row-1,col-1,1),(row-1,col+1,2))#down+backward               
            
end     


function site_exists(Point,VIM)
    ## VIM has a non-zero entry at location(row,col) if the site exists and zero otherwise
    
    ### check first if point is allowed on square lattice 
    (Row,Col,Orb)=Point
    (W,L)=size(VIM) ###The L and W are just the dimentions of the lattice
    ### do we need an offset?
    if (Row < 1) || (Row > W)
        return false
    elseif (Col < 1) || (Col > L)
        return false
    elseif VIM[Row,Col]==0 
        ## in a square lattice this should never happen,  in a fractal it will be where the holes are
        return false
    else
        return true
    end
end

function FindPotentials(L1:: Int,L2:: Int, TransMission; I0=1)
    ## L1, L2 are leads through which the current is non-zero, I0 is the strength of that current
  
  
    Energies=size(TransMission)[1]
    ### can fix one of them, for now fix V[1], second index is the energy value
    V=fill(0.,Energies,4)
    
    V0=fill(0.,3)
    if L1==L2
        println("error: both leads are the same")
    end
    eps=0.0001 # margin of numerical error used for sanity checks
    ### set current values, they should never change
    Idummy=fill(0.,3)
    ### dummy current for the calculation
    I=fill(0.,4)
    if L1!=4
        Idummy[L1]=I0
    end
    if L2!=4
        Idummy[L2]=-I0
    end
    I[L1]=I0
    I[L2]=-I0
    #### if there is no transport return
    for w=1:Energies ### runs over energy values
        ### disregard last line such that M becomes invertible, this corresponds to setting V[4,w]=0
        M=fill(0.,3,3)



 
        for j=1:3, k=1:3,
            M2=0
            if j==k
                for l=1:4
                    M2+=TransMission[w,j,l]
                end
            end     
            M[j,k]=-TransMission[w,j,k]+M2
        end              
        if det(M)==0
            println("Matrix to be inverted at energystep w=$w has determinant 0")
            println(M) 
            return fill(0.,Energies,4)
        end
        ### next calculate the 3 potentials, not yet shifted
        V0=inv(M)*Idummy
        ### testvalue for V[4], first set up  matrix
        

        ### todo: this makes no sense this way, I[4] is fixed at 0,might as well leave it out, also no absolute values???? FIXME
        test=I[4]-(TransMission[w,4,1]*V0[1]+TransMission[w,4,2]*V0[2]+TransMission[w,4,3]*V0[3])
        if test>eps
            println("result not within numerical margin")
            println("error size = $test")
        end
        

    
        ## shift such that V[a1]=0
        if L1==4 ### no shift neccessary, V[4]=0
            for j=1:3
                V[w,j]=V0[j]
            end
            V[w,4]=0
        else ### need to shift
             
            for j=1:3
                V[w,j]=V0[j]-V0[L1]
            end
            ## set fourth term
            V[w,4]=-V0[L1]
            
        end
    end



    return V

end

function DoubleLeadMatrix(LeadHamiltonian)

Width=size(LeadHamiltonian)[1]## should be a square matrix
RealHamiltonian=fill(im*0., 2*Width,2*Width)
    for j=1:Width,k=1:Width
        RealHamiltonian[2*j,2*k]=LeadHamiltonian[j,k]
        RealHamiltonian[2*j-1,2*k-1]=LeadHamiltonian[j,k]
    end
    return RealHamiltonian
end





function BuildLeadHamiltonian(LeadAttachmentPoints,j,t)

    leadsize=size(LeadAttachmentPoints[j])[1]
    if t==0
       t=1
    end     
    Hamiltonian=DoubleLeadMatrix(BuildHamiltonian(getChainParams(div(leadsize,2),1,t)))

   println("Hamiltonian in Lead $j with leadsize $leadsize")
   display(Hamiltonian)
    return Hamiltonian
end




function BuildLeadToLeadHopping(LeadAttachmentPoints,j,t)
    leadsize=size(LeadAttachmentPoints[j])[1]
    if t==0
        t=1
    end

    Hopping=fill(0., leadsize, leadsize)
    for i=1:leadsize
        Hopping[i,i]=-t
    end
    return Hopping
end



TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close Haldanemodel.jl")
