if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open GenericHamiltonian.jl")
TabLevel=TabLevel*"    "

sum(LOAD_PATH.==pwd())==0 && push!(LOAD_PATH, pwd()) ###Push the current direcoty into the path

###This file defines the functions nececarry to diagponalise the serpinsly carpet, gaskes and triangle....

println("Including GenericHamiltonian.jl")

using ProgressTimer
using LinearAlgebra


include("../core/Misc.jl")
include("../core/PlotMisc.jl")
include("../lattices/LatticeMisc.jl")

###These two come teogether
function GenericHamParamsTuple(Params)
    return  (Params["Ham"],Params["NeigbourList"],Params["IPM"])
end

function GenericHamParamsNames(Params)
    return  ("Ham","NeigbourList","IPM")
end

function getGenericHamParams(HamiltonianIn;IVM=nothing,IPM=nothing,
                             HamDim=nothing)
    if HamDim!=nothing && (HamDim,HamDim) != size(HamiltonianIn)
        throw("the Hamiltonian dimention $(size(HamiltonianIn)), does not match the desired one ($HamDim,$HamDim)")
    end
            
    if length(size(HamiltonianIn))!=2
        throw("Hamiltonian with size $(size(HamiltonianIn)) is not a 2D-matrix")
    end
    if size(HamiltonianIn)[1] != size(HamiltonianIn)[2]
        throw("The two dimentions of the hamiltonian $(size(HamiltonianIn)) are not the same")
    end
    if !ishermitian(HamiltonianIn)
        throw("Input Hamiltonian is not hermittian! Sorry i don't like this....")
    end

    HamDim=size(HamiltonianIn)[1]
    ###The IVM can cimply label the elemts of the hamiltonian
    if IVM==nothing
        IVM=[x for x in 1:HamDim]
    else
        if size(IVM)[end]!= HamDim
            throw("The IVM does not have the same numbe rof elements as the hamiltonian?")
        end
        validate_IVM(IVM)
    end
    ##The Neighbor list is a list of list
    NeigbourList=Array{Array{Int64, 1}}(undef,HamDim)
    ##loop over all the ments an extract the neigbours
    for i in 1:HamDim
        TmpList = findall(!iszero, HamiltonianIn[:,i])
        ### [:,j] since julia is collumn stored ###
        ## Since Ham i hermittian both direcitons are fine ##
        TmpList = TmpList[findall(TmpList .!= i)]
        NeigbourList[i] = TmpList
    end
    #println("NeigbourList:")
    #display(NeigbourList)
    ###Load the parameters
    Params=Dict("Ham"=>copy(HamiltonianIn),
                "NeigbourList"=>NeigbourList,
                "ParamsTuple"=>GenericHamParamsTuple)
    ###Load the functions related to the parameters
    if IPM==nothing
        Params["IPM"]=nothing
    else
        if size(IVM)[end] != size(IPM)[end]
            throw("The IVM $(size(IVM)) does not have the same size as the IPM $(size(IPM))?!")
        end
        Params["IPM"]=copy(IPM)
    end
    Params["ParamsNames"]=GenericHamParamsNames
    Params["IVM"]=IVM
    Params["IVMtoIPM"]=get_GenericHam_IPM
    Params["GenHamElems"]=getGenericHamElems
    Params["GetPossibleNeighbours"]=GetGenericHamNeighbours
    ####Load some of the names that can be extracted
    return Params
end

function get_GenericHam_IPM(IVM,ParamsTuple::Tuple)
    ##Add dummy y-variable
    IPM = ParamsTuple[3]
    if IPM == nothing
        IPM = fill(0.0,2,length(IVM))
        IPM[1,:]=IVM
    end
    return IPM
end

function GetGenericHamNeighbours(indx,ParamsTuple::Tuple)
    return ParamsTuple[2][indx]
end

function getGenericHamElems(ParamsTuple::Tuple,FromPoint,ToPoint)
    #println("FromPoint:",FromPoint)
    #println("ToPoint:",ToPoint)
    (Ham,_)=ParamsTuple
    #println("Ham:",Ham)
    return Ham[FromPoint,ToPoint] ###Return the ham elements
end

println("Leaving GenericHamiltonian.jl")
TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close GenericHamiltonian.jl")
