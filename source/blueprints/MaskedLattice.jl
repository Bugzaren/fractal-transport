if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open MaskedLattice.jl")
TabLevel=TabLevel*"    "

###This file defines the functions nececarry to diagponalise the serpinsly carpet, gaskes and triangle....

using ProgressTimer
using Random
using PyPlot

include("../core/Misc.jl")
#include("../core/RecursiveGreens.jl")
include("../core/PlotMisc.jl")
include("../lattices/LatticeMisc.jl")

###These two come teogether
function MaskedLatticeParamsTuple(Params)
    return  (Params["Mask"],Params["LT"],Params["W"],Params["t"],Params["ph"],Params["GaugeChoise"])
end

function MaskedLatticeParamsNames(Params)
    return  ("Mask","LT","W","t","ph","GaugeChoise")
end

function getMaskedLatticeParams(TheLatticeMaskIn::Array{Bool,2};
                                Phase::Real=0.0,t::Real=1.0,W::Real=0.0,
                                LT='S',Gauge="LandauY",Plot=false,Verbose=false)
    println("LatticeType: ",LT)
    LT = MakeChar(LT)
    
    TheLatticeMask=copy(TheLatticeMaskIn)
    if Plot
        figure()
        imshow(TheLatticeMask[end:-1:1,:],cmap="gnuplot2_r")
        title("The available lattice points")
        #colorbar()
    end

    ###Ellemtns, ordered after row and then column
    IndVecMap,Ncount=GetNonZeroElements(TheLatticeMask) 
    GaugeChoise=GaugeChoiseToNum(Gauge)

    ###Load the parameters
    Params=Dict("Mask"=>TheLatticeMask,
                "ph"=>Phase,"t"=>t,
                "W"=>W,"LT"=>LT,
                "GaugeChoise"=>GaugeChoise)
    ###Load the functions related to the parameters
    Params["ParamsTuple"]=MaskedLatticeParamsTuple
    Params["ParamsNames"]=MaskedLatticeParamsNames
    Params["IVM"]=IndVecMap
    Params["IVMtoIPM"]=get_MaskedLattice_IPM
    Params["GenHamElems"]=getSMaskedLatticeElems
    Params["GetPossibleNeighbours"]=GetMaskedLatticeNeighbours
    ####Load some of the names that can be extracted
    return Params
end

function get_MaskedLattice_IPM(IVM,ParamsTuple::Tuple)
    LT=ParamsTuple[2]
    if LT=='T'
        IPM = get_triangular_IPM(IVM)
    elseif LT=='R'
        IPM = get_anti_triangular_IPM(IVM)
    elseif LT=='X' || LT=='S'
        IPM = IVM .* 1.0
    end
    return IPM
end

function GetMaskedLatticeNeighbours((row,col),ParamsTuple::Tuple)
    LT=ParamsTuple[2]
    return GetLatticeNeighbours((row,col),LT)
end

function getSMaskedLatticeElems(ParamsTuple::Tuple,FromPoint,ToPoint)
    #println("FromPoint:",FromPoint)
    #println("ToPoint:",ToPoint)
    #println("Params:",Params)
    (Mask,LT,W,t,Phase,GaugeChoise)=ParamsTuple
    HamElem=0.0
    if FromPoint == ToPoint
        if W!=0.0
            HamElem=W*2*(rand()-.5)
        end
    else
        if  MaskedLatticeBondIsPresennt(Mask,FromPoint,ToPoint,LT)
            HamElem=PhaseFactor(t,ToPoint,FromPoint,Phase,GaugeChoise)
        end
    end
    return HamElem
end

function MaskedLatticeBondIsPresennt(Mask,FromPoint,ToPoint,LT)
    ###Check that The sites exist
    if Mask[FromPoint[1]+1,FromPoint[2]+1] && Mask[ToPoint[1]+1,ToPoint[2]+1]
        return is_bond_allowed(FromPoint[1],FromPoint[2],ToPoint[1],ToPoint[2],LT)
    else
        throw(DomainError([FromPoint,ToPoint],"The From=$FromPoint or To=$ToPoint points are not on the map."))
    end
end

function MakeChar(LT)
    if typeof(LT) == Char ###Chack correct type
        return LT
    else
        if typeof(LT) == String
            if length(LT) == 1
                return LT[1]
            end
        end
    end

    throw(DomainError(LT,"Lattice type should be a single character"))
    return LT
end


TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close MaskedLattice.jl")
