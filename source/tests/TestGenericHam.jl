if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open TestGenericHam.jl")
TabLevel=TabLevel*"    "

using Test
using PyPlot ###Delberately not put in Misc.jl
using SparseArrays


include("MiscTest.jl")
include("../blueprints/GenericHamiltonian.jl")

function TestGenHamiltonian()
    @testset "Test Generic Hamiltonian blueprint" begin
        @testset "Test Hamiltonian is reproduced" begin
            @test reproduce_ham(1,1.0)
            @test reproduce_ham(1,0.0)
            @test reproduce_ham(10,1.0)
            @test reproduce_ham(10,0.5)
            @test reproduce_ham(20,1.0)
            @test reproduce_ham(20,0.1)
            @test reproduce_ham(40,0.01)
        end
        @testset "Test space Hamiltonian is reproduced" begin
            @test reproduce_sparce_ham(1,1)
            @test reproduce_sparce_ham(1,0)
            @test reproduce_sparce_ham(10,100)
            @test reproduce_sparce_ham(10,10)
            @test reproduce_sparce_ham(20,100)
            @test reproduce_sparce_ham(20,10)
            @test reproduce_sparce_ham(40,10)
        end
    end
end



function reproduce_ham(HamDim,ElemProb)
    #ElemPos = nothing
    ElemPos = 10 .*rand(2,HamDim)
    RandElems=(-.5 .+ rand(HamDim,HamDim)) .+ im*(-.5 .+ rand(HamDim,HamDim))
    ChooseElems=rand(HamDim,HamDim) .< ElemProb
    Ham = RandElems .* ChooseElems
    Ham = Ham + Ham'

    Params = getGenericHamParams(Ham,IPM=ElemPos)
    ReHam = BuildHamiltonian(Params,Verbose=false,Plot=false)
    if !isapprox(Ham,ReHam,atol=sqrt(eps()))
        throw("The ordiginal hamiltonian\n$Ham\nis not the same as the reconstructed one\n$ReHam\n")
    end
    true
end



function reproduce_sparce_ham(HamDim,NoElems)
    #ElemPos = nothing
    ElemPos = 10 .*rand(2,HamDim)
    ColIndx=[Int.(ceil.(rand(NoElems)*HamDim));HamDim]
    RowIndx=[Int.(ceil.(rand(NoElems)*HamDim));HamDim]
    RandElems=[(-.5 .+ rand(NoElems)) .+ im*(-.5 .+ rand(NoElems));0.0]
    Ham=sparse(ColIndx,RowIndx,RandElems);
    ###Make hermittian
    Ham = Ham + Ham'

    Params = getGenericHamParams(Ham,IPM=ElemPos,HamDim=HamDim)
    ReHam = BuildHamiltonian(Params,Verbose=false,Plot=false)
    if !isapprox(Ham,ReHam,atol=sqrt(eps()))
        throw("The ordiginal hamiltonian\n$Ham\nis not the same as the reconstructed one\n$ReHam\n")
    end
    true
end
TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close TestGenericHam.jl")
