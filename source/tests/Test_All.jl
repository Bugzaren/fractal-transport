if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open Test_All.jl")
TabLevel=TabLevel*"    "

using Test


include("TestHamiltonians.jl")
include("TestRecursive3D.jl")

include("TestTransport.jl")
include("TestRecursiveGreens.jl")

include("TestSierpinski.jl")

include("TestNegativeSigns.jl")

include("TestTransportCornerCases.jl")

struct CustomTestSet <: Test.AbstractTestSet
description::AbstractString
results::Vector
CustomTestSet(desc) = new(desc, [])
end

Test.record(ts::CustomTestSet, child::Test.AbstractTestSet) = push!(ts.results, child)
Test.record(ts::CustomTestSet, res::Test.Result) = push!(ts.results, res)

function Test.finish(ts::CustomTestSet)
    if Test.get_testset_depth() > 0
        Test.record(Test.get_testset(), ts)
    end
    ts
end

function DisplayTest(TestRes)
    NonPassList=filter(x -> !(x isa Test.Pass), TestRes.results)
    for NonPass in NonPassList
        if NonPass isa CustomTestSet
            DisplayTest(NonPass)
        else
            display(NonPass)
            println()
        end
    end
end


##### ............  Running all the test-sets  .............
res=@testset CustomTestSet "Transport Tests" begin
    TestSigns()
    TestHamiltonianSlizes()
    TestRecursive3D()
    TestRecursiveGreens()
    TestTransport()
    TestSierpinski()
    TestTransportCornerCases()
    
    ###------------- Comment in/out below
end
DisplayTest(res)
println()


TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close Test_All.jl")
