if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open TestNegativeSigns.jl")
TabLevel=TabLevel*"    "

using Test

include("../core/ChainAttachments.jl")
include("../core/ScanFunctions.jl")
include("MiscTest.jl")

function TestSigns()
    @testset "Tests sign reversal of the hamiltonian" begin
        @test test_chain_signs()
    end
end    


##using Profile

function test_chain_signs()
    Terminals=2
    ###Height and widht of the scattering area
    ScatteringAreaWidth=2 ###We use this expliclity later
    ScatteringAreaLength=5
    ### If thre are more than 2 leads, the width of the remaining leads....
    LeadWidth=3
    ### The range of energies (chemica poentials) to cosider
    EnergyRange=-3.5:0.01:3.5
    ### A regulator...
    eta=sqrt(eps())
    ### Values of the hopping elements
    tplus=+1.0
    tminus=-1.0
    ### Here we create the parameters needed for the parameters
    ## ( you would have to write your own version of this )
    ParamsPlus=getChainParams(ScatteringAreaLength,ScatteringAreaWidth, 0.0,t=tplus,minimal=true)
    ParamsMinus=getChainParams(ScatteringAreaLength,ScatteringAreaWidth, 0.0,t=tminus,minimal=true)


    ##Here we work out which sites are attaching to the leads
    ## ( you would have to write your own version of this )
    AttPointsPlus=get_chain_attachment_points(ParamsPlus["IVM"],Terminals,LeadWidth)
AttPointsMinus=get_chain_attachment_points(ParamsMinus["IVM"],Terminals,LeadWidth)
    ##Here we wokr out preciecly how we should slize the lattice
    ## This funciton you can use
    SlizeDataPlus=SlizeTheLattice(ParamsPlus,AttPointsPlus,Plot=false)
    SlizeDataMinus=SlizeTheLattice(ParamsMinus,AttPointsMinus,Plot=false)    


    ###Here we  construct the hamiltonian for the leads
    ### ( You should be able to use pars of this )
    ChainHamPlus=ConstructChainHamiltonian(AttPointsPlus,SlizeDataPlus,ParamsPlus,t=tplus)
    ChainHamMinus=ConstructChainHamiltonian(AttPointsMinus,SlizeDataMinus,ParamsMinus,t=tminus)
    ChainHamMixed=ConstructChainHamiltonian(AttPointsPlus,SlizeDataPlus,ParamsPlus,t=tplus)
    #### Add a sign change to the hamiltonian such that the berry phases vanish
    ###Here we use that ScatteringAreaWidth=2
    MixedHopping=diagm(0=>[1.,-1.])
    ChainHamMixed[3][1]=1.0*MixedHopping
    ChainHamMixed[3][2]=1.0*MixedHopping
    
    println("-----ChainHamPlus--")
    display(ChainHamPlus)
    println("-----ChainHamMinus--")
    display(ChainHamMinus)
    println("-----ChainHamMixed--")
    display(ChainHamMixed)

    ####Here we compute the actaul transport numbers
    #### (This one should be usable out of the box...... but migh need som interland cleaning up later....)
    AllTransportPlus=do_MultichanelChainTransport_Slize(AttPointsPlus,EnergyRange,ParamsPlus,eta=eta,
                                                        SlizeData=SlizeDataPlus,ChainHamiltonian=ChainHamPlus)
    AllTransportMinus=do_MultichanelChainTransport_Slize(AttPointsMinus,EnergyRange,ParamsMinus,eta=eta,
                                                         SlizeData=SlizeDataMinus,ChainHamiltonian=ChainHamMinus)
    AllTransportPM=do_MultichanelChainTransport_Slize(AttPointsMinus,EnergyRange,ParamsMinus,eta=eta,
                                                         SlizeData=SlizeDataMinus,ChainHamiltonian=ChainHamPlus)
    AllTransportPMFixed=do_MultichanelChainTransport_Slize(AttPointsMinus,EnergyRange,ParamsMinus,eta=eta,
                                                         SlizeData=SlizeDataMinus,ChainHamiltonian=ChainHamMixed)


    ####Finally we simplt plot the transmission coefficients between the first and second lead....
    if false
        figure()
        plot(EnergyRange .+ 0.00,AllTransportPlus[:,1,2],label="Plus")
        plot(EnergyRange .+ 0.05,AllTransportMinus[:,1,2],label="Minus")
        plot(EnergyRange .+ 0.10,AllTransportPM[:,1,2],label="Plus-Minus")
        plot(EnergyRange .+ 0.15,AllTransportPMFixed[:,1,2],label="Plus-Minus-Fixed")
        ylim([0,ScatteringAreaWidth+.1])
        legend(loc="best")
    end
    comparematrices(AllTransportPlus,AllTransportMinus,"AllTransportPlus","AllTransportMinus")
    comparematrices(AllTransportPlus,AllTransportPMFixed,"AllTransportPlus","AllTransportPMFixed")
    return true
end



TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close TestNegativeSigns.jl")
