if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open TestHamiltonians.jl")
TabLevel=TabLevel*"    "

include("../lattices/Sierpinski.jl")
include("../lattices/SquareLattice.jl")
include("../lattices/CubeLattice.jl")
include("../lattices/WeylLattice.jl")


include("../core/ChainAttachments.jl")
include("MiscTest.jl")

function TestHamiltonianSlizes()
    @testset "Test Generation of hamiltoians" begin
        @testset "Test Generation of sirpinsky hamiltoians" begin
            @test test_slize_ham(getChainParams(1,1,rand()))
            @test test_slize_ham(getChainParams(1,10,rand()))
            @test test_slize_ham(getChainParams(10,1,rand()))
            @test test_slize_ham(getChainParams(5,5,rand()))
            
            @test test_slize_ham(getSierpinskiParams(2,2,rand(),LT="SCS"))
            @test test_slize_ham(getSierpinskiParams(2,2,rand(),LT="SCT"))
            @test test_slize_ham(getSierpinskiParams(2,2,rand(),LT="SCR"))
            @test test_slize_ham(getSierpinskiParams(2,2,rand(),LT="SCX"))
            
            @test test_slize_ham(getSierpinskiParams(3,2,rand(),LT="STS"))
            @test test_slize_ham(getSierpinskiParams(3,2,rand(),LT="STT"))
            @test test_slize_ham(getSierpinskiParams(3,2,rand(),LT="STR"))
            @test test_slize_ham(getSierpinskiParams(3,2,rand(),LT="STX"))
            
            @test test_slize_ham(getSierpinskiParams(3,2,rand(),LT="SGS"))
            @test test_slize_ham(getSierpinskiParams(3,2,rand(),LT="SGT"))
            @test test_slize_ham(getSierpinskiParams(3,2,rand(),LT="SGR"))
            @test test_slize_ham(getSierpinskiParams(3,2,rand(),LT="SGX"))
        end
        
        @testset "Test Generation of Weyl hamiltoian" begin
            @test test_slize_ham(getWeylParams(1,1,1))
            @test test_slize_ham(getWeylParams(3,1,1))
            @test test_slize_ham(getWeylParams(1,3,1))
            @test test_slize_ham(getWeylParams(1,1,3))
            @test test_slize_ham(getWeylParams(1,2,3))
            @test test_slize_ham(getWeylParams(3,1,2))
            @test test_slize_ham(getWeylParams(3,2,1))
            @test test_slize_ham(getWeylParams(2,2,2))
            @test test_slize_ham(getWeylParams(4,2,2))
        end
        
        @testset "Test Generation of Cube hamiltoian" begin
            @test test_slize_ham(getCubeParams(1,1,1))
            @test test_slize_ham(getCubeParams(3,1,1))
            @test test_slize_ham(getCubeParams(1,3,1))
            @test test_slize_ham(getCubeParams(1,1,3))
            @test test_slize_ham(getCubeParams(1,2,3))
            @test test_slize_ham(getCubeParams(3,1,2))
            @test test_slize_ham(getCubeParams(3,2,1))
            @test test_slize_ham(getCubeParams(2,2,2))
            @test test_slize_ham(getCubeParams(4,2,2))
        end

        @testset "Slize_funciton" begin
            @test test_IVM_VIM_maps_d3()
            @test test_slice_hopp_ham(getCubeParams(2,3,2),2,1)
            @test test_slice_hopp_ham(getCubeParams(3,2,1),2,2)
            @test test_slice_hopp_ham(getCubeParams(2,10,2),2,2)
        end
        
        @testset "Slize_funciton" begin
            @test test_Slize_funciton()
            @test test_slice_hopp_ham(getChainParams(5,5,0.11),4,3)
        end
    end
end



function test_IVM_VIM_maps_d3()
    Params=getCubeParams(2,2,2)
    IVM=Params["IVM"].+rand(-1:1,3)
    println("IVM:")
    display(IVM)
    VIM=VIM_from_IVM(IVM)
    (VIMap,(MinX,MinY,MinZ))=VIM
    println("VIMap")
    display(VIMap)
    println("MinX,MinY,MinZ: $MinX, $MinY, $MinZ")
    NumElems=size(IVM)[2]
    Ntries=100000
    Indexes=Int.(ceil.(rand(Ntries).*NumElems))
    for INDX in Indexes
        Elem=IVM[:,INDX].+rand(-10:10,3)
        Res=IsOnLattice(Elem,IVM)
        Res2=IsAnElement(Elem,VIM)
        if Res!=Res2
            println("Elem = $Elem")
            @test Res==Res2
        end
    end
    true
end



function test_slize_ham(Params)
    Ham=BuildHamiltonian(Params)
    IVM=Params["IVM"]
    NewHam = Ham .* 0.0 ###Rest the ham
    Elems=size(IVM)[2]
    HamGenerator=Params["GenHamElems"]
    ParamTuple=Params["ParamsTuple"](Params)
    ERRELEMS=""
    for J1 in 1:Elems
        Pos1=IVM[:,J1]
        for J2 in 1:Elems
            Pos2=IVM[:,J2]
            NewHam[J1,J2]=HamGenerator(ParamTuple,Pos1,Pos2)
            if !isapprox(Ham[J1,J2],NewHam[J1,J2],atol=sqrt(eps()))
                ERRELEMS=(ERRELEMS*"($J1,$J2) ~ $Pos1->$Pos2\n"*
                          "Exp -> "*string(Ham[J1,J2])*" != "*string(NewHam[J1,J2])*" <- Got\n")
            end
        end
    end
    if !isapprox(Ham,NewHam,atol=sqrt(eps()))
        figure()
        imshow(abs.(Ham))
        title("Ham")
        figure()
        imshow(abs.(NewHam))
        title("NewHam")
        throw(DomainError(Params,"The Full and elemet wise hamiltonians are not the same\n"*ERRELEMS))
                          #"Full:\n$Ham\nand elemet:\n$NewHam\nwith diff\n"*
                          #string(Ham-NewHam)*"\n"*ERRELEMS))
    end
    true
end


function test_slice_hopp_ham(Params,Terminals,LeadWidth)
    HamGenerator=Params["GenHamElems"]
    SlizeHamFull,IVM=Params["Ham"](Params)
    ParamsTuple=Params["ParamsTuple"](Params)
    GetNeigbours=Params["GetPossibleNeighbours"]
    AttPoints=get_chain_attachment_points(IVM,Terminals,LeadWidth)
    println("Attachment Points:",AttPoints)
    SlizeDict,LeadtoSlize=SlizeTheLattice(Params,AttPoints)
    for Slize1 in 1:length(keys(SlizeDict))
        for Slize2 in 1:length(keys(SlizeDict))
            if Slize1==Slize2
                GeneratedHam=MakeSlizeHam(ParamsTuple,HamGenerator,IVM,SlizeDict[Slize1],0.0)
            else
                GeneratedHam=MakeHoppingHam(ParamsTuple,HamGenerator,IVM,SlizeDict[Slize1],SlizeDict[Slize2],GetNeigbours)
            end
            SlizeHamRaw=SlizeHamFull[SlizeDict[Slize1],SlizeDict[Slize2]]
            if !isapprox(SlizeHamRaw,GeneratedHam,atol=sqrt(eps()))
                figure()
                imshow(abs.(SlizeHamRaw))
                title("SlizeHamRaw")
                figure()
                imshow(abs.(GeneratedHam))
                title("GeneratedHam")
                throw(DomainError(Params,"The Full and elemet wise hamiltonians are not the same from slize $Slize1 to Slize $Slize1\n"*
                                  "SlizeHamRaw:\n$SlizeHamRaw\nand GeneratedHam:\n$GeneratedHam\nwith diff\n"*
                                  string(SlizeHamRaw-GeneratedHam)*"\n"))
            end
        end
    end
    true
end


function test_Slize_funciton()
    ResultsData=(Dict{Integer,Array{Int64,1}}(2=>[2, 7],3=>[3, 5, 8],1=>[1, 4, 6]), [1, 3, 2, 2])
    Params=getSierpinskiParams(1,1,LT="SCS")
    SideIndexes=get_chain_attachment_points(Params["IVM"],4,1)
    RES=SlizeTheLattice(Params,SideIndexes)
    #display(RES)
    @test ResultsData==RES

    
    ResultsData=(Dict{Integer,Array{Int64,1}}(7=>[34, 43, 52],4=>[6, 7, 8, 14, 22, 31, 40, 49, 58, 68, 78, 79, 80],2=>[2, 3, 4, 11, 20, 29, 38, 47, 56, 65, 74, 75, 76],3=>[5, 12, 13, 21, 30, 39, 48, 57, 66, 67, 77],5=>[9, 15, 16, 17, 18, 23, 27, 32, 36, 41, 45, 50, 54, 59, 63, 69, 70, 71, 72, 81],6=>[24, 25, 26, 33, 35, 42, 44, 51, 53, 60, 61, 62],1=>[1, 10, 19, 28, 37, 46, 55, 64, 73]), [1, 5, 2, 4, 2, 4])
    
    Params=getSierpinskiParams(2,0,LT="SCS")
    SideIndexes=get_chain_attachment_points(Params["IVM"],6,3)
    RES=SlizeTheLattice(Params,SideIndexes)
    #display(RES)
    @test ResultsData==RES
    true
end

TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close TestHamiltonians.jl")
