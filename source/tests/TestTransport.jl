if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open TestTransport.jl")
TabLevel=TabLevel*"    "

using Test
using PyPlot

include("../lattices/CubeLattice.jl")
include("../core/ScanFunctions.jl")
include("../lattices/Sierpinski.jl")
include("../lattices/Haldanemodel.jl")
include("MiscTest.jl")

function TestTransport()
    @testset "Tests for TestTransport" begin
        @testset "Test Mismatch slizes and Lead attachments" begin
            @test test_non_existing_sites()
            @test test_slize_attachments_missmatch()
            @test test_slize_Haldane_missmatch()
        end
        @testset "Double chain_length transport" begin
            @test_broken test_Scan_And_Direct(1,2,1)
            @test test_Scan_And_Direct(2,2,1)
            @test test_Scan_And_Direct(10,2,1)
            @test test_Scan_And_Direct(2,3,10)
            @test_broken test_Scan_And_Direct(1,2,2)
            @test test_Scan_And_Direct(3,3,3)
            @test test_Scan_And_Direct(4,5,2)
        end
        @testset "Double chain_length transport" begin
            @test test_chain_copies(1,2,1)
            @test test_chain_copies(10,2,1)
            @test test_chain_copies(1,3,10)
            @test test_chain_copies(2,2,2)
            @test test_chain_copies(3,3,3)
            @test test_chain_copies(4,5,2)
        end
        
        @testset "3D transport" begin
            @test test_transport_Full_vs_Ham(getCubeParams(4,4,4),2,1,atol=10.0^-5)
            @test test_transport_Full_vs_Ham(getCubeParams(2,10,2),2,1,atol=10.0^-5)
        end
        
        @testset "rotation freedom chain" begin
            @test test_transport_rotation_chain(2,5,4,1,atol=10^-4)
            @test test_transport_rotation_chain(2,5,4,3,atol=10^-4)
            @test test_transport_rotation_chain(2,7,2,atol=10^-4)
            @test test_transport_rotation_chain(3,7,4,1,atol=10^-4)
            @test test_transport_rotation_chain(2,7,4,3,atol=10^-4)
            @test test_transport_rotation_chain(3,7,6,1,atol=10^-4)
            @test test_transport_rotation_chain(2,7,6,2,atol=10^-4)
        end
        @testset "gauge freedom" begin
            for sg in 2:3
                for fg in 0:sg
                    @test test_transport_gauge_freedom(sg,fg,2)
                    for width in 2:3
                        @test test_transport_gauge_freedom(sg,fg,3,width)
                        @test test_transport_gauge_freedom(sg,fg,6,width)
                    end
                end
            end
        end
        
        @testset "attachment points" begin
            for width in 2:3
                @test test_attachment_points(2,0,3,width,atol=10^-4)
                @test test_attachment_points(3,1,4,width,atol=10^-4)
                @test test_attachment_points(2,2,6,width,atol=10^-5)
                @test test_attachment_points(3,3,3,width,atol=10^-4)
                @test test_attachment_points(2,2,4,width,atol=10^-5)
                @test test_attachment_points(3,3,6,width,atol=10^-4)
            end
        end
        
        
        @testset "Slize calculation vs Hamiltonian calulation" begin
            @test test_slize_vs_Ham(getChainParams(2,1, 0.), 2, 2,eta=100*eps())
            @test test_slize_vs_Ham(getChainParams(2,2, 0.), 2, 2,eta=100*eps(),Energy=0.5)
            @test test_slize_vs_Ham(getChainParams(3,3, 0.), 4, 1,eta=100*eps())
            @test test_slize_vs_Ham(getChainParams(3, 3, 0.11), 3, 1,Energy=1.0,eta=100*eps())
            @test test_slize_vs_Ham(getChainParams(3, 3, 0.11), 4, 1,Energy=1.0,eta=100*eps())
            @test test_slize_vs_Ham(getSierpinskiParams(2,2,0.11),4,3,eta=100*eps())
            @test test_slize_vs_Ham(getChainParams(5,5,0.11),4,3,eta=100*eps())
        end

    end
end


##using Profile



function test_slize_vs_Ham(Params,Terminals,LeadWidth;Energy=rand(10),eta=sqrt(eps()))
    IVM=Params["IVM"]
    AttPoints=get_chain_attachment_points(IVM,Terminals,LeadWidth)
    SlizeData=SlizeTheLattice(Params,AttPoints)

    println("SlizeData:")
    display(SlizeData)
    
    println("..................TransportSlize1..........")
    AllTransportSlize1=do_MultichanelChainTransport_Slize(AttPoints,Energy,Params,eta=eta,
                                                       SlizeData=SlizeData)
    println("..................TransportSlize2..........")
    AllTransportSlize2=do_MultichanelChainTransport_Slize(AttPoints,Energy,Params,eta=eta)
    println("..................TransportHam..........")
    Hamiltonian=BuildHamiltonian(Params)
    AllTransportHam=do_MultichanelChainTransport_Ham(AttPoints,Energy,Hamiltonian,eta=eta)

    for EnIndx in 1:length(Energy)
        TransportSlize1=AllTransportSlize1[EnIndx,:,:]
        TransportSlize2=AllTransportSlize2[EnIndx,:,:]
        TransportHam=AllTransportHam[EnIndx,:,:]
        LocEnergy=Energy[EnIndx]
        if !isapprox(TransportSlize1,TransportSlize2,atol=sqrt(eps()))
            #plot_attachments(AttPoints,IVM)
            throw(DomainError(Params,"The two slize versions are not the same\n"*
                              "Slize1:\n$TransportSlize1\nand Slize2:\n"*
                              "$TransportSlize2\nwith diff\n"*
                              string(TransportSlize1-TransportSlize2)*"\n"*
                              "are not the same for Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$LocEnergy"))
        end
        if !isapprox(TransportHam,TransportSlize1,atol=sqrt(eps()))
            #plot_attachments(AttPoints,IVM)
            throw(DomainError(Params,"The Slize and Hamiltonian transport is not the same\n"*
                              "Slize:\n$TransportSlize1\nand Ham:\n"*
                              "$TransportHam\nwith diff\n"*
                              string(TransportSlize1-TransportHam)*"\n"*
                              "are not the same for Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$LocEnergy"))
        end
    end
    true
end


function test_transport_rotation_chain(width=5,length=10,Terminals=2,LeadWidth=3;
                                       eta=sqrt(eps()),atol=sqrt(eps()))
    #Phi=0.6536559778882765
    #Phi=0.5
    Phi=0.11
    Energy1=0.5944447862478932
    Params1 = getChainParams(length,width,Phi)
    do_test_transport_Ham_vs_Slize(Params1,Energy1,Phi,Terminals,LeadWidth;eta=eta,atol=atol)
    true
end




function test_attachment_points(sg=2,fg=2,Terminals=2,LeadWidth=1;atol=sqrt(eps()))
    LT="SCS"
    Energy=rand()
    Phi=rand()
    Params = getSierpinskiParams(sg,fg,Phi,LT=LT)
    Ham=BuildHamiltonian(Params)
    IVM=Params["IVM"]
    AttPoints=get_chain_attachment_points(IVM,Terminals,LeadWidth)
    @test Terminals==length(AttPoints)
    #println("AttPoints")
    #display(AttPoints)
    TranportDirect=do_MultichanelChainTransport_Slize(AttPoints,Energy,Params)[1,:,:]
    #println("TranportDirect")
    #display(TranportDirect)
    TranportDirectHam=do_MultichanelChainTransport_Ham(AttPoints,Energy,Ham)[1,:,:]
    #println("TranportDirectHam")
    #display(TranportDirectHam)
    if !isapprox(TranportDirect,TranportDirectHam,atol=atol)
        #plot_attachments(AttPoints,IVM)
        throw(DomainError(Params,"The original Slize and hamiltonian transport vectors\n:$TranportDirect\nand\n"*
                          "$TranportDirectHam\n"*"with diff\n"*
                          string(TranportDirect-TranportDirectHam)*"\n"*
                          "are not the same for Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$Energy and tolerance=$atol"))
    end
    
    
    ### Store the array pointers here for rotation
    for LeadNoChoise in 1:Terminals
        AttPointsReverse = Array{typeof(AttPoints[1])}(undef,Terminals)
        #display(AttPointsReverse)
        for LeadNo in 1:Terminals
            #println("Writing at lead no $LeadNo. Using the following template")
            Target=copy(AttPoints[LeadNo])
            #display(Target)
            if LeadNo==LeadNoChoise
                #println("Reverse at lead no $LeadNo.")
                AttPointsReverse[LeadNo]=reverse(Target)
            else
                AttPointsReverse[LeadNo]=Target
            end
            #display(AttPointsReverse)
        end
        #println("AttPointsReverse")
        #display(AttPointsReverse)
        TranportReverse=do_MultichanelChainTransport_Slize(AttPointsReverse,Energy,Params)[1,:,:]
        #println("TranportReverse")
        #display(TranportReverse)
        TranportReverseHam=do_MultichanelChainTransport_Ham(AttPointsReverse,Energy,Ham)[1,:,:]
        #println("TranportReverseHam")
        #display(TranportReverseHam)
        if !isapprox(TranportDirect,TranportReverseHam,atol=atol)
            #plot_attachments(AttPointsReverse,IVM)
            throw(DomainError(Params,"The two trasport vectors\n:$TranportDirect\nand\n"*
                              "$TranportReverseHam\n"*
                              "with diff\n"*
                              string(TranportDirect-TranportReverseHam)*"\n"*
                              "are not the same for reversing lead=$LeadNoChoise,"*
                              "Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$Energy"))
        end
        if !isapprox(TranportReverseHam,TranportReverse,atol=atol)
            plot_attachments(AttPointsReverse,IVM)
            throw(DomainError(Params,"The Slize and hamiltonian transport vectors\n:$TranportReverseHam\nand\n"*
                              "$TranportReverse\n"*
                              "with diff\n"*
                              string(TranportReverseHam-TranportReverse)*"\n"*
                              "are not the same for reversing lead=$LeadNoChoise,"*
                              "Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$Energy"))
        end
    end
    true
end


##using Profile

function test_transport_Full_vs_Ham(Params,Terminals,LeadWidth;atol=sqrt(eps))
    Energy=rand(10)
    #Energy=0.0
    IVM=Params["IVM"]
    ParamsTuple=Params["ParamsTuple"](Params)
    FullHam=BuildHamiltonian(Params)
    eta=sqrt(eps())
    AttPoints=get_chain_attachment_points(IVM,Terminals,LeadWidth)
    SlizeData=SlizeTheLattice(Params,AttPoints)
    TransportSlize1=0
    TransportSlize2=0
    TransportDyn=0
    ##@profile 1==1
    ##Profile.clear()
    println("..................TransportSlize1..........")
    TransportSlize1=do_MultichanelChainTransport_Ham(AttPoints,Energy,FullHam,
                                                     eta=eta)[1,:,:]
    println("..................TransportDyn..........")
    TransportDyn=do_MultichanelChainTransport_Slize(AttPoints,Energy,Params,eta=eta,SlizeData=SlizeData)[1,:,:]
    
    if !isapprox(TransportDyn,TransportSlize1,atol=atol)
        #plot_attachments(AttPoints,IVM)
        throw(DomainError(ParamsTuple,"The Slize and Dynamic transport is not the same\n"*
                          "Slize:\n$TransportSlize1\nand Dyn:\n"*
                          "$TransportDyn\nwith diff\n"*
                          string(TransportSlize1-TransportDyn)*"\n"*
                          "are not the same for Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$Energy"))
    end
    if !isapprox(TransportDyn,TransportSlize1,atol=atol)
        #plot_attachments(AttPoints,IVM)
        throw(DomainError(ParamsTuple,"The two slize versions are not the same\n"*
                          "Slize1:\n$TransportSlize1\nand Slize2:\n"*
                          "$TransportSlize2\nwith diff\n"*
                          string(TransportSlize1-TransportSlize2)*"\n"*
                          "are not the same for Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$Energy"))
    end
    true
end



function test_chain_copies(Width,Length,Height;Energy=-3.5:1:3.5)
    eta=sqrt(eps())
    Params1=getCubeParams(Width,Length,Height)
    Params2=getCubeParams(2*Width,Length,Height)
    AttPoints1=get_chain_attachment_points(Params1["IVM"],2,1)
    AttPoints2=get_chain_attachment_points(Params2["IVM"],2,1)
    ##@profile 1==1
    ##Profile.clear()
    return true
    println("..................Transport x 1..........")
    Transport1=do_MultichanelChainTransport_Slize(AttPoints1,Energy,Params1,eta=eta,Verbose=false)[:,1,1]
    println("..................Transport x 2..........")
    Transport2=do_MultichanelChainTransport_Slize(AttPoints2,Energy,Params2,eta=eta,Verbose=false)[:,1,1]
    
    if !isapprox(Transport1,Transport2,atol=10^-5)
        throw(DomainError((Params1,Params2),
                          "The transport for two double length is not the same\n"*
                          "Length:\n$Transport1\nand 2xLength:\n"*
                          "$Transport2\nwith diff\n"*
                          string(Transport1-Transport2)*"\n"*
                          "are not the same for 2 Terminals and Energy=$Energy"))
    end
    true
end



function test_Scan_And_Direct(Width,Length,Height)
    Energy=-4:1:4
    eta=sqrt(eps())
    Params=getCubeParams(Width,Length,Height)
    AttPoints=get_chain_attachment_points(Params["IVM"],2,1)
    ##@profile 1==1
    ##Profile.clear()
    println("..................Transport ..........")
    Transport1=do_MultichanelChainTransport_Slize(AttPoints,Energy,Params,eta=eta)[:,:,:]
    println("..................MultiTransport..........")
    Transport2=MultiTransport(Params,Energy,eta=eta)[:,:,:]
    
    if !isapprox(Transport1,Transport2,atol=sqrt(eps()))
        throw(DomainError((Params),
                          "The transport by calling do_Multicanell diecly and the MutiTransport is not the same\n"*
                          "do_Mul...:\n$Transport1\nand MultiTransport:\n"*
                          "$Transport2\nwith diff\n"*
                          string(Transport1-Transport2)*"\n"*
                          "are not the same for 2 Terminals and Energy=$Energy"))
    end
    true
end




function test_transport_gauge_freedom(sg=2,fg=2,Terminals=2,LeadWidth=1)
    #sg=2,fg=2,Terminals=2,LeadWidth=1
    LT="SCS"
    Energy=rand()
    Phi=rand()
    ParamsLX = getSierpinskiParams(sg,fg,Phi,LT=LT,Gauge="LandauX")
    ParamsLY = getSierpinskiParams(sg,fg,Phi,LT=LT,Gauge="LandauY")
    ParamsSym = getSierpinskiParams(sg,fg,Phi,LT=LT,Gauge="Symmetric")

    
    IVM=ParamsLX["IVM"]
    AttPoints=get_chain_attachment_points(IVM,Terminals,LeadWidth)
    SlizeData=SlizeTheLattice(ParamsLX,AttPoints,Plot=false)

    ###Compute the leat hamiltonias that we may compare themm
    LeadHamLX=ConstructChainHamiltonian(AttPoints,SlizeData,ParamsLX,t=1.0)
    LeadHamLY=ConstructChainHamiltonian(AttPoints,SlizeData,ParamsLY,t=1.0)
    LeadHamSym=ConstructChainHamiltonian(AttPoints,SlizeData,ParamsSym,t=1.0)

    #println("Compute Transport Landau X (redo everything)")
    TranportLandauX=do_MultichanelChainTransport_Slize(AttPoints,Energy,ParamsLX)[1,:,:]
    #println("Compute Transport Landau X")
    TranportLandauX=do_MultichanelChainTransport_Slize(AttPoints,Energy,ParamsLX,ChainHamiltonian=LeadHamLX,SlizeData=SlizeData)[1,:,:]
    #println("Compute Transport Landau Y")
    TranportLandauY=do_MultichanelChainTransport_Slize(AttPoints,Energy,ParamsLY,ChainHamiltonian=LeadHamLY,SlizeData=SlizeData)[1,:,:]
    #println("Compute Transport Symmetric")
    TranportSymetric=do_MultichanelChainTransport_Slize(AttPoints,Energy,ParamsSym,ChainHamiltonian=LeadHamSym,SlizeData=SlizeData)[1,:,:]
    
    if !isapprox(TranportLandauX,TranportLandauY,atol=sqrt(eps()))
        #plot_attachments(AttPoints,IVM)
        throw(DomainError(ParamsLX,"The LandauX and LandauY transport vectors\n:$TranportLandauX\nand\n"*
                          "$TranportLandauY\nwith diff\n"*
                          string(TranportLandauX-TranportLandauY)*"\n"*
                          "are not the same for Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$Energy"))
    end
    if !isapprox(TranportLandauX,TranportSymetric,atol=sqrt(eps()))
        #plot_attachments(AttPoints,IVM)
        throw(DomainError(ParamsLX,"The LandauX and Symmetric transport vectors\n:$TranportLandauX\nand\n"*
                          "$TranportSymetric\nwith diff\n"*
                          string(TranportLandauX-TranportSymetric)*"\n"*
                          "are not the same for Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$Energy"))
    end
    true
end

function test_non_existing_sites()
    Params = getChainParams(4,4)
    AttachmentPoints=[[1,2],[20,21]]
    @test_throws DomainError SlizeTheLattice(Params,AttachmentPoints)
end



function test_slize_attachments_missmatch()
    
    Params = getChainParams(4,4)
    AttachmentPoints=[[1,2],[3,4],[16,15,14,13,12]]
    LatticeSlizes=SlizeTheLattice(Params,AttachmentPoints,Plot=false)
    Energy=rand()
    ###Just make sure that thei can be done
    TranportSlize=do_MultichanelChainTransport_Slize(AttachmentPoints,Energy,Params,
                                                     SlizeData=LatticeSlizes)[1,:,:]
    
end


function test_slize_Haldane_missmatch()
    Width=2
    Length=3
    Energy=-4.5:1:4.5
    PH=getHaldaneParams(Width,Length,B2=0);
    ## can take out
    Terminals=4
    B1=PH["B1"]
    IVM=PH["IVM"]
    ###Figure out where to attach the leads
    ## List of all points for attaching two leads at x=1 and x=L
    DummyLeadAttachmentPoints=get_chain_attachment_points(PH["IVM"],2)
    ## split this into four lists in order to treat differen orbitals with different leads
    
    print("LeadAttachmentPoints:")
    display(DummyLeadAttachmentPoints)
    
    Lead1=findall(x->x==1,IVM[:,DummyLeadAttachmentPoints[1]][3,:])
    Lead2=findall(x->x==2,IVM[:,DummyLeadAttachmentPoints[1]][3,:])
    
    Lead3=DummyLeadAttachmentPoints[2][findall(x->x==1,IVM[:,DummyLeadAttachmentPoints[2]][3,:])]
    Lead4=DummyLeadAttachmentPoints[2][findall(x->x==2,IVM[:,DummyLeadAttachmentPoints[2]][3,:])]
    
    
    LeadAttachmentPoints=(Lead1,Lead2,Lead3,Lead4)

    println("Split LeadAttachmentPoints:")
    println(LeadAttachmentPoints)
    
    
    
    ## This funciton you can use
    SlizeData=SlizeTheLattice(PH,LeadAttachmentPoints,Plot=false,Verbose=false)
    println("SlizeData:")
    display(SlizeData)
    
    
    ###Construct the hamiltonian for the leads
    BlockChainHam=BuildHamiltonian(getChainParams(Width,1,t=B1))
    ###PLot the elemts
    #imshow(abs.(FullChainHam))
    
    ###Create the LeToCenter Hamiltonian and take care of staggering for the chanel with a different coupling signt in the lead and the center 
    LeadToCenterUp=fill(0. , Width, Width)
    LeadToCenterDown=fill(0. , Width, Width)
    LeadToLead=fill(0.,Width,Width)
    for j=1:Width
        LeadToCenterUp[j,j]=sign(B1)^j*B1
        LeadToCenterDown[j,j]=sign(-B1)^j*B1
        ## inside the Lead nothing gets staggered
    LeadToLead[j,j]=B1
    end
    
    ###Now the build the Lead Dictionary
    LeadHamInfo=(##Inside Lead slizeLead both orbitals and left and right are identical 
                 Dict(1=>copy(BlockChainHam),2=>copy(BlockChainHam),3=>copy(BlockChainHam),4=>copy(BlockChainHam)),   
                 ###Lead to Lead again everything is identical
                 Dict(1=>copy(LeadToLead), 2=>copy(LeadToLead),3=>copy(LeadToLead), 4=>copy(LeadToLead)),
                 ###Lead to Scattering Area, spin up and down have different staggering
    Dict(1=>copy(LeadToCenterUp),2=>copy(LeadToCenterDown),3=>copy(LeadToCenterUp),4=>copy(LeadToCenterDown)))
    
    @test_throws DimensionMismatch do_MultichanelChainTransport_Slize(LeadAttachmentPoints,Energy,PH,
                                                                     SlizeData=SlizeData,
                                                                      ChainHamiltonian=LeadHamInfo)
    ###Change the LeadHam Infor inline
    MatchHamiltonianToSlize!(LeadAttachmentPoints,SlizeData,LeadHamInfo)
    do_MultichanelChainTransport_Slize(LeadAttachmentPoints,Energy,PH,
                                       SlizeData=SlizeData,
                                       ChainHamiltonian=LeadHamInfo)
    
    
end

TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close TestTransport.jl")
