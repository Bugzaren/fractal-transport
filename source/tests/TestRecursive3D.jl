if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open TestRecursive3D.jl")
TabLevel=TabLevel*"    "

###
import Test
println("including TestRecursive3D.jl")

include("../core/ChainAttachments.jl")
include("../lattices/WeylLattice.jl")
include("../lattices/CubeLattice.jl")
include("MiscTest.jl")
include("../core/Greens.jl")



function TestRecursive3D()
    @testset "Tests for 3D-recursion methods" begin
        @testset "Test again exact resulsts" begin
            Energy=1. + (sqrt(5)-1)*rand()
            Gexact=getExact2x1WeylSurfaceGreens(Energy)
            @test test_exact_surface_greens(getWeylParams(2,2,1),
                                            Energy=Energy,Gexact=Gexact)
        end
        @testset "Test again exact resulsts" begin
            @test test_exact_transmission(getChainParams(2,1),
                                          Energy=[2+rand(),1.5*rand(),-1.5*rand()],
                                          TExact=[0.0,1.0,1.0])
            @test test_exact_transmission(getChainParams(5,1),
                                          Energy=[2+rand(),1.5*rand(),-1.5*rand()],
                                          TExact=[0.0,1.0,1.0])
            @test test_exact_transmission(getWeylParams(2,1,1),
                                          Energy=1.9 .* rand(20),
                                          TExact=2.0.* ones(20))
            @test test_exact_transmission(getWeylParams(2,1,1),
                                          Energy=2.05 .+ rand(20),
                                          TExact=0.0.* ones(20))
            Energy=1. .+ (sqrt(5)-1)*rand(20)
            @test test_exact_transmission(getWeylParams(2,2,1),
                                          Energy=Energy,
                                          TExact=4.0.* ones(20))
            end
        @testset "Ham Surface green functoin" begin
            @test test_surface_greens(getChainParams(2,3),mu=.5)
            @test test_surface_greens(getCubeParams(2,3,5),mu=1.5)
            @test test_surface_greens(getWeylParams(3,3,2),mu=1.2)
            
            @test test_surface_greens(getChainParams(2,10),mu=.5)
            @test test_surface_greens(getCubeParams(2,5,5),mu=2.5)
            @test test_surface_greens(getWeylParams(2,5,5),mu=2.1)
        end
        @testset "Ham transmission" begin
            @test test_eqivalent_transmission3D(getCubeParams(3,3,3),getCubeParams(5,3,3))
            
            @test test_eqivalent_transmission3D(getWeylParams(2,1,1),getWeylParams(3,1,1))
            @test test_eqivalent_transmission3D(getWeylParams(3,2,1),getWeylParams(6,2,1))
            @test test_eqivalent_transmission3D(getWeylParams(2,4,4),getWeylParams(4,4,4))
        end
    end
end


function test_exact_surface_greens(Params;Energy=nothing,Gexact=nothing,SEexact=nothing,FBexact=nothing)
    eta=sqrt(eps())
    IVM=Params["IVM"]
    Terminals=2
    println("Energy:",Energy)

    AttPoints=get_chain_attachment_points(IVM,Terminals,0)
    println("AttPoints:",AttPoints)

    SlizeData=SlizeTheLattice(Params,AttPoints,Plot=false)
    Slize1 =SlizeData[1][1]
    Slize2 =SlizeData[1][2]


    ParamsTuple=Params["ParamsTuple"](Params)
    HamGenerator=Params["GenHamElems"]
    GetNeigbours=Params["GetPossibleNeighbours"]
   
    Ham=MakeSlizeHam(ParamsTuple,HamGenerator,IVM,Slize1,0.0)
    ##Add on site potential
    
    Hop=MakeHoppingHam(ParamsTuple,HamGenerator,IVM,Slize1,Slize2,GetNeigbours)


    println("On-SiteHam:")
    display(Ham)
    println("Hopp-Ham:")
    display(Hop)
    inv(Hop)

    
    SurfGreens=SolveSurfaceGreens(Ham,Hop;Verbose=false,mu=Energy)
    println("SurfGreens:")
    display(SurfGreens)

    ##Compare to Gexact
    println("Exact Surface Greens:")
    display(Gexact)

    if !isapprox(SurfGreens,Gexact,atol=sqrt(eps()))
        throw(DomainError(Params,"The Surface greens is not the same as the exact answer!\n"*
                          "SurfGreens:\n$SurfGreens\nand Gexact:\n"*
                          "$Gexact\nwith differnce\n"*
                          string(SurfGreens-Gexact)*"\n"*
                          "for Terminals=$Terminals and Energy=$Energy"))
    end
    true
end




function test_exact_transmission(Params;Energy=nothing,TExact=nothing)
    eta=sqrt(eps())
    IVM=Params["IVM"]
    Terminals=2

    AttPoints=get_chain_attachment_points(IVM,Terminals,0)
    println("AttPoints:",AttPoints)

    SlizeData=SlizeTheLattice(Params,AttPoints,Plot=false)
    Slize1 =SlizeData[1][1]
    Slize2 =SlizeData[1][2]


    ParamsTuple=Params["ParamsTuple"](Params)
    HamGenerator=Params["GenHamElems"]
    GetNeigbours=Params["GetPossibleNeighbours"]
   
    Ham=MakeSlizeHam(ParamsTuple,HamGenerator,IVM,Slize1,0.0)
    Hop=MakeHoppingHam(ParamsTuple,HamGenerator,IVM,Slize1,Slize2,GetNeigbours)

    ChainHam=(Ham,Hop,Hop)
    println("ChainHam:",ChainHam)

    println("..................Transport Param1..........")
    TransportSlize=do_MultichanelChainTransport_Slize(AttPoints,Energy,Params,eta=eta,
                                                       SlizeData=SlizeData,
                                                      ChainHamiltonian=ChainHam)
    #println("TransportSlize:")
    #display(TransportSlize)
    if Terminals==2
        TNumeric=TransportSlize[:,1,2]
    end
    if !isapprox(TNumeric,TExact,atol=10^(-5))
        #plot_attachments(AttPoints,IVM)
        throw(DomainError(Params,"The transport is not the same as the exact answer!\n"*
                          "TNumeric:\n$TNumeric\nand TExact:\n"*
                          "$TExact\nwith differnce\n"*
                          string(TNumeric-TExact)*"\n"*
                          "for Terminals=$Terminals and Energy=$Energy"))
    end
    true
end



function test_surface_greens(Params;mu=0.0)
    ###This tests that the surface green funcitons can be solved
    eta=sqrt(eps())
    IVM=Params["IVM"]
    
    AttPoints=get_chain_attachment_points(IVM,2,0)
    #println("AttPoints:",AttPoints)
    
    SlizeData,LeadtoSlize=SlizeTheLattice(Params,AttPoints,Plot=false)
    
    Slize1 =SlizeData[1]
    Slize2 =SlizeData[2]
    ParamsTuple=ParamsTuple=Params["ParamsTuple"](Params)
    HamGenerator=Params["GenHamElems"]
    GetNeigbours=Params["GetPossibleNeighbours"]
    
    Ham11=MakeSlizeHam(ParamsTuple,HamGenerator,IVM,Slize1,0.0)
    Ham22=MakeSlizeHam(ParamsTuple,HamGenerator,IVM,Slize2,0.0)
    Ham12=MakeHoppingHam(ParamsTuple,HamGenerator,IVM,Slize1,Slize2,GetNeigbours)
    Ham21=MakeHoppingHam(ParamsTuple,HamGenerator,IVM,Slize2,Slize1,GetNeigbours)
    
    #println("Ham11")
    #display(Ham11)
    #rintln("Ham22")
    #display(Ham22)
    #println("Ham12")
    #display(Ham12)
    #println("Ham21")
    #display(Ham21)

    ###Here we check that the slizes are equivalent
    if !isapprox(Ham11,Ham22,atol=sqrt(eps()))
        throw(DomainError(Params,"The Ham11 and Ham11 are not the same\n"*
                          "Ham11:\n$Ham11\nand Ham22:\n"*
                          "$Ham22\nwith diff\n"*
                          string(Ham11-Ham22)))
    end
    Ham12Dagger=copy(Ham12')
    MaxPos=findmax(abs.(Ham12))[2] ###Only pso intersting
    Lambdafactor=Ham12Dagger[MaxPos] / Ham12[MaxPos]
    #println("Pos of largest integer ",MaxPos)
    println("Lambdafactor ",Lambdafactor)
    Ham12DaggerRed=Ham12Dagger ./ Lambdafactor
    ###Here we check that the hopping Hamiltonian is hermitian up to an overall factor lambda
    
    if !isapprox(Ham12,Ham12DaggerRed,atol=sqrt(eps()))
        throw(DomainError(Params,"The Ham12 and conj(Ham12)/lambda are not the same\n"*
                          "Ham12:\n$Ham12\nand conj(Ham12)/lambda:\n"*
                          "$Ham12DaggerRed\nwith diff\n"*
                          string(Ham12-Ham12DaggerRed)))
    end

    SurfaceGreens=SolveSurfaceGreens(Ham11,Ham12;mu=mu)
    ValidateSurfaceGreens(SurfaceGreens,Ham11,Ham12;mu=mu)
    true   
end


function test_eqivalent_transmission3D(Params1,Params2;Energy=rand())
    eta=sqrt(eps())
    IVM1=Params1["IVM"]
    IVM2=Params2["IVM"]
    Terminals=2

    AttPoints1=get_chain_attachment_points(IVM1,Terminals,0)
    AttPoints2=get_chain_attachment_points(IVM2,Terminals,0)
    println("AttPoints1:",AttPoints1)
    println("AttPoints2:",AttPoints2)

    SlizeData1=SlizeTheLattice(Params1,AttPoints1,Plot=false)
    SlizeData2=SlizeTheLattice(Params2,AttPoints2,Plot=false)


    Slize11 =SlizeData1[1][1]
    Slize12 =SlizeData1[1][2]
    Slize21 =SlizeData2[1][1]
    Slize22 =SlizeData2[1][2]
    
    ParamsTuple1=Params1["ParamsTuple"](Params1)
    ParamsTuple2=Params2["ParamsTuple"](Params2)
    HamGenerator1=Params1["GenHamElems"]
    HamGenerator2=Params2["GenHamElems"]
    GetNeigbours1=Params1["GetPossibleNeighbours"]
    GetNeigbours2=Params2["GetPossibleNeighbours"]
   
    Ham1=MakeSlizeHam(ParamsTuple1,HamGenerator1,IVM1,Slize11,0.0)
    Ham2=MakeSlizeHam(ParamsTuple2,HamGenerator2,IVM2,Slize21,0.0)
    Hop1=MakeHoppingHam(ParamsTuple1,HamGenerator1,IVM1,Slize11,Slize12,GetNeigbours1)
    Hop2=MakeHoppingHam(ParamsTuple2,HamGenerator2,IVM2,Slize21,Slize22,GetNeigbours2)

    ChainHam1=(Ham1,Hop1,Hop1)
    ChainHam2=(Ham2,Hop2,Hop2)

    comparematrices(Ham1,Ham2,"Ham1","Ham2")

    println("..................Transport Param1..........")
    TransportSlize1=do_MultichanelChainTransport_Slize(AttPoints1,Energy,Params1,eta=eta,
                                                       SlizeData=SlizeData1,
                                                       ChainHamiltonian=ChainHam1)[1,:,:]
    println("..................Transport Param2..........")
    TransportSlize2=do_MultichanelChainTransport_Slize(AttPoints2,Energy,Params2,eta=eta,
                                                       SlizeData=SlizeData2,
                                                       ChainHamiltonian=ChainHam2)[1,:,:]
    
    if !isapprox(TransportSlize1,TransportSlize2,atol=10^(-5))
        #plot_attachments(AttPoints,IVM)
        throw(DomainError(Params1,"The transport through the two slizes is not the same!\n"*
                          "TransportSlize1:\n$TransportSlize1\nand Dyn:\n"*
                          "$TransportSlize2\nwith TransportSlize2\n"*
                          string(TransportSlize1-TransportSlize2)*"\n"*
                          "Terminals=$Terminals and Energy=$Energy"))
    end
    true
end



function getExact2x1WeylSurfaceGreens(mu)
    ###Here mu is assumer to be 5t^2>mu^2>t^2
    t=1.0
    SqrtPart=sqrt((5.0-mu^2/t^2)/(-t^2+mu^2))
    DiagTerm= (1/(2.0*t^2))*(mu+(im*abs(t)*abs(mu)*SqrtPart))
    OffDiagTerm=.5*(1/t+im*SqrtPart*sign(mu)*sign(t))
    SurfaceGreens=diagm(0=>fill(DiagTerm,4))
    SurfaceGreens[1,4]=-OffDiagTerm
    SurfaceGreens[2,3]=OffDiagTerm
    SurfaceGreens[3,2]=OffDiagTerm
    SurfaceGreens[4,1]=-OffDiagTerm
    return SurfaceGreens
end
TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close TestRecursive3D.jl")
