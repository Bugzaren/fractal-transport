if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open Profile.jl")
TabLevel=TabLevel*"    "

using Profile
using BenchmarkTools
#using TimerOutputs

include("../lattices/Sierpinski.jl")
include("../core/Greens.jl")

function test_my_function()
    Params=getSierpinskiParams(4,4)
    EList=rand(10) .- .5
    SideIndexes=get_chain_attachment_points(Params["IVM"],4,2)
    SlizeData=SlizeTheLattice(Params,SideIndexes)
    do_MultichanelChainTransport_Slize(SideIndexes,EList,Params)
end

function test_my_function_long()
    Ntries=1
    TIMESTRUCT=TimingInit()
    for Try in 1:Ntries
        test_my_function()
        TIMESTRUCT=TimingProgress(TIMESTRUCT,Try,Ntries;Message="Profiling")
    end
end

println("Run initiate")
test_my_function()
#Profile.clear()
println("Run for profiling")
#@profile test_my_function_long()
println("Profiling complete")
#Profile.print(format=:flat)
@benchmark test_my_function()
TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close Profile.jl")
