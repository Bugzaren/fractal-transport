if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open TestTransportCornerCases.jl")
TabLevel=TabLevel*"    "

using Test
using PyPlot

include("MiscTest.jl")
include("../blueprints/MaskedLattice.jl")
include("../core/ChainAttachments.jl")
include("../core/Greens.jl")

function TestTransportCornerCases()
    @testset "Tests for LatticesWithHoles" begin
        Mask=fill(true,3,3);Mask[2,2]=false #works
        Params=getMaskedLatticeParams(Mask,Plot=false)
        @test test_sym_transport_spectrum(Params,-4:0.01:4)
        Mask=fill(true,4,4);Mask[2:3,2:3].=false  #does not work
        Params=getMaskedLatticeParams(Mask,Plot=false)
        @test test_sym_transport_spectrum(Params,-4:0.01:4)
        Mask=fill(true,4,3);Mask[2:3,2].=false #works
        Params=getMaskedLatticeParams(Mask,Plot=false)
        @test test_sym_transport_spectrum(Params,-4:0.01:4)
        #### no padding
        Mask=fill(true,3,2);Mask[2,1:2].=false # works
        Params=getMaskedLatticeParams(Mask,Plot=false)
        @test test_sym_transport_spectrum(Params,-4:0.01:4)
        #### one padding
        Mask=fill(true,3,4);Mask[2,2:3].=false # does not works
        Params=getMaskedLatticeParams(Mask,Plot=false)
        #### two padding
        @test test_sym_transport_spectrum(Params,-4:0.01:4)
        Mask=fill(true,3,6);Mask[2,3:4].=false # does not works
        Params=getMaskedLatticeParams(Mask,Plot=false)
        @test test_sym_transport_spectrum(Params,-4:0.01:4)
    end
end

function test_sym_transport_spectrum(Params,Energy=rand(10);eta=sqrt(eps()))
    ####test that the transport spectrum is symmetric

    #eta=0
    
    AttPoints=get_chain_attachment_points(Params["IVM"],2)
    TheSlizing=SlizeTheLattice(Params,AttPoints,Plot=false)

    TransportPos=do_MultichanelChainTransport_Slize(AttPoints,Energy,Params,eta=eta,
                                                     SlizeData=TheSlizing)
    TransportNeg=do_MultichanelChainTransport_Slize(AttPoints,-Energy,Params,eta=eta,
                                                     SlizeData=TheSlizing)
    

    atol=10. ^-4
    if !isapprox(TransportPos[:,1,2],TransportNeg[:,1,2],atol=atol)
        figure()
        plot(Energy,TransportPos[:,1,2],":",label="1 -> 2  E > 0")
        plot(Energy,TransportNeg[:,1,2],"-.",label="1 -> 2  E < 0")
        plot(Energy,TransportPos[:,2,1],"--",label="2 -> 1  E > 0")
        plot(Energy,TransportNeg[:,2,1],"-.",label="2 -> 1  E < 0")

        legend(loc="best")
        for EnIndx in 1:length(Energy)
            EVal=Energy[EnIndx]
            TP=TransportPos[EnIndx,1,2]
            TN=TransportNeg[EnIndx,1,2]
            if !isapprox(TP,TN,atol=atol)
                throw(DomainError(Params,"The transport is not symmetric for E=+-$EVal with_values $TP and $TN and diff="*string(TP-TN)))
            end
        end
    end
    true
end

TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close TestTransportCornerCases.jl")
