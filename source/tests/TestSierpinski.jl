if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open TestSierpinski.jl")
TabLevel=TabLevel*"    "

###
import Test

#### Start by loading the Sierlinskye package
include("../lattices/Sierpinski.jl")
include("../core/ChernNumbers.jl")
include("../core/Greens.jl")
include("MiscTest.jl")
include("../core/ChainAttachments.jl")

function TestSierpinski()
    @testset "Tests of the Sierpinski" begin
        @testset "rotation freedom of sierpinski carpet" begin
            @test test_sirpinsky_spectrum(3,1,rand())
            @test test_sirpinsky_spectrum(2,2,rand())
            @test test_sirpinsky_spectrum(3,0,rand())

            @test test_transport_Sierpinski(1,0,2,atol=10^-4)
            @test test_transport_Sierpinski(2,0,2,atol=10^-4)
            @test test_transport_Sierpinski(3,1,4,1,atol=10^-4)
            @test test_transport_Sierpinski(2,2,4,3,atol=10^-4)
            @test test_transport_Sierpinski(3,3,6,1,atol=10^-4)
            @test test_transport_Sierpinski(2,1,6,3,atol=10^-4)
        end
        @testset "Gauge Choise Tests" begin
            @test test_gauge_choises(2,2,0.1,"SCS")
            @test test_gauge_choises(3,2,0.12,"STT")
            @test test_gauge_choises(3,2,0.28,"STR")
            @test test_gauge_choises(3,2,0.31,"SGT")
        end


        @testset "test rotation symmetry" begin
            @test test_rotation_symmetry_fractal(2,0,rand())
            @test test_rotation_symmetry_fractal(2,1,rand())
            @test test_rotation_symmetry_fractal(2,2,rand())
            @test test_rotation_symmetry_fractal(3,3,rand())
	    @test test_rotation_symmetry_fractal(3,3,0.227)
            @test test_rotation_symmetry_fractal(3,2,0.227)
        end

        @testset "Test rot quantum numbers" begin
            @test test_rot_numbers(1,0,0.1,"SCS")
            @test test_rot_numbers(1,1,0.23,"SCS")
            @test test_rot_numbers(2,0,0.23,"SCS")
            @test test_rot_numbers(3,3,0.45,"SCS")
        end
        
        
        @testset "Test the lattice types" begin
            @test test_IVM_VIM_maps_d2()
            @test test_GetBoundarySites()
            @test test_validate_LT()
            @test test_wrong_Lattices()
            @test test_green_functions()
        end

        @testset "test computation of chern numbers" begin
            @test test_ChernNumbers(2,0,rand(),4*(rand()-.5);LT="SCS")
            @test test_ChernNumbers(4,1,rand(),4*(rand()-.5);LT="STT")
            @test test_ChernNumbers(4,2,rand(),4*(rand()-.5);LT="SGT")
            @test test_ChernNumbers(2,2,rand(),4*(rand()-.5);LT="SCT",Base=4)
        end
        
        @testset "test projector matrices" begin
            @test test_projector_matrix(2,0,rand(),-4;LT="SCS")
            @test test_projector_matrix(2,0,rand(),4;LT="SCS")
            
            @test test_projector_matrix(2,0,rand(),4*(rand()-.5);LT="SCS")
            @test test_projector_matrix(4,1,rand(),4*(rand()-.5);LT="STT")
            @test test_projector_matrix(4,2,rand(),4*(rand()-.5);LT="SGT")
            @test test_projector_matrix(2,2,rand(),4*(rand()-.5);LT="SCT",Base=4)
        end
        @testset "Test the new matrix constructor works" begin
            @test test_hamiltonian_generator_Sierpinski(3,2,rand(),LT="SCS")
            @test test_hamiltonian_generator_Sierpinski(4,2,rand(),LT="STT")
            @test test_hamiltonian_generator_Sierpinski(4,1,rand(),LT="SGT")
        end
    end
end


function test_IVM_VIM_maps_d2()
    Params=getSierpinskiParams(2,2)
    IVM=Params["IVM"].+rand(-2:2,2)
    VIM=VIM_from_IVM(IVM)
    NumElems=size(IVM)[2]
    Ntries=1000000
    Indexes=Int.(ceil.(rand(Ntries).*NumElems))
    for INDX in Indexes
        Elem=IVM[:,INDX].+rand(-2:2,2)
        Res=IsOnLattice(Elem,IVM)
        Res2=IsAnElement(Elem,VIM)
        @test Res==Res2
    end
    true
end


function test_transport_Sierpinski(sg=2,fg=2,Terminals=2,LeadWidth=3;eta=sqrt(eps()),atol=sqrt(eps()))
    LT="SCS"
    Phi=0.6536559778882765
    Energy1=0.5944447862478932
    Params1 = getSierpinskiParams(sg,fg,Phi,LT=LT)
    do_test_transport_Ham_vs_Slize(Params1,Energy1,Phi,Terminals,LeadWidth;eta=eta,atol=atol)
    true
end


function test_sirpinsky_spectrum(sg=2,fg=2,Phi=rand())
    LT="SCS"
    Params = getSierpinskiParams(sg,fg,Phi,LT=LT)

    Params["GaugeChoise"]=GaugeChoiseToNum("LandauX")
    Ham=BuildHamiltonian(Params)
    ESLaundauX=eigen(Ham)
    Params["GaugeChoise"]=GaugeChoiseToNum("LandauY")
    Ham=BuildHamiltonian(Params)
    ESLaundauY=eigen(Ham)
    Params["GaugeChoise"]=GaugeChoiseToNum("Symmetric")
    Ham=BuildHamiltonian(Params)
    ESSym=eigen(Ham)
    ###Test_identical spectra
    @test isapprox(ESLaundauY.values,ESLaundauX.values,atol=sqrt(eps()))
    @test isapprox(ESLaundauY.values,ESSym.values,atol=sqrt(eps()))
    ###Test different eigenvecctors
    @test !isapprox(ESLaundauY.vectors,ESLaundauX.vectors,atol=sqrt(eps()))
    @test !isapprox(ESLaundauY.vectors,ESSym.vectors,atol=sqrt(eps()))
    #### Test taht spectrum is symmetric
    @test isapprox(sort(ESLaundauY.values),sort(-ESLaundauY.values),atol=sqrt(eps()))
    true
end



function test_rotation_symmetry_fractal(SG=2,FG=2,Phi=rand();Plot=false)
    #### Test taht if the hamiltonian is rotated and a gauge transformation is applied there is a symmetry.
    LT="SCS"
    Params=getSierpinskiParams(SG,FG,Phi,LT="SCS")
    Ham,IVM=CreateSierpinskiHamiltonian(Params)
    #println(Params)
    HamRot=Ham[end:-1:1,end:-1:1]
    if Plot
        PlotHamiltonian(Ham,IVM)
        title("Original Ham")
        PlotHamiltonian(HamRot,IVM)
        title("Rotated Ham")
    end
    Nsize=Params["N"]
    HamRotGauge=copy(HamRot)
    HamGauge=copy(Ham)
    ###We now make a gauge change
    for xinx in 0:(Nsize-1), yinx in 0:(Nsize-1)
        ##Chanes to differnt landau gauge
        IDNum = IsOnLattice((xinx,yinx),IVM)
        if IDNum!=nothing
            Phase=exp(-im*2*pi*xinx*yinx*Phi)
            PhaseRot=exp(im*2*pi*(Nsize-1)*yinx*Phi)
            HamGauge[IDNum,:]=HamGauge[IDNum,:].*Phase
            HamGauge[:,IDNum]=HamGauge[:,IDNum].*conj(Phase)
            HamRotGauge[IDNum,:]=HamRotGauge[IDNum,:].*PhaseRot
            HamRotGauge[:,IDNum]=HamRotGauge[:,IDNum].*conj(PhaseRot)
        end
    end
    if Plot
        PlotHamiltonian(HamGauge,IVM)
        title("Original Ham gauge tansform")
        PlotHamiltonian(HamRotGauge,IVM)
        title("Rotated Ham gauge transform")
    end
    if !isapprox(HamRotGauge,Ham,atol=sqrt(eps()))
        throw(DomainError("Rotated Hamiltonian not the same as starting hamiltonian\n:$Params"))
    end
    true
end



function test_gauge_choises(SG=2,FG=0,Phi=0.1,LT="SCS")
    Params=getSierpinskiParams(SG,FG,Phi,LT="SCS")
    ParamsSym=getSierpinskiParams(SG,FG,Phi,LT="SCS",Gauge="Symmetric")
    OldHam,OldIVM=do_create_old_style_ham(Params)
    NewHam=BuildHamiltonian(Params)
    SymHam=BuildHamiltonian(ParamsSym)
    DimH=size(Params["IVM"])[2]
    ###Check the sizes
    Test.@test size(OldHam)[1]==size(NewHam)[1]
    Test.@test size(OldHam)[1]==DimH
    ###Check the maps agree
    Test.@test sum(abs.(OldIVM-Params["IVM"]))==0
    Test.@test sum(abs.(OldIVM-ParamsSym["IVM"]))==0
    ###Test that hamiltomnians are the same
    LanSym=true
    for Elem1 in 1:DimH
        for Elem2 in 1:DimH
            OldE=OldHam[Elem1,Elem2]
            NewE=NewHam[Elem1,Elem2]
            SymE=SymHam[Elem1,Elem2]
            Test.@test isapprox(OldE,NewE;atol=sqrt(eps()))
            if LanSym
                LanSym=isapprox(SymE,NewE;atol=sqrt(eps()))
            end
        end
    end 
    Test.@test !LanSym ###test that the symmetric gause is differnt than landay gauge

    ###test that Landau gauge and symmetri gaive give the same eigenvalues
    ESNew = eigen(NewHam)
    ESSym = eigen(SymHam)
    for Eigno in 1:DimH 
        ENew=ESNew.values[Eigno]
        ESym=ESSym.values[Eigno]
         Test.@test isapprox(ENew,ESym;atol=sqrt(eps()))
    end
    true
end



function test_rot_numbers(SG,FG,Phi,LT)
    Params=getSierpinskiParams(SG,FG,Phi,LT="SCS",Gauge="Symmetric")
    NewHam,NewIVM=CreateSierpinskiHamiltonian(Params)
    Ham0,IVM0=CreateSierpinskiHamiltonianRotSym(Params,0)#,Plot=true)
    Ham1,IVM1=CreateSierpinskiHamiltonianRotSym(Params,1)#,Plot=true)
    Ham2,IVM2=CreateSierpinskiHamiltonianRotSym(Params,2)#,Plot=true)
    Ham3,IVM3=CreateSierpinskiHamiltonianRotSym(Params,3)#,Plot=true)

    DL=(size(Ham0)[1],size(Ham1)[1],size(Ham2)[1],size(Ham3)[1])
    display(DL)
    ###Check the sizes
    Test.@test size(NewHam)[1]==sum(DL)
    ###Test that hamiltomnians are the same
    ESNew = sort(eigen(NewHam).values)
    #println("ESNew")
    #display(ESNew)
    ESSym0 = eigen(Ham0).values
    ESSym1 = eigen(Ham1).values
    ESSym2 = eigen(Ham2).values
    ESSym3 = eigen(Ham3).values
    ESSym=sort([ESSym0;ESSym1;ESSym2;ESSym3])
    for Eigno in 1:sum(DL)
        Test.@test isapprox(ESNew[Eigno],ESSym[Eigno];atol=sqrt(eps()))
    end
    true
end



function test_green_functions()
    ###Sole a small sierpinski problem
    EigSys=SolveHamiltonian(getSierpinskiParams(2,0,0))
    ###Compute Naive greens function
    Energy=rand()+im
    NG=GreensFunctionFixedEnergy(EigSys,Energy);
    for i in 1:length(EigSys.values)
        for j in 1:length(EigSys.values)
            NGFE=GreensFunctionFixedElement(EigSys,[Energy],(i,j));
            ###Test that the lemens that the same
            Test.@test NG[i,j] ≈ NGFE[1]
        end
    end
    true
end

function test_validate_LT()
    for X2 = ["C","G","T"]
        for X3 = ["S","T","R","X"]
            Test.@test nothing == validate_LT("S"*X2*X3) 
        end
    end
    true
end

function test_GetBoundarySites()
    for X2 = ["C","G","T"]
        Test.@test nothing != GetBoundarySites(2,0,"S"*X2*"S","L")
        Test.@test nothing != GetBoundarySites(2,0,"S"*X2*"S","R")
        Test.@test nothing != GetBoundarySites(2,0,"S"*X2*"S","T")
        Test.@test nothing != GetBoundarySites(2,0,"S"*X2*"S","B")
    end
    true
end



function test_wrong_Lattices()
    ###Middle !=A
    Test.@test_throws DomainError getSierpinskiParams(1,0,0,LT="SAS")
    ###Ends !=A
    Test.@test_throws DomainError getSierpinskiParams(1,0,0,LT="SCA")
    ##Begining ==S
    Test.@test_throws DomainError getSierpinskiParams(1,0,0,LT="TAS")
    ##Only four letters
    Test.@test_throws DomainError getSierpinskiParams(1,0,0,LT="SCSS")
    true
end


function test_hamiltonian_generator_Sierpinski(sg,fg,phase;LT="SCS")
    ###Create the parameters, ###we add a little bit of disorder 
    Params=getSierpinskiParams(sg,fg,phase,LT=LT)
    Ham1=BuildHamiltonian(Params)###This build it using the generic funcitons
    Ham2,_=CreateSierpinskiHamiltonian(Params)###This builds is using the specialized funcitons
    @test isapprox(Ham1,Ham2,atol=sqrt(eps()))
    true
end



function test_ChernNumbers(SizeGen,FracGen,Phase,EnergyList;LT="SCS",Base=10)
    EigSys=SolveHamiltonian(getSierpinskiParams(SizeGen,FracGen,Phase;LT=LT))
    IndVecMap,_=GetNonZeroElements(GetTheSierpinskyLattice(SizeGen,FracGen,LT))


    Params=getSierpinskiParams(SizeGen,FracGen,Phase;LT=LT)
    IVMStartPoint=SierpiskyChernStart(Params)

    
    ####Determine the regions A,B,C used in Nuepert paper http://arxiv.org/abs/1807.00367v1
    (RegionA,RegionB,RegionC)=get_region_indices(Params,IVMStartPoint,Base)
    (RegionAp,RegionBp,RegionCp,FullRegion)=condense_region(RegionA,RegionB,RegionC)
    
    TIMESTRUCT=TimingInit()
    for EnIndx in 1:length(EnergyList)
        PM=get_projector_matrix(EigSys,EnergyList[EnIndx])
        PMp=get_projector_matrix(EigSys,EnergyList[EnIndx],FullRegion)
        Chern1=get_chern_number(PM,RegionA,RegionB,RegionC)
        Chern2=get_chern_number(PMp,RegionAp,RegionBp,RegionCp)
        Test.@test Chern1 ≈ Chern2
        TIMESTRUCT=TimingProgress(TIMESTRUCT,EnIndx,length(EnergyList),Message="ChernNumber")
    end
    true
end


function test_projector_matrix(SizeGen,FracGen,Phase,EnergyList;LT="SCS",Base=10)
    println("-----------------------------------")
    EigSys=SolveHamiltonian(getSierpinskiParams(SizeGen,FracGen,Phase;LT=LT))
    IndVecMap,_=GetNonZeroElements(GetTheSierpinskyLattice(SizeGen,FracGen,LT))

    Params=getSierpinskiParams(SizeGen,FracGen,Phase;LT=LT)
    IVMStartPoint=SierpiskyChernStart(Params)
    
    ####Determine the regions A,B,C used in Nuepert paper http://arxiv.org/abs/1807.00367v1
    (RegionA,RegionB,RegionC)=get_region_indices(Params,IVMStartPoint,Base)
    (RegionAp,RegionBp,RegionCp,FullRegion)=condense_region(RegionA,RegionB,RegionC)
    
    TIMESTRUCT=TimingInit()
    for EnIndx in 1:length(EnergyList)
        ####Create the enry matrices by explicity counding form the bottom, from the top,
        ###Or from choosing wihc one is best
        ePM=get_projector_matrix(EigSys,EnergyList[EnIndx],Slice="empty")
        fPM=get_projector_matrix(EigSys,EnergyList[EnIndx],Slice="full")
        bPM=get_projector_matrix(EigSys,EnergyList[EnIndx],Slice="best")
        Test.@test isapprox(0,sum(abs.(ePM-fPM));atol=sqrt(eps()))
        Test.@test isapprox(0,sum(abs.(ePM-bPM));atol=sqrt(eps()))
        ePMp=get_projector_matrix(EigSys,EnergyList[EnIndx],FullRegion,Slice="empty")
        fPMp=get_projector_matrix(EigSys,EnergyList[EnIndx],FullRegion,Slice="full")
        bPMp=get_projector_matrix(EigSys,EnergyList[EnIndx],FullRegion,Slice="best")
        Test.@test isapprox(0,sum(abs.(ePMp-fPMp));atol=sqrt(eps()))
        Test.@test isapprox(0,sum(abs.(ePMp-bPMp));atol=sqrt(eps()))
        TIMESTRUCT=TimingProgress(TIMESTRUCT,EnIndx,length(EnergyList),Message="ChernNumber")
    end
    true
end


function do_create_old_style_ham(Params)
    ### Code for hamiltonian generation as of 2019-01-29 (rev 2498)
    LT=Params["LT"];SizeGen=Params["sg"];FracGen=Params["fg"]
    Phase=Params["ph"];W=Params["W"];tW=Params["tW"];t=Params["t"]

    validate_LT(LT)
    N=get_lattice_size(SizeGen,LT)
    TheLattice=GetTheSierpinskyLattice(SizeGen,FracGen,LT) ###Plot if exists
    IndVecMap,Ncount=GetNonZeroElements(TheLattice) ###Ellemtns, ordered after row and then column
    Hamiltonian = ones(Complex,Ncount,Ncount) .* 0.0

    for j in 1:Ncount
        Hamiltonian[j,j]=W*2*(rand()-.5)
        (row,col)=IndVecMap[:,j]
        j2=IsOnLattice((row,col-1),IndVecMap) # (backward)
        if j2!=nothing && Sierpinski_bond_is_present(LT,SizeGen,FracGen,(row,col),(row,col-1))
            trand=t*(1.0+tW*2*(rand()-.5))
            Hamiltonian[j,j2] = -trand*exp(im*pi*row*2*Phase)
            Hamiltonian[j2,j] = -trand*exp(-im*pi*row*2*Phase)
        end
        j2=IsOnLattice((row,col+1),IndVecMap) ## (forward)
        if j2!=nothing && Sierpinski_bond_is_present(LT,SizeGen,FracGen,(row,col),(row,col+1))
            trand=t*(1.0+tW*2*(rand()-.5))
            Hamiltonian[j,j2] = -trand*exp(-im*pi*row*2*Phase)
            Hamiltonian[j2,j] = -trand*exp(im*pi*row*2*Phase)
        end
        j2=IsOnLattice((row-1,col),IndVecMap)# (down)
        if j2!=nothing && Sierpinski_bond_is_present(LT,SizeGen,FracGen,(row,col),(row-1,col))
            trand=t*(1.0+tW*2*(rand()-.5))
            Hamiltonian[j,j2] = -trand
            Hamiltonian[j2,j] = -trand
        end
        j2=IsOnLattice((row+1,col),IndVecMap)  # (up)
        if j2!=nothing && Sierpinski_bond_is_present(LT,SizeGen,FracGen,(row,col),(row+1,col))
            trand=t*(1.0+tW*2*(rand()-.5))
            Hamiltonian[j,j2] = -trand
            Hamiltonian[j2,j] = -trand
        end
        if LT[3]=='T' || LT[3]=='X'  ### Triangular lattice
            j2=IsOnLattice((row+1,col-1),IndVecMap) ## (back/up)
            if j2!=nothing && Sierpinski_bond_is_present(LT,SizeGen,FracGen,(row,col),(row+1,col-1))
                trand=t*(1.0+tW*2*(rand()-.5))
                Hamiltonian[j,j2] = -trand*exp(im*pi*2*(row+.5)*Phase)
                Hamiltonian[j2,j] = -trand*exp(-im*pi*2*(row+.5)*Phase)
            end
            j2=IsOnLattice((row-1,col+1),IndVecMap) ## (forw/down)
            if j2!=nothing && Sierpinski_bond_is_present(LT,SizeGen,FracGen,(row,col),(row-1,col+1))
                trand=t*(1.0+tW*2*(rand()-.5))
                Hamiltonian[j,j2] = -trand*exp(-im*pi*2*(row-.5)*Phase)
                Hamiltonian[j2,j] = -trand*exp(im*pi*2*(row-.5)*Phase)
            end
        end
        if LT[3]=='R' || LT[3]=='X'  ### Triangular lattice
            j2=IsOnLattice((row-1,col-1),IndVecMap) ## (back/down)
            if j2!=nothing && Sierpinski_bond_is_present(LT,SizeGen,FracGen,(row,col),(row-1,col-1))
                trand=t*(1.0+tW*2*(rand()-.5))
                Hamiltonian[j,j2] = -trand*exp(im*pi*2*(row-.5)*Phase)
                Hamiltonian[j2,j] = -trand*exp(-im*pi*2*(row-.5)*Phase)
            end
            j2=IsOnLattice((row+1,col+1),IndVecMap) ## (forw/up)
            if j2!=nothing && Sierpinski_bond_is_present(LT,SizeGen,FracGen,(row,col),(row+1,col+1))
                trand=t*(1.0+tW*2*(rand()-.5))
                Hamiltonian[j,j2] = -trand*exp(-im*pi*2*(row+.5)*Phase)
                Hamiltonian[j2,j] = -trand*exp(im*pi*2*(row+.5)*Phase)
            end
        end
    end
    return (Hamiltonian,IndVecMap)
end








function CreateSierpinskiHamiltonian(Params;Plot=false,Verbose=true)
    LT=Params["LT"]
    SizeGen=Params["sg"]
    FracGen=Params["fg"]
    Phase=Params["ph"]
    W=Params["W"]
    tW=Params["tW"]
    t=Params["t"]
    GaugeChoise=Params["GaugeChoise"]
    IndVecMap=Params["IVM"]
    Ncount=size(IndVecMap)[2]
    
    if Verbose
        println("Creating empty hamiltonian for sg=$SizeGen, fg=$FracGen, ph=$Phase with noise strecght W=$W and hopping strngth noice tW=$tW")
    end
    StartTime=now()

    Hamiltonian = fill(0.0+im*0.0,Ncount,Ncount)


    if Verbose 
        EndTime=now()
        DiffTime=PrettyTime(EndTime-StartTime)
        println("It took $DiffTime to create the empty hamiltonian the Hamiltonian")
    end

    
    ###Empty hamiltonian
    ###Populate with hopping elements

    if Verbose 
        println("Building hamiltonian for sg=$SizeGen, fg=$FracGen, ph=$Phase")
    end
    StartTime=now()

    TIMESTRUCT=TimingInit()
    for j in 1:Ncount
        ####Add diaognal element
        Hamiltonian[j,j]=W*2*(rand()-.5)
        #println("----------------")
        (row,col)=IndVecMap[:,j]
        FromPoint=(row,col)
        PopulateHam!(Hamiltonian,j,IndVecMap,FromPoint,(row,col-1),
                     Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen)#backward
        PopulateHam!(Hamiltonian,j,IndVecMap,FromPoint,(row,col+1),
                     Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen)#forward
        PopulateHam!(Hamiltonian,j,IndVecMap,FromPoint,(row-1,col),
                     Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen)#down
        PopulateHam!(Hamiltonian,j,IndVecMap,FromPoint,(row+1,col),
                     Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen)#up
        if LT[3]=='T' || LT[3]=='X'  ### Triangular lattice
            PopulateHam!(Hamiltonian,j,IndVecMap,FromPoint,(row+1,col-1),
                         Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen)#(back/up)
            PopulateHam!(Hamiltonian,j,IndVecMap,FromPoint,(row-1,col+1),
                         Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen)#(forw/down)
        end
        if LT[3]=='R' || LT[3]=='X'  ### Triangular lattice
            PopulateHam!(Hamiltonian,j,IndVecMap,FromPoint,(row+1,col+1),
                         Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen)#(forw/up)
            PopulateHam!(Hamiltonian,j,IndVecMap,FromPoint,(row-1,col-1),
                         Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen)#(back/down)
        end

        TIMESTRUCT=TimingProgress(TIMESTRUCT,j,Ncount;Message="j=$j row=$row col=$col")
    end

    if Verbose 
        EndTime=now()
        DiffTime=PrettyTime(EndTime-StartTime)
        println("It took $DiffTime to complete the Hamiltonian")
    end
    
    HamSize=size(IndVecMap)[2]
    #if false #### disable thats below
    if HamSize < 10 && Plot
        display(Hamiltonian)
        println()
    end
    
    if HamSize < 500 && Plot
        figure()
        imshow(abs.(Hamiltonian))
        title("The elements of the hamiltonian")
        colorbar()
    end
    
    if Plot ###&& Ncount < 1500
        PlotHamiltonian(Hamiltonian,get_Sierpinski_IPM(IndVecMap,Params))
    end
    
    return (Hamiltonian,IndVecMap)
end


function CreateSierpinskiHamiltonianRotSym(Params,RQ::Integer;Plot=false,Verbose=false)
    LT=Params["LT"];SizeGen=Params["sg"];FracGen=Params["fg"];
    Phase=Params["ph"];W=Params["W"];tW=Params["tW"]
    t=Params["t"];GaugeChoise=Params["GaugeChoise"]
    if GaugeChoise != 1 ###Not mymetric gauge
        throw(DomainError("Currenly rot quantun numbers is only implemented for symmetric gauge"))
    end
    if LT != "SCS" ###NOT square lattice
        throw(DomainError("Currenly rot quantun numbers is only implemented for square lattice"))
    end
    validate_LT(LT);N=get_lattice_size(SizeGen,LT)
    if Verbose 
        println("Creating the lattice for $LT at sg=$SizeGen, fg=$FracGen")
    end
    if RQ<0 || RQ>=4
        println("RQ not in the scope 0<=RQ<=3, periodicity not implemented")
    end

    ####Begin by building the lattice####
    TheLattice=GetTheSierpinskyLattice(SizeGen,FracGen,LT) ###Plot if exists
    #display(TheLattice)

    ###We define an offset
    ###We know that N=2*n+1 so we choose offset=n to be in the middle
    Offset=(N-1)/2
    
    if Verbose     
        println("Constructing lattice-vector-map $LT at for sg=$SizeGen, fg=$FracGen")
    end
    IndVecMap,Ncount=GetNonZeroElements(TheLattice,(Offset,Offset)) ###Ellemtns, ordered after row and then column

    if Verbose     
        println("List of Nonzero elements:")
        display(IndVecMap)
        println("Ncount=$Ncount")
    end

    OriginExists= IsOnLattice((0,0),IndVecMap) != nothing
    if Verbose     
        OriginExists && println("The origin is a lattice point")
    end
   
    ###Now we consider only the elements in the (lefts upper right quadrangle)
    ### Thus we throw away all other elements from the matrix
    ### Thsi also ensures that the zero elemt is the fist element of the matrix
    ToFQuarter=(IndVecMap[1,:] .>= 0) .& (IndVecMap[2,:] .> 0) ###x>=0,y>0
    if RQ==0 && OriginExists
        ToFCenter=(IndVecMap[1,:] .== 0) .& (IndVecMap[2,:] .== 0) ###x=y=0
        IVMCut=IndVecMap[:,ToFQuarter .| ToFCenter ]
    else
        IVMCut=IndVecMap[:,ToFQuarter]
    end
    NewCount=size(IVMCut)[2]

    if Verbose     
        println("List of Reduced Nonzero elements:")
        display(IVMCut)
        println("NewCount=$NewCount")
    end
    
    if Verbose 
        println("Creating empty hamiltonian for sg=$SizeGen, fg=$FracGen, ph=$Phase with noise strecght W=$W and hopping strngth noice tW=$tW")
    end
    StartTime=now()

    Hamiltonian = ones(Complex,NewCount,NewCount) .* 0.0
    
    ###Empty hamiltonian
    ###Populate with hopping elements

    
    if Verbose 
        println("Building hamiltonian for sg=$SizeGen, fg=$FracGen, ph=$Phase")
    end
    TIMESTRUCT=TimingInit()
    for j in 1:NewCount
        ####Add diaognal element
        Hamiltonian[j,j]=W*2*(rand()-.5)
        #println("----------------")
        (row,col)=IVMCut[:,j]
        FromPoint=(row,col)
        #println("FromPoint:",FromPoint)
        PopulateHamRot!(Hamiltonian,j,IVMCut,FromPoint,(row,col-1),
                     Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen,RQ=RQ)#backward
        PopulateHamRot!(Hamiltonian,j,IVMCut,FromPoint,(row,col+1),
                     Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen,RQ=RQ)#forward
        PopulateHamRot!(Hamiltonian,j,IVMCut,FromPoint,(row-1,col),
                     Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen,RQ=RQ)#down
        PopulateHamRot!(Hamiltonian,j,IVMCut,FromPoint,(row+1,col),
                     Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen,RQ=RQ)#up
        if LT[3]=='T' || LT[3]=='X'  ### Triangular lattice
            PopulateHamRot!(Hamiltonian,j,IVMCut,FromPoint,(row+1,col-1),
                         Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen,RQ=RQ)#(back/up)
            PopulateHamRot!(Hamiltonian,j,IVMCut,FromPoint,(row-1,col+1),
                         Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen,RQ=RQ)#(forw/down)
        end
        if LT[3]=='R' || LT[3]=='X'  ### Triangular lattice
            PopulateHamRot!(Hamiltonian,j,IVMCut,FromPoint,(row+1,col+1),
                         Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen,RQ=RQ)#(forw/up)
            PopulateHamRot!(Hamiltonian,j,IVMCut,FromPoint,(row-1,col-1),
                         Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen,RQ=RQ)#(back/down)
        end
        TIMESTRUCT=TimingProgress(TIMESTRUCT,j,Ncount;Message="j=$j row=$row col=$col")
    end

    if Verbose 
        EndTime=now()
        DiffTime=PrettyTime(EndTime-StartTime)
        println("It took $DiffTime to complete the Hamiltonian")
    end
    
    HamSize=size(IndVecMap)[2]
    #if false #### disable thats below
    if HamSize < 10 && Plot
        display(Hamiltonian)
        println()
    end
    
    if HamSize < 500 && Plot
        figure()
        imshow(abs.(Hamiltonian))
        title("The elements of the hamiltonian")
        colorbar()
    end
    
    if N < 40 && Plot
        PlotHamiltonian(Hamiltonian,IVMCut)
    end
    
    return (Hamiltonian,IVMCut)
end




function PopulateHam!(Ham,j,IVM,FromPoint,ToPoint,Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen;PlusRot=false,ExtraPhase=0.0)
    j2=IsOnLattice(ToPoint,IVM)
    if j2!=nothing && j2 > j && Sierpinski_bond_is_present(LT,SizeGen,FracGen,FromPoint,ToPoint)
        if tW!=0.0
            trand=t*(1.0+tW*2*(rand()-.5))
        else
            trand=t
        end
        Ham[j2,j]=Ham[j2,j]+PhaseFactor(trand,FromPoint,ToPoint,Phase,GaugeChoise)*exp(im*ExtraPhase)
        Ham[j,j2]=Ham[j,j2]+PhaseFactor(trand,ToPoint,FromPoint,Phase,GaugeChoise)*exp(-im*ExtraPhase)
    end
end


function PopulateHamRot!(Ham,j,IVM,FromPoint,ToPoint,Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen;RQ::Integer=0)
    if FromPoint!=(0,0)
        for RotNo in 0:3
            ###Rotate the hopping points
            if RotNo>0
                ToPoint=(-ToPoint[2],ToPoint[1])
                FromPoint=(-FromPoint[2],FromPoint[1])
            end
            PopulateHam!(Ham,j,IVM,FromPoint,ToPoint,Phase,GaugeChoise,tW,t,LT,SizeGen,FracGen,ExtraPhase=RQ*RotNo*pi/2.0)
        end
    else ###If we are lloking at the center element, we do not need to worry about rotations
        ### But he amplitude comes in with a factor of 2 (we put this factor on the hopping, t)
        PopulateHam!(Ham,j,IVM,FromPoint,ToPoint,Phase,GaugeChoise,tW,2*t,LT,SizeGen,FracGen)
    end
end


if TabLevel == "    "
    ###Run the test if the file s not included
    TestSierpinski() 
end

TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close TestSierpinski.jl")
