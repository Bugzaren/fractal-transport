if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open MiscTest.jl")
TabLevel=TabLevel*"    "

include("../core/DeprecatedTransport.jl")
include("../core/SliceLattice.jl")

function comparematrices(Ham1,Ham2,Name1,Name2)
    if !isapprox(Ham1,Ham2,atol=sqrt(eps()))
        throw(ErrorException("The matrix $Name1 and $Name2 are nto the same!\n"*
                             "$Name1:\n$Ham1\n"*
                             "$Name2:\n$Ham2\n"*
                             "with diff\n"*
                             string(Ham1-Ham2)*"\n"))
    end
end


function do_test_transport_Ham_vs_Slize(Params1,Energy1,Phi,Terminals,LeadWidth;eta=sqrt(eps()),atol=sqrt(eps()))
    IVM=Params1["IVM"]
    Ham1=BuildHamiltonian(Params1)
    #PlotHamiltonian(Ham1,IVM)
    AttPoints=get_chain_attachment_points(IVM,Terminals,LeadWidth)
    println("AttPoints:",AttPoints)
    #plot_attachments(AttPoints,IVM)


    SlizeData=SlizeTheLattice(Params1,AttPoints,Plot=false)
    
    ChainFromHam = ConstructChaintoFullHamiltonian(AttPoints,Ham1)
    ChainFromSlize = ConstructChainHamiltonian(AttPoints,SlizeData,Params1)
    for Tno in 1:Terminals
        @test isapprox(ChainFromHam[1][Tno],ChainFromSlize[1][Tno],atol=atol)
        @test isapprox(ChainFromHam[2][Tno],ChainFromSlize[2][Tno],atol=atol)
        #@test isapprox(ChainFromHam[3][Tno],ChainFromSlize[3][Tno],atol=atol)
    end

    E0 = Energy1 + im*eta
    ####Test the auxilliary Greens functions
    DictsSlize=CreateTheAxillaryGreensFunctions(
        E0,Terminals,ChainFromSlize)
    
    DictsSlizeHam=CreateTheAxillaryGreensFunctions(
        E0,Terminals,ChainFromHam)
    for Tno in 1:Terminals
        @test isapprox(DictsSlize[1][Tno],DictsSlizeHam[1][Tno],atol=atol)
        @test isapprox(DictsSlize[2][Tno],DictsSlizeHam[2][Tno],atol=atol)
        #@test isapprox(DictsSlize[3][Tno],DictsSlizeHam[3][Tno],atol=atol)
    end
    

    
    Transport11Ham=do_MultichanelChainTransport_Ham(AttPoints,Energy1,Ham1,eta=eta)[1,:,:]
    Transport11Slize=do_MultichanelChainTransport_Slize(AttPoints,Energy1,Params1,eta=eta)[1,:,:]
    if !isapprox(Transport11Ham,Transport11Slize,atol=atol)
        #plot_attachments(AttPoints,IVM)
        throw(DomainError(Params1,"The Hamitonian and Slize transport is not the same\n"*
                          "Ham:\n$Transport11Ham\nand Slize:\n"*
                          "$Transport11Slize\nwith diff\n"*
                          string(Transport11Ham-Transport11Slize)*"\n"*
                          "are not the same for Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$Energy1"))
    end
    

    ###Now we compare the different values

    if Terminals==2
        Duals=[2,1]
    elseif Terminals==4
        Duals=[2,1,4,3]
    elseif Terminals==6
        Duals=[2,1,6,5,4,3]
    end
    for T1 in 1:Terminals
        for T2 in T1:Terminals
            compare_transport(Transport11Ham,(T1,T2),(Duals[T1],Duals[T2]),
                              Terminals,LeadWidth,Energy1,Phi,Params1,atol=atol)
        end
    end
    true
end




function compare_transport(Transport11,(From1,To1),(From2,To2),
                            Terminals,LeadWidth,Energy,Phi,Params;atol=sqrt(eps()))
    if !isapprox(Transport11[From1,To1],Transport11[From2,To2],atol=atol)
        #plot_attachments(AttPoints,IVM)
        println("Transport11")
        display(Transport11)
        throw(DomainError(Transport11,"The transport matrix does not have the corrects rotations:\n"*
                          "Transport11[$From1,$To1]="*string(Transport11[From1,To1])*" is not the same as\n"*
                          "Transport11[$From2,$To2]="*string(Transport11[From2,To2])*"\n"*
                          "For Terminals=$Terminals,LeadWidth=$LeadWidth and Energy=$Energy and params $Params"))
    end
end
TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close MiscTest.jl")
