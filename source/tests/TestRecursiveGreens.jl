if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open TestRecursiveGreens.jl")
TabLevel=TabLevel*"    "

###
import Test

#### Start by loading the Sierlinskye package
include("../core/ScanFunctions.jl")
include("../lattices/Sierpinski.jl")


function TestRecursiveGreens()
    @testset "Tests for recursive greens functions methods" begin
        @testset "Test external leads constructor" begin
            @test test_external_leads_constructor(getSierpinskiParams(3,2,0.2),
                                                  Terminals=2)
            @test test_external_leads_constructor(getSierpinskiParams(2,0,0.11),
                                                  Terminals=4,LeadWidth=2)
            @test test_external_leads_constructor(getSierpinskiParams(2,2,0.25),
                                                  Terminals=3,LeadWidth=3)
            @test test_external_leads_constructor(getChainParams(5,4,0.11),
                                                  Terminals=4,LeadWidth=2)
        end
        
        @testset "Test that Greens functions are identical" begin
            @test test_identical_green_functions(getChainParams(4,2,0.0),
                                                 Terminals=4,LeadWidth=2)
            @test test_identical_green_functions(getChainParams(4,2,0.11),
                                                 Terminals=4,LeadWidth=2)
            @test test_identical_green_functions(getChainParams(5,5,0.09),
                                                 Terminals=6,LeadWidth=1)
            @test test_identical_green_functions(getChainParams(4,5,0.09),
                                                 Terminals=3,LeadWidth=1)
            @test test_identical_green_functions(getChainParams(9,7,0.2),
                                                 Terminals=2)
            @test test_identical_green_functions(getChainParams(5,4,0.11),
                                                 Terminals=4,LeadWidth=2)
            @test test_identical_green_functions(getSierpinskiParams(2,2,0.25),
                                                 Terminals=3,LeadWidth=3)
        end
        
        @testset "Test the full multichanel calculations" begin
            @test test_slize_multi_chan(1,0,Terminals=2)
            @test test_slize_multi_chan(2,0,Terminals=2)
            @test test_slize_multi_chan(2,2,Terminals=2)
            @test test_slize_multi_chan(2,2,Terminals=3,LeadWidth=1)
            @test test_slize_multi_chan(2,2,Terminals=4,LeadWidth=1)
            @test test_slize_multi_chan(2,2,Terminals=3,LeadWidth=2)
            @test test_slize_multi_chan(2,2,Terminals=4,LeadWidth=2)
            @test test_slize_multi_chan(3,0,Terminals=6,LeadWidth=3)
            ###This caused an actaull error 
            @test test_slize_multi_chan(3,0,0.34795365544965784,Terminals=6,LeadWidth=3)
        end

        @testset "Test voltage calcs transport" begin
            @test test_chain()
            @test test_transport_scanners()
        end
        
        @testset "Test slize vs ED transport" begin
            @test test_slize_and_ED_transport(3,4,rand())
            @test test_Sierpinski_slize_and_ED_transport(2,0,rand(),"SCS")
            @test test_Sierpinski_slize_and_ED_transport(3,1,rand(),"SCS")
        end
        
        @testset "test static and auto generated sierpinski ham" begin
            ####Sierpinski carpet
            @test test_slize_sierpinski_ham_with_mag_field(2,1,rand(),"SCS")
            @test test_slize_sierpinski_ham_with_mag_field(2,0,rand(),"SCS")
            @test test_slize_sierpinski_ham_with_mag_field(1,1,rand(),"SCS")
            ####Sierpinski gacet
            for sg in 1:3
                for fg in 0:(sg-1)
                    @test test_slize_sierpinski_ham_with_mag_field(sg,fg,rand(),"SGT")
                end
            end
            ####Sierpinski triangle
            for sg in 1:3
                for fg in 0:(sg-1)
                    @test test_slize_sierpinski_ham_with_mag_field(sg,fg,rand(),"STT")
                end
            end
        end


        @testset "test static and auto generated square ham" begin
            for indx in 1:10
                @test test_slize_ham_with_mag_field(rand(1:10),rand(1:10),rand())
                @test test_auto_generated_ham(rand(1:10),rand(1:10),rand())
            end
        end

        @testset "test equivalence siepinsky square" begin
            @test test_equivalence_siepinsky_square(1,rand())
            @test test_equivalence_siepinsky_square(2,rand())
            @test test_equivalence_siepinsky_square(3,rand())
        end


        @testset "test equivalence square ham" begin
            ###Simple inversion
            @test test_equvalence_square_lattice(1,1) 
            @test test_equvalence_square_lattice(2,1)
            ### Uses Dyson eqn
            @test test_equvalence_square_lattice(1,2) 
            @test test_equvalence_square_lattice(2,2) 
            @test test_equvalence_square_lattice(3,2)
            ###Requites recusions
            @test test_equvalence_square_lattice(3,3)
            @test test_equvalence_square_lattice(4,7)
        end
        
        @testset "test recursion performance" begin
            @test test_performance_recursion(3,3,rand(),"SCS",10)
            @test test_performance_recursion(4,3,rand(),"STT",10)
            #@test test_performance_recursion(4,4,rand(),"SCS",10)
        end

        @testset "Test multichanel transport" begin
            @test_broken TestMultichanel(1,1)
            @test_broken TestMultichanel(2,2)
            @test TestMultichanel(3,3)
            @test_broken TestMultichanel(3,1)
            @test_broken TestMultichanel(1,3)
            @test TestMultichanel(3,2)
            @test TestMultichanel(4,2)
        end
        
    end
end

function test_identical_green_functions(Params;Terminals=3,LeadWidth=1)
    ####He we chack that the Green functions that are computed are the same
    Ham=BuildHamiltonian(Params)
    ###Construct the side incexes
    SideIndexes=get_chain_attachment_points(Params["IVM"],Terminals,LeadWidth)
    ###Slize the lattice
    SlizeData=SlizeTheLattice(Params,SideIndexes,Plot=false)

    ###Now we compute the lead hamiltonians
    ChainHamfromSlize=ConstructChainHamiltonian(SideIndexes,SlizeData,Params)
    ChainHamfromFullHam=ConstructChaintoFullHamiltonian(SideIndexes,Ham)


    #println("~~~~~~  ChainHamfromSlize ~~~~~~")
    #DisplayChainHamiltinainInfo(ChainHamfromSlize,Terminals)
    #println("~~~~~~  ChainHamfromFullHam ~~~~~~")
    #DisplayChainHamiltinainInfo(ChainHamfromFullHam,Terminals)

    ChemicalPotential=0.3+.2*im ####Set the chemical potential
    
    DictGsiSlize,DictSelfEnSlize,DictGammaSlize=
        CreateTheAxillaryGreensFunctions(ChemicalPotential,Terminals,ChainHamfromSlize)
    DictGsiHam,DictSelfEnHam,DictGammaHam=
        CreateTheAxillaryGreensFunctions(ChemicalPotential,Terminals,ChainHamfromFullHam)


    for Lead in 1:Terminals
        ensure_the_same(DictGsiSlize[Lead],
                        DictGsiHam[Lead];atol=sqrt(eps()),
                        Alab="DictGsiSlize[$Lead]",
                        Blab="DictGsiHam[$Lead]")
        ensure_the_same(DictSelfEnSlize[Lead],
                        DictSelfEnHam[Lead];atol=sqrt(eps()),
                        Alab="DictSelfEnSlize[$Lead]",
                        Blab="DictSelfEnHam[$Lead]")
        ensure_the_same(DictGammaSlize[Lead],
                        DictGammaHam[Lead];atol=sqrt(eps()),
                        Alab="DictGammaSlize[$Lead]",
                        Blab="DictGammaHam[$Lead]")
        println("ChainHamfromSlize[3][Lead]:")
        display(ChainHamfromSlize[3][Lead])
        println("ChainHamfromFullHam[3][Lead]:")
        display(ChainHamfromFullHam[3][Lead])
        #ensure_the_same(ChainHamfromSlize[3][Lead],
        #                ChainHamfromFullHam[3][Lead];atol=sqrt(eps()),
        #                Alab="ChainHamfromSlize[3][$Lead]",
        #                Blab="ChainHamfromFullHam[3][$Lead]")
    end
    
    
    HamGen=Params["GenHamElems"]
    GMatrixSlize = GreensFunRecursiveSlize(HamGen,SlizeData[1],ChemicalPotential,Params,
                                           DictGsiSlize,ChainHamfromSlize[3],SlizeData[2])
    
    
    GMatrixHam = GreensFunFullHamiltonian(Ham,SideIndexes,ChemicalPotential,
                                          DictGsiHam,ChainHamfromFullHam[3],Plot=false)
    
    ###Now we check that the green functions are the same
    for Lead1 in 1:Terminals
        for Lead2 in 1:Terminals
            if Lead1!=Lead2
                @test ensure_the_same(GMatrixSlize[Lead1,Lead2],
                                      GMatrixHam[Lead1,Lead2];atol=sqrt(eps()),
                                      Alab="GMatrixSlize[$Lead1,$Lead2]",
                                      Blab="GMatrixSlize[$Lead1,$Lead2]")
            end
        end
    end
    true
end



function test_external_leads_constructor(Params;Terminals=3,LeadWidth=1)
    ####Here we test that the chain hamiltonians that are created are the same irrespective of if they come from the Hamiltonian or from the Slize function
    
    Ham=BuildHamiltonian(Params)
    ###Construct the side incexes
    SideIndexes=get_chain_attachment_points(Params["IVM"],Terminals,LeadWidth)
    ###Slize the lattice
    SlizeData=SlizeTheLattice(Params,SideIndexes,Plot=false)
    ###Compute the Chain Hamiltonians by genrati ng the hamiltonain on the fly
    ChainHamfromSlize=ConstructChainHamiltonian(SideIndexes,SlizeData,Params;ExplicitHam=nothing)
    ###Compute the Chain Hamiltonians by looking at the explicit hamiltonian
    ChainHamfromHam=ConstructChainHamiltonian(SideIndexes,SlizeData;ExplicitHam=Ham)
    ###Compute the Chain Hamiltonians by looking at the explicit hamiltonian
    ChainHamfromFullHam=ConstructChaintoFullHamiltonian(SideIndexes,Ham)
    for Lead in 1:Terminals
        #println("- - - - - - - - - - - - - ")
        #println("SideIndexes[Lead=$Lead]: ",SideIndexes[Lead])
        ###Extract the local slize indexes
        LocalSlizeIndexes=SlizeData[1][SlizeData[2][Lead]]
        #println("SlizeData[Lead=$Lead]: ",LocalSlizeIndexes)
        for indx in 1:3
            ###Ensure the two magtirces are the same
            @test ensure_the_same(ChainHamfromSlize[indx][Lead],
                                  ChainHamfromHam[indx][Lead];atol=sqrt(eps()),
                                  Alab="ChainHamfromSlize[$indx][$Lead]",
                                  Blab="ChainHamfromHam[$indx][$Lead]")
            ###
            #println("......................")
            #println("ChainHamfromSlize[$indx][$Lead]")
            #println("ChainHamfromFullHam[$indx][$Lead]")
            #display(ChainHamfromSlize[indx][Lead])
            #display(ChainHamfromFullHam[indx][Lead])
            if indx==3
                ###Here we need to look only a the excerpt of sites that actaully attach to the lattice. There rest should be zero by definition
                LocalAttachment=SideIndexes[Lead]
                ###We extract a list that conaints the sites (in order of apearance)
                ###And another one for all the sites that are not connected
                SiteMap,NonSiteMap=ConstrucSiteMapAndNonSiteMap(LocalAttachment,LocalSlizeIndexes)
                #println("SideIndexes[Lead=$Lead]: ",SideIndexes[Lead])
                #println("SlizeData[Lead=$Lead]: ",LocalSlizeIndexes)
                #println("SiteMap: ",SiteMap)
                #println("NonSiteMap: ",NonSiteMap)
                RestrictedLeadtoSAHam=ChainHamfromSlize[indx][Lead][SiteMap,:]
                @test ensure_the_same(RestrictedLeadtoSAHam,
                                      ChainHamfromFullHam[indx][Lead];atol=sqrt(eps()),
                                      Alab="RestrictedLeadtoSAHam",
                                      Blab="ChainHamfromFullHam[$indx][$Lead]")
                if length(NonSiteMap)>0 ###Test that the other elements are zero
                    RestrictedLeadtoOtherSAHam=ChainHamfromSlize[indx][Lead][NonSiteMap,:]
                    @test ensure_the_same(RestrictedLeadtoOtherSAHam,
                                          RestrictedLeadtoOtherSAHam.*0;atol=sqrt(eps()),
                                          Alab="RestrictedLeadtoOtherSAHam",
                                          Blab="Zero matrix")
                end
            else
                @test ensure_the_same(ChainHamfromSlize[indx][Lead],
                                      ChainHamfromFullHam[indx][Lead];atol=sqrt(eps()),
                                      Alab="ChainHamfromSlize[$indx][$Lead]",
                                      Blab="ChainHamfromFullHam[$indx][$Lead]")
            end
        end
    end
    true
end


function ConstrucSiteMapAndNonSiteMap(LocalAttachment,LocalSlizeIndexes)
    ###Loop over all the local sites and put them either in the list of the local attachments
    ### or in the list of the local-non attachments
    SiteMap=fill(0,length(LocalAttachment))
    NonSiteMap=fill(0,length(LocalSlizeIndexes)-length(LocalAttachment))
    NextNonSite=1 ###Counter for how many non-sites we have
    for SiteNo in 1:length(LocalSlizeIndexes)
        LocalSite=LocalSlizeIndexes[SiteNo]
        FoundSite=false
        for AttachNo in 1:length(LocalAttachment)
            LocalAttach=LocalAttachment[AttachNo]
            if LocalSite == LocalAttach ###They match
                ###Make a note at the corresponding place in the SiteMap
                SiteMap[AttachNo]=SiteNo
                FoundSite=true
                break ### Then break out of the loop
            end
        end
        ### if after compleeting the loop we have not found the correct site.
        ### Then we add it to the NonSiteMap
        if !FoundSite
            NonSiteMap[NextNonSite]=SiteNo
            NextNonSite+=1
        end
    end ###Then do the next site
    return SiteMap,NonSiteMap
end

    

function test_slize_multi_chan(sg=2,fg=2,Phi=rand();Terminals=2,LeadWidth=1)
    LT="SCS"
    println("Setup parameters")
    Params = getSierpinskiParams(sg,fg,Phi,LT=LT)
    EnergyVals=rand()
    ParamsTuple=Params["ParamsTuple"](Params)

    if fg==0
        Width=get_lattice_size(sg,LT)
        ParamsChain = getChainParams(Width,Width,Phi)
        SierpinskiHamiltonian=BuildHamiltonian(Params)
        ChainHamiltonian=BuildHamiltonian(ParamsChain)
        ###Test that the hamiltonians are the same
        if !isapprox(SierpinskiHamiltonian,ChainHamiltonian,atol=sqrt(eps()))
            throw(DomainError(ParamsTuple,"The siepinsky and Chain Hamiltonians are differntn\n"*
                              "Chain=\n$ChainHamiltonian\nand Sierpinski=\n$SierpinskiHamiltonian\n"*
                              "with diff\n"*
                              string(SierpinskiHamiltonian-ChainHamiltonian)*"\n"))
        end
    end
    
    Eta=sqrt(eps())
        #rand()*0.01
    for n in 1
        println("-------------")
        if fg==0
            println("Do Dyn chain style")
            @time TranportSlizeChain=MultiTransport(ParamsChain,EnergyVals,
                                             Terminals=Terminals,LeadWidth=LeadWidth,
                                             eta=Eta,EvalType="Slize")
        end
        println("Do Slize style")
        @time TranportSlize=MultiTransport(Params,EnergyVals,
                                           Terminals=Terminals,LeadWidth=LeadWidth,
                                           eta=Eta,EvalType="Slize")
        ####Do the same calulating but don't go though the multifunctions
        SideIndexes=get_chain_attachment_points(Params["IVM"],Terminals,LeadWidth)
        TranportSlize2=do_MultichanelChainTransport_Slize(SideIndexes,EnergyVals,Params;eta=Eta)
        if !isapprox(TranportSlize2,TranportSlize,atol=sqrt(eps()))
            throw(DomainError(ParamsTuple,"Calling Multitransport for Slizing does not give same transport result as direclty calling the underlying function\n"*
                              "Multitransport=\n$TranportSlize\nand do_Multitransport=\n$TranportSlize2\n"*
                              "with diff\n"*
                              string(TranportSlize-TranportSlize2)*"\n"*
                              "for Eval=$EnergyVals, Eta=$Eta."))
            end

        
        
        println("Do Ham style")
        @time TranportHam=MultiTransport(Params,EnergyVals,
                                         Terminals=Terminals,LeadWidth=LeadWidth,
                                         eta=Eta,EvalType="Ham")
        SierpinskiHamiltonian=BuildHamiltonian(Params)
        ####Do the same calulating but don't go though the multifunctions
        TranportHam2=do_MultichanelChainTransport_Ham(SideIndexes,EnergyVals,SierpinskiHamiltonian;eta=Eta)
        if !isapprox(TranportHam2,TranportHam,atol=sqrt(eps()))
            throw(DomainError(ParamsTuple,"Calling Multitransport for Hamiltonian does not give same transport result as direclty calling the underlying function\n"*
                              "Multitransport=\n$TranportHam\nand do_Multitransport=\n$TranportHam2\n"*
                              "with diff\n"*
                              string(TranportHam-TranportHam2)*"\n"*
                              "for Eval=$EnergyVals, Eta=$Eta."))
            end


        ###These that the differnt outputs give the same results....
        println("TranportHam:\n",TranportHam)
        println("TranportSlize:\n",TranportSlize)
        if fg==0
            println("TranportSlizeChain:\n",TranportSlizeChain)
        end
        if fg==0
            if !isapprox(TranportSlizeChain,TranportSlize,atol=sqrt(eps()))
                throw(DomainError(ParamsTuple,"Attaching leads to the chain does not give same transport result as the naive sierpinski\n"*
                                  "Chain=\n$TranportSlizeChain\nand Sierpinski=\n$TranportSlize\n"*
                                  "with diff\n"*
                                  string(TranportSlizeChain-TranportSlize)*"\n"*
                                  "for Eval=$EnergyVals, Eta=$Eta."))
            end
        end
        if !isapprox(TranportHam,TranportSlize,atol=sqrt(eps()))
            throw(DomainError(ParamsTuple,"Attaching leads to the chain does not give same transport result as the Hamiltonian calculation\n"*
                              "Ham=\n$TranportHam\nand Slize=\n$TranportSlize\n"*
                              "with diff:\n"*
                              string(TranportHam-TranportSlize)*"\n"*
                              "for Eval=$EnergyVals, Eta=$Eta."))
        end
        
    end
    
    true
end

function test_transport_scanners()
    Params=getSierpinskiParams(3,3)
    Evals=1.1:0.20:2
    Parval=0.1:0.05:0.2
    Params["ph"]=Parval[1]
    ###Thes that evals can be reproduced
    Transport2=MultiTransport(Params,Evals,EvalType="Ham",Terminals=2,LeadWidth=1,
                              Voltages=false,Plot=false)
    println("Shape.Transport2:",size(Transport2))
    for EvanNo in 1:length(Evals)
        Transport1=ScanMultiTransport(Params,Parval[1],"ph",Evals[EvanNo],
                                      EvalType="Ham",Terminals=2,LeadWidth=1)
        println("Shape.Transport1:",size(Transport1))
        @test isapprox(Transport1[1,1,:,:],Transport2[EvanNo,:,:],atol=eps())
    end

    ###Thes that evals can be reproduced

    Transport1,Volt1=ScanMultiTransport(Params,Parval,"ph",Evals[1],
                                        EvalType="Ham",Terminals=2,LeadWidth=1,Voltages=true)
    println("Shape.Transport1:",size(Transport1))
    for EvanNo in 1:length(Parval)
        Params["ph"]=Parval[EvanNo]
        Transport2=MultiTransport(Params,Evals[1],EvalType="Ham",Terminals=2,LeadWidth=1)
        println("Shape.Transport2:",size(Transport2))
        @test isapprox(Transport1[EvanNo,1,:,:],Transport2[1,:,:],atol=eps())
    end
    true
end
        


function test_Sierpinski_slize_and_ED_transport(sg,fg,Phi,LT)
    ###Get the full green function for the sierpinski lattices
    Params = getSierpinskiParams(sg,fg,Phi,LT=LT)
    EigSys=SolveHamiltonian(Params)
    IndVecMap=Params["IVM"]
    
    Width=get_lattice_size(sg,LT)
    AttPoints=get_chain_attachment_points(IndVecMap,2,Width)
    EnergyVals=rand()
    Eta=1 ###Unreasonalbe value just to see method gives same result
    TranES=do_ChainTransportES(Width,EnergyVals,EigSys,IndVecMap;eta=Eta)
    TranSlize=do_MultichanelChainTransport_Slize(AttPoints,EnergyVals,Params;eta=Eta)
    println("TranES:    ",size(TranES))
    println("TranSlize: ",size(TranSlize))
    display(TranSlize)
    Test.@test isapprox(TranES,TranSlize[:,1,2],atol=10^-3)
    true
end


function test_slize_and_ED_transport(Width,Length,Phi;Eta=1)
    ###Unreasonalbe value of eta just to see method gives same result
    ###Get the full green function for the sierpinski lattices
    ChainHam,IndVecMap=CreateChainHamiltonian(Length,Width,Phi)
    EigSys=eigen(ChainHam)
    Params = getChainParams(Length,Width,Phi)
    AttPoints=get_chain_attachment_points(Params["IVM"],2,Width)
    
    EnergyVals=.25
    TranES=do_ChainTransportES(Width,EnergyVals,EigSys,IndVecMap;eta=Eta)
    TranHam=do_MultichanelChainTransport_Ham(AttPoints,EnergyVals,ChainHam;eta=Eta)
    TranSlize=do_MultichanelChainTransport_Slize(AttPoints,EnergyVals,Params;eta=Eta)
    #println("TranES:    ",size(TranES))
    #display(TranES)
    #println("TranHam: ",size(TranHam))
    #display(TranHam)
    #println("TranSlize: ",size(TranSlize))
    #display(TranSlize)
    Test.@test isapprox(TranHam[:,1,2],TranSlize[:,1,2],atol=sqrt(eps()))
    Test.@test isapprox(TranES,TranSlize[:,1,2],atol=sqrt(eps()))
    true
end

function test_slize_sierpinski_ham_with_mag_field(sg,fg,Phi,LT)
    ###Get the full green function for the sierpinski lattices
    Params = getSierpinskiParams(sg,fg,Phi,LT=LT)
    SierpinskiHam=BuildHamiltonian(Params)
    IndVecMap=Params["IVM"]
    #println("Params:")
    #display(Params)
    ncols=get_lattice_size(sg,LT)
    nrows=get_lattice_size(sg,LT)
    ###Test that the hmiltoina elemsts can be generated on the fly
    for colfrom in 1:ncols
        colfromindx=get_col_indx(IndVecMap,colfrom-1)
        for colto in 1:ncols
            #println("colfrom,colto=$colfrom,$colto")
            colftoindx=get_col_indx(IndVecMap,colto-1)
            #println("colfromindx: $colfromindx")
            #println("colftoindx: $colftoindx")
            ChainSlizeFull=SierpinskiHam[colfromindx,colftoindx]
            ChainSlizeLocal=GetHamiltonianSlizes(colfrom-1,colto-1,Params)
            #println("ChainSlizeFull:")
            #display(ChainSlizeFull)
            #println("ChainSlizeLocal:")
            #display(ChainSlizeLocal)
            if !isapprox(ChainSlizeFull,ChainSlizeLocal,atol=sqrt(eps()))
                throw(DomainError(Params,"The chain-slizes ($colfrom -> $colto) is not equal for The generated hamiltonian and the full one\n"*
                                  "The Full is:\n$ChainSlizeFull\n"*
                                  "The Sliced is:\n$ChainSlizeLocal\n"))
            end
        end
    end
    true
end

function test_equivalence_siepinsky_square(sg,Phase)
    ###Get the full green function for the sierpinski lattices
    N=get_lattice_size(sg,"SCS")
    Params=getSierpinskiParams(sg,0,Phase,LT="SCS")
    SierpinskiH=BuildHamiltonian(Params)
    IndVecMapS=Params["IVM"]
    ChainH,IndVecMapC=CreateChainHamiltonian(N,N,Phase)
    
    ###Thet that the indvec maps are identical
    @test IndVecMapS == IndVecMapC
    @test isapprox(SierpinskiH,ChainH,atol=sqrt(eps()))
    true
end



function test_performance_recursion(sg,fg,Phi,LT,Times)
    eta=1e-0
    Params = getSierpinskiParams(sg,fg,Phi,LT=LT)
    SierpinskiH=BuildHamiltonian(Params)
    IndVecMap=Params["IVM"]
    ##Get the hamiltonian

    StartTime=now()
    EigenSystem = eigen(SierpinskiH)
    EndTime=now()
    DiffTime=Dates.canonicalize(Dates.CompoundPeriod(Dates.Millisecond(EndTime-StartTime)))
    println("It took $DiffTime to fully diagonalize")


    LeftSideIndx=get_border_border_indexes(IndVecMap,"L")
    RightSideIndx=get_border_border_indexes(IndVecMap,"R")
    BorderIndexes=unique(vcat(LeftSideIndx,RightSideIndx)) ###This is not sorted!
    
    for _ in 1:Times
        println("---------")
        Energy=rand()+im*eta
        ###get the naive greens function
        println("Directly invert")
        @time _ = GreensFuncInvert(SierpinskiH,Energy)

        ### Construct from eigvals
        println("Constrct from eigvals")
        @time _ = GreensFunctionFixedEnergy(EigenSystem,Energy;
                                           FromList=BorderIndexes,
                                           ToList=BorderIndexes)

        ####FIXME There is not funcitons checning the actual recursive algorthims at the moment!!!!
    end
    true
end

function test_slize_ham_with_mag_field(nrows,ncols,Phi)
    ###Get the full green function for the square lattice
    ChainH,IndVecMap=CreateChainHamiltonian(ncols,nrows,Phi)
    Params = getChainParams(ncols,nrows,Phi)
    ###Test that the hmiltoina elemsts can be generated on the fly
    for colfrom in 1:ncols
        colfromindx=get_col_indx(IndVecMap,nrows,colfrom-1)
        for colto in 1:ncols
            #println("colfrom,colto=$colfrom,$colto")
            colftoindx=get_col_indx(IndVecMap,nrows,colto-1)
            ChainSlizeFull=ChainH[colfromindx,colftoindx]
            ChainSlizeLocal=getChainSlices(colfrom-1,colto-1,Params)
            #println("ChainSlizeFull:")
            #display(ChainSlizeFull)
            #println("ChainSlizeLocal:")
            #display(ChainSlizeLocal)
            Test.@test isapprox(ChainSlizeFull,ChainSlizeLocal,atol=sqrt(eps()))
        end
    end
    true
end

    
function test_auto_generated_ham(nrows,ncols,Phase)
    eta=1e-0
    Energy=rand()+im*eta
    ###Get the full green function for the square lattice
    ##Get the hamiltonian
    ChainH,IndVecMap=CreateChainHamiltonian(ncols,nrows,Phase)
    Params = getChainParams(ncols,nrows,Phase)
    ###Test that the hmiltoina elemsts can be generated on the fly
    for colfrom in 1:ncols
        colfromindx=get_col_indx(IndVecMap,nrows,colfrom-1)
        for colto in 1:ncols
            #println("1 colfrom,colto=$colfrom,$colto")
            colftoindx=get_col_indx(IndVecMap,nrows,colto-1)
            #println("colftoindx=$colftoindx")
            ChainSlizeFull=ChainH[colfromindx,colftoindx]
            #println("2 colfrom,colto=$colfrom,$colto")
            ChainSlizeLocal=getChainSlices(colfrom-1,colto-1,Params)
            #println("3 colfrom,colto=$colfrom,$colto")
            #println("ChainSlizeFull:")
            #display(ChainSlizeFull)
            #println("ChainSlizeLocal:")
            #display(ChainSlizeLocal)
            Test.@test isapprox(ChainSlizeFull,ChainSlizeLocal,atol=sqrt(eps()))
        end
    end
    true
end

function test_equvalence_square_lattice(nrows,ncols)
    eta=1e-0
    Energy=rand()+im*eta
    ###Get the full green function for the square lattice
    ##Get the hamiltonian
    ChainH,IndVecMap=CreateChainHamiltonian(ncols,nrows,0)
    ###Diaognalize
    EigenSystem = eigen(ChainH)

    col1indx=get_col_indx(IndVecMap,nrows,0)
    colNindx=get_col_indx(IndVecMap,nrows,ncols-1)
    
    #println("ChainH:")
    #display(ChainH)


    GreensFunNaive = GreensFuncInvert(ChainH,Energy)
    #println("GreensFunNaive:")
    #display(GreensFunNaive)
    
    GreensFunFull = GreensFunctionFixedEnergy(EigenSystem,Energy)
    #println("GreensFunFull:")
    #display(GreensFunFull)

    for i in 1:(nrows*ncols)
        for j in 1:(nrows*ncols)
            Test.@test isapprox(GreensFunNaive[i,j],GreensFunFull[i,j],atol=sqrt(eps()))
        end
    end
    
    
    Greens11FunCorner = GreensFunctionFixedEnergy(EigenSystem,Energy,
                                                FromList=col1indx,
                                                  ToList=col1indx)
    Greens1NFunCorner = GreensFunctionFixedEnergy(EigenSystem,Energy,
                                                FromList=col1indx,
                                                  ToList=colNindx)
    GreensN1FunCorner = GreensFunctionFixedEnergy(EigenSystem,Energy,
                                                  FromList=colNindx,
                                                  ToList=col1indx)
    GreensNNFunCorner = GreensFunctionFixedEnergy(EigenSystem,Energy,
                                                  FromList=colNindx,
                                                  ToList=colNindx)
    Greens11FunFullCorner=GreensFunFull[col1indx,col1indx]
    Greens1NFunFullCorner=GreensFunFull[col1indx,colNindx]
    GreensN1FunFullCorner=GreensFunFull[colNindx,col1indx]
    GreensNNFunFullCorner=GreensFunFull[colNindx,colNindx]


    for i in 1:nrows
        for j in 1:nrows
            Test.@test isapprox(Greens11FunFullCorner[i,j],Greens11FunCorner[i,j],atol=sqrt(eps()))
            Test.@test isapprox(Greens1NFunFullCorner[i,j],Greens1NFunCorner[i,j],atol=sqrt(eps()))
            Test.@test isapprox(GreensN1FunFullCorner[i,j],GreensN1FunCorner[i,j],atol=sqrt(eps()))
            Test.@test isapprox(GreensNNFunFullCorner[i,j],GreensNNFunCorner[i,j],atol=sqrt(eps()))
        end
    end
    
    


    ####Now we do the same with the recursive method
    ####First we construct a naive function with uses the Known hamiltonian to ensure sanity
    Greens11,Greens1N,GreensN1,GreensNN = GreensFunRecursiveES(ChainH,Energy,IndVecMap)

    #println("Greens1N:")
    #display(Greens1N)
    #println("GreensN1:")
    #display(GreensN1)
    #println("GreensNN:")
    #display(GreensNN)

    

    
    ###Finally we compare the recursive greens function with the exact one

    println("Run tests for row=$nrows, col=$ncols")
    for i in 1:nrows
        for j in 1:nrows
            Test.@test isapprox(Greens11[i,j],Greens11FunFullCorner[i,j],atol=sqrt(eps()))
            Test.@test isapprox(GreensNN[i,j],GreensNNFunFullCorner[i,j],atol=sqrt(eps()))
            Test.@test isapprox(Greens1N[i,j],Greens1NFunFullCorner[i,j],atol=sqrt(eps()))
            Test.@test isapprox(GreensN1[i,j],GreensN1FunFullCorner[i,j],atol=sqrt(eps()))
        end
    end
    true
end



function TestMultichanel(NX=3,NY=3)
    Erange=-5.23:1.79:3.1
    TransMatSlize=ChainTransportTransverse(NX,NY,Erange,Phase=0.3,EvalType="Slize",
                                           eta=0.01,Plot=false)
    TransMatHam=ChainTransportTransverse(NX,NY,Erange,Phase=0.3,EvalType="Ham",
                                         eta=0.01,Plot=false)
    @test isapprox(TransMatSlize,TransMatHam,atol=sqrt(eps()))
    true
end




function GreensFunRecursiveDynamicOLD(HamGen,HamSize,Energy,Params)
    ###The recursive equations that we need are
    
    ##G_NN	= (1-g_NN*V_Nn*g_nn*V_nN)^(-1) g_NN
    ##G_N1	= -(1-g_NN*V_Nn*g_nn*V_nN)^(-1) g_NN*VNn*gn1
    ##G_1N	=-g_1n*V_nN*G_NN

    TIMESTRUCT=TimingInit()
    ###We begin by working put the system size
    HamSlizeSizes=HamSize(Params)
    Cols=length(HamSlizeSizes)
    #println("HamSlizeSizes")
    #display(HamSlizeSizes)
    #println("Cols=$Cols")

    ###Here we assume that all lattice points are present at all slizes

    hNN=HamGen(0,0,Params)
    GNN=GreensFuncInvert(hNN,Energy)
    G1N=copy(GNN)
    GN1=copy(GNN)
    G11=copy(GNN)
    
    ###We can now assume that all points exist
    ###Now we extract the First and second collumn

    N=1
    #### Now we set up the recusive step
    TIMESTRUCT=TimingProgress(TIMESTRUCT,1,Cols;Message="Done col=$N of $Cols")

    
    for N in 2:Cols ###Recusively add rows
        n=N-1

        ### Update the old indexes
        g11 = G11
        gnn = GNN
        g1n = G1N
        gn1 = GN1
        hNN = HamGen(N-1,N-1,Params)
        VnN = HamGen(n-1,N-1,Params)
        VNn = HamGen(N-1,n-1,Params)
        gNN = GreensFuncInvert(hNN,Energy)

        SelfEnergyMatrix=inv( diagm(0 => ones(HamSlizeSizes[N])) - gNN*VNn*gnn*VnN)

        GNN	= SelfEnergyMatrix * gNN
        GN1	= -SelfEnergyMatrix * gNN*VNn*gn1
        G1N	= -g1n*VnN*GNN
        G11	= g11 - g1n*VnN*GN1
        TIMESTRUCT=TimingProgress(TIMESTRUCT,N,Cols;Message="Done col=$N of $Cols")
    end
        
    
    return  G11,G1N,GN1,GNN
    
end



function ChainTransportTransverse(Length=10,Width=10,EnergyVals=-4.1001:0.1:4.001;
                                  eta=sqrt(eps()),W=0.0,Phase=0.0,EvalType="Ham",Plot=true,
                                  Terminals=4,LeadWidth=1,Eshift=0.000)
    CParams=getChainParams(Length,Width,Phase,W=W)
    MultiTransport(CParams,EnergyVals;eta=eta,EvalType=EvalType,Plot=Plot,
                   Terminals=Terminals,LeadWidth=LeadWidth,Eshift=Eshift)
end


function do_ChainTransportES(Width,EnergyVals,EigenSystem,IndVecMap;
                             eta=1e-10,LeftLeadSide="L",RightLeadSide="R")
    if size(IndVecMap)[2]==0 ###i.e. the list is empty
        ###This means there is nothing in between and we should be able to connect the two wires directly
        return do_trivial_chain_transport(Width,EnergyVals,eta=eta)
    end
    LeftSideIndx=get_border_border_indexes(IndVecMap,LeftLeadSide)
    RightSideIndx=get_border_border_indexes(IndVecMap,RightLeadSide)
    test_nonmatching_chains(length(LeftSideIndx),Width,LeftLeadSide)
    test_nonmatching_chains(length(RightSideIndx),Width,RightLeadSide)
    MultChanel=do_MultichanelChainTransport_ES([LeftSideIndx,RightSideIndx],EnergyVals,
                                               EigenSystem;eta=eta)
    return MultChanel[:,1,2]###Extract the left to right part
end


function test_chain()
    ##Same for all
    LW=2;Ph=0.33;Elist=-2.5:0.1:0;
    ###Sierpinski
    SParams=getSierpinskiParams(2,0,Ph)
    NN=9
    CParams=getChainParams(NN,NN,Ph)
    ##Two term
    STrans=MultiTransport(SParams,Elist,Terminals=2,LeadWidth=LW);
    CTrans=MultiTransport(CParams,Elist,Terminals=2,LeadWidth=LW);
    @test isapprox(STrans,CTrans,atol=sqrt(eps()))
    ##three term
    STrans=MultiTransport(SParams,Elist,Terminals=4,LeadWidth=LW);
    CTrans=MultiTransport(CParams,Elist,Terminals=4,LeadWidth=LW);
    @test isapprox(STrans,CTrans,atol=sqrt(eps()))
    ### six term
    STrans=MultiTransport(SParams,Elist,Terminals=6,LeadWidth=LW);
    CTrans=MultiTransport(CParams,Elist,Terminals=6,LeadWidth=LW);
    @test isapprox(STrans,CTrans,atol=sqrt(eps()))
    true
end

TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close TestRecursiveGreens.jl")
