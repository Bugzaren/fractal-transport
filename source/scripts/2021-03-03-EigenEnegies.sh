
set -e -u 
LC_NUMERIC=C ##Makes sure there are points instead of commas
B0LIST=$(seq -2.0 0.1 2.0)

FG=1
SG=3



  


TASKLIST=instructions_${SG}_${FG}_DOS
LOGFILE=log_${SG}_${FG}
echo "" >>$LOGFILE
echo "" >> $TASKLIST
for B0Val in $B0LIST ; do
        
            name=" \"include(\\\"source/scripts/fractal_dos.jl\\\");computeDOS($SG,$FG,$B0Val)\" "
            echo "julia -e $name" >> $TASKLIST
            echo " echo \"calculation for  B0=$B0Val done \" >> $LOGFILE " >> $TASKLIST
       
done


