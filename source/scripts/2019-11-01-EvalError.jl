function InverseOfAdjoint()
    TheMatrix = fill(im*0.0,2,2)
    TheMatrix[1,2]=im
    TheMatrix[2,1]=im

    println("TheMatrix:")
    display(TheMatrix)
    
    TheAdjoint=TheMatrix'
    println("TheAdjoint:")
    display(TheAdjoint)
    
    InverseMatrix=inv(TheMatrix)
    println("InverseMatrix:")
    display(InverseMatrix)

    InverseNumericAdjoint=inv(TheAdjoint .* 1.0)
    println("InverseNumericAdjoint:")
    display(InverseNumericAdjoint)

    
    InverseAdjoint=inv(TheAdjoint)
    println("InverseAdjoint:")
    display(InverseAdjoint)
end

#InverseOfAdjoint()

function CreateMatrix(Ncount;Plot=true)
    TheMatrix = fill(0.0,Ncount,Ncount)

    if Plot
        if isdefined(Main, :PyPlot)
            println("PyPlot already loaded")
            PyPlot.figure()
            PyPlot.imshow(abs.(TheMatrix))
            PyPlot.colorbar()
        else
            println("PyPlot loading PyPlot")
            @eval using PyPlot
            Base.invokelatest(PyPlot.figure)
            Base.invokelatest(PyPlot.imshow, abs.(TheMatrix))
            Base.invokelatest(PyPlot.colorbar)
        end
    end
    return TheMatrix
end

#CreateMatrix(10;Plot=false)
#CreateMatrix(10;Plot=true)





