include("../lattices/HaldaneFractal.jl")
include("../core/ChainAttachments.jl");
include("../core/SliceLattice.jl");
include("../core/Greens.jl");
using DelimitedFiles


function computeDOS(SizeGen,FracGen,B0,mu=0.1, B1=1.0,B2=1.0,B3=1.0)
    ### vary B0 to go through the phase diagram
    PH=getHaldaneSierpinskiParams(SizeGen,FracGen,B0,B1,B2,B3,mu);
    println("Parameters done")
    
    println("generate Hamiltonian")
    Hamiltonian=do_CreateCentralHamiltonian(PH)
    println("compute Eigensystem")
    Eigensystem  = eigen(Hamiltonian)
    Eigenvalues = fill(0.,size(Eigensystem.values)[1])
		
    Eigenvectors = Eigensystem.vectors
	for j=1:size(Eigenvalues)[1]
			Eigenvalues[j]=real(Eigensystem.values[j])
	end
    println("computed eigensystem")
    
    ### saving file
    
    dir="Eigenvalues_SizeGen=$SizeGen"*"_FracGen=$FracGen"
    name="_B0=$(PH["B0"])_Fracgen=$(FracGen)_Sizegen=$(SizeGen).txt"
    println("write to file with name"*dir*"/Eigenvalues"*name)
    dir2="Eigenvectors_SizeGen=$SizeGen"*"_FracGen=$FracGen"
    

    
    open(dir*"/Eigenvalues"*name,"w") do io
        writedlm(io,Eigenvalues,'\t')
    end;

    open(dir2*"/Eigenvectors"*name,"w") do io
        writedlm(io,Eigenvectors,'\t')
    end;
end

