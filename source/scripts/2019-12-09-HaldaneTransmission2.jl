
include("../lattices/Haldanemodel.jl");
include("../core/ChainAttachments.jl");
include("../core/SliceLattice.jl");
include("../core/Greens.jl");

Width=243
Height=243
Energy=-0.75:0.005:0.75
HallLeadWidth=5

## set parameters such that we have Chern number 1
#PH=getHaldaneParams(Height,Width,B0=-2.5,B1=0.5,B2=1,B3=0.25);

### set parameters such that we have Chern number 2
PH=getHaldaneParams(Height,Width,B0=-1.5,B1=0.5,B2=1.0,B3=0.25);


Terminals=4
B1=PH["B1"]
IVM=PH["IVM"]

### last entry: width of the transverse terminals
LeadAttachmentPoints=get_chain_attachment_points(PH["IVM"],Terminals,HallLeadWidth)

print("LeadAttachmentPoints")
display(LeadAttachmentPoints)

println("Start Slizing")
SlizeData=SlizeTheLattice(PH,LeadAttachmentPoints,Plot=false,Verbose=false)

println("generate Lead Hamiltonians")

BlockChainDict=Dict()
LeadToLeadDict=Dict()
LeadToCenterDict=Dict()
### evtl later put a LeadToCenterDict if we want something more advanced
for j =1:Terminals
    BlockChainDict[j]=BuildLeadHamiltonian(LeadAttachmentPoints,j,B1)                ## these functions are in the Haldanemodel file
    ##### Need to konow the size of the big slize
    SlizeIDs=SlizeData[1][SlizeData[2][j]]
    SlizeSize=length(SlizeIDs)
    LeadSize=length(LeadAttachmentPoints[j])
    MinimalLeadToLead=BuildLeadToLeadHopping(LeadAttachmentPoints,j,B1)
    LeadToLeadDict[j]=MinimalLeadToLead
    ###We want to build a bitter matricx, padded with zeros
    ExtenedLeadtoCenter=fill(0.0im,SlizeSize,LeadSize)
    ##Find the potions of the elemets within the nez extemed matrix
    LeadToCenterMap=[findall(x->x==y,SlizeIDs)[1]
                     for y in LeadAttachmentPoints[j]]
    ExtenedLeadtoCenter[LeadToCenterMap,:]=MinimalLeadToLead
    LeadToCenterDict[j]=ExtenedLeadtoCenter
end
 
LeadHamInfo=(BlockChainDict,LeadToLeadDict,LeadToCenterDict)      ### the second LeadToLead replaces a LeadToCenter at this point  

println("LeadHamDictionary generated")




TransMission=do_MultichanelChainTransport_Slize(LeadAttachmentPoints,Energy,PH,
                                                SlizeData=SlizeData,
                                                ChainHamiltonian=LeadHamInfo;
                                                LeadVoltage=fill(0.,size(Energy)[1])
                                                )
figure()




for i in 1:Terminals
    for j in (i+1):Terminals
        plot(Energy,TransMission[:,i,j] .+ 0.0*rand(),label="$i -> $j")
        plot(Energy,-TransMission[:,j,i] .+ 0.0*rand(),"--",label="$j -> $i")
    end
    plot(Energy,0 .* Energy,"-k")
end
legend(loc="best")

L1=1
L2=2

println("compute potentials")
figure()
Potentials=FindPotentials(L1,L2,TransMission)
for i in 1:4
    plot(Energy,Potentials[:,i]  ,label="V$i")


end




legend(loc="best")














