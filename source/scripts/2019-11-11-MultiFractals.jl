sum(LOAD_PATH.==pwd())==0 && push!(LOAD_PATH, pwd()) ###Push the current direcoty into the path
include("..\\lattices\\Sierpinski.jl")

using ProgressTimer
using Polynomials


function DoBoxCounting(WaveFunction,IVM,GridResList;Plot=false)
    #### Perform the box counting step
    ###WaveFunction:The site resolved wave-function
    ###IVM:Index-vector map (the speciona reoslutoin of the wave function)
    ###GridResList: List of grid resolutions

    ###A dictionary that will conain the counting of the various boxes
    BoxDict=Dict()
    ###A list contatining the number of boxes for each size
    NumBoxList=fill(0,length(GridResList))
    NumFloatBoxList=fill(0.0,length(GridResList))

    ###Figure out the linear size of the system (asume 2D for now)
    MinX=minimum(IVM[1,:])
    MaxX=maximum(IVM[1,:])
    MinY=minimum(IVM[2,:])
    MaxY=maximum(IVM[2,:])
    SizeX=MaxX-MinX+1
    SizeY=MaxY-MinY+1
    #println("SizeX: $SizeX, SizeY:$SizeY")
    NumSites=length(WaveFunction)
    for SizeNo in 1:length(GridResList)
        #println("---------")
        GridResolution=GridResList[SizeNo]
        #println("GridResolution[$SizeNo]=$GridResolution")
        NumBoxesX=div(SizeX-1,GridResolution).+1
        NumBoxesY=div(SizeX-1,GridResolution).+1
        #println("NumBoxesX[$SizeNo]=$NumBoxesX")
        NumBoxList[SizeNo]=NumBoxesX*NumBoxesY
        NumFloatBoxList[SizeNo]=(SizeX*SizeY)/(GridResolution^2)
        BoxWeight=fill(0.0,NumBoxesX,NumBoxesY) ###Creates the box counter
        ##Loop through the sites and check wich box they are in.....
        for SiteNo in 1:NumSites
            (X,Y)=IVM[:,SiteNo]
            #println("X,Y=$X,$Y")
            #println("GridResolution:$GridResolution")
            BoxX=Int(floor((X-MinX)/GridResolution))
            BoxY=Int(floor((Y-MinY)/GridResolution))
            #println("BoxX,BoxY=$BoxX,$BoxY")
            BoxWeight[BoxX+1,BoxY+1]+=abs(WaveFunction[SiteNo])^2
            #println("BoxWeight=",BoxWeight[BoxX+1,BoxY+1])
        end
        if Plot ###PLot the box resolution if nececarry
            figure()
            imshow(BoxWeight)
            colorbar()
            title("Box size: $GridResolution")
        end
        BoxDict[SizeNo]=BoxWeight
    end
    return BoxDict,NumBoxList,NumFloatBoxList
end


function ComputeSingularitySpectrum(BoxDict,NumBoxList,NumFloatBoxList,Qvals)
    InvParRatList=fill(0.0,length(GridResList))
    LogIvalues=fill(0.0,length(GridResList),length(Qvals))
    SlopeList=fill(0.0,length(Qvals)) ###List with slope varialbles
    SlopeFloatList=fill(0.0,length(Qvals)) ###List with slope varialbles
    for QNum in 1:length(Qvals)
    Qfixed=Qvals[QNum]
        #println("Q-value:",Qfixed)
        for SizeNo in 1:length(GridResList)
            BoxWeight=BoxDict[SizeNo]
            InvParRatList[SizeNo]=sum(BoxWeight.^Qfixed)
        end
        LogN=log.(NumBoxList)
        LogI=-log.(InvParRatList)
        LogIvalues[:,QNum]=LogI
        ####Do a linear fit
        LinearFit=polyfit(LogN,LogI,1)
        SlopeList[QNum]=LinearFit(1)-LinearFit(0) ###Ugggly hack
        ####Do a linear fit on the floating point function
        LogN=log.(NumFloatBoxList)
        LinearFit=polyfit(LogN,LogI,1)
        SlopeFloatList[QNum]=LinearFit(1)-LinearFit(0) ###Ugggly hack

    end

    if true
        figure()
        for QNum in 1:length(Qvals)
            Qfixed=Qvals[QNum]
            LogN=log.(NumBoxList)
            LogI=LogIvalues[:,QNum]
            plot(LogN,LogI,"-p",label="Q=$Qfixed")
            ###Make a linear fit

        end
        xlabel("Log(Number of boxes)")
        ylabel("Log(inverse partition ratio)")
        title("Inverse Part Ratio vs Number of boxes")
        legend(loc="best")

        figure()
        for QNum in 1:length(Qvals)
            Qfixed=Qvals[QNum]
            LogN=log.(NumFloatBoxList)
            LogI=LogIvalues[:,QNum]
            plot(LogN,LogI,"-p",label="Q=$Qfixed")
        end
        xlabel("Log(Number of boxes)")
        ylabel("Log(inverse partition ratio)")
        title("Inverse Part Ratio vs (unrounded) Number of boxes")
        legend(loc="best")

        figure()
        plot(Qvals,SlopeList,"p",label="Actual Boxes")
        plot(Qvals,SlopeFloatList,"p",label="Floating point Boxes")
        xlabel("q")
        ylabel("slope")
        #ylim([0,ylim()[2]])
        legend(loc="best")
    end


    TauQ=SlopeList ####The slope is the tau_q
    ###The list of slopes is the tau_q paramter


    ###Alpha is the derivative of the slope with respect to q
    AlphaOfQ=diff(TauQ)./diff(Qvals)
    NewQ=Qvals[1:(end-1)]+0.5*diff(Qvals)
    NewTauQ=TauQ[1:(end-1)].+0.5*AlphaOfQ.*diff(Qvals)


    FofAlphaOfQ=NewQ .* AlphaOfQ - NewTauQ
    if true
        figure()
        plot(NewQ,AlphaOfQ,"p")
        title("alpha(q)")
        xlabel("Q")
        ylabel("alpha_q")

        figure()
        plot(Qvals,TauQ,"p")
        plot(NewQ,NewTauQ,"p")
        title("tau(q)")
        xlabel("Q")
        ylabel("tau_q")


        figure()
        plot(AlphaOfQ,FofAlphaOfQ,"-p")
        title("f(alpha) agains alpha")
        xlabel("alpha")
        ylabel("f(alpha)")
    end

    return FofAlphaOfQ,AlphaOfQ
end


Gen=3
Depth=2
LT="SCS"
Phi=0.11 ###Adding a magnetic field
W=0.1 ###A little bit of disorder to break degenracies


MaxSysSize=10 ####We done want the bigger boxes anyway
GridResList=1:MaxSysSize

Qvals=[x for x in -3:.1:5]###Convers an (abstract)  UnitRange object into an actual list

WfnList=[1,10,300,350] #A###Pot inte pby hand states you are interested in

if true
    dict=getSierpinskiParams(Gen,Depth,Phi,W=W,LT="SCS")
    Eigsystem=SolveHamiltonian(dict,Plot=true)
end


###Extract the mapt of lattice sites
IVM=dict["IVM"] ###Index vector map..

close("all")

#MinX=minimum(IVM[1,:])
#MaxX=maximum(IVM[1,:])
#MinY=minimum(IVM[2,:])
#MaxY=maximum(IVM[2,:])
#MaxSysSize=maximum((MaxX-MinX+1,MaxY-MinY+1))



HamDim=length(Eigsystem.values)
#WfnList=1:HamDim

ListDim=length(WfnList) ###FIXME MADE SMALL
FList=fill(0.0,length(Qvals)-1,HamDim)
AlphaList=fill(0.0,length(Qvals)-1,HamDim)

if true
###Extract the eigenvector
TIMESTRUCT=TimingInit()
for WfnNo in 1:ListDim
    WaveFunction=Eigsystem.vectors[:,WfnList[WfnNo]]
    BoxDict,NumBoxList,NumFloatBoxList=DoBoxCounting(WaveFunction,IVM,GridResList)
    FofAofQ,AofQ = ComputeSingularitySpectrum(BoxDict,NumBoxList,NumFloatBoxList,Qvals)
    FList[:,WfnNo]=FofAofQ
    AlphaList[:,WfnNo]=AofQ
    global TIMESTRUCT
    TIMESTRUCT=TimingProgress(TIMESTRUCT,WfnNo,HamDim;Message="Compute Singularity spectrum")
end

figure()
plot(AlphaList,FList)


end
