include("../lattices/HaldaneFractal.jl")
include("../core/ChainAttachments.jl");
include("../core/SliceLattice.jl");
include("../core/Greens.jl");
using DelimitedFiles

function runTransmission(SizeGen,FracGen,B0,B1=0.5,B2=1.0,B3=0.25)
    ### vary B0 to go through the phase diagram
    PH=getHaldaneSierpinskiParams(SizeGen,FracGen,B0,B1,B2,B3);
    println("Parameters done")
    Energy=-3:0.05:3

    Terminals=4
    B1=PH["B1"]
    IVM=PH["IVM"]
    HallLeadWidth=5
    ### last entry: width of the transverse terminals
    println("LeadAttachment")
    LeadAttachmentPoints=get_chain_attachment_points(PH["IVM"],Terminals,HallLeadWidth)
    println("done")
    println("Slize the Lattice")
    SlizeData=SlizeTheLattice(PH,LeadAttachmentPoints,Plot=false,Verbose=false)
    println("done")

    BlockChainDict=Dict()
    LeadToLeadDict=Dict()
    LeadToCenterDict=Dict()
    ### evtl later put a LeadToCenterDict if we want something more advanced
    for j =1:Terminals
        println("run BulidLeadHamiltonian")
        BlockChainDict[j]=BuildLeadHamiltonian(LeadAttachmentPoints,j,B1)
        println("slize data")
        SlizeIDs=SlizeData[1][SlizeData[2][j]]
        SlizeSize=length(SlizeIDs)
        
        LeadSize=length(LeadAttachmentPoints[j])
        println("leadtolead")
        MinimalLeadToLead=BuildLeadToLeadHopping(LeadAttachmentPoints,j,B1)
        
        LeadToLeadDict[j]=MinimalLeadToLead
        ###We want to build a bitter matricx, padded with zeros
        
        ExtenedLeadtoCenter=fill(0.0im,SlizeSize,LeadSize)
        ##Find the potions of the elemets within the nez extemed matrix
        println("extendLead")
        LeadToCenterMap=[findall(x->x==y,SlizeIDs)[1]
                     for y in LeadAttachmentPoints[j]]
        ExtenedLeadtoCenter[LeadToCenterMap,:]=MinimalLeadToLead
        LeadToCenterDict[j]=ExtenedLeadtoCenter
        println("Lead $j done")
    end

    LeadHamInfo=(BlockChainDict,LeadToLeadDict,LeadToCenterDict) 
    println("starting to calculate transmission")
    TransMission=do_MultichanelChainTransport_Slize(LeadAttachmentPoints,Energy,PH,
                                                SlizeData=SlizeData,
                                                ChainHamiltonian=LeadHamInfo;
                                                LeadVoltage=fill(0.,size(Energy)[1])
                                                )




    #Potentials=FindPotentials(1,2,TransMission)
    #return TransMission
    ##Foldername
    dir="Data_SizeGen=$SizeGen"*"_FracGen=$FracGen"
    name="_B0=$(PH["B0"])_Fracgen=$(FracGen)_Sizegen=$(SizeGen).txt"
    

    #### Fixme: find out what output is actually being written here and fix it


    open(dir*"/Transmission"*name,"w") do io
        writedlm(io,TransMission,',')
    end;


    #open(dir*"/Potentials"*name,"w") do io
    #    writedlm(io,Potentials,',')
    #end;
end







function runTransmission2(SizeGen,FracGen,E,B0,mu=0.1,B1=1.0,B2=1.0,B3=1.0)
    ### vary B0 to go through the phase diagram
    PH=getHaldaneSierpinskiParams(SizeGen,FracGen,B0,B1,B2,B3,mu);
    println("Parameters done")
    

    Terminals=4
    B1=PH["B1"]
    IVM=PH["IVM"]
    HallLeadWidth=5
    ### last entry: width of the transverse terminals
    println("LeadAttachment")
    LeadAttachmentPoints=get_chain_attachment_points(PH["IVM"],Terminals,HallLeadWidth)
    println("done")
    println("Slize the Lattice")
    SlizeData=SlizeTheLattice(PH,LeadAttachmentPoints,Plot=false,Verbose=false)
    println("done")

    BlockChainDict=Dict()
    LeadToLeadDict=Dict()
    LeadToCenterDict=Dict()
    ### evtl later put a LeadToCenterDict if we want something more advanced
    for j =1:Terminals
        println("run BulidLeadHamiltonian")
        BlockChainDict[j]=BuildLeadHamiltonian(LeadAttachmentPoints,j,B1)
        println("slize data")
        SlizeIDs=SlizeData[1][SlizeData[2][j]]
        SlizeSize=length(SlizeIDs)
        
        LeadSize=length(LeadAttachmentPoints[j])
        println("leadtolead")
        MinimalLeadToLead=BuildLeadToLeadHopping(LeadAttachmentPoints,j,B1)
        
        LeadToLeadDict[j]=MinimalLeadToLead
        ###We want to build a bitter matricx, padded with zeros
        
        ExtenedLeadtoCenter=fill(0.0im,SlizeSize,LeadSize)
        ##Find the potions of the elemets within the nez extemed matrix
        println("extendLead")
        LeadToCenterMap=[findall(x->x==y,SlizeIDs)[1]
                     for y in LeadAttachmentPoints[j]]
        ExtenedLeadtoCenter[LeadToCenterMap,:]=MinimalLeadToLead
        LeadToCenterDict[j]=ExtenedLeadtoCenter
        println("Lead $j done")
    end

    LeadHamInfo=(BlockChainDict,LeadToLeadDict,LeadToCenterDict) 
    println("starting to calculate transmission")
    TransMission=do_MultichanelChainTransport_Slize(LeadAttachmentPoints,E,PH,
                                                SlizeData=SlizeData,
                                                ChainHamiltonian=LeadHamInfo;
                                               LeadVoltage=0.
                                                )[1,:,:]


    ## first index would be energy, second and third are the leads

    #Potentials=FindPotentials(1,2,TransMission)
    #return TransMission
    ##Foldername
    dir="Data_SizeGen=$SizeGen"*"_FracGen=$FracGen"
    name="_E=$(E)_B0=$(PH["B0"])_mu=$(PH["mu"])Fracgen=$(FracGen)_Sizegen=$(SizeGen).txt"



    open(dir*"/Transmission"*name,"w") do io
        writedlm(io,TransMission,'\t')
    end;

    #open(dir*"/Potentials"*name,"w") do io
    #    writedlm(io,Potentials,',')
    #end;
end
