sum(LOAD_PATH.==pwd())==0 && push!(LOAD_PATH, pwd()) ###Push the current directory into the path


using Printf

###
include("ChainAttachments.jl")
include("SliceLattice.jl")
include("WeylLattice.jl")
include("Greens.jl")


using PyPlot

function do_RunWeyl(Height= 10,Width = 10,Length= 10;LeadDirt=0.001,W=0.0,Energy=-4:0.1:4,LeadVoltage=nothing,Side="F-B",eta=sqrt(eps()))
    
    if LeadVoltage==nothing
        InternalLeadVoltage=Energy .* 1.0
        PrintLV="mu"
    else
        InternalLeadVoltage=Energy*0.0 .+ LeadVoltage
        PrintLV="$LeadVoltage"
    end

    println("eta=$eta")
    etastr=@sprintf "%1.1e" eta
    println("etastr=$etastr")

    
    ExpTrans=2*Width*Height
    
    Params=getWeylParams(Length,Width,Height,W=W)
    
    
    IVM=Params["IVM"]
    IPM=WeylIPM(IVM) ###Use this to put particles in their correct spacial positions...
    Terminals=2

    if Side=="F-B"
        AttPoints=(get_border_border_indexes(IPM,"F"),get_border_border_indexes(IPM,"B"))
    elseif Side=="L-R"
        AttPoints=(get_border_border_indexes(IPM,"L"),get_border_border_indexes(IPM,"R"))
    elseif Side=="U-D"
        AttPoints=(get_border_border_indexes(IPM,"U"),get_border_border_indexes(IPM,"D"))
    end
   
    SlizeData=SlizeTheLattice(Params,AttPoints,Plot=false)
    Slize1 =SlizeData[1][1]
    Slize2 =SlizeData[1][2]
    
    ###Use the  weak dirt in the leads to break degeneracies
    Params["W"]=LeadDirt
    ParamsTuple=Params["ParamsTuple"](Params)
    HamGenerator=Params["GenHamElems"]
    GetNeigbours=Params["GetPossibleNeighbours"]
    
    Ham=MakeSlizeHam(ParamsTuple,HamGenerator,IVM,Slize1,0.0)
    Hop=MakeHoppingHam(ParamsTuple,HamGenerator,IVM,Slize1,Slize2,GetNeigbours)
    
    ChainHam=(Ham,Hop,Hop)
    
    ###Use the actuall W for the transmisison calulatoin
    Params["W"]=W
    
    println("..................Transport Param1..........")
    TransportSlize=do_MultichanelChainTransport_Slize(AttPoints,Energy,Params,eta=eta,
                                                      SlizeData=SlizeData,
                                                      ChainHamiltonian=ChainHam,
                                                      LeadVoltage=InternalLeadVoltage)



    SaveAsciiFile("TransmissionData/Weyl_l_$(Length)_h_$(Height)_w_$(Width)_W_$(W)_ld_$(LeadDirt)"*
                  "_lV_$(PrintLV)_S_$(Side)_e_$(etastr)",TransportSlize)
    
    figure()
    ylim((0,Height*Width*2+1))
    plot(Energy,TransportSlize[:,1,2])
    ylabel("Transmission")
    xlabel("Energy")
    title("Transmission for Length=$Length, Height=$Height, Width=$Width\nW=$W, LeadDirt=$LeadDirt, LeadVoltage=$PrintLV, Side=$Side, eta=$(etastr)")
    SavePyFile("pic/Weyl_l_$(Length)_h_$(Height)_w_$(Width)_W_$(W)_ld_$(LeadDirt)"*
               "_lV_$(PrintLV)_S_$(Side)_e_$(etastr)")
end
