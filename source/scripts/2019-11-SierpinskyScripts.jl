

function MakeButteryflyData(SizeGen::Int,FracGen::Int;LT="SCS",Start=0,Stop=1,Step=0.005)
    for Phase in Start:Step:Stop
        println("Phase=$Phase")
        SolveSierpinski(SizeGen,FracGen,Phase,LT=LT,
                        SaveDir="Phi_swipe_data/data_$LT"*"_$SizeGen"*"_$FracGen")
    end
end


function SierpinskiTransport(SizeGen=2,FracGen=0,EnergyVals=-5:0.1:5; Phase=0.0,W=0.0,eta=sqrt(eps()),
                             t=1.0,LT="SCS",Plot=false,SaveHDF5=false,SaveDir=nothing,
                             LeftLeadSide="L",RightLeadSide="R",EvalType="Dyn")
    if EvalType == "ES"
        ####Here we work out what lattice sites are connected
        (_,EigSys,IndVecMap)=SolveSierpinski(SizeGen,FracGen,Phase;t=t,LT=LT,
                                             Plot=Plot,SaveHDF5=SaveHDF5,SaveDir=SaveDir)
        Width=get_lattice_size(SizeGen,LT)
        Transmission=do_ChainTransportES(Width,EnergyVals,EigSys,IndVecMap;
                                         eta=eta,LeftLeadSide=LeftLeadSide,RightLeadSide=RightLeadSide)
    elseif EvalType == "Dyn"
        Params = getSierpinskiParams(SizeGen,FracGen,Phase,LT=LT,W=W)
        Width=get_lattice_size(SizeGen,LT)
        Transmission=do_ChainTransportDyn(Width,EnergyVals,getSierpinskiSlices,
                                          getSierpinskiSize,Params;eta=eta)
    end       
 

    #ELinspace=-4:0.01:4
    if true
        figure()
        plot(EnergyVals,EnergyVals.*0,"-k")
        plot(EnergyVals,Transmission,"-")
        #plot(ELinspace./2 .+ 2,Width*acos.(ELinspace ./ 4) ./ (pi),"--k")
        #plot(-ELinspace./2 .- 2,Width*acos.(ELinspace ./ 4) ./ (pi),"--k")
        title("Transmission LT=$LT, sg=$SizeGen, fg=$FracGen, ph=$Phase, W=$W")
        ylim([0,minimum([Width,maximum(Transmission)])+.5])
        xlabel("Energy")
        ylabel("Transmission")
        IDSTRING="LT_$LT"*"_sg_$SizeGen"*"_fg_$FracGen"*"_ph_$Phase"*"_W_$W"*"_LL_$LeftLeadSide"*"_RL_$RightLeadSide"
        SavePyFile("pic_transmission/Transmission_"*IDSTRING)
    end
    return Transmission
end


function MakeTransportMatrixData(SizeGen::Int,FracGen::Int,PhiRange,EnergyRange;
                       LT="SCS",W::Real=0)
    TransmissionMatrix=zeros(length(EnergyRange),length(PhiRange))
    Width=get_lattice_size(SizeGen,LT)
    
    println("W=$W for transport")
    TIMESTRUCT=TimingInit()
    i=1
    for Phase in PhiRange
        println("Phase=$Phase")
        Transmission=SierpinskiTransport(SizeGen,FracGen,EnergyRange,Phase=Phase,LT=LT,W=W)
        TransmissionMatrix[:,i]=Transmission
        TIMESTRUCT=TimingProgress(TIMESTRUCT,i,length(PhiRange);
                                  Message="..............\nComputing transmission scan\n...........")
        i+=1
    end
    Extent=[1.5*PhiRange[1]-.5*PhiRange[2],1.5*PhiRange[end]-.5*PhiRange[end-1],
            1.5*EnergyRange[1]-.5*EnergyRange[2],1.5*EnergyRange[end]-0.5*EnergyRange[end-1]]
    EnHash=CRC32c.crc32c(string(EnergyRange))
    PhiHash=CRC32c.crc32c(string(PhiRange))
    IDSTRING="LT_$LT"*"_sg_$SizeGen"*"_fg_$FracGen"*"_W_$W"*"_EnHash_$EnHash"*"_PhiHash_$PhiHash"

    figure()
    maxval=minimum([Width,maximum(abs.(TransmissionMatrix))])
    imshow(TransmissionMatrix[end:-1:1,:],aspect="auto",extent=Extent,
           vmin=0,vmax=maxval,cmap="gist_heat_r")
    title("Transmission for $LT sg=$SizeGen, fg=$FracGen, W=$W")
    colorbar()
    xlabel("\\Phi/\\Phi_0")
    ylabel("Transmission")
    SavePyFile("pic_trans_scan/Transmission_"*IDSTRING)

    figure()
    imshow(log.(TransmissionMatrix[end:-1:1,:] .+ 1),aspect="auto",extent=Extent,
           vmin=0,vmax=log(maxval+1),cmap="gist_heat_r")
    title("Transmission for $LT sg=$SizeGen, fg=$FracGen, W=$W")
    cbar=colorbar(ticks=log.(1:(maxval+1)))
    cbar["ax"]["set_yticklabels"]([string(x) for x in 0:maxval])

    
    xlabel("\\Phi/\\Phi_0")
    ylabel("Transmission")
    SavePyFile("pic_trans_scan/Log_Transmission_"*IDSTRING)
end



function get_density_of_states(Eigenvalues,EnergyList)
    if length(EnergyList)==1
        return EnergyList,nothing
    end
    BinVals=zeros(length(EnergyList)+1)
    BinVals[2:(end-1)]=0.5*(EnergyList[1:(end-1)]+EnergyList[2:end])
    BinVals[1]=2*EnergyList[1]-BinVals[2]
    BinVals[end]=2*EnergyList[end]-BinVals[end-1]
    h=fit(Histogram,Eigenvalues,BinVals)
    Density=h.weights #./ diff(h.edges[1])
    return Density
end

