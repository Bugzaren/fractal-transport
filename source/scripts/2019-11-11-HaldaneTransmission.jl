
include("../lattices/Haldanemodel.jl");
include("../core/ChainAttachments.jl");
include("../core/SliceLattice.jl");
include("../core/Greens.jl");

Width=20
Height=20
PH=getHaldaneParams(Height,Width,B0=1,B1=1,B2=1,B3=1);
## can take out
Terminals=4
B1=PH["B1"]
IVM=PH["IVM"]

###Figure out where to attach the leads
## List of all points for attaching two leads at x=1 and x=L
DummyLeadAttachmentPoints=get_chain_attachment_points(PH["IVM"],4,5)
## split this into four lists in order to treat different orbitals with different leads

throw(Error("Fix from here"))

print("LeadAttachmentPoints")
display(DummyLeadAttachmentPoints)
# 1st orbital on the left side 
Lead1=findall(x->x==1,IVM[:,DummyLeadAttachmentPoints[1]][3,:])
# 2nd orbital on the left side
Lead2=findall(x->x==2,IVM[:,DummyLeadAttachmentPoints[1]][3,:])
# 1st orbital right
Lead3=DummyLeadAttachmentPoints[2][findall(x->x==1,IVM[:,DummyLeadAttachmentPoints[2]][3,:])]
# 2nd orbital left
Lead4=DummyLeadAttachmentPoints[2][findall(x->x==2,IVM[:,DummyLeadAttachmentPoints[2]][3,:])]


LeadAttachmentPoints=( Lead1,
                        Lead2,
                        Lead3,
                       Lead4
                        )

println(LeadAttachmentPoints)



## This funciton you can use
SlizeData=SlizeTheLattice(PH,LeadAttachmentPoints,Plot=false,Verbose=false)
### leads need a coupling constant even if the nearest neighbour hopping is 0
if B1==0
    B1=1
end    
###Construct the hamiltonian for the leads
BlockChainHam=BuildHamiltonian(getChainParams(Width,1,t=-B1))

 
LeadToCenterUp=fill(0. , Width, Width)
LeadToCenterDown=fill(0. , Width, Width)
LeadToLead=fill(0.,Width,Width)
for j=1:Width
    LeadToCenterUp[j,j]=sign(B1)^j*B1
    LeadToCenterDown[j,j]=sign(-B1)^j*B1
    ## inside the Lead nothing gets staggered
    LeadToLead[j,j]=-B1
end





###Now the build the Lead Dictionary
LeadHamInfo=(##Inside Lead slizeLead both orbitals and left and right are identical 
#             Dict(1=>copy(FullChainHam),2=>copy(FullChainHam)), 
             Dict(1=>copy(BlockChainHam),2=>copy(BlockChainHam),3=>copy(BlockChainHam),4=>copy(BlockChainHam)
                 ),   
             ###Lead to Lead again everything is identical
#             Dict(1=>copy(LeadToLeadHam),2=>copy(LeadToLeadHam)),
            Dict(1=>copy(LeadToLead), 2=>copy(LeadToLead),3=>copy(LeadToLead), 4=>copy(LeadToLead)
                ),
             ###Lead to Scattering Area, spin up and down have different staggering
            # Dict(1=>copy(LeadToLeadHam),2=>copy(LeadToLeadHam)))
            Dict(1=>copy(LeadToCenterUp),
                 2=>fill(0.,Width,Width),
                    #copy(LeadToCenterDown),
                3=>#fill(0.,Width,Width),
                 copy(LeadToCenterUp),
                 4=>fill(0.,Width,Width)
                    #copy(LeadToCenterDown)
            ))




###Update the Leadham info TO take infor acount that the slizes are bigger than jsut the attachment points...

MatchHamiltonianToSlize!(LeadAttachmentPoints,SlizeData,LeadHamInfo)
Energy=-7:0.01:7

TransMission=do_MultichanelChainTransport_Slize(LeadAttachmentPoints,Energy,PH,
                                                SlizeData=SlizeData,
                                                ChainHamiltonian=LeadHamInfo;
                                                LeadVoltage=fill(0.,size(Energy)[1])
                                                )
figure()

#lot(Energy,TransMission[:,1,2] .+ 0.1*rand(),label="2nd to 1st orbital Leads")


for i in 1:4
    for j in (i+1):4
        plot(Energy,TransMission[:,i,j] .+ 0.1*rand(),label="$i -> $j")
       
    end
end
legend(loc="best")
### take care that these leads actually exist above!
L1=1
L2=3

println("compute potentials")    
figure()
Potentials=FindPotentials(L1,L2,TransMission)
for i in 1:4
    plot(Energy,Potentials[:,i]  ,label="V$i")

    
end




legend(loc="best")
