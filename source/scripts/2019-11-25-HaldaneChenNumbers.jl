

#close("all");
include("../lattices/Haldanemodel.jl");
include("../core/ChernNumbers.jl");

Width=20
Length=20
Points=100
Params=getHaldaneParams(Width,Length,B0=-1.5,B1=.5,B2=1,B3=.25)
E=-8:0.1:8
StartPos=[10,10,0]
ChernNumbers(Params,E,StartPos,Plot=true,Base=Points)
