#!/bin/bash
## This script lists all the dependencies that different *.jl files have
## FIXME: same module can be lsited twise (un-neccesary) (not a problem for make though)

set -e
set -u

SRC_JL=$1

echo "digraph G"
echo "{"

##Fortran Files
for f in ${SRC_JL} ; do
    ## Search through .jl for 'include("<file>")',
    ## extract the <file>,
    dep_list=$(grep '^\s*include(\"' -i $f |sed 's|.*include("\(.*\).jl").*|\1|g')
    ## sort the unique ones (tr ads a new line so that sort can do it's thing)
    dep_list=$(echo $dep_list | tr ' ' '\n' | sort -u  )
    ### echo out
    SFile=$(basename $f | sed 's|\(.*\).jl|\1|g' )
    for dep in $dep_list ; do
	SDEP=$(basename $dep)
	echo "\"$SFile\" -> \"$SDEP\""
    done
    ####Make dependecy on itself (to find orpaned modules)###    
    echo "\"$SFile\" -> \"$SFile\""
done

echo "}"
