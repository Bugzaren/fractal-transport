This set of julia codes is designed and developed to facilitate transport calculations
using the recursive greens-functions technique. It does however also encompass a
variety of other methods useful for diagonalizing hamiltonians as well as
visualizing them.

The code is build around constructing a "blueprint" for the hamiltonian of interest.
This blueprint is then used by the other methods to do their bidding. Worthy of note
is that the blueprint itself is not actually required to build the actual hamiltonian
(Although that could in principle be necessary sometimes).
Rather, the blueprint contains the instructions to build the hamiltonian on the fly.

## The Blueprint
Here we take a closer look at the blueprint. The blueprint is itself a julia dictionary. If is required to contain a few specific keys that can be searched for. In this respect this is much like an API that other methods can use. However since julia is not "object oriented" in the sense that objects have methods. See for instance [this discussion](https://stackoverflow.com/a/34928086).

Lets say you have written your own blueprint (or "Parameter dictionary") which is callable as 
```python
MyParams = genMyFavouriteParams(A_BUNCH_OF_PARAMS)
```
We now require the keys `"ParamsTuple"`, `"IVM"`, `"GetPossibleNeighbours"`, `"GenHamElems"`. Their respective use is described below:
* `ParamsTuple=MyParams["ParamsTuple"](MyParams)`
The `ParamsTuple` is essentially a tuple containing all the information needed to specify the system.
This could be variables such as "width" or "height", but if could also be more elaborate 
object like arrays/vectors with information about the disorder (if it's important to be able to reproduce a particular disorder realization).  
* `IVM=MyParams["IVM"]`
The IVM (Index vector map) is an array of dimensions `D`x`Nsize` where `Nsize` is the number
of elements in the hamiltonian, and `D` is the dimension of the "Cartesian" indexation of each site. The `IVM` needs to be ordered such that the coordinates for the slices  `Position=IVM[:,HamElem]` are first ordered on dimension `D`, then `D-1`,....,`1`.
* `NeigbourList=MyParams["GetPossibleNeighbours"](ParamsTuple,FromPosition)`
This is a list of all possible coordinates that can be (potentially) reached from the coordinate  `FromPosition`.
The rationale of this function is that is enables faster construction of Hamiltonians which are sparse (And most tight binding Hamiltonians tend to be sparse in one way or another.)
* `HamElement=MyParams["GenHamElems"](ParamsTuple,FromPosition,ToPosition)`
Given two positions `FromPosition` and `ToPosition` this function (together with the `ParamsTuple`) gives the hopping element between these two positions.
The position variables are listed in the `IVM`. You do not need to assure that `HamElement` is zero between for all non-zero elements,
only that it is zero for the elements that are listed in the `NeigbourList` of the `FromPosition`.

Note here how e.g. `ParamsTuple` is generated  by sending the blueprint `MyParams` as an argument to the function `MyParams["ParamsTuple"]`. 
This is a deliberate design choice to make sure that the `ParamsTuple` is always up to date, in case the blueprint changes by external editing (yes one can think of scenarios where this is useful)

Other useful keys that may be used of present are
* `MyParams["ParamsNames"](MyParams)`
Some functions may wish to print the parameters of the Blueprint. In that case, thus function will return their names as a tuple (or list)
* `MyParams["IVMtoIPM"]`
There is functionality that will want to print the spatial layout of the tight binding lattice. For this purpose the `IVM` might not be optimal. One may then instead supply an IPM (index position map)
which contains the actual (floating point) positions of the hopping sites. This is for instance useful if the lattice is triangular rather than square.
* `MyParams["title"]`
* `MyParams["IDSTRING"]`
* `MyParams["IDSHORT"]`
These thee keys may be used to generate various descriptive string of the system, that may be used as titles in figures, or as part of filenames, for saving data.


## Diagonalizers
Let's assume that you are now happy with your blueprint and now want to create the hamiltonian and diagonalize it.
For this purpose you include the file `"source/core/Misc.jl"` which contains methods for this.
A minimal example code would be 

```python
MyParams = genMyFavouriteParams(A_BUNCH_OF_PARAMS)
Ham=BuildHamiltonian(MyParams,Plot=true)
```
which builds the Hamiltonian `Ham` as a complex valued 2D-array. The ordering of the values will be the same as the `IVM` ordering.
By setting the optional parameter `Plot=true` a bunch of plots depicting the hamitlonians and it's elements will be generated.

Next, we might ant to diagonalize the hamiltonian. This can be done in two different ways.
We can use our blueprint `MyParams` and feed it into the function `SolveHamiltonian` as
```python
MyParams = genMyFavouriteParams(A_BUNCH_OF_PARAMS)
EigenSystem=SolveHamiltonian(MyParams,Plot=true)
```
We may instead choose to use the Hamiltonian that we generated earlier as
```python
MyParams = genMyFavouriteParams(A_BUNCH_OF_PARAMS)
Ham=BuildHamiltonian(MyParams,Plot=true)
EigenSystem=SolveHamiltonian(MyParams,Plot=true;Ham=Ham)
```
Of course, if we are note interested in plotting and such things, then one can directly use the build in eigensolver as
```python
MyParams = genMyFavouriteParams(A_BUNCH_OF_PARAMS)
Ham=BuildHamiltonian(MyParams,Plot=true)
EigenSystem=eigen(Ham)
```
the eigensolver used the package `LinearAlgebra` but this will have been imported anyway through `"source/core/Misc.jl"`.



## Transport Calculators
Happy that our hamiltonian is generated to specification (and it's also the one you wanted!), we can do some fun stuff and compute the transport through the structure.

### Two-terminal transport
For now let's assume tat our structure is square and that we only  want to attach leads to the left and to the right. We will also assume that the leads that we attach are "normal" tight binding (square lattice) leads with hopping parameter `-t`. We may then run the following set of codes:

```python
    include("source/core/Greens.jl")
    include("source/core/ChainAttachments.jl")
    Terminals=2 ## We want two terminals (one to the left, one to the right)

    ### The range of energies (chemica poentials) to cosider
    EnergyRange=-3.5:0.01:3.5 ##An example of an energy range

    ### Here we create the parameters needed for the parameters
    MyParams = genMyFavouriteParams(A_BUNCH_OF_PARAMS)

    ##Here we work out which sites are attaching to the leads
    AttPoints=get_chain_attachment_points(MyParams["IVM"],Terminals)

    ###The function makes the leads as wide as the square itself
    ####Here we compute the actaul transport numbers
    Transport=do_MultichanelChainTransport_Slize(
		AttPoints,EnergyRange,MyParams)

    ####Finally we  plot the transmission coefficients between the first and second lead....
    using PyPlot
    figure()
    plot(EnergyRange,Transport[:,1,2])
    title("Transmission through our structure")
```