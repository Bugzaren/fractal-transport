### Want: Heat plot for given SG and FG, x-axis is B0, y-Axis is E color scale is T. 
using PyPlot
using DelimitedFiles
using LinearAlgebra
using StatsBase
### will only work on format used by script run on cluster, gen4 will not work right now

function PlotTransmissionMap(SizeGen,FracGen;Lead1=1,Lead2=3)

    ### Energies and magnetic fields we are interested in
    BList=collect(-10:0.1:5)
    #BList=collect(-2:0.1:2)
    EList=collect(-2.5:0.05:2.5)
    #println(size(BList)) 
    #### initialize array we want to plot. This is going to be the Transmission from lead Lead1 to Lead2 at given energy and magnetic field
    R=fill(0.0,size(EList)[1],size(BList)[1])
    ### tried to interchange it such that B is on the x-axis and E on the y axis, check if this indeed worked! 

    dirname="Data/Data_SizeGen=$(SizeGen)_FracGen=$(FracGen)"
    ### fill the Transmission matrix
    for j=1:size(BList)[1]
        for k=1:size(EList)[1]
            B0=BList[j]
            E=EList[k]
            filename="/Transmission_E=$(E)_B0=$(B0)_Fracgen=$(FracGen)_Sizegen=$(SizeGen).txt"
            
            T=(readdlm(dirname*filename, Float64))
     #       println("Transmission for B0=$B0 , E=$E")
     #       println(T)
            ### technically this function gives back the potential but for R=V/I0=V for I0=1
            ## index 3 because we want potential on third lead
            V=HallPotential(T)
            R[k,j]=V[3]-V[4]
            
    #        println("Volatges")
    #        println(R[j,k])
        end
    end

    println("computation done, proceed with plotting")
     ### want to generate a matrix with  V3 (V1=0 so this should be the hall voltage

    ### V[j,k]=Hallpotentials Blist[j,k],Elist[j,k][3] oder so

    PyPlot.figure()
   PyPlot.imshow(R,cmap="bwr",  vmin=-2, vmax=2, extent=[-2,13,-2.5,2.5])
    PyPlot
    PyPlot.title("Hall resistance SG=$SizeGen FG=$FracGen")
    PyPlot.colorbar()
    PyPlot.xlabel("M")
    PyPlot.ylabel("Energy")
              PyPlot.axvline(x=12,linestyle="dashed",color="green")
    PyPlot.axvline(x=0,linestyle="dashed",color="green")
    PyPlot.axvline(x=8,linestyle="dashed",color="green")


end



function HallPotential(T;L1=1,L2=2,I0=1)

### Transmission is a 4x4 matrix for a specific E and B0 configuration 
### I0 is the current strength, it is set up such that I1=-I2=I0, the Hall Voltages will we chosen such that I3=I4

### at zero temperature: Linear response corresponds to I=T*(V1-V2) -> need to solve for V, 4x4 problem not invertible but we can solve 3x3 problem 

### this is what will be returned later
    V=fill(0.,4)

    if L1==L2
        println("error: both leads are the same")
    #elseif (L1*L2==2)||(L1*L2==12)  ### this means that either L1=1 and L2=2 or vice versa, this is the longitudinal leads, second case is L1=3 & L2=4, these are both the transversal leads
    #    println("This result will not be the Hall resistance")
    end
    eps=0.01 # margin of numerical error used for sanity checks
    ### define reduced matrices for calculation
    Idummy=fill(0.,3)
    Vdummy=fill(0.,3)
    ### dummy current for the calculation
    ### the fourth line will be taken out for inverting the dummy matrix
    if L1!=4
        Idummy[L1]=I0
    end
    if L2!=4
        Idummy[L2]=-I0
    end
    I=fill(0.,4,4)
    I[L1]=I0
    I[L2]=-I0
    ### find the matrix that needs to be inverted (This is not exactly the transmission), see hallresistance file
    rinv=fill(0.,3,3)
    for j=1:3, k=1:3
        diag=0
        rinv[j,k]=-T[j,k]
        if j==k
            diag=T[j,1]+T[j,2]+T[j,3]+T[j,4]
            rinv[j,k]=-T[j,k]+diag
        end
           
    end
    
    ### put in


    if det(rinv)<eps
       #println("transmission matrix can not be inverted due to 0 determinant, return zero resistance ")
        return fill(NaN,4,4)
    end


    ### calculate dummy 3x3 potentials
    Vdummy=inv(rinv)*Idummy


    ### shift such that V[L1]=0
    if L1==4
        ### no shift necessary
        for j=1:3
            V[j]=Vdummy[j]
        end
        V[4]=0

    else
        for j=1:3
            V[j]=Vdummy[j]-Vdummy[L1]
        end
        V[4]=-Vdummy[L1]
    end
    return V
end

using Plots

function PlotDOS(SizeGen,FracGen)
        BList=collect(-10:0.1:5)
        EList=collect(-2.5:0.05:2.5) ### will be used for bins in histogram. This is the same resolution than what was used for the transmission for comparability
                                     ### There are plenty of values larger or lower that +-2.5, those are bulk modes and we don't care   
   
        ### initialize 2d array that will be plotted in the end
        folder= "Data/Eigenvalues_SizeGen=$(SizeGen)_FracGen=$(FracGen)/"
        eigenvaluenumber=2*(3^SizeGen)^2
        Energymatrix= fill(0., eigenvaluenumber,size(BList)[1])
#        println(size(Energymatrix))
        D=fill(0.,eigenvaluenumber,size(BList)[1])
        DOSmap=fill(0.,size(EList)[1],size(BList)[1])
        for j in 1:size(BList)[1]
            B0=BList[j]
        ### generate string for the correct file and folder
           

            file = "Eigenvalues_B0=$(B0)_Fracgen=$(FracGen)_Sizegen=$(SizeGen).txt"
            ### open file containing the correct Eigenvalues
#            println("call path "*folder*file) 
            VString=readdlm(folder*file ,String)
#            println("read in Vstring")
           # println(typeof(VString[:,1]))
            #println(VString[:,1])
            #return VString
            V=fill(0.,size(VString)[1])

            if size(VString)[1]>eigenvaluenumber
                println("something is wrong with matrix size of system")
            end
            for k=1:size(VString)[1]
#                println("k=$k")
                V[k]=real(parse(ComplexF64,VString[k,1]))
               # Energymatrix[k,j]=V[k] 
            end
          V=sort!(V)
           for k=1:size(VString)[1]
               Energymatrix[k,j]=V[k]
           end
        #    return V 
            
            D=Energymatrix[:,2:end]-Energymatrix[:,1:(end-1)]           
           
            
            DOSdummy=fit(Histogram,V,EList).weights
           # println(DOSdummy)
            
             for k = 1:(size(EList)[1]-1)
                DOSmap[k,j]=DOSdummy[k]
             end
                       

        end
 #       println(DOSmap)
        ## check syntax for this

   #heatmap(BList,Energymatrix)   
#    heatmap(BList,EList,log.(DOSmap))    
     
      PyPlot.figure()
    PyPlot.xlabel("M")
    PyPlot.ylabel("δE")
    

     PyPlot.title("Energy differential  plot for Sizegen=$(SizeGen),Fracgen=$(FracGen)")
    PyPlot.imshow(D,cmap="bwr",aspect="auto",extent=[-2,13,0,eigenvaluenumber])
        PyPlot.axvline(x=8,linestyle="dashed",color="green")
    PyPlot.axvline(x=0,linestyle="dashed",color="green")
    PyPlot.axvline(x=12,linestyle="dashed",color="green")


#    PyPlot.heatmap(BList,EList,DOSmap) #according to documentation this should work but not even with the generic example it knows heatmap :(
    PyPlot.figure()
        PyPlot.xlabel("M")
        PyPlot.ylabel("Energy eigenstate")
    PyPlot.imshow(Energymatrix, cmap="bwr", aspect="auto",extent=[-2,13,0,eigenvaluenumber])
     PyPlot.title("Energy matrix  plot for Sizegen=$(SizeGen),Fracgen=$(FracGen)")
    
          PyPlot.axvline(x=12,linestyle="dashed",color="green")
    PyPlot.axvline(x=0,linestyle="dashed",color="green")
    PyPlot.axvline(x=8,linestyle="dashed",color="green")


    PyPlot.figure()
        PyPlot.imshow(log.(DOSmap),extent=[-2,13,-10,10]  )
      PyPlot.title("DOS  plot for Sizegen=$(SizeGen),Fracgen=$(FracGen)")
    PyPlot.colorbar()
          PyPlot.axvline(x=12,linestyle="dashed",color="red")
    PyPlot.axvline(x=0,linestyle="dashed",color="red")
    PyPlot.axvline(x=8,linestyle="dashed",color="red")
    PyPlot.xlabel("M")
    PyPlot.ylabel("E")  
        
#    return DOSmap        

end    
